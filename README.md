Welcome to Tutor Dash!
======================

`Find a Tutor. Be a Tutor.`

Tutor Dash is a mobile android application that greatly benefit university students’ perception of tutoring! This application serves as a hosting platform where university students can search for tutors, advertise their own tutoring services, or do both. In a way, Tutor Dash is similar to Uber in concept, but the functionality and interface is <i>much</i> different. Tutor Dash is a platform <b>exclusively for university students</b> with the aim
of improving the current processes of searching for tutors and providing tutoring services. Tutor Dash accomplishes this by consolidating various features already present on other digital tutoring platforms with new features that have yet to be implemented on any other platform. Ultimately, Tutor Dash’s goals are to make tutoring services more accessible to students looking for tutors and to make finding students to tutor easier for tutors looking for new clients.