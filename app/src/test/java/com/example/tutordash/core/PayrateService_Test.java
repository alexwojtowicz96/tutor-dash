package com.example.tutordash.core;

import androidx.annotation.NonNull;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

import com.example.tutordash.model.CourseAnalytics;
import com.example.tutordash.model.SchoolAnalytics;
import com.example.tutordash.model.User;
import com.example.tutordash.model.Course;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import com.google.firebase.firestore.DocumentReference;

public class PayrateService_Test {
    Course course;
    CourseAnalytics courseInfo;
    SchoolAnalytics schoolInfo;
    Float newCourseRating;
    User user;
    String courseID;
    Float testPayrate;
    PayrateService PayrateService;

    @Before
    public void setup() {
        courseID = "Test";
        newCourseRating = 3.6f;
        testPayrate = 6f;

        courseInfo = new CourseAnalytics();
        courseInfo.setAvgPayRate(6f);
        courseInfo.setAvgTutorRating(3f);
        courseInfo.setCourseID("Test");
        courseInfo.setTotalSessions(3f);

        schoolInfo = new SchoolAnalytics();
        schoolInfo.setAvgCoursePay(6f);
        schoolInfo.setAvgTutorRating(3f);
        schoolInfo.setSchoolID("Test University");
        schoolInfo.setTotalSessions(50f);

        course = new Course();
        course.setPayRate(7f);
        course.setRating(3.5);

        user = new User();

        PayrateService = new PayrateService(newCourseRating,
                course, courseID);
    }

    @Test
    public void calculateNewPayrate() {

    }

    @Test
    public void adjustDemandPayrate(){
        boolean test = false;
        PayrateService = new PayrateService(3.5f, course, courseID);
        testPayrate = PayrateService.adjustDemandPayrate(schoolInfo,
                courseInfo, .01f, 10f, 5f);
        if(testPayrate > (float)3.50){
            test = true;
        }
        else {
            test = false;
        }

        assertTrue(test);
    }

    @Test
    public void adjustRatingPayrate() {
        boolean test = false;
        PayrateService = new PayrateService(3.5f, course, courseID);
        testPayrate = PayrateService.adjustRatingPayrate(courseInfo,
                newCourseRating, .01f, 6f);
        if(testPayrate > 3.50){
            test = true;
        }
        else {
            test = false;
        }

        assertTrue(test);
    }

    @Test
    public void pushCourseInfo() {

    }

    @Test
    public void pushSchoolInfo(){

    }
}
