package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.User;
import com.google.firebase.firestore.GeoPoint;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Ignore;

public class LocationService_Test {

    Float distance;
    GeoPoint pointA;
    GeoPoint pointB;

    @Before
    public void setup() {

    }

    //---------------------------------------------------------------------------------------------
    /**
     * This test will test whether or not the getMilesAway returns the correct number of miles.
     */
    @Test
    public void getMilesAway() {
        // Test one
        pointA = new GeoPoint(36.7968, 76.1783);
        pointB = new GeoPoint(90, 90);
        boolean test = false;

        double dist = LocationService.getMilesAway(pointA, pointB);
        int dist_int = (int)dist;
        if (dist_int == 3675) {
            test = true;
        }
        else {
            test = false;
        }
        assertTrue(test);

        //Test two
        test = false;
        pointA = new GeoPoint(1, 1);
        pointB = new GeoPoint(90, 90);

        dist = LocationService.getMilesAway(pointA, pointB);
        dist_int = (int)dist;
        if (dist_int == 6149) {
            test = true;
        }
        else {
            test = false;
        }
        assertTrue(test);

        // Test three
        test = false;
        pointA = new GeoPoint(36.8853, 76.3059);
        pointB = new GeoPoint(36.8853, 76.1783);

        dist = LocationService.getMilesAway(pointA, pointB);
        dist_int = (int)dist;
        if (dist_int == 7) {
            test = true;
        }
        else {
            test = false;
        }
        assertTrue(test);

        // Test fourth
        test = false;
        pointA = new GeoPoint(41.8781, 87.6298);
        pointB = new GeoPoint(36.8508, 76.2859);

        dist = LocationService.getMilesAway(pointA, pointB);
        dist_int = (int)dist;
        if (dist_int == 697) {
            test = true;
        }
        else {
            test = false;
        }
        assertTrue(test);

        // Test Five
        test = false;
        pointA = new GeoPoint(0, 0);
        dist = LocationService.getMilesAway(pointA, pointB);
        dist_int = (int)dist;
        if (dist_int == -1) {
            test = true;
        }
        assertTrue(test);
    }
}
