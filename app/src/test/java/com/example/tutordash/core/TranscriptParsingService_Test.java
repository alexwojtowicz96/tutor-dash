package com.example.tutordash.core;

import com.example.tutordash.model.CourseEntry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;

public class TranscriptParsingService_Test {

    String testResDir;
    File pdfSample1;
    String pdfSample1Path;
    File notPdf;
    String notPdfPath;
    List<CourseEntry> courseEntriesSample1;

    @Before
    public void setup() {
        String appDir = System.getProperty("user.dir");
        String sep = File.separator;
        testResDir = appDir + sep + "src" + sep + "test" + sep + "res" + sep;

        pdfSample1Path = testResDir + "outline.pdf";
        pdfSample1 = new File(pdfSample1Path);
        notPdfPath = testResDir + "notPdf.txt";
        notPdf = new File(notPdfPath);

        File courseEntrySample = new File(testResDir + "CourseEntry_list-sample.txt");
        courseEntriesSample1 = new LinkedList<CourseEntry>();
        try {
            Scanner scanner = new Scanner(courseEntrySample);
            while (scanner.hasNextLine()) {
                String courseEntryString = scanner.nextLine();
                CourseEntry courseEntry = new CourseEntry(courseEntryString);
                courseEntriesSample1.add(courseEntry);
            }
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    //---------------------------------------------------------------------------------------------
    /**
     * This doesn't test a method in TranscriptParsingService.
     * Rather, use it to write the contents of a pdf to /app/src/test/res/testOutputFiles/
     * Just modify the INPUT_FILE_PATH variable with the file you want to parse.
     * By default, it is set to be called "test", but you can rename it if you want.
     */
    @Test
    @Ignore ("Brandon, use this to upload your transcript, then edit out your personal info.")
    public void writePdfStringToFile() {
        boolean wasExecuted = true;
        final String INPUT_FILE_PATH = "/Users/Duncan/Desktop/transcriptexample.jpg"; // manually enter the absolute path
        final String OUTPUT_FILENAME = "apples_1"; // Just edit the filename here
        try {
            File file = new File(INPUT_FILE_PATH);
            if (file.isFile() && file.exists()) {
                String text = TranscriptParsingService.pdfToString(file);
                BufferedWriter writer = new BufferedWriter(new FileWriter(
                        testResDir + "testOutputFiles" + File.separator + OUTPUT_FILENAME));

                writer.write(text);
                writer.close();
            }
            else {
                throw new Exception("Does this file exist? If so, is it a PDF?");
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            wasExecuted = false;
        }

        assertTrue(wasExecuted);
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     */
    @Test
    public void isPdf_Test() throws IOException {
        if (!pdfSample1.isFile() || !notPdf.isFile()) {
            if (!pdfSample1.isFile()) {
                fail("Can't find resource: " + pdfSample1Path);
            }
            else {
                fail("Can't find resource: " + notPdfPath);
            }
        }
        else {
            assertTrue(TranscriptParsingService.isPdf(pdfSample1));
            assertFalse(TranscriptParsingService.isPdf(notPdf));
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     */
    @Test
    public void pdfToString_Test() {
        // Visual inspection...
        System.out.println(TranscriptParsingService.pdfToString(pdfSample1));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void isValidTranscript_Test() {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void isParsable_Test() {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void isOfficial_Test() {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void alphabetizeAscending_Test() {
        final int SIZE_BEFORE_ORDER = courseEntriesSample1.size();
        courseEntriesSample1 = TranscriptParsingService.alphabetizeAscending(courseEntriesSample1);
        if (courseEntriesSample1.isEmpty() || courseEntriesSample1.size() != SIZE_BEFORE_ORDER) {
            fail("An error occurred while putting list in ascending order");
        }
        else {
            int i = 1;
            for (CourseEntry course : courseEntriesSample1) {
                System.out.println("COURSE #" + i);
                System.out.println("ID ...... " + course.getCourseId());
                System.out.println("NAME .... " + course.getCourseName());
                System.out.println("GRADE ... " + course.getGrade());
                System.out.println("UNIV .... " + course.getUniversityName());
                System.out.println();
                i += 1;
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void alphabetizeDescending_Test() {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void pruneDuplicateCourses_Test() {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO
     */
    @Test
    public void parseTranscript_Test() {

    }
    //---------------------------------------------------------------------------------------------
}
