package com.example.tutordash.model;

import android.util.Log;

import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.MessageService;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The model for a user. Keep in mind that not every field is in the database as it appears in
 * the model. For example, courses are stored as a collection in the user's record. Sessions
 * are handled completely differently. They are stored in their own collection, and that is
 * why users have references to sessions in that collection based on the status of that
 * session (pending, cancelled, or finished).
 */
public class User extends Object {

    public static final String TAG = "UserModel";

    private String bio;                             // User biography
    private String email;                           // User email (their actual email)
    private String fName;                           // User's first name
    private boolean isAvail;                        // Is the user currently available?
    private boolean isTutor;                        // Is the user a tutor?
    private String lName;                           // user's last name;
    private GeoPoint location;                      // Users location coordinates
    private boolean offersInPerson;                 // Does this tutor offer in-person sessions?
    private boolean offersOnline;                   // Does this tutor offer online sessions?
    private String picURL;                          // Path of url image in Firebase Storage
    private String school;                          // Name of the user's school
    private Timestamp timeOfLastRequest;            // The last time someone requested this tutor
    private Float tuteeRating;                      // Overall tutee rating
    private Float tutorRating;                      // Overall tutor rating
    private String uName;                           // User's username
    private Map<String, Course> courses;            // A map of all courses for this user
    private List<String> coursesSeekingAssistance;  // List of course Ids a user needs help with
    private boolean isVerified;                     // Has this user confirmed their email?
    private String authEmail;                       // Random email used for Firebase Auth
    private Timestamp dateCreated;                  // Date the user was created
    private boolean isBlacklisted;                  // Is this user blacklisted?
    private boolean hasTuteeNotifications;          // Does this user allow other tutees to send them notifications?
    private boolean hasTutorNotifications;          // Does this user allow other tutors to send them notifications?
    private List<String> offeredCourseStrings;      // A list of all course Ids that this user tutors
    private List<String> sessionsPendingIds;        // A list of all session Ids that are currently pending
    private List<String> sessionsCancelledIds;      // A list of all session Ids that have been cancelled
    private List<String> sessionsFinishedIds;       // A list of all session Ids that have been completed (in the past)
    private Map<String, Session> sessions;          // A map of all sessions (pending, cancelled, finished)
    private List<String> requestIds;                // List of all sessionIDs that a tutor has not responded to
    private String pendingSessionRatingID;          // ID of the session that a user needs to supply a rating for
    private Float hoursAsTutee;                     // Number of hours that this user has been a tutee for
    private Float hoursAsTutor;                     // Number of hours that this user has tutored
    //---------------------------------------------------------------------------------------------

    /**
     * Creates a blank user? Maybe we won't use this.
     */
    public User() {
        this.uName = "";
        this.email = "";
    }

    /**
     * Creates a user with all of their properties.
     */
    public User(
            String bio,
            String email,
            String fName,
            boolean isAvail,
            boolean isTutor,
            String lName,
            GeoPoint location,
            boolean offersInPerson,
            boolean offersOnline,
            String picURL,
            String school,
            Timestamp timeOfLastRequest,
            Float tuteeRating,
            Float tutorRating,
            String uName,
            Map<String, Course> courses,
            List<String> coursesSeekingAssistance,
            boolean isVerified,
            String authEmail,
            Map<String, Session> sessions,
            Timestamp dateCreated,
            boolean isBlacklisted,
            boolean hasTuteeNotifications,
            boolean hasTutorNotifications,
            List<String> offeredCourseStrings,
            List<String> sessionsPendingIds,
            List<String> sessionsCancelledIds,
            List<String> sessionsFinishedIds,
            List<String> requestIds,
            String pendingSessionRatingID,
            Float hoursAsTutee,
            Float hoursAsTutor
    ) {
        this.bio = bio;
        this.email = email;
        this.fName = fName;
        this.isAvail = isAvail;
        this.isTutor = isTutor;
        this.lName = lName;
        this.location = location;
        this.offersInPerson = offersInPerson;
        this.offersOnline = offersOnline;
        this.picURL = picURL;
        this.school = school;
        this.timeOfLastRequest = timeOfLastRequest;
        this.tuteeRating = tuteeRating;
        this.tutorRating = tutorRating;
        this.uName = uName;
        this.courses = courses;
        this.coursesSeekingAssistance = coursesSeekingAssistance;
        this.isVerified = isVerified;
        this.authEmail = authEmail;
        this.sessions = sessions;
        this.dateCreated = dateCreated;
        this.isBlacklisted = isBlacklisted;
        this.hasTuteeNotifications = hasTuteeNotifications;
        this.hasTutorNotifications = hasTutorNotifications;
        this.offeredCourseStrings = offeredCourseStrings;
        this.sessionsPendingIds = sessionsPendingIds;
        this.sessionsCancelledIds = sessionsCancelledIds;
        this.sessionsFinishedIds = sessionsFinishedIds;
        this.requestIds = requestIds;
        this.pendingSessionRatingID = pendingSessionRatingID;
        this.hoursAsTutee = hoursAsTutee;
        this.hoursAsTutor = hoursAsTutor;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public boolean isAvail() {
        return isAvail;
    }

    public void setAvail(boolean avail) {
        isAvail = avail;
    }

    public boolean isTutor() {
        return isTutor;
    }

    public void setTutor(boolean tutor) {
        isTutor = tutor;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public boolean isOffersInPerson() {
        return offersInPerson;
    }

    public void setOffersInPerson(boolean offersInPerson) {
        this.offersInPerson = offersInPerson;
    }

    public boolean isOffersOnline() {
        return offersOnline;
    }

    public void setOffersOnline(boolean offersOnline) {
        this.offersOnline = offersOnline;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public Timestamp getTimeOfLastRequest() {
        return timeOfLastRequest;
    }

    public void setTimeOfLastRequest(Timestamp timeOfLastRequest) {
        this.timeOfLastRequest = timeOfLastRequest;
    }

    public Float getTuteeRating() {
        return tuteeRating;
    }

    public void setTuteeRating(Float tuteeRating) {
        this.tuteeRating = tuteeRating;
    }

    public Float getTutorRating() {
        return tutorRating;
    }

    public void setTutorRating(Float tutorRating) {
        this.tutorRating = tutorRating;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Map<String, Course> getCourses() {
        return courses;
    }

    public void setCourses(Map<String, Course> courses) {
        this.courses = courses;
    }

    public List<String> getCoursesSeekingAssistance() {
        return this.coursesSeekingAssistance;
    }

    public String getAuthEmail() {
        return this.authEmail;
    }

    public void setAuthEmail(String authEmail) {
        this.authEmail = authEmail;
    }

    public boolean isVerified() {
        return this.isVerified;
    }

    public void setVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public Map<String, Session> getSessions() {
        return this.sessions;
    }

    public void setSessions(Map<String, Session> sessions) {
        this.sessions = sessions;
    }

    public Timestamp getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isBlacklisted() {
        return this.isBlacklisted;
    }

    public void setBlacklisted(boolean blacklisted) {
        this.isBlacklisted = blacklisted;
    }

    public boolean getHasTutorNotifications() {
        return this.hasTutorNotifications;
    }

    public void setHasTutorNotifications(boolean choice) {
        this.hasTutorNotifications = choice;
    }

    public boolean getHasTuteeNotifications() {
        return this.hasTuteeNotifications;
    }

    public void setHasTuteeNotifications(boolean choice) {
        this.hasTuteeNotifications = choice;
    }

    public void setCoursesSeekingAssistance(List<String> courses) {
        this.coursesSeekingAssistance = courses;
    }

    public List<String> getOfferedCourseStrings() {
        return this.offeredCourseStrings;
    }

    public void setOfferedCourseStrings(List<String> offeredCourseStrings) {
        this.offeredCourseStrings = offeredCourseStrings;
    }

    public List<String> getSessionsPendingIds() {
        if (this.sessionsPendingIds != null) {
            return this.sessionsPendingIds;
        }
        else {
            return new ArrayList<String>();
        }
    }

    public void setSessionsPendingIds(List<String> ids) {
        this.sessionsPendingIds = ids;
    }

    public List<String> getSessionsCancelledIds() {
        if (this.sessionsCancelledIds != null) {
            return this.sessionsCancelledIds;
        }
        else {
            return new ArrayList<String>();
        }
    }

    public List<String> getSessionsFinishedIds() {
        if (this.sessionsFinishedIds != null) {
            return this.sessionsFinishedIds;
        }
        else {
            return new ArrayList<String>();
        }
    }

    public void setSessionsCancelledIds(List<String> sessionIds) {
        this.sessionsCancelledIds = sessionIds;
    }

    public void setSessionsFinishedIds(List<String> sessionIds) {
        this.sessionsFinishedIds = sessionIds;
    }

    public List<String> getRequestIds() {
        if (this.requestIds != null) {
            return this.requestIds;
        }
        else {
            return new ArrayList<String>();
        }
    }

    public void setRequestIds(List<String> ids) {
        this.requestIds = ids;
    }

    public String getPendingSessionRatingID() {
        return this.pendingSessionRatingID;
    }

    public void setPendingSessionRatingID(String sessionID) {
        this.pendingSessionRatingID = sessionID;
    }

    public Float getHoursAsTutee() {
        return this.hoursAsTutee;
    }

    public void setHoursAsTutee(Float hours) {
        this.hoursAsTutee = hours;
    }

    public Float getHoursAsTutor() {
        return this.hoursAsTutor;
    }

    public void setHoursAsTutor(Float hours) {
        this.hoursAsTutor = hours;
    }
    //---------------------------------------------------------------------------------------------

    public void addCourseForSeekingAssistance(String course) {
        if (this.coursesSeekingAssistance == null) {
            this.coursesSeekingAssistance = new ArrayList<String>();
        }
        this.coursesSeekingAssistance.add(course);
    }
    //---------------------------------------------------------------------------------------------

    public boolean isValidUser() {
        return (!this.uName.equals("") || !this.email.equals("") || !this.authEmail.equals(""));
    }

    public String getFullName(){
        return this.fName + " " + this.lName;
    }

    public Map<String, Course> coursesOffered() {
        Map<String, Course> coursesOffered = new HashMap<String, Course>();
        if (this.courses != null) {
            Set<String> courseKeys = this.courses.keySet();
            for (String course : courseKeys) {
                if (this.courses.get(course).isOffered()) {
                    coursesOffered.put(course, this.courses.get(course));
                }
            }
        }

        return coursesOffered;
    }
    //---------------------------------------------------------------------------------------------

    public Map<String, Course> coursesNotOffered() {
        Map<String, Course> coursesNotOffered = new HashMap<String, Course>();
        if (this.courses != null) {
            Set<String> courseKeys = this.courses.keySet();
            for (String course : courseKeys) {
                if (!this.courses.get(course).isOffered()) {
                    coursesNotOffered.put(course, this.courses.get(course));
                }
            }
        }

        return coursesNotOffered;
    }
    //---------------------------------------------------------------------------------------------

    public void putNewCourse(String key, Course course) {
        if (!this.courses.containsKey(key)) {
            this.courses.put(key, course);
        }
    }
    //---------------------------------------------------------------------------------------------

    public void putNewRequestId(String id) {
        if (this.requestIds == null) {
            this.requestIds = new ArrayList<String>();
        }
        this.requestIds.add(id);
    }
    //---------------------------------------------------------------------------------------------

    public void removeRequestId(String id) {
        if (this.requestIds != null && this.requestIds.size() > 0) {
            if (this.requestIds.contains(id)) {
                this.requestIds.remove(id);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void updateExistingCourse(String key, Course course) {
        if (this.courses.containsKey(key)) {
            this.courses.remove(key);
            this.courses.put(key, course);
        }
    }
    //---------------------------------------------------------------------------------------------

    public void putNewPendingSessionId(String id) {
        if (this.sessionsPendingIds == null || this.sessionsPendingIds.size() == 0) {
            this.sessionsPendingIds = new ArrayList<String>();
            this.sessionsPendingIds.add(id);
        }
        else {
            if (!this.sessionsPendingIds.contains(id)) {
                this.sessionsPendingIds.add(id);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void putSessionInCollection(Session session) {
        if (session != null) {
            this.sessions.put(session.getSessionID(), session);
        }
        else {
            Log.d(TAG, "Cannot put null session in user.sessions!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Only use this if you need to read values of users that may be null.
     */
    public User initNullValues() {
        User user = new User(
                this.bio,
                this.email,
                this.fName,
                this.isAvail,
                this.isTutor,
                this.lName,
                this.location,
                this.offersInPerson,
                this.offersOnline,
                this.picURL,
                this.school,
                this.timeOfLastRequest,
                this.tuteeRating,
                this.tutorRating,
                this.uName,
                this.courses,
                this.coursesSeekingAssistance,
                this.isVerified,
                this.authEmail,
                this.sessions,
                this.dateCreated,
                this.isBlacklisted,
                this.hasTuteeNotifications,
                this.hasTutorNotifications,
                this.offeredCourseStrings,
                this.sessionsPendingIds,
                this.sessionsCancelledIds,
                this.sessionsFinishedIds,
                this.requestIds,
                this.pendingSessionRatingID,
                this.hoursAsTutee,
                this.hoursAsTutor
        );

        if (user.getBio() == null) {
            user.setBio(new String());
        }
        if (user.getEmail() == null) {
            user.setEmail(new String());
        }
        if (user.getfName() == null) {
            user.setfName(new String());
        }
        if (user.getlName() == null) {
            user.setlName(new String());
        }
        if (user.getLocation() == null) {
            user.setLocation(new GeoPoint(0, 0));
        }
        if (user.getPicURL() == null) {
            user.setPicURL(new String());
        }
        if (user.getSchool() == null) {
            user.setSchool(new String());
        }
        if (user.getTimeOfLastRequest() == null) {
            user.setTimeOfLastRequest(new Timestamp(new Date(0)));
        }
        if (user.getTuteeRating() == null) {
            user.setTuteeRating(Float.valueOf(0));
        }
        if (user.getTutorRating() == null) {
            user.setTutorRating(Float.valueOf(0));
        }
        if (user.getuName() == null) {
            user.setuName(new String());
        }
        if (user.getCourses() == null) {
            user.setCourses(new HashMap<String, Course>());
        }
        if (user.getCoursesSeekingAssistance() == null) {
            user.setCoursesSeekingAssistance(new ArrayList<String>());
        }
        if (user.getAuthEmail() == null) {
            user.setAuthEmail(new String());
        }
        if (user.getSessions() == null) {
            user.setSessions(new HashMap<String, Session>());
        }
        if (user.getDateCreated() == null) {
            user.setDateCreated(new Timestamp(new Date(0)));
        }
        if (user.getOfferedCourseStrings() == null) {
            user.setOfferedCourseStrings(new ArrayList<String>());
        }
        if (user.getSessionsPendingIds() == null) {
            user.setSessionsPendingIds(new ArrayList<String>());
        }
        if (user.getSessionsCancelledIds() == null) {
            user.setSessionsCancelledIds(new ArrayList<String>());
        }
        if (user.getSessionsFinishedIds() == null) {
            user.setSessionsFinishedIds(new ArrayList<String>());
        }
        if (user.getRequestIds() == null) {
            user.setRequestIds(new ArrayList<String>());
        }
        if (user.getPendingSessionRatingID() == null) {
            user.setPendingSessionRatingID(new String());
        }
        if (user.getHoursAsTutee() == null) {
            user.setHoursAsTutee(Float.valueOf(0));
        }
        if (user.getHoursAsTutor() == null) {
            user.setHoursAsTutor(Float.valueOf(0));
        }

        return user;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Subscribes the user to all of their topics. The idea is that for every course a tutor
     * is offering, they are subscribed to a topic that alerts them when tutees are seeking
     * their help in one of their offered courses.
     *
     * The same logic applies to tutees and their coursesSeekingAssistance collection. They want
     * to be subscribed to a topic that alerts them when tutors who are tutoring a course they are
     * seeking help in send out an alert.
     */
    public void subscribe() {
        subscribeToDMs();
        subscribeToReceiveTuteeNotifications();
        subscribeToReceiveTutorNotifications();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Unsubscribes the user from all of their topics. Understand that this is only relative
     * to the device, so if you say user.unsubscribe() on some other user for some reason, it's
     * not going to unsubscribe that user from their notifications...
     */
    public void unsubscribe() {
        unsubscribeFromDMs();
        unsubscribeToDisableTuteeNotifications();
        unsubscribeToDisableTutorNotifications();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Only subscribes to tutor notification feeds.
     */
    public void subscribeToReceiveTuteeNotifications() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        if (coursesOffered() != null && coursesOffered().size() > 0) {
            Set<String> courses = coursesOffered().keySet();
            for (String c : courses) {
                String topic = MessageService.getTopic(c, this.school, false);
                messaging.subscribeToTopic("/topics/" + topic);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Unsubscribes from any tutor notification feeds.
     */
    public void unsubscribeToDisableTuteeNotifications() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        if (coursesOffered() != null && coursesOffered().size() > 0) {
            Set<String> courses = coursesOffered().keySet();
            for (String c : courses) {
                String topic = MessageService.getTopic(c, this.school, false);
                messaging.unsubscribeFromTopic("/topics/" + topic);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Only subscribes to tutee notification feeds.
     */
    public void subscribeToReceiveTutorNotifications() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        if (coursesSeekingAssistance != null && coursesSeekingAssistance.size() > 0) {
            for (String c : this.coursesSeekingAssistance) {
                String topic = MessageService.getTopic(c, this.school, true);
                messaging.subscribeToTopic("/topics/" + topic);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Unsubscribes from any tutee notification feeds.
     */
    public void unsubscribeToDisableTutorNotifications() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        if (coursesSeekingAssistance != null && coursesSeekingAssistance.size() > 0) {
            for (String c : this.coursesSeekingAssistance) {
                String topic = MessageService.getTopic(c, this.school, true);
                messaging.unsubscribeFromTopic("/topics/" + topic);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Subscribes the user to direct message feeds from any user to themselves.
     */
    public void subscribeToDMs() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();
        messaging.subscribeToTopic("/topics/" + MessageService.DM_TOPIC + this.uName);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Unsubscribes the user from any direct message feeds to themselves.
     */
    public void unsubscribeFromDMs() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();
        messaging.unsubscribeFromTopic("/topics/" + MessageService.DM_TOPIC + this.uName);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Use this to initialize subscriptions (like when a user logs in). It will analyze their
     * hasTutorNotifications and hasTuteeNotifications properties to determine which subscriptions
     * should apply
     */
    public void initializeSubscriptions() {
        subscribeToDMs();
        if (this.hasTuteeNotifications) {
            subscribeToReceiveTuteeNotifications();
        }
        if (this.hasTutorNotifications) {
            subscribeToReceiveTutorNotifications();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Finds the available time slots (15 minute increments), and returns this data in a list.
     * For a time slot to be available, the user must not have a session scheduled starting or
     * overlapping at that particular time slot.
     *
     * If a time slot is taken, this will actually replace the text in that slot with "-".
     * IMPORTANT : Keep in mind this will only take into account session that have been accepted!
     *
     * @param date
     * @return list of 15 increments starting at 12:00am on the given date.
     */
    public ArrayList<String> findAvailableTimeSlots(Date date) {
        ArrayList<String> timeSlots = new ArrayList<String>();

        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 4; j++) {
                String slot = "";
                String xm;
                if (i < 12) { // am
                    xm = "am";
                    Integer hour = i;
                    if (hour.equals(0)) {
                        hour = 12;
                    }
                    slot += hour.toString();
                }
                else { // pm
                    xm = "pm";
                    Integer hour = i - 12;
                    if (hour.equals(0)) {
                        hour = 12;
                    }
                    slot += hour.toString();
                }
                Integer minutes = j * 15;
                String minutesString = minutes.toString();
                if (minutesString.length() == 1) {
                    minutesString = "0" + minutesString;
                }
                slot += ":" + minutesString;
                slot += xm;

                timeSlots.add(slot);
            }
        }

        if (this.sessions != null && this.sessions.size() > 0) {
            Set<String> sessionIDs = this.sessions.keySet();
            for (String s : sessionIDs) {
                Session session = this.sessions.get(s);
                if (session != null && session.findIsValidSession() && session.getWasAccepted()) {
                    if (session.getIsPending() && !session.getWasCancelled()) {
                        if (CalendarService.isSameDay(session.getStart().toDate(), date)) {
                            boolean inDate = false;
                            boolean stillInSession = false;
                            int itr = 0;
                            int hour1, hour2, min1, min2;
                            final String slot = "-";
                            Calendar calendar1 = GregorianCalendar.getInstance();
                            Calendar calendar2 = GregorianCalendar.getInstance();
                            calendar1.setTime(session.getStart().toDate());
                            calendar2.setTime(session.getEnd().toDate());

                            hour1 = calendar1.get(Calendar.HOUR_OF_DAY);
                            hour2 = calendar2.get(Calendar.HOUR_OF_DAY);
                            min1 = calendar1.get(Calendar.MINUTE);
                            min2 = calendar2.get(Calendar.MINUTE);

                            for (int i = 0; i < 24; i++) {
                                for (int j = 0; j < 4; j++) {
                                    if (i == hour1 && (j * 15) == min1) {
                                        inDate = true;
                                        stillInSession = true;
                                    }
                                    while (inDate) {
                                        timeSlots.set(itr, slot);
                                        inDate = false;
                                    }
                                    if (stillInSession) {
                                        inDate = true;
                                    }
                                    if (i == hour2 && (j * 15) == min2) {
                                        inDate = false;
                                        stillInSession = false;
                                    }
                                    itr++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return timeSlots;
    }
    //---------------------------------------------------------------------------------------------
}
