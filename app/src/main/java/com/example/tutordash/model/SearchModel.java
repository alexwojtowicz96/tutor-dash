package com.example.tutordash.model;

import android.util.Log;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.LocationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchModel {

    public static final String TAG = "SearchModel";

    private boolean isTuteeMode;
    private String courseId;
    private Float milesAway;
    private boolean onlyIncludeAvailableUsers;
    private String usernameQuery;

    public SearchModel() {
        this.isTuteeMode = true;
        this.courseId = "";
        this.milesAway = Float.valueOf(-1);
        this.onlyIncludeAvailableUsers = false;
        this.usernameQuery = null;
    }

    public SearchModel(
            boolean isTuteeMode,
            String courseId,
            Float milesAway,
            boolean onlyIncludeAvailableUsers
    ) {
        this.isTuteeMode = isTuteeMode;
        this.courseId = courseId;
        this.milesAway = milesAway;
        this.onlyIncludeAvailableUsers = onlyIncludeAvailableUsers;
    }

    /**
     * Creates default search model, but sets isTuteeMode to whatever
     * @param isTuteeMode
     */
    public SearchModel(boolean isTuteeMode) {
        this.isTuteeMode = isTuteeMode;
        this.courseId = "";
        this.milesAway = Float.valueOf(-1);
        this.onlyIncludeAvailableUsers = false;
        this.usernameQuery = null;
    }
    //---------------------------------------------------------------------------------------------

    public void setTuteeMode(boolean isTuteeMode) {
        this.isTuteeMode = isTuteeMode;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public void setMilesAway(Float milesAway) {
        this.milesAway = milesAway;
    }

    public void setOnlyIncludeAvailableUsers(boolean include) {
        this.onlyIncludeAvailableUsers = include;
    }

    public void setUsernameQuery(String usernameQuery) {
        this.usernameQuery = usernameQuery;
    }

    public boolean getTuteeMode() {
        return this.isTuteeMode;
    }

    public String getCourseId() {
        return this.courseId;
    }

    public Float getMilesAway() {
        return milesAway;
    }

    public boolean isOnlyIncludeAvailableUsers() {
        return this.onlyIncludeAvailableUsers;
    }

    public String getUsernameQuery() {
        return this.usernameQuery;
    }

    /**
     * Given a list of users to look through and a model of search criteria, get the
     * results that this model specifies.
     *
     * @param allUsers
     * @param model
     * @return
     */
    public static ArrayList<User> getQuery(ArrayList<User> allUsers, SearchModel model) {
        ArrayList<User> queryResults = new ArrayList<User>();
        if (model.getUsernameQuery() != null && model.getUsernameQuery().length() > 2) {
            String username = model.getUsernameQuery();
            for (User u : allUsers) {
                if (u.getuName().toLowerCase().contains(username) ||
                        u.getuName().toLowerCase().equals(username)) {
                    queryResults.add(u);
                }
            }
        }
        else {
            final String courseID = model.getCourseId();
            final Float milesAway = model.getMilesAway();
            final boolean onlyAvail = model.isOnlyIncludeAvailableUsers();
            Map<String, Float> locationMap = new HashMap<String, Float>();

            // 111 - courseID=NOTNULL, milesAway=NOTNULL, onlyAvail=TRUE
            if (!courseID.equals("") && !milesAway.equals(Float.valueOf(-1)) && onlyAvail) {
                locationMap = LocationService.pruneLocationMap(
                        LocationService.getUserLocationMap(allUsers),
                        milesAway
                );
                for (User u : allUsers) {
                    boolean courseReq = satisfiesOfferedCourseRequirement(courseID, u);
                    if (!model.isTuteeMode) {
                        courseReq = satisfiedSeekingCourseRequirement(courseID, u);
                    }
                    if (satisfiesAvailabilityRequirement(u) &&
                            satisfiesLocationRequirement(locationMap, u) &&
                            courseReq) {
                        queryResults.add(u);
                    }
                }
            }

            // 110 - courseID=NOTNULL, milesAway=NOTNULL, onlyAvail=FALSE
            else if (!courseID.equals("") && !milesAway.equals(Float.valueOf(-1)) && !onlyAvail) {
                locationMap = LocationService.pruneLocationMap(
                        LocationService.getUserLocationMap(allUsers),
                        milesAway
                );
                for (User u : allUsers) {
                    boolean courseReq = satisfiesOfferedCourseRequirement(courseID, u);
                    if (!model.isTuteeMode) {
                        courseReq = satisfiedSeekingCourseRequirement(courseID, u);
                    }
                    if (satisfiesLocationRequirement(locationMap, u) &&
                            courseReq) {
                        queryResults.add(u);
                    }
                }
            }

            // 101 - courseID=NOTNULL, milesAway=NULL   , onlyAvail=TRUE
            else if (!courseID.equals("") && milesAway.equals(Float.valueOf(-1)) && onlyAvail) {
                for (User u : allUsers) {
                    boolean courseReq = satisfiesOfferedCourseRequirement(courseID, u);
                    if (!model.isTuteeMode) {
                        courseReq = satisfiedSeekingCourseRequirement(courseID, u);
                    }
                    if (satisfiesAvailabilityRequirement(u) &&
                            courseReq) {
                        queryResults.add(u);
                    }
                }
            }

            // 100 - courseID=NOTNULL, milesAway=NULL   , onlyAvail=FALSE
            else if (!courseID.equals("") && milesAway.equals(Float.valueOf(-1)) && !onlyAvail) {
                for (User u : allUsers) {
                    boolean courseReq = satisfiesOfferedCourseRequirement(courseID, u);
                    if (!model.isTuteeMode) {
                        courseReq = satisfiedSeekingCourseRequirement(courseID, u);
                    }
                    if (courseReq) {
                        queryResults.add(u);
                    }
                }
            }

            // 011 - courseID=NULL   , milesAway=NOTNULL, onlyAvail=TRUE
            else if (courseID.equals("") && !milesAway.equals(Float.valueOf(-1)) && onlyAvail) {
                locationMap = LocationService.pruneLocationMap(
                        LocationService.getUserLocationMap(allUsers),
                        milesAway
                );
                for (User u : allUsers) {
                    if (satisfiesAvailabilityRequirement(u) &&
                            satisfiesLocationRequirement(locationMap, u)) {
                        queryResults.add(u);
                    }
                }
            }

            // 010 - courseID=NULL   , milesAway=NOTNULL, onlyAvail=FALSE
            else if (courseID.equals("") && !milesAway.equals(Float.valueOf(-1)) && !onlyAvail) {
                locationMap = LocationService.pruneLocationMap(
                        LocationService.getUserLocationMap(allUsers),
                        milesAway
                );
                for (User u : allUsers) {
                    if (satisfiesLocationRequirement(locationMap, u)) {
                        queryResults.add(u);
                    }
                }
            }

            // 001 - courseID=NULL   , milesAway=NULL   , onlyAvail=TRUE
            else if (courseID.equals("") && milesAway.equals(Float.valueOf(-1)) && onlyAvail) {
                for (User u : allUsers) {
                    if (satisfiesAvailabilityRequirement(u)) {
                        queryResults.add(u);
                    }
                }
            }

            // 000 - Empty search (will never reach this point)
            else {
                queryResults = allUsers;
                if (!(courseID.equals("") && milesAway.equals(Float.valueOf(-1)) && !onlyAvail)) {
                    StringBuilder b = new StringBuilder();
                    b.append("Error while parsing search results. Unexpected input combination :\n");
                    b.append("\tcourseID  : " + courseID + "\n");
                    b.append("\tmilesAway : " + milesAway.toString() + "\n");
                    b.append("\tonlyAvail : " + onlyAvail + "\n");
                    b.append("Expected =>\n");
                    b.append("\tcourseID  : NULL\n");
                    b.append("\tmilesAway : -1.0\n");
                    b.append("\tonlyAvail : False\n");
                    Log.d(TAG, b.toString());
                }
            }
        }

        return queryResults;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes all null values and gets only the tutors from list of users who are not
     * blacklisted
     */
    public static ArrayList<User> getOnlyTutors(ArrayList<User> users) {
        ArrayList<User> onlyTutors = new ArrayList<User>();
        if (users != null & users.size() > 0) {
            for (User u : users) {
                if (u.isValidUser() && u.isTutor() && !u.isBlacklisted()) {
                    onlyTutors.add(u);
                }
            }
        }

        return onlyTutors;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes all null values and gets only the tutees from list of users who are not
     * blacklisted. Everyone is a tutee by default.
     */
    public static ArrayList<User> getOnlyTutees(ArrayList<User> users) {
        ArrayList<User> onlyTutees = new ArrayList<User>();
        if (users != null & users.size() > 0) {
            for (User u : users) {
                if (u.isValidUser() && !u.isBlacklisted()) {
                    onlyTutees.add(u);
                }
            }
        }

        return onlyTutees;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if the model is the default model. Ignores isTuteeMode since tutors can
     * search as well (in tutor mode), and their default model would have this set to false.
     *
     * @return isDefault
     */
    public boolean isDefault() {
        boolean isDefault = true;
        if (!this.courseId.equals("")) {
            isDefault = false;
        }
        else if (!this.milesAway.equals(Float.valueOf(-1))) {
            isDefault = false;
        }
        else if (this.onlyIncludeAvailableUsers) {
            isDefault = false;
        }
        else if (this.usernameQuery != null) {
            isDefault = false;
        }

        return isDefault;
    }
    //---------------------------------------------------------------------------------------------

    public static boolean satisfiesLocationRequirement(Map<String, Float> map, User u) {
        return (map.containsKey(u.getuName()));
    }
    //---------------------------------------------------------------------------------------------

    public static boolean satisfiesOfferedCourseRequirement(String courseId, User u) {
        return (u.coursesOffered().containsKey(courseId));
    }
    //---------------------------------------------------------------------------------------------

    public static boolean satisfiedSeekingCourseRequirement(String courseId, User u) {
        return (u.getCoursesSeekingAssistance().contains(courseId));
    }
    //---------------------------------------------------------------------------------------------

    public static boolean satisfiesAvailabilityRequirement(User u) {
        return (u.isAvail());
    }
    //---------------------------------------------------------------------------------------------
}
