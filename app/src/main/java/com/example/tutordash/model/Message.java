package com.example.tutordash.model;

import com.example.tutordash.core.CalendarService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Use this to model a message in the MessageActivity
 */
public class Message {

    public static final String TAG = "MessageModel";

    private String name;
    private String message;
    private String timestamp;

    public Message() { }

    public Message(String name, String message, String timestamp) {
        this.name = name;
        this.message = message;
        this.timestamp = timestamp;
    }
    //---------------------------------------------------------------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDateTime() {
        try {
            Date date = new Date(Long.valueOf(timestamp) * 1000L);
            SimpleDateFormat simpleDate = new SimpleDateFormat("HH:mm MM/dd/yy");
            simpleDate.setTimeZone(TimeZone.getDefault());
            String[] timeAndDate = simpleDate.format(date).split(" ");
            String formattedTime = CalendarService.convertFromMilitaryTime(timeAndDate[0] + ":00");

            return formattedTime + " " + timeAndDate[1];
        }
        catch (Exception ex) {
            return "12:00am 00/00/00";
        }
    }
    //---------------------------------------------------------------------------------------------

    public boolean findIfUserSentMessage(String uName) {
        if (this.name != null) {
            if (this.name.equals(uName)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initNullValues() {
        if (this.timestamp == null || this.timestamp.isEmpty()) {
            this.timestamp = "12:00am 00/00/00";
        }
        if (this.message == null) {
            this.message = "";
        }
        if (this.name == null) {
            this.name = "";
        }
    }
    //---------------------------------------------------------------------------------------------

    public String returnMessageFragment() {
        String msg = this.message;
        if (msg.length() > ChatUser.MAX_CHARS_ALLOWED) {
            msg = ChatUser.trimMessage(msg);
        }

        return msg;
    }
    //---------------------------------------------------------------------------------------------
}
