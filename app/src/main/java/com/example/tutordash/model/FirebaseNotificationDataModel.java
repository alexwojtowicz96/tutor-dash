package com.example.tutordash.model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * This model is a container for the private information that is required to send notifications
 * through Firebase Messaging.
 */
public class FirebaseNotificationDataModel {

    public static final String TAG = "FirebaseNotifDataModel";

    private String senderID;
    private String legacyServerKey;
    private String serverKey;
    private Timestamp dateUpdated;
    public boolean hasAttemptedInit;
    //---------------------------------------------------------------------------------------------

    public FirebaseNotificationDataModel() {
        this.senderID = null;
        this.legacyServerKey = null;
        this.serverKey = null;
        this.dateUpdated = null;
        this.hasAttemptedInit = false;
        this.initModel();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * GETS AND SETS
     * ============================================================================================
     */
    public String getSenderID() {
        return this.senderID;
    }

    public String getLegacyServerKey() {
        return this.legacyServerKey;
    }

    public String getServerKey() {
        return this.serverKey;
    }

    public Timestamp getDateUpdated() {
        return this.dateUpdated;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public void setLegacyServerKey(String legacyServerKey) {
        this.legacyServerKey = legacyServerKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Pulls the data from Firestore to init this data model.
     */
    public void initModel() {
        try {
            FirebaseFirestore database = FirebaseFirestore.getInstance();
            DocumentReference ref = database.collection("supportCreds").document("notificationKeys");
            ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        DocumentSnapshot result = task.getResult();
                        try {
                            serverKey = (String) result.get("serverKey");
                            legacyServerKey = (String) result.get("legacyServerKey");
                            senderID = (String) result.get("senderID");
                            dateUpdated = (Timestamp) result.get("dateUpdated");
                            if (!findIsValidModel()) {
                                throw new Exception("All attributes were found?");
                            }
                            hasAttemptedInit = true;
                        }
                        catch (Exception ex) {
                            Log.d(TAG, "Failed to get data :\n" + ex.getLocalizedMessage());
                            hasAttemptedInit = true;
                        }
                    }
                    else {
                        String ex = "";
                        if (task.getException() != null) {
                            ex = task.getException().getLocalizedMessage();
                        }
                        Log.d(TAG, "Failed to initialize data model :\n" + ex);
                        hasAttemptedInit = true;
                    }
                }
            });
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
            hasAttemptedInit = true;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if this model is valid.
     */
    public boolean findIsValidModel() {
        return (this.serverKey != null && !this.serverKey.isEmpty())
                && (this.legacyServerKey != null && !this.legacyServerKey.isEmpty())
                && (this.senderID != null && !this.senderID.isEmpty())
                && (this.dateUpdated != null);
    }
    //---------------------------------------------------------------------------------------------
}
