package com.example.tutordash.model;

/**
 * This is the local model that will help contain the data for a Customer (for Braintree).
 * It should be primarily used when passing the customer data to the faux "server".
 */
public class BraintreeCustomer {

    public static final String TAG = "BraintreeCustomer";

    public static final String DEFAULT_PHONE = "000-000-0000";
    public static final String DEFAULT_FAX = "111-111-1111";
    public static final String DEFAULT_WEBSITE = "http://www.google.com";
    public static final String DEFAULT_COMPANY = "Tutor Dash User";
    public static final String DEFAULT_CUSTOMER_ID = "generic_td_cid";
    public static final String DEFAULT_BRAINTREE_ID = "generic_td_bid";
    public static final String DEFAULT_FIRST_NAME = "Tpouuwqp";
    public static final String DEFAULT_LAST_NAME = "Jueyfvuio";
    public static final String DEFAULT_EMAIL = "default@email.net";

    private String customerId;   // ID for the customer
    private String braintreeId;  // Don't use this... I think it's unnecessary.
    private String company;      // User's company name
    private String firstName;    // User's first name
    private String lastName;     // User's last name
    private String phone;        // User's phone number
    private String fax;          // User's fax number
    private String website;      // User's website URL
    private String email;        // User's true email address
    //---------------------------------------------------------------------------------------------

    private BraintreeCustomer() {}

    public BraintreeCustomer(User user) {
        this.braintreeId = null;
        this.customerId = null;
        this.firstName = null;
        this.lastName = null;
        this.company = null;
        this.phone = null;
        this.fax = null;
        this.website = null;
        this.email = null;

        if (user != null && user.isValidUser()) {
            this.customerId = user.getuName();
            this.braintreeId = DEFAULT_BRAINTREE_ID + "_" + customerId;
            this.firstName = user.getfName();
            this.lastName = user.getlName();
            this.email = user.getEmail();
            initNullsWithDefaults();
        }
        else {
            initNullsWithDefaults();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * GETS
     * ============================================================================================
     */
    public String getCustomerId() {
        return this.customerId;
    }

    public String getBraintreeId() {
        return this.braintreeId;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getCompany() {
        return this.company;
    }

    public String getWebsite() {
        return this.website;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getFax() {
        return this.fax;
    }

    public String getEmail() {
        return this.email;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * SETS
     * ============================================================================================
     */
    public void setCustomerId(String id) {
        this.customerId = id;
    }

    public void setBraintreeId(String id) {
        this.braintreeId = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Valid customers must have a non-null and non-default first name, last name, email, and
     * customerId. Everything else needs to be non-null (Everything must be initialized).
     */
    public boolean isValid() {
        boolean isValid = true;
        if (isEmptyOrDefault(firstName, "firstName")) {
            isValid = false;
        }
        if (isEmptyOrDefault(lastName, "lastName")) {
            isValid = false;
        }
        if (isEmptyOrDefault(customerId, "customerId")) {
            isValid = false;
        }
        if (isEmptyOrDefault(email, "email")) {
            isValid = false;
        }
        if (isNullOrEmpty(braintreeId)) {
            isValid = false;
        }
        if (isNullOrEmpty(website)) {
            isValid = false;
        }
        if (isNullOrEmpty(company)) {
            isValid = false;
        }
        if (isNullOrEmpty(phone)) {
            isValid = false;
        }
        if (isNullOrEmpty(fax)) {
            isValid = false;
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    public void initNullsWithDefaults() {
        if (!(this.firstName != null && !this.firstName.isEmpty())) {
            this.firstName = DEFAULT_FIRST_NAME;
        }
        if (!(this.lastName != null && !this.lastName.isEmpty())) {
            this.lastName = DEFAULT_LAST_NAME;
        }
        if (!(this.braintreeId != null && !this.braintreeId.isEmpty())) {
            this.braintreeId = DEFAULT_BRAINTREE_ID;
        }
        if (!(this.customerId != null && !this.customerId.isEmpty())) {
            this.customerId = DEFAULT_CUSTOMER_ID;
        }
        if (!(this.company != null && !this.company.isEmpty())) {
            this.company = DEFAULT_COMPANY;
        }
        if (!(this.phone != null && !this.phone.isEmpty())) {
            this.phone = DEFAULT_PHONE;
        }
        if (!(this.fax != null && !this.fax.isEmpty())) {
            this.fax = DEFAULT_FAX;
        }
        if (!(this.website != null && !this.website.isEmpty())) {
            this.website = DEFAULT_WEBSITE;
        }
        if (!(this.email != null && !this.email.isEmpty())) {
            this.email = DEFAULT_EMAIL;
        }
    }
    //---------------------------------------------------------------------------------------------

    private boolean isEmptyOrDefault(String string, String attributeName) {
        boolean isEmptyOrDefault = false;

        if (isNullOrEmpty(string)) {
            isEmptyOrDefault = true;
        }
        else {
            if (attributeName.equals("firstName")) {
                if (string.equals(DEFAULT_FIRST_NAME)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("lastName")) {
                if (string.equals(DEFAULT_LAST_NAME)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("customerId")) {
                if (string.equals(DEFAULT_CUSTOMER_ID)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("company")) {
                if (string.equals(DEFAULT_COMPANY)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("braintreeId")) {
                if (string.equals(DEFAULT_BRAINTREE_ID)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("website")) {
                if (string.equals(DEFAULT_WEBSITE)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("phone")) {
                if (string.equals(DEFAULT_PHONE)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("fax")) {
                if (string.equals(DEFAULT_FAX)) {
                    isEmptyOrDefault = true;
                }
            }
            else if (attributeName.equals("email")) {
                if (string.equals(DEFAULT_EMAIL)) {
                    isEmptyOrDefault = true;
                }
            }
        }

        return isEmptyOrDefault;
    }
    //---------------------------------------------------------------------------------------------

    private boolean isNullOrEmpty(String string) {
        boolean isNullOrEmpty = false;
        if (string == null) {
            isNullOrEmpty= true;
        }
        else if (string.isEmpty()) {
            isNullOrEmpty = true;
        }

        return isNullOrEmpty;
    }
    //---------------------------------------------------------------------------------------------
}
