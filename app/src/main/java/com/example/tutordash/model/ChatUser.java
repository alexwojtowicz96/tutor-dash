package com.example.tutordash.model;

import com.google.firebase.firestore.IgnoreExtraProperties;

/**
 * This is used in the rendering of the message landing
 */
@IgnoreExtraProperties
public class ChatUser {

    public static final String TAG = "ChatUser";
    public static final int MAX_CHARS_ALLOWED = 24;

    private String name;
    private String timestamp;
    private String lastMessage;
    private String userName;
    private String lastMessageSender;
    private boolean hasBeenRead;
    private boolean valid;

    public ChatUser() { }

    public ChatUser(
            String name,
            String timestamp,
            String lastMessage,
            String userName,
            String lastMessageSender,
            boolean hasBeenRead,
            boolean valid
    ) {
        this.name = name;
        this.timestamp = timestamp;
        this.lastMessage = lastMessage;
        this.userName = userName;
        this.lastMessageSender = lastMessageSender;
        this.hasBeenRead = hasBeenRead;
        this.valid = valid;
    }
    //---------------------------------------------------------------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public String getLastMessageFragment() {
        return trimMessage(lastMessage);
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessageSender() {
        return this.lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public boolean getHasBeenRead() {
        return this.hasBeenRead;
    }

    public void setHasBeenRead(boolean hasBeenRead) {
        this.hasBeenRead = hasBeenRead;
    }

    public boolean getIsValid() {
        return this.valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    //---------------------------------------------------------------------------------------------

    public static String trimMessage(String message) {
        if (message == null) {
            return "";
        }
        else {
            String msg = "";
            if (message.length() > MAX_CHARS_ALLOWED) {
                char[] msgArr = message.toCharArray();
                for (char c : msgArr) {
                    msg += c;
                    if (msg.length() == MAX_CHARS_ALLOWED) {
                        break;
                    }
                }
            }
            else {
                msg = message;
            }

            return msg;
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initNullValues() {
        if (this.name == null) {
            this.name = "";
        }
        if (this.timestamp == null) {
            this.timestamp = "0";
        }
        if (this.lastMessage == null) {
            this.lastMessage = "";
        }
        if (this.userName == null) {
            this.userName = "";
        }
        if (this.lastMessageSender == null) {
            this.lastMessageSender = "";
        }
    }
    //---------------------------------------------------------------------------------------------

    public String getMsgForMsgLanding() {
        if (this.lastMessage != null) {
            if (this.lastMessage.length() <= MAX_CHARS_ALLOWED) {
                return this.lastMessage;
            }
            else {
                return this.getLastMessageFragment() + "...";
            }
        }
        return "";
    }
    //---------------------------------------------------------------------------------------------

    public static String returnTrimmedMessage(String msg) {
        ChatUser x = new ChatUser();
        x.setLastMessage(msg);

        return x.getMsgForMsgLanding();
    }
    //---------------------------------------------------------------------------------------------
}
