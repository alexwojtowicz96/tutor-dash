package com.example.tutordash.model;

/**
 * Holds data for each course in schoolAnalytics > courseAnalytics
 */
public class CourseAnalytics {

    public static final String TAG = "CourseAnalytics";

    private String courseID;
    private Float avgPayRate;
    private Float avgTutorRating;
    private Float numTutorOffering;
    private Float standardDeviation;
    private Float totalSessions;

    public CourseAnalytics() {
        this.courseID = null;
        this.avgPayRate = null;
        this.avgTutorRating = null;
        this.numTutorOffering = null;
        this.standardDeviation = null;
        this.totalSessions = null;
    }
    //---------------------------------------------------------------------------------------------

    public CourseAnalytics(
            String courseID,
            Float avgPayRate,
            Float avgTutorRating,
            Float numTutorOffering,
            Float standardDeviation,
            Float totalSessions
    ) {
        this.courseID = courseID;
        this.avgPayRate = avgPayRate;
        this.avgTutorRating = avgTutorRating;
        this.numTutorOffering = numTutorOffering;
        this.standardDeviation = standardDeviation;
        this.totalSessions = totalSessions;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets and Sets
     */
    public String getCourseID() {
        return courseID;
    }

    public Float getAvgPayRate() {
        return avgPayRate;
    }

    public Float getNumTutorOffering() {
        return numTutorOffering;
    }

    public Float getAvgTutorRating() {
        return avgTutorRating;
    }

    public Float getStandardDeviation() {
        return standardDeviation;
    }

    public Float getTotalSessions() {
        return totalSessions;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public void setAvgPayRate(Float avgPayRate) {
        this.avgPayRate = avgPayRate;
    }

    public void setAvgTutorRating(Float avgTutorRating) {
        this.avgTutorRating = avgTutorRating;
    }

    public void setNumTutorOffering(Float numTutorOffering) {
        this.numTutorOffering = numTutorOffering;
    }

    public void setStandardDeviation(Float standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public void setTotalSessions(Float totalSessions) {
        this.totalSessions = totalSessions;
    }
    //---------------------------------------------------------------------------------------------
}
