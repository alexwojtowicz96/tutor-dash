package com.example.tutordash.model;

import com.google.firebase.Timestamp;

import java.util.Date;

/**
 * This class model the credentials stored in Firestore that we need in order to create
 * web conferences with Vidyo.io. Since we are running a free trial, the idea is that we can
 * just create new accounts if we need them and refresh the credentials in Firestore. When
 * we serialize this data to a local model, this will be used to contain it.
 */
public class VidyoIoDataModel {

    public static final String TAG = "VidyoIoDataModel";
    public static final String HOST = "prod.vidyo.io";
    public static final String DEFAULT_ROOM = "DemoRoom";
    public static final int DEFAULT_PARTICIPANTS = 2;

    private String developerKey;
    private String applicationID;
    private String applicationName;
    private Timestamp membershipStart;
    private Timestamp membershipEnd;
    private String provisionToken;

    public VidyoIoDataModel() {
        this.applicationID = null;
        this.developerKey = null;
        this.applicationName = null;
        this.membershipStart = null;
        this.membershipEnd = null;
        this.provisionToken = null;
    }

    public VidyoIoDataModel(
            String developerKey,
            String applicationID,
            String applicationName,
            Timestamp membershipStart,
            Timestamp membershipEnd,
            String provisionToken
    ) {
        this.membershipStart = membershipStart;
        this.applicationID = applicationID;
        this.developerKey = developerKey;
        this.applicationName = applicationName;
        this.membershipEnd = membershipEnd;
        this.provisionToken = provisionToken;
    }
    //---------------------------------------------------------------------------------------------

    public Timestamp getMembershipStart() {
        return this.membershipStart;
    }

    public Timestamp getMembershipEnd() {
        return this.membershipEnd;
    }

    public String getDeveloperKey() {
        return this.developerKey;
    }

    public String getApplicationID() {
        return this.applicationID;
    }

    public String getApplicationName() {
        return this.applicationName;
    }

    public String getProvisionToken() {
        return this.provisionToken;
    }

    public void setMembershipStart(Timestamp membershipStart) {
        this.membershipStart = membershipStart;
    }

    public void setMembershipEnd(Timestamp membershipEnd) {
        this.membershipEnd = membershipEnd;
    }

    public void setApplicationID(String appID) {
        this.applicationID = appID;
    }

    public void setDeveloperKey(String devKey) {
        this.developerKey = devKey;
    }

    public void setApplicationName(String appName) {
        this.applicationName = appName;
    }

    public void setProvisionToken(String token) {
        this.provisionToken = token;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns TRUE if model is valid, FALSE otherwise. A valid model has all attributes
     * initialized to non-null/non-empty values.
     *
     * @return TRUE if valid, FALSE otherwise
     */
    public boolean findIfModelIsValid() {
        return ((this.provisionToken != null && !this.provisionToken.isEmpty())
                && (this.applicationName != null && !this.applicationName.isEmpty())
                && (this.developerKey != null && !this.developerKey.isEmpty())
                && (this.applicationID != null && !this.applicationID.isEmpty())
                && (this.membershipEnd != null && this.membershipStart != null));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns TRUE if membership has expired. FALSE otherwise.
     *
     * @return TRUE if expired.
     */
    public boolean findIfMembershipHasExpired() {
        if (this.membershipEnd != null) {
            Date now = new Date();
            Date expiration = this.membershipEnd.toDate();

            return !now.before(expiration);
        }
        else {
            return true;
        }
    }
    //---------------------------------------------------------------------------------------------
}
