package com.example.tutordash.model;

/**
 *
 */
public class CalendarEventModel {

    public static final String TAG = "CalendarEventModel";

    private String sessionTitle;
    private String sessionCourseID;
    private String sessionRole;
    private String sessionStatus;
    private String sessionDate; // Not needed?
    private String sessionTimeframe;
    private String sessionID;

    public CalendarEventModel() {}

    public String getSessionTitle() {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    public String getSessionCourseID() {
        return sessionCourseID;
    }

    public void setSessionCourseID(String sessionCourseID) {
        this.sessionCourseID = sessionCourseID;
    }

    public String getSessionRole() {
        return sessionRole;
    }

    public void setSessionRole(String sessionRole) {
        this.sessionRole = sessionRole;
    }

    public String getSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(String sessionStatus) {
        this.sessionStatus = sessionStatus;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getSessionTimeframe() {
        return sessionTimeframe;
    }

    public void setSessionTimeframe(String sessionStartTime) {
        this.sessionTimeframe = sessionStartTime;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
    //---------------------------------------------------------------------------------------------
}
