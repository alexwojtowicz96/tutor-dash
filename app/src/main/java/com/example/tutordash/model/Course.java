package com.example.tutordash.model;

/**
 * The model for a course (each user has a subcollection of these objects in Firestore.
 */
public class Course {

    private Number hours;        // Total number of experience hours in this course
    private boolean isEligible;  // Is the user eligible to tutor this course?
    private boolean isOffered;   // Is the user currently offering this course?
    private Number payRate;      // The user's pay-rate for this course
    private Number rating;       // The user's rating for this course

    public Course() {
        this.hours = 0;
        this.isEligible = false;
        this.isOffered = false;
        this.payRate = 0;
        this.rating = 0;
    }

    public Course(
            Number hours,
            boolean isEligible,
            boolean isOffered,
            Number payRate,
            Number rating
    ) {
        this.hours = hours;
        this.isEligible = isEligible;
        this.isOffered = isOffered;
        this.payRate = payRate;
        this.rating = rating;
    }
    //---------------------------------------------------------------------------------------------

    public Number getHours() {
        if (this.hours != null) {
            return this.hours;
        }
        else {
            return 0;
        }
    }

    public boolean isEligible() {
        return this.isEligible;
    }

    public boolean isOffered() {
        return this.isOffered;
    }

    public Number getPayRate() {
        if (this.payRate != null) {
            return this.payRate;
        }
        else {
            return 0;
        }
    }

    public Number getRating() {
        if (this.rating != null) {
            return this.rating;
        }
        else {
            return 0;
        }
    }

    public void setHours(Number hours) {
        this.hours = hours;
    }

    public void setIsEligible(boolean isEligible) {
        this.isEligible = isEligible;
    }

    public void setIsOffered(boolean isOffered) {
        this.isOffered = isOffered;
    }

    public void setPayRate(Number payRate) {
        this.payRate = payRate;
    }

    public void setRating(Number rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Course { \n" +
                "\thours      = " + this.hours + ",\n" +
                "\tisEligible = " + this.isEligible + ",\n" +
                "\tisOffered  = " + this.isOffered + ",\n" +
                "\tpayRate    = " + this.payRate + ",\n" +
                "\trating     = " + this.rating + ",\n" +
                "}\n";
    }
    //---------------------------------------------------------------------------------------------

    public void initNullValues() {
        if (this.rating == null) {
            this.rating = 0;
        }
        if (this.payRate == null) {
            this.payRate = 0;
        }
        if (this.hours == null) {
            this.hours = 0;
        }
    }
    //---------------------------------------------------------------------------------------------
}
