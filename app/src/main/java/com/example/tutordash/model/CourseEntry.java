package com.example.tutordash.model;

import com.example.tutordash.core.GradeUtils;

/**
 * A CourseEntry is a course ID mapped to a grade. This model (along with the methods)
 * is intended to be used when working with courses in a transcript.
 */
public class CourseEntry {

    private String grade;
    private String courseId;
    private String courseName;
    private String universityName;

    public CourseEntry() {
        this.grade = new String();
        this.courseId = new String();
        this.courseName = new String();
        this.universityName = new String();
    }

    public CourseEntry(String courseEntryString) throws Exception {
        if (this.stringFitsCourseEntryPattern(courseEntryString)) {
            this.convertStringToCourseEntry(courseEntryString);
        }
        else {
            throw new Exception("String => '" + courseEntryString + "' does not fit CourseEntry pattern");
        }
    }

    public String getGrade() {
        return this.grade;
    }

    public String getCourseId() {
        return this.courseId;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public String getUniversityName() {
        return this.universityName;
    }

    public void setGrade(String grade) throws Exception {
        if (this.gradeIsValid(grade)) {
            this.grade = grade.toUpperCase();
        }
        else {
            throw new Exception("Grade value supplied is not valid: " + grade);
        }
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId.toUpperCase();
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName.toUpperCase();
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName.toUpperCase();
    }

    /**
     * Determines if a string is a valid grade based on the ODU grading key found here:
     * https://www.odu.edu/content/dam/odu/offices/university-registrar1/docs/GradeKey.pdf
     *
     * @param grade
     * @return TRUE if valid grade, FALSE otherwise
     */
    public boolean gradeIsValid(String grade) {
        return GradeUtils.isGrade(grade.toUpperCase());
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts CourseEntry object to string. Here's an example:
     * ""CS350","INTRO TO SOFTWARE ENGINEERING","A","OLD DOMINION UNIVERSITY""
     *
     * @return CourseEntry.toString();
     */
    @Override
    public String toString() {
        return  "\"" + this.courseId +       "\"" + "," +
                "\"" + this.courseName +     "\"" + "," +
                "\"" + this.grade +          "\"" + "," +
                "\"" + this.universityName + "\"";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if a string is compatible to be converted to a CourseEntry object.
     *
     * @param string
     * @return TRUE if compatible, FALSE otherwise
     */
    public boolean stringFitsCourseEntryPattern(String string) {
        boolean isValid = true;
        if (string.equals(new String())) {
            isValid = false;
        }
        else {
            String[] elems = string.replaceAll("\"", "").split(",");
            if (elems.length != 4) {
                isValid = false;
            }
            else if (!this.gradeIsValid(elems[2])) {
                isValid = false;
            }
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts a recognized courseEntry string to a CourseEntry object.
     *
     * @param string
     */
    public void convertStringToCourseEntry(String string) throws Exception {
        String[] elems = string.replaceAll("\"", "").split(",");
        this.setCourseId(elems[0]);
        this.setCourseName(elems[1]);
        this.setGrade(elems[2]);
        this.setUniversityName(elems[3]);
    }
    //---------------------------------------------------------------------------------------------
}
