package com.example.tutordash.model;

import com.google.firebase.Timestamp;

import java.util.Date;

public class Token {

    private String key;
    private Timestamp timestamp;
    private int timeout;

    public Token() {
        this.key = "";
        this.timeout = 0;
        this.timestamp = new Timestamp(new Date(0));
    }

    public Token(String key, Timestamp timestamp, int timeout) {
        this.key = key;
        this.timestamp = timestamp;
        this.timeout = timeout;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public String getKey() {
        return this.key;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isValid() {
        boolean isValid = true;
        if (this.key.equals("")) {
            isValid = false;
        }
        if (this.timestamp.equals(new Timestamp(new Date(0)))) {
            isValid = false;
        }
        if (this.timeout <= 0) {
            isValid = false;
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the token is still active based on the time properties.
     * @return isActive
     */
    public boolean isActive() {
        boolean isActive = false;
        final Date now = new Date();
        final Date then = this.timestamp.toDate();

        long seconds = (now.getTime() - then.getTime()) / 1000;
        float minutes = (Float.valueOf(seconds) / Float.valueOf(60));
        if (minutes <= Float.valueOf(this.timeout)) {
            isActive = true;
        }

        return isActive;
    }
    //---------------------------------------------------------------------------------------------
}
