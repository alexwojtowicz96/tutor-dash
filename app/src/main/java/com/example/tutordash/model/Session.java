package com.example.tutordash.model;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.FileUtils;
import com.google.firebase.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The model for tutoring sessions
 */
public class Session {

    public static final String TAG = "Session";

    /**
     * This is the amount of time (minutes) that is padded around the start time. So, for example,
     * if this number is 10 and the session starts at 10:00am, then the waiting window allows
     * for the session to start at any time between 09:55am and 10:05am.
     */
    public static final int WAITING_WINDOW = 20;

    private String sessionID;                 // document ID of session
    private boolean wasCancelled;             // TRUE if someone cancelled the session
    private boolean hasHappened;              // TRUE if session already occurred (wasn't cancelled) [*** not using this]
    private boolean isPending;                // TRUE if session is set to occur in the future
    private boolean isOnline;                 // TRUE if online option was selected
    private String tuteeUID;                  // document ID of tutee
    private String tutorUID;                  // document ID of tutor
    private Timestamp start;                  // Start time (with date)
    private Timestamp end;                    // End time (with date)
    private Integer expectedMinutes;          // Specified minutes at time of session creation.
    private Float expectedAmountOwed;         // Estimated amount owed by tutee based on pay-rate and length.
    private Float hourlyPayRate;              // Pay-rate of tutor for this course at time of session
    private boolean tutorPresent;             // TRUE if tutor was present at time of session start
    private boolean tuteePresent;             // TRUE if tutee was present at time of session start
    private boolean sessionHasConcluded;      // TRUE if session has concluded normally by both users
    private boolean tuteeHasConcluded;        // TRUE if tutee has opted to end session
    private boolean tutorHasConcluded;        // TRUE if tutor has opted to end session
    private String cancellerUID;              // If it exists, the document ID of the user who cancelled
    private String courseID;                  // document ID of the course in question
    private boolean wasAccepted;              // TRUE if the tutor accepted the request
    private String tuteePicUrl;               // Path to tutee profile avatar in Firebase Storage
    private String tutorPicUrl;               // Path to tutor profile avatar in Firebase Storage
    private List<String> involvedUsers;       // A list of all UIDs involved in the session (tutor and tutee)
    private int alarmCode;                    // ID of the alarm that will be used in the reminder
    private boolean tutorHasPaid;             // TRUE if the tutor has paid. FALSE otherwise.
    private Map<String, String> paymentTags;  // Extra information that we can use to reference payments.

    /**
     * Generates default session (empty session with random ID)
     */
    public Session() {
        generateDefault();
    }
    //---------------------------------------------------------------------------------------------

    public Session(String sessionID) {
        generateDefault();
        this.sessionID = sessionID;
        this.alarmCode = generateAlarmCode();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Creates a session from pre-determined values.
     */
    public Session(
            String sessionID,
            boolean isOnline,
            String tuteeUID,
            String tutorUID,
            Timestamp start,
            Timestamp end,
            Integer expectedMinutes,
            Float expectedAmountOwed,
            Float hourlyPayRate,
            boolean tutorPresent,
            boolean tuteePresent,
            boolean sessionHasConcluded,
            boolean tuteeHasConcluded,
            boolean tutorHasConcluded,
            String cancellerUID,
            boolean isPending,
            boolean hasHappened,
            boolean wasCancelled,
            String courseID,
            boolean wasAccepted,
            String tuteePicUrl,
            String tutorPicUrl,
            List<String> involvedUsers,
            int alarmCode,
            boolean tutorHasPaid,
            Map<String, String> paymentTags
    ) {
        this.sessionID = sessionID;
        this.isOnline = isOnline;
        this.tuteeUID = tuteeUID;
        this.tutorUID = tutorUID;
        this.start = start;
        this.end = end;
        this.expectedMinutes = expectedMinutes;
        this.expectedAmountOwed = expectedAmountOwed;
        this.hourlyPayRate = hourlyPayRate;
        this.tutorPresent = tutorPresent;
        this.tuteePresent = tuteePresent;
        this.sessionHasConcluded = sessionHasConcluded;
        this.tuteeHasConcluded = tuteeHasConcluded;
        this.tutorHasConcluded = tutorHasConcluded;
        this.cancellerUID = cancellerUID;
        this.isPending = isPending;
        this.hasHappened = hasHappened;
        this.wasCancelled = wasCancelled;
        this.courseID = courseID;
        this.wasAccepted = wasAccepted;
        this.tuteePicUrl = tuteePicUrl;
        this.tutorPicUrl = tutorPicUrl;
        this.involvedUsers = involvedUsers;
        this.alarmCode = alarmCode;
        this.tutorHasPaid = tutorHasPaid;
        this.paymentTags = paymentTags;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets and Sets
     */
    public String getSessionID() {
        return this.sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public boolean isOnline() {
        return this.isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public String getTuteeUID() {
        return this.tuteeUID;
    }

    public void setTuteeUID(String tuteeUID) {
        this.tuteeUID = tuteeUID;
    }

    public String getTutorUID() {
        return this.tutorUID;
    }

    public void setTutorUID(String tutorUID) {
        this.tutorUID = tutorUID;
    }

    public Timestamp getStart() {
        return this.start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return this.end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Integer getExpectedMinutes() {
        return this.expectedMinutes;
    }

    public void setExpectedMinutes(Integer minutes) {
        this.expectedMinutes = minutes;
    }

    public Float getExpectedAmountOwed() {
        return this.expectedAmountOwed;
    }

    public void setExpectedAmountOwed(Float amountOwed) {
        this.expectedAmountOwed = amountOwed;
    }

    public Float getHourlyPayRate() {
        return this.hourlyPayRate;
    }

    public void setHourlyPayRate(Float payRate) {
        this.hourlyPayRate = payRate;
    }

    public boolean getTuteePresent() {
        return this.tuteePresent;
    }

    public void setTuteePresent(boolean present) {
        this.tuteePresent = present;
    }

    public boolean getTutorPresent() {
        return this.tutorPresent;
    }

    public void setTutorPresent(boolean present) {
        this.tutorPresent = present;
    }

    public boolean getSessionHasConcluded() {
        return this.sessionHasConcluded;
    }

    public void setSessionHasConcluded(boolean concluded) {
        this.sessionHasConcluded = concluded;
    }

    public boolean getTuteeHasConcluded() {
        return this.tuteeHasConcluded;
    }

    public void setTuteeHasConcluded(boolean concluded) {
        this.tuteeHasConcluded = concluded;
    }

    public boolean getTutorHasConcluded() {
        return this.tutorHasConcluded;
    }

    public void setTutorHasConcluded(boolean concluded) {
        this.tutorHasConcluded = concluded;
    }

    public String getCancellerUID() {
        return this.cancellerUID;
    }

    public void setCancellerUID(String cancellerUID) {
        this.cancellerUID = cancellerUID;
    }

    public boolean getIsPending() {
        return this.isPending;
    }

    public boolean getHasHappened() {
        return this.hasHappened;
    }

    public boolean getWasCancelled() {
        return this.wasCancelled;
    }

    public void setIsPending(boolean isPending) {
        this.isPending = isPending;
    }

    public void setHasHappened(boolean hasHappened) {
        this.hasHappened = hasHappened;
    }

    public void setWasCancelled(boolean wasCancelled) {
        this.wasCancelled = wasCancelled;
    }

    public String getCourseID() {
        return this.courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public void setWasAccepted(boolean accepted) {
        this.wasAccepted = accepted;
    }

    public boolean getWasAccepted() {
        return this.wasAccepted;
    }

    public String getTuteePicUrl() {
        return this.tuteePicUrl;
    }

    public String getTutorPicUrl() {
        return this.tutorPicUrl;
    }

    public void setTuteePicUrl(String url) {
        this.tuteePicUrl = url;
    }

    public void setTutorPicUrl(String url) {
        this.tutorPicUrl = url;
    }

    public List<String> getInvolvedUsers() {
        return this.involvedUsers;
    }

    public void setInvolvedUsers(List<String> users) {
        this.involvedUsers = users;
    }

    public int getAlarmCode() {
        return this.alarmCode;
    }

    public void setAlarmCode(int code) {
        this.alarmCode = code;
    }

    public boolean getTutorHasPaid() {
        return tutorHasPaid;
    }

    public void setTutorHasPaid(boolean tutorhasPaid) {
        this.tutorHasPaid = tutorhasPaid;
    }

    public Map<String, String> getPaymentTags() {
        return this.paymentTags;
    }

    public void setPaymentTags(Map<String, String> tags) {
        this.paymentTags = tags;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if session can be used in data comparisons
     */
    public boolean findIsValidSession() {
        return (this.tutorUID != null &&
                this.tuteeUID != null &&
                this.start != null &&
                this.expectedAmountOwed != null &&
                this.hourlyPayRate != null &&
                this.sessionID != null &&
                this.expectedMinutes != null &&
                (
                        this.wasCancelled ||
                        this.isPending ||
                        this.findIfHasHappened()
                )
                &&
                (
                        this.involvedUsers.size() == 2 &&
                        this.involvedUsers.contains(tutorUID) &&
                        this.involvedUsers.contains(tuteeUID)
                )
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the session exists in the future
     */
    public boolean findIsUpcoming() {
        boolean isUpcoming = false;
        long startTimeSeconds = this.start.toDate().getTime() / 1000;
        long nowTimeSeconds = new Date().getTime() / 1000;

        return (startTimeSeconds - nowTimeSeconds > 0);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the session is waiting to be started (via handshake agreement).
     * As long as both users are not present and the session is within the waiting window,
     * then this session is awaiting handshake.
     *
     * To clear up any confusion, if the both users are not there and the waiting window is not
     * active, then this session is NOT awaiting start. However, as long as the session's start
     * time is after the current time, it IS upcoming.
     */
    public boolean findIsAwaitingHandshake() {
        boolean isAwaitingHandshake = false;
        if (findIsWithinWaitingWindow()) {
            if (!this.tuteePresent || !this.tutorPresent) {
                isAwaitingHandshake = true;
            }
        }

        return isAwaitingHandshake;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the session is within the waiting window. This is based on the start date
     * and the WAITING_WINDOW constant.
     */
    public boolean findIsWithinWaitingWindow() {
        boolean isWithinWindow = false;
        long startTimeSeconds = this.start.toDate().getTime() / 1000;
        long nowTimeSeconds = new Date().getTime() / 1000;

        long secondsSpan = Math.abs(startTimeSeconds - nowTimeSeconds);
        if (secondsSpan <= ((long) ((Session.WAITING_WINDOW / 2) * 60))) {
            isWithinWindow = true;
        }

        return isWithinWindow;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * A session is active if both the tutor and tutee are currently in the meeting.
     * This means both have acknowledged the handshake and neither have declared the end
     * of the session.
     */
    public boolean findIsActive() {
        return (this.tuteePresent &&
                this.tutorPresent &&
                !this.sessionHasConcluded &&
                !(this.tutorHasConcluded || this.tuteeHasConcluded));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * A session is awaiting its conclusion if only one of the users has declared the end of
     * the session. The session is awaiting conclusion as long as one or the other BUT NOT BOTH
     * have decided to conclude.
     */
    public boolean findIsAwaitingConclusion() {
        return (this.tuteeHasConcluded || this.tutorHasConcluded) &&
                !(this.tuteeHasConcluded && this.tutorHasConcluded);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes default session.
     */
    public void generateDefault() {
        this.sessionID = FileUtils.generateToken(22, 30);
        this.alarmCode = generateAlarmCode();
        this.isOnline = false;
        this.tuteeUID = null;
        this.tutorUID = null;
        this.start = null;
        this.end = null;
        this.expectedMinutes = null;
        this.expectedAmountOwed = null;
        this.hourlyPayRate = null;
        this.tutorPresent = false;
        this.tuteePresent = false;
        this.sessionHasConcluded = false;
        this.tuteeHasConcluded = false;
        this.tutorHasConcluded = false;
        this.cancellerUID = null;
        this.isPending = false;
        this.hasHappened = false;
        this.wasCancelled = false;
        this.courseID = null;
        this.wasAccepted = false;
        this.tutorPicUrl = null;
        this.tuteePicUrl = null;
        this.involvedUsers = new ArrayList<String>();
        this.tutorHasPaid = false;
        this.paymentTags = new HashMap<String, String>();
    }
    //---------------------------------------------------------------------------------------------

    public boolean findIfHasHappened() {
        return (this.tutorHasConcluded && this.tuteeHasConcluded);
    }
    //---------------------------------------------------------------------------------------------

    public void putInvolvedUsers() {
        this.involvedUsers = new ArrayList<String>();
        if (this.tutorUID != null) {
            this.involvedUsers.add(tutorUID);
        }
        if (this.tuteeUID != null) {
            this.involvedUsers.add(tuteeUID);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes all null values to an initial value.
     */
    public void initNullValues() {
        if (sessionID == null) {
            sessionID = FileUtils.generateToken(22, 30);
        }
        if (tuteeUID == null) {
            tuteeUID = "";
        }
        if (tutorUID == null) {
            tutorUID = "";
        }
        if (start == null) {
            start = new Timestamp(new Date(0));
        }
        if (end == null) {
            end = new Timestamp(new Date(0));
        }
        if (expectedMinutes == null) {
            expectedMinutes = 0;
        }
        if (expectedAmountOwed == null) {
            expectedAmountOwed = Float.valueOf(0);
        }
        if (hourlyPayRate == null) {
            hourlyPayRate = Float.valueOf(0);
        }
        if (cancellerUID == null) {
            cancellerUID = "";
        }
        if (courseID == null) {
            courseID = "";
        }
        if (tutorPicUrl == null) {
            tutorPicUrl = "";
        }
        if (tuteePicUrl == null) {
            tuteePicUrl = "";
        }
        if (involvedUsers == null) {
            putInvolvedUsers();
        }
        if (paymentTags == null) {
            paymentTags = new HashMap<String, String>();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Basically, this does the opposite of initNullValues(). This only applies to strings though.
     * So if you had null values for floats, those will remain 0.
     */
    public void makeNullValues() {
        if (sessionID != null && sessionID.equals("")) {
            sessionID = null;
        }
        if (tuteeUID != null && tuteeUID.equals("")) {
            tuteeUID = null;
        }
        if (tutorUID != null && tutorUID.equals("")) {
            tutorUID = null;
        }
        if (start != null && start.equals(new Timestamp(new Date(0)))) {
            start = null;
        }
        if (end != null && end.equals(new Timestamp(new Date(0)))) {
            end = null;
        }
        if (cancellerUID != null && cancellerUID.equals("")) {
            cancellerUID = null;
        }
        if (courseID != null && courseID.equals("")) {
            courseID = null;
        }
        if (tutorPicUrl != null && tutorPicUrl.equals("")) {
            tutorPicUrl = null;
        }
        if (tuteePicUrl != null && tuteePicUrl.equals("")) {
            tuteePicUrl = null;
        }
        if (involvedUsers != null && involvedUsers.size() == 0) {
            involvedUsers = null;
        }
        if (paymentTags != null && paymentTags.size() == 0) {
            paymentTags = null;
        }
    }
    //---------------------------------------------------------------------------------------------

    private int generateAlarmCode() {
        return ActivityUtils.generateRandomInt(999, 9999999);
    }
    //---------------------------------------------------------------------------------------------
}
