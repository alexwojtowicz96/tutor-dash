package com.example.tutordash.model;

/**
 * This model will hold all of the private data that should ideally live on the server that
 * we aren't using at the moment.
 */
public class BraintreeDataModel {

    public static final String TAG = "BraintreeDataModel";

    private String tokenizationKey;
    private String apiPrivateKey;
    private String apiPublicKey;
    private String merchantID;
    private String cseKey;
    private String environment;
    //---------------------------------------------------------------------------------------------

    public BraintreeDataModel() {}

    public BraintreeDataModel(
            String tokenizationKey,
            String apiPrivateKey,
            String apiPublicKey,
            String merchantID,
            String cseKey,
            String environment
    ) {
        this.tokenizationKey = tokenizationKey;
        this.apiPrivateKey = apiPrivateKey;
        this.apiPublicKey = apiPublicKey;
        this.merchantID = merchantID;
        this.cseKey = cseKey;
        this.environment = environment;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * GETS
     * ============================================================================================
     */
    public String getTokenizationKey() {
        return this.tokenizationKey;
    }

    public String getMerchantID() {
        return this.merchantID;
    }

    public String getApiPrivateKey() {
        return this.apiPrivateKey;
    }

    public String getApiPublicKey() {
        return this.apiPublicKey;
    }

    public String getCseKey() {
        return this.cseKey;
    }

    public String getEnvironment() {
        return this.environment;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * SETS
     * ============================================================================================
     */
    public void setTokenizationKey(String tokenizationKey) {
        this.tokenizationKey = tokenizationKey;
    }

    public void setApiPrivateKey(String key) {
        this.apiPrivateKey = key;
    }

    public void setApiPublicKey(String key) {
        this.apiPublicKey = key;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public void setCseKey(String cseKey) {
        this.cseKey = cseKey;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the environment is a sandbox environment or not.
     * @return TRUE if sandbox, FALSE otherwise.
     */
    public boolean findIfIsSandbox() {
        if (this.environment != null && !this.environment.isEmpty()) {
            if (this.environment.trim().toLowerCase().equals("sandbox")) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the environment is production or not.
     * @return TRUE if production, FALSE otherwise.
     */
    public boolean findIfIsProduction() {
        if (!this.findIfIsSandbox()) {
            if (this.environment.trim().toLowerCase().equals("production")) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the environment is development or not.
     * @return TRUE if development, FALSE otherwise.
     */
    public boolean findIfIsDevelopment() {
        if (!this.findIfIsSandbox()) {
            if (this.environment.trim().toLowerCase().equals("development")) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if this model's state is valid. ALL attributes must be initialized!
     * @return TRUE if valid, FALSE otherwise.
     */
    public boolean findIfIsValidModel() {
        boolean isValid = true;
        if (!(this.merchantID != null && !this.merchantID.isEmpty())) {
            isValid = false;
        }
        else if (!(this.tokenizationKey != null && !this.tokenizationKey.isEmpty())) {
            isValid = false;
        }
        else if (!(this.environment != null && !this.environment.isEmpty())) {
            isValid = false;
        }
        else if (!(this.apiPublicKey != null && !this.apiPublicKey.isEmpty())) {
            isValid = false;
        }
        else if (!(this.apiPrivateKey != null && !this.apiPrivateKey.isEmpty())) {
            isValid = false;
        }
        else if (!(this.cseKey != null && !this.cseKey.isEmpty())) {
            isValid = false;
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------
}
