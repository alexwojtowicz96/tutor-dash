package com.example.tutordash.model;

/**
 * Use this to model course selections in a recycler view.
 */
public class RenderCourseModel {

    private String courseId;
    private boolean isSelected;
    private boolean isDisabled;

    public RenderCourseModel() {
        this.courseId = "COURSE_ID";
        this.isSelected = false;
        this.isDisabled = false;
    }

    public RenderCourseModel(String courseId, boolean isSelected, boolean isDisabled) {
        this.courseId = courseId;
        this.isSelected = isSelected;
        this.isDisabled = isDisabled;
    }

    public String getCourseId() {
        return this.courseId;
    }

    public boolean getIsSelected() {
        return this.isSelected;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean getIsDisabled() {
        return this.isDisabled;
    }

    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }
}
