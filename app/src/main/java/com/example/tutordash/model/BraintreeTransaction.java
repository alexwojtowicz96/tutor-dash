package com.example.tutordash.model;

import com.google.firebase.Timestamp;

import java.util.Date;
import java.util.List;

/**
 * This class models a transaction that has been completed by a user. Every transaction needs
 * to be submitted in order to exist in Firestore. This is essential a wrapper for the
 * transactions that exist in Braintree's Vault. This model will be stored in Firestore with
 * very basic information so that we can reference the transactions at a later date.
 */
public class BraintreeTransaction {

    public static final String TAG = "BraintreeTransaction";
    public static final String DEFAULT_RECEIVER = "TUTOR_DASH_DEV_TEAM";

    private String transactionID;     // ID of the transaction that is created in Braintree
    private String initiatorUID;      // The username of the user who initiated the transaction (tutee)
    private String receiverUID;       // The username of the user who received the payment (tutor)
    private Timestamp dateSubmitted;  // The date the transaction was submitted
    private Timestamp dateUpdated;    // The date the the transaction was last updated
    private String paymentMethod;     // e.g., CREDIT_CARD, PAYPAL_ACCOUNT
    private Float amount;             // How much money in $
    private boolean isVoid;           // TRUE if the transaction has since been voided
    private List<String> tags;        // Some extra data that can be null.
    private List<String> involved;    // Involved usernames associated with the transaction
    //---------------------------------------------------------------------------------------------

    public BraintreeTransaction() {}

    public BraintreeTransaction(
            String transactionID,
            String initiatorUID,
            String receiverUID,
            Timestamp dateSubmitted,
            Timestamp dateUpdated,
            String paymentMethod,
            Float amount,
            boolean isVoid,
            List<String> tags,
            List<String> involved
    ) {
        this.transactionID = transactionID;
        this.initiatorUID = initiatorUID;
        this.receiverUID = receiverUID;
        this.dateSubmitted = dateSubmitted;
        this.dateUpdated = dateUpdated;
        this.paymentMethod = paymentMethod;
        this.isVoid = isVoid;
        this.tags = tags;
        this.amount = amount;
        this.involved = involved;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * GETS
     * ============================================================================================
     */
    public String getTransactionID() {
        return this.transactionID;
    }

    public String getReceiverUID() {
        return this.receiverUID;
    }

    public String getInitiatorUID() {
        return this.initiatorUID;
    }

    public Timestamp getDateSubmitted() {
        return this.dateSubmitted;
    }

    public Timestamp getDateUpdated() {
        return this.dateUpdated;
    }

    public boolean getIsVoid() {
        return this.isVoid;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public Float getAmount() {
        return this.amount;
    }

    public List<String> getInvolved() {
        return this.involved;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * SETS
     * ============================================================================================
     */
    public void setTransactionID(String id) {
        this.transactionID = id;
    }

    public void setReceiverUID(String uid) {
        this.receiverUID = uid;
    }

    public void setInitiatorUID(String uid) {
        this.initiatorUID = uid;
    }

    public void setIsVoid(boolean isVoid) {
        this.isVoid = isVoid;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setDateSubmitted(Timestamp date) {
        this.dateSubmitted = date;
    }

    public void setDateUpdated(Timestamp date) {
        this.dateUpdated = date;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public void setInvolved(List<String> involved) {
        this.involved = involved;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if this transaction model is valid or not.
     * @return TRUE if valid model, FALSE otherwise.
     */
    public boolean findIfIsValidModel() {
        return (transactionID != null && !transactionID.isEmpty())
                && (receiverUID != null && !receiverUID.isEmpty())
                && (initiatorUID != null && !initiatorUID.isEmpty())
                && (paymentMethod != null && !paymentMethod.isEmpty())
                && (amount != null && amount >= Float.valueOf(0))
                && (dateSubmitted != null && !dateSubmitted.toDate().equals(new Date(0))
                && (dateUpdated != null && !dateUpdated.toDate().equals(new Date(0)))
                && (this.involved != null && this.involved.size() > 0));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if this.tags contains tag
     *
     * @param tag
     * @return TRUE if tag is contained within this.tags, FALSE otherwise
     */
    public boolean findIfHasTag(String tag) {
        if (this.tags != null && this.tags.size() > 0 && this.tags.contains(tag)) {
            return true;
        }
        else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns TRUE if the supplied word is found in the tags.
     *
     * @param word
     * @return TRUE if found, FALSE otherwise
     */
    public boolean findIfHasWordInTags(String word) {
        if (this.tags != null && this.tags.size() > 0) {
            StringBuilder b = new StringBuilder();
            for (String s : this.tags) {
                String[] arr = s.split(" ");
                for (String w : arr) {
                    b.append(w);
                }
            }

            final String toSearch = b.toString();
            return toSearch.toUpperCase().contains(word.toUpperCase());
        }
        else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------
}
