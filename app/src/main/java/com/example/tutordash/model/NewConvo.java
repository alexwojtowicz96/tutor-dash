package com.example.tutordash.model;

public class NewConvo {

    public static final String TAG = "NewConvo";
    private String name;

    public NewConvo() {}

    public NewConvo(String name) {
       this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
//-------------------------------------------------------------------------------------------------
