package com.example.tutordash.model;

import java.util.Map;

/**
 * Models the data found in each document in schoolAnalytics
 */
public class SchoolAnalytics {

    public static final String TAG = "SchoolAnalytics";

    private String schoolID;
    private Float avgCoursePay;
    private Float avgTutorRating;
    private Float totalSessions;
    private Float totalCourses;
    private Map<String, CourseAnalytics> courseAnalytics;
    //---------------------------------------------------------------------------------------------

    public SchoolAnalytics() {
        this.schoolID = "";
        this.avgCoursePay = null;
        this.avgTutorRating = null;
        this.totalSessions = null;
        this.courseAnalytics = null;
        this.totalCourses = null;
    }
    //---------------------------------------------------------------------------------------------

    public SchoolAnalytics(
            String schoolID,
            Float avgCoursePay,
            Float avgTutorRating,
            Float totalSessions,
            Float totalCourses,
            Map<String, CourseAnalytics> courseAnalytics
    ) {
        this.schoolID = schoolID;
        this.avgCoursePay = avgCoursePay;
        this.avgTutorRating = avgTutorRating;
        this.totalSessions = totalSessions;
        this.courseAnalytics = courseAnalytics;
        this.totalCourses = totalCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets and sets
     */
    public String getSchoolID() {
        return schoolID;
    }

    public Float getAvgCoursePay() {
        return avgCoursePay;
    }

    public Float getAvgTutorRating() {
        return avgTutorRating;
    }

    public Float getTotalSessions() {
        return totalSessions;
    }

    public Float getTotalCourses() {
        return totalCourses;
    }

    public Map<String, CourseAnalytics> getCourseAnalytics() {
        return courseAnalytics;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public void setAvgCoursePay(Float avgCoursePay) {
        this.avgCoursePay = avgCoursePay;
    }

    public void setAvgTutorRating(Float avgTutorRating) {
        this.avgTutorRating = avgTutorRating;
    }

    public void setTotalSessions(Float totalSessions) {
        this.totalSessions = totalSessions;
    }

    public void setTotalCourses(Float totalCourses) {
        this.totalCourses = totalCourses;
    }

    public void setcourseAnalytics(Map<String, CourseAnalytics> courseAnalytics) {
        this.courseAnalytics = courseAnalytics;
    }
    //---------------------------------------------------------------------------------------------
}
