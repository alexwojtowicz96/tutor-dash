package com.example.tutordash.model;

import java.util.ArrayList;

/**
 *
 */
public class CalendarEvent {

    public static final String TAG = "CalendarEvent";
    public static ArrayList<CalendarEvent> calendarListings;

    public String date = "";
    public String sessionTitle = ""; // Tutoring session with ____
    public String sessionCourse = ""; // Course ID
    public String sessionRole = ""; // Tutor or tutee
    public String sessionTime = ""; // Start to end
    public String sessionStatus = ""; // Cancelled, pending, or completed
    public String sessionID = "";
    //---------------------------------------------------------------------------------------------

    public CalendarEvent(
            String date,
            String sessionTitle,
            String sessionCourse,
            String sessionRole,
            String sessionTime,
            String sessionStatus,
            String sessionID
    ) {
        this.date = date;
        this.sessionTitle = sessionTitle;
        this.sessionCourse = sessionCourse;
        this.sessionRole = sessionRole;
        this.sessionTime = sessionTime;
        this.sessionStatus = sessionStatus;
        this.sessionID = sessionID;
    }
    //---------------------------------------------------------------------------------------------
}

