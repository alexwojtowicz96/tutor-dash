package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.model.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * This activity serves as the landing for the member profile. Depending on who is viewing the
 * profile will determine how it renders (mainly, which cards will render).
 *
 * You might notice that there are 7 cards in this activity, but only at most 5 are ever in
 * use. This is leftover from a previous idea, but it isn't hurting anything. If more options
 * need to be added later, this design accounts for that.
 */
public class MemberProfileActivity extends AppCompatActivity {

    public static final String TAG = "MemberProfileActivity";

    public CircleImageView avatar;
    public TextView usernameView;
    public TextView emailView;
    public TextView changeAvatarView;
    public CardView card1;
    public CardView card2;
    public CardView card3;
    public CardView card4;
    public CardView card5;
    public CardView card6;
    public CardView card7;
    public ImageButton btn1;
    public ImageButton btn2;
    public ImageButton btn3;
    public ImageButton btn4;
    public ImageButton btn5;
    public ImageButton btn6;
    public ImageButton btn7;
    public TextView cardText1;
    public TextView cardText2;
    public TextView cardText3;
    public TextView cardText4;
    public TextView cardText5;
    public TextView cardText6;
    public TextView cardText7;

    public Context context;
    public StorageReference storageReference;
    public boolean isCurrentUserProfile = false;
    //---------------------------------------------------------------------------------------------

    /**
     * onCreate()
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_profile);

        this.context = this.getApplicationContext();
        this.storageReference = FirebaseStorage.getInstance().getReference();
        this.isCurrentUserProfile = getIfIsCurrentUserProfile();

        if (hasIntegrity()) {
            this.avatar = findViewById(R.id.member_avatar);
            this.usernameView = findViewById(R.id.member_username);
            this.emailView = findViewById(R.id.member_email);
            this.changeAvatarView = findViewById(R.id.member_changeAvatar);
            this.card1 = findViewById(R.id.member_option_1);
            this.card2 = findViewById(R.id.member_option_2);
            this.card3 = findViewById(R.id.member_option_3);
            this.card4 = findViewById(R.id.member_option_4);
            this.card5 = findViewById(R.id.member_option_5);
            this.card6 = findViewById(R.id.member_option_6);
            this.card7 = findViewById(R.id.member_option_7);
            this.btn1 = findViewById(R.id.member_option_btn_1);
            this.btn2 = findViewById(R.id.member_option_btn_2);
            this.btn3 = findViewById(R.id.member_option_btn_3);
            this.btn4 = findViewById(R.id.member_option_btn_4);
            this.btn5 = findViewById(R.id.member_option_btn_5);
            this.btn6 = findViewById(R.id.member_option_btn_6);
            this.btn7 = findViewById(R.id.member_option_btn_7);
            this.cardText1 = findViewById(R.id.member_option_text_1);
            this.cardText2 = findViewById(R.id.member_option_text_2);
            this.cardText3 = findViewById(R.id.member_option_text_3);
            this.cardText4 = findViewById(R.id.member_option_text_4);
            this.cardText5 = findViewById(R.id.member_option_text_5);
            this.cardText6 = findViewById(R.id.member_option_text_6);
            this.cardText7 = findViewById(R.id.member_option_text_7);

            this.bindDataToView();
        }
        else {
            String msg = "Something went wrong. Please try again later.";
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if data has integrity so the current user can proceed in this activity.
     *
     * @return TRUE if data has integrity, FALSE otherwise
     */
    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            hasIntegrity = false;
            Log.d(TAG, "The current user was null!");
        }
        else if (!ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "The current user was not valid!");
        }

        if (!this.isCurrentUserProfile) {
            if (ActivityUtils.TMP_USR == null) {
                hasIntegrity = false;
                Log.d(TAG, "ActivityUtils.TMP_USR was null! Are you trying to view another " +
                        "user's profile? This can't be null for you to do this.");
            }
            else if (!ActivityUtils.TMP_USR.isValidUser()) {
                hasIntegrity = false;
                Log.d(TAG, "ActivityUtils.TMP_USR was invalid! Are you trying to view another " +
                        "user's profile? This must be valid for you to do this.");
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if this view is the current user's profile. IMPORTANT! This is dependent
     * on an extra getting passed to the activity called "USERNAME_CONFIRMATION" and it
     * must contain the uName of the currently signed-in user.
     *
     * @return TRUE if the current user's profile is being viewed, FALSE otherwise.
     */
    public boolean getIfIsCurrentUserProfile() {
        boolean isCurrentUser = false;
        Bundle intentExtras = getIntent().getExtras();
        String uName = intentExtras.getString("USERNAME_CONFIRMATION");

        if (ActivityUtils.CURRENT_USER_OBJECT != null) {
            User user = ActivityUtils.CURRENT_USER_OBJECT.initNullValues();
            if (uName != null) {
                if (uName.equals(user.getuName())) {
                    isCurrentUser = true;
                }
            }
        }

        return isCurrentUser;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Check to make sure we are using the right object to populate the views onStart().
     */
    @Override
    public void onStart() {
        super.onStart();
        this.isCurrentUserProfile = getIfIsCurrentUserProfile();
        this.bindDataToView();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Binds the data to the view.
     */
    public void bindDataToView() {
        this.card1.setVisibility(View.GONE);
        this.card2.setVisibility(View.GONE);
        this.card3.setVisibility(View.GONE);
        this.card4.setVisibility(View.GONE);
        this.card5.setVisibility(View.GONE);
        this.card6.setVisibility(View.GONE);
        this.card7.setVisibility(View.GONE);
        this.changeAvatarView.setVisibility(View.INVISIBLE);

        User user;
        if (this.isCurrentUserProfile) {
            user = ActivityUtils.CURRENT_USER_OBJECT;
            this.usernameView.setText(user.getuName());
            this.emailView.setText(user.getEmail());
            if (user.getPicURL() != null && !user.getPicURL().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference.child(user.getPicURL())).into(this.avatar);
            }
            this.changeAvatarView.setVisibility(View.VISIBLE);
            setupCardsForMyProfile();
        }
        else {
            user = ActivityUtils.TMP_USR;
            this.usernameView.setText(user.getuName());
            this.emailView.setText(user.getEmail());
            if (user.getPicURL() != null && !user.getPicURL().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference.child(user.getPicURL())).into(this.avatar);
            }
            setupCardsForOtherProfile();
        }

        this.setClickListeners();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Cards are setup like this:
     *      1. My Info       2. Calendar
     *      3. My Messages   4. Finance
     *      5. Settings
     */
    public void setupCardsForMyProfile() {
        this.card1.setVisibility(View.VISIBLE);
        this.card2.setVisibility(View.VISIBLE);
        this.card3.setVisibility(View.VISIBLE);
        this.card4.setVisibility(View.VISIBLE);
        this.card5.setVisibility(View.VISIBLE);

        this.btn1.setImageResource(R.drawable.contact);
        this.cardText1.setText("My Info");
        this.btn2.setImageResource(R.drawable.bookmark);
        this.cardText2.setText("Calendar");
        this.btn3.setImageResource(R.drawable.chat);
        this.cardText3.setText("My Messages");
        this.btn4.setImageResource(R.drawable.finance);
        this.cardText4.setText("Finance");
        this.btn5.setImageResource(R.drawable.options);
        this.cardText5.setText("Settings");
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Cards are setup like this:
     *      1. User Info         2. Calendar
     *      3. Request Session   4. Send Message
     */
    public void setupCardsForOtherProfile() {
        this.card1.setVisibility(View.VISIBLE);
        this.card2.setVisibility(View.VISIBLE);
        this.card3.setVisibility(View.VISIBLE);
        this.card4.setVisibility(View.VISIBLE);

        this.btn1.setImageResource(R.drawable.contact);
        this.cardText1.setText("User Info");
        this.btn2.setImageResource(R.drawable.bookmark);
        this.cardText2.setText("Calendar");
        this.btn3.setImageResource(R.drawable.schedule_icon);
        this.cardText3.setText("Request Session");
        this.btn4.setImageResource(R.drawable.new_message);
        this.cardText4.setText("Send Message");
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets all click listeners to determine what happens when the user clicks on certain elements.
     *
     * If the current user is viewing their profile, they will have 5 cards setup like this:
     *      1. My Info       2. Calendar
     *      3. My Messages   4. Finance
     *      5. Settings
     *
     * If the current user is viewing another user's profile, they will see 4 cards setup like this:
     *      1. User Info         2. Calendar
     *      3. Request Session   4. Send Message
     */
    public void setClickListeners() {
        this.changeAvatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AvatarUploadActivity.class);
                startActivity(intent);
            }
        });

        if (this.isCurrentUserProfile) {
            this.btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UserInfoActivity.class);
                    intent.putExtra("IS_CURRENT_USER", "true");
                    startActivity(intent);
                }
            });
            this.btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CalendarActivity.class);
                    startActivity(intent);
                }
            });
            this.btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MessageLandingActivity.class);
                    startActivity(intent);
                }
            });
            this.btn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FinanceActivity.class);
                    startActivity(intent);
                }
            });
            this.btn5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AccountSettingsActivity.class);
                    startActivity(intent);
                }
            });
        }
        else {
            this.btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UserInfoActivity.class);
                    intent.putExtra("IS_CURRENT_USER", "false");
                    startActivity(intent);
                }
            });
            this.btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CalendarActivity.class);
                    intent.putExtra("IS_CURRENT_USER", "false");
                    startActivity(intent);
                }
            });
            this.btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!(ActivityUtils.TMP_USR.isTutor() &&
                            ActivityUtils.TMP_USR.getCourses() != null &&
                            ActivityUtils.TMP_USR.coursesOffered().size() > 0))
                    {
                        String msg = ActivityUtils.TMP_USR.getuName() + " is not a tutor!";
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Intent intent = new Intent(context, SelectCourseForSessionActivity.class);
                        startActivity(intent);
                    }
                }
            });
            this.btn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MessageActivity.class);
                    intent.putExtra("MESSAGING", ActivityUtils.TMP_USR.getuName());
                    startActivity(intent);
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------
}