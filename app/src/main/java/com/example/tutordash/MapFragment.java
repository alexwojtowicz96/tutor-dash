package com.example.tutordash;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.tutordash.adapter.MapInfoWindowAdapter;
import com.example.tutordash.base.MainDiscoveryFragment;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.LocationService;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.model.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;

import static android.content.Context.LOCATION_SERVICE;

public class MapFragment extends Fragment implements OnMapReadyCallback, MainDiscoveryFragment {

    public static final String NAME = "Map Discovery";
    public static final String TAG = "MapFragment";
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private GoogleMap map;
    private MapView mapView;
    private LatLng latLng;
    private LocationManager locManager;
    private String provider;
    private ArrayList<User> allUsers;
    FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        initLocationDependencies();

        database = FirebaseFirestore.getInstance();

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        mapView = view.findViewById(R.id.googleMapView);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        return view;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the location stuff to what exists in the main activity. If for some
     * reason those values are null, then reinitialize them in this fragment.
     */
    public void initLocationDependencies() {
        MainDiscoveryActivity activity = ((MainDiscoveryActivity) getActivity());
        if (activity.latLng != null) {
            this.latLng = activity.latLng;
        }
        else {
            this.latLng = new LatLng(0.00, 0.00);
        }

        if (activity.locManager != null) {
            this.locManager = activity.locManager;
        }
        else {
            this.locManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        }

        if (activity.provider != null) {
            this.provider = activity.provider;
        }
        else {
            this.provider = locManager.GPS_PROVIDER;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets this.latLng (well, it tries to do so at least).
     * If location permissions are not enabled, this will send a request.
     * If the request is accepted, the location is updated.
     * If permissions are already enabled, then location is set to last known location.
     */
    public void getLocation() {
        boolean needsFineLocation = ActivityCompat.checkSelfPermission(
                getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED;
        boolean needsCourseLocation = ActivityCompat.checkSelfPermission(
                getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED;
        if (needsCourseLocation && needsFineLocation) {
            requestLocationPermission();
        }
        else {
            Location location = this.locManager.getLastKnownLocation(this.provider);
            if (location != null) {
                this.latLng = new LatLng(location.getLatitude(), location.getLongitude());
            }
            else if (ActivityUtils.CURRENT_USER_OBJECT.getLocation() != null &&
                    !ActivityUtils.CURRENT_USER_OBJECT.getLocation().equals(new GeoPoint(0, 0))) {
                this.latLng = new LatLng(
                        ActivityUtils.CURRENT_USER_OBJECT.getLocation().getLatitude(),
                        ActivityUtils.CURRENT_USER_OBJECT.getLocation().getLongitude()
                );
            }
            else {
                this.latLng = new LatLng(0, 0);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Request location permission from user.
     */
    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                PermissionUtils.REQUEST_CODE_FOR_LOCATION
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the location listener so that this.latLng can receive "real-time" updates.
     * Not sure if they are technically real-time, but they are close enough.
     */
    public void setLocationListener() {
        DocumentReference docRef = database.collection("users")
                .document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        try {
            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    final GeoPoint currLocation = ActivityUtils.CURRENT_USER_OBJECT.getLocation();

                    if (!LocationService.isMarginalDistance(currLocation, new GeoPoint(latLng.latitude, latLng.longitude))) {
                        docRef.update("location", new GeoPoint(latLng.latitude, latLng.longitude))
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            ActivityUtils.CURRENT_USER_OBJECT
                                                    .setLocation(new GeoPoint(latLng.latitude, latLng.longitude));
                                        }
                                        else {
                                            Log.e(TAG, "Couldn't write new location to Firestore");
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {}

                @Override
                public void onProviderEnabled(String provider) {}

                @Override
                public void onProviderDisabled(String provider) {}
            };

            this.locManager.requestLocationUpdates(this.provider,
                    LocationService.MIN_UPDATE_TIME_SECONDS,
                    LocationService.MIN_UPDATE_DISTANCE_METERS,
                    locationListener
            );
        }
        catch (SecurityException ex) {
            getLocation();
            Log.d(TAG, ex + "");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Once the map is ready, this is called. It is an asynchronous callback, so who knows
     * when this is executed. For this purpose, I have initialized the point on the
     * map to be (0, 0) just in case the value of this.latLng is still null by the time the
     * map is ready.
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            this.getLocation();

            map = googleMap;
            map.setMyLocationEnabled(true);

            if (this.latLng == null) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 15));
            }
            else {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                setLocationListener();
            }
        }
        catch (SecurityException ex) {
            Log.d(TAG, ex + "");
        }
        addNearbyUsers();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    //---------------------------------------------------------------------------------------------

    public ArrayList<User> getUsersList() {
        MainDiscoveryActivity activity = ((MainDiscoveryActivity) getActivity());
        if (activity != null) {
            if (activity.getAllUsersList() != null) {
                return (((MainDiscoveryActivity) getActivity()).getAllUsersList());
            }
            else {
                Log.e(TAG, "No users were found!");
                return new ArrayList<User>();
            }
        }
        else {
            Log.e(TAG, "Activtity was null!");
            return new ArrayList<User>();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void notifyAdapter() {
        //Do nothing
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Refreshes the map and puts all markers back with update values.
     */
    public void addNearbyUsers() {
        class Task implements Runnable {

            Task() {}

            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        map.clear();
                        allUsers = getUsersList();
                        GeoPoint userGeopoint = new GeoPoint(latLng.latitude, latLng.longitude);
                        for (final User user : allUsers) {
                            Float distance = LocationService.getMilesAway(userGeopoint, user.getLocation());
                            MapInfoWindowAdapter infoAdapter = new MapInfoWindowAdapter(getContext());
                            map.setInfoWindowAdapter(infoAdapter);
                            if (distance < LocationService.MAX_VISIBLE_MILES_AWAY) {

                                MarkerOptions options;
                                if (user.isTutor() && user.isAvail()) {
                                    options = new MarkerOptions()
                                            .position(new LatLng(user.getLocation().getLatitude(), user.getLocation().getLongitude()))
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                                }
                                else {
                                    options = new MarkerOptions()
                                            .position(new LatLng(user.getLocation().getLatitude(), user.getLocation().getLongitude()))
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                                }

                                Marker userMarker = map.addMarker(options);
                                userMarker.setTag(user);
                            }
                        }
                    }
                }, 2000);
            }
        }

        Task task = new Task();
        task.run();
    }
    //---------------------------------------------------------------------------------------------

}
