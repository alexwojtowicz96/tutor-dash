package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.PayPalAccount;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.BraintreeFauxServer;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.LocationService;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.core.PaymentService;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.dialog.ForbidSessionDialog;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 * When the current user navigates to another user's profile, there should be an option to
 * "Schedule A Session". After clicking on that option, the current user will be navigated
 * to this activity.
 *
 * Assumptions:
 *  - ActivityUtils.CURRENT_USER_OBJECT is the currently signed in (and authenticated) user.
 *  - ActivityUtils.TMP_USR is the user who's profile is being displayed.
 *  - "Schedule A Session" option is not visible if the current user is viewing their own profile.
 *  - The tutee has an outgoing payment account setup.
 *  - The tutor has an incoming payment account setup.
 *
 * Assuming integrity, here's my idea. They get to this screen. All of the info to schedule a
 * new session with a specified date and time renders. After like 1 or 2 seconds,
 * a popup fragment appears. This asks them if they want to schedule as session
 * ASAP (next available time slot on the tutor's schedule). This ONLY appears if the tutor is
 * currently available though, so if the tutor is not currently available,
 * the tutee does not have the option to send a request ASAP since there is
 * a very small chance that the tutor will accept a request in like 15 minutes if
 * they are currently unavailable.
 */
public class ScheduleSessionActivity extends AppCompatActivity implements BraintreeActivity {

    public static final String TAG = "ScheduleSessionActivity";

    public Button submitBtn;
    public CircleImageView avatarHolder;
    public TextView usernameHolder;
    public FrameLayout beaconHolder;
    public TextView milesAwayHolder;
    public TextView totalHoursHolder;
    public TextView avgRatingHolder;
    public TextView specificRatingHolder;
    public TextView tutorChargingViewHolder;
    public TextView tutorChargingAmountHolder;
    public RatingBar overallRatingBar;
    public RatingBar specificRatingBar;
    public TextView selectPrompt;
    public Spinner yearSpinner;
    public Spinner monthSpinner;
    public Spinner daySpinner;
    public Spinner startTimeSpinner;
    public Spinner endTimeSpinner;
    public FrameLayout fragmentContainer;
    public Spinner deliveryMethodSpinner;
    public BraintreeFragment braintreeFragment;

    public int activeColor;
    public int inactiveColor;
    public int starColor;
    public int colorBlack;
    public Drawable activeBeacon;
    public Drawable inactiveBeacon;

    public final String DEFAULT_YEAR_SPINNER_TEXT = "YEAR";
    public final String DEFAULT_MONTH_SPINNER_TEXT = "MONTH";
    public final String DEFAULT_DAY_SPINNER_TEXT = "DAY";
    public final String DEFAULT_START_TIME_SPINNER_TEXT = " ";
    public final String DEFAULT_END_TIME_SPINNER_TEXT = " ";
    public final String DEFAULT_DELIVERY_METHOD_TEXT = "DELIVERY METHOD";
    public final String TAKEN_TIME = "-";

    public String yearSpinnerText = DEFAULT_YEAR_SPINNER_TEXT;
    public String monthSpinnerText = DEFAULT_MONTH_SPINNER_TEXT;
    public String daySpinnerText = DEFAULT_DAY_SPINNER_TEXT;
    public String startTimeSpinnerText = DEFAULT_START_TIME_SPINNER_TEXT;
    public String endTimeSpinnerText = DEFAULT_END_TIME_SPINNER_TEXT;
    public String deliveryMethodText = DEFAULT_DELIVERY_METHOD_TEXT;

    public boolean isOnConfirmScreen;
    public boolean canSubmit = true;

    public FirebaseFirestore database;
    public StorageReference storageReference;
    public BraintreeFauxServer server;
    public List<Customer> customers;

    public String forbidSessionTitle = "";
    public String forbidSessionReason = "";

    public String paymentDescription = "";
    public String canonicalPaymentName = "";
    public String paymentName = "";
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_session);

        if (!hasIntegrity()) {
            String msg = "Something went wrong. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "Integrity constraints were not met when trying to create activity");
            finish();
        }
        else {
            this.customers = new ArrayList<Customer>();
            this.braintreeFragment = null;
            this.server = new BraintreeFauxServer(this);

            this.isOnConfirmScreen = false;
            this.storageReference = FirebaseStorage.getInstance().getReference();
            this.database = FirebaseFirestore.getInstance();
            ActivityUtils.TMP_USR = ActivityUtils.TMP_USR.initNullValues();
            this.populateSessionsForOtherUser();

            this.starColor = getApplicationContext().getResources().getColor(R.color.pencil);
            this.colorBlack = getApplicationContext().getResources().getColor(R.color.black);
            this.activeColor = getApplicationContext().getResources().getColor(R.color.beacon_green);
            this.inactiveColor = getApplicationContext().getResources().getColor(R.color.beacon_grey);

            DrawableBuilder builder = new DrawableBuilder();
            activeBeacon = builder.oval().height(30).width(30).solidColor(activeColor).build();
            builder = new DrawableBuilder();
            inactiveBeacon = builder.oval().height(30).width(30).solidColor(inactiveColor).build();

            this.initView();
            this.setSubmitRequestListener();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the view. Spinners will be initialized, but keep in mind that the start and
     * end time spinners need to be updated depending on the selected value in the other.
     */
    public void initView() {
        this.submitBtn = findViewById(R.id.submitRequestBtn);
        this.avatarHolder = findViewById(R.id.tutorAvatarImage);
        this.usernameHolder = findViewById(R.id.usernameHolder);
        this.beaconHolder = findViewById(R.id.tutorBeacon);
        this.milesAwayHolder = findViewById(R.id.milesAwayHolder);
        this.totalHoursHolder = findViewById(R.id.courseXpHolder);
        this.avgRatingHolder = findViewById(R.id.averageRatingHolder);
        this.specificRatingHolder = findViewById(R.id.specificRatingHolder);
        this.tutorChargingViewHolder = findViewById(R.id.tutorChargingView);
        this.tutorChargingAmountHolder = findViewById(R.id.tutorChargingAmount);
        this.overallRatingBar = findViewById(R.id.averageRatingBar);
        this.specificRatingBar = findViewById(R.id.specificRatingBar);
        this.selectPrompt = findViewById(R.id.selectDateTimePrompt);
        this.yearSpinner = findViewById(R.id.yearSelectSpinner);
        this.monthSpinner = findViewById(R.id.monthSelectSpinner);
        this.daySpinner = findViewById(R.id.daySelectSpinner);
        this.startTimeSpinner = findViewById(R.id.startTimeSelectSpinner);
        this.endTimeSpinner = findViewById(R.id.endTimeSelectSpinner);
        this.fragmentContainer = findViewById(R.id.confirmRequestFragmentContainer);
        this.deliveryMethodSpinner = findViewById(R.id.deliveryMethodSpinner);

        initDataInViews();
        initYearSpinner();
        initMonthSpinner();
        initDaySpinner("none", "no year");
        initStartTimeSpinner();
        initEndTimeSpinner();
        initDeliveryMethodSpinner();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes all elements in view clickable (default)
     */
    public void setViewsClickable() {
        this.submitBtn.setClickable(true);
        this.avatarHolder.setClickable(true);
        this.usernameHolder.setClickable(true);
        this.beaconHolder.setClickable(true);
        this.milesAwayHolder.setClickable(true);
        this.totalHoursHolder.setClickable(true);
        this.avgRatingHolder.setClickable(true);
        this.specificRatingHolder.setClickable(true);
        this.tutorChargingViewHolder.setClickable(true);
        this.tutorChargingAmountHolder.setClickable(true);
        this.overallRatingBar.setClickable(true);
        this.specificRatingBar.setClickable(true);
        this.selectPrompt.setClickable(true);
        this.yearSpinner.setEnabled(true);
        this.monthSpinner.setEnabled(true);
        this.daySpinner.setEnabled(true);
        this.startTimeSpinner.setEnabled(true);
        this.endTimeSpinner.setEnabled(true);
        this.deliveryMethodSpinner.setEnabled(true);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes all elements in view unclickable
     */
    public void setViewsUnclickable() {
        this.submitBtn.setClickable(false);
        this.avatarHolder.setClickable(false);
        this.usernameHolder.setClickable(false);
        this.beaconHolder.setClickable(false);
        this.milesAwayHolder.setClickable(false);
        this.totalHoursHolder.setClickable(false);
        this.avgRatingHolder.setClickable(false);
        this.specificRatingHolder.setClickable(false);
        this.tutorChargingViewHolder.setClickable(false);
        this.tutorChargingAmountHolder.setClickable(false);
        this.overallRatingBar.setClickable(false);
        this.specificRatingBar.setClickable(false);
        this.selectPrompt.setClickable(false);
        this.yearSpinner.setEnabled(false);
        this.monthSpinner.setEnabled(false);
        this.daySpinner.setEnabled(false);
        this.startTimeSpinner.setEnabled(false);
        this.endTimeSpinner.setEnabled(false);
        this.deliveryMethodSpinner.setEnabled(false);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * The other user in this instance is ActivityUtils.TMP_USR. This method will query
     * Firestore for all sessions that a user has pending. NOTE: These are PENDING sessions!
     * Not all of the other user's sessions will be in ActivityUtils.TMP_USR.sessions!
     *
     * Why? Because all we need to determine if a time slot is unavailable are sessions that
     * can be schedule. Therefore, we only care about the other user's pending sessions.
     */
    public void populateSessionsForOtherUser() {
        if (ActivityUtils.TMP_USR.getSessionsPendingIds().size() > 0) {
            for (String sessionID : ActivityUtils.TMP_USR.getSessionsPendingIds()) {
                database.collection("sessions").document(sessionID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            try {
                                Session session = task.getResult().toObject(Session.class);
                                if (session != null && session.findIsValidSession()) {
                                    ActivityUtils.TMP_USR.putSessionInCollection(session);
                                }
                                else {
                                    throw new Exception("This session was not found to be valid : " + sessionID);
                                }
                            }
                            catch (Exception ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        else {
                            String ex = "";
                            if (task.getException() != null) {
                                ex = task.getException().getMessage();
                            }
                            Log.d(TAG, "Could not get session : " + sessionID + "\n" + ex);
                        }
                    }
                });
            }
        }
        else {
            Log.d(TAG, "No sessions pending for user : " + ActivityUtils.TMP_USR.getuName());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Dynamically populates data such as price, miles away, ratings, etc. These items will
     * not change while the user is currently viewing the activity. (Well, maybe availability
     * will change, but that's it.)
     */
    public void initDataInViews() {
        try {
            // Set uName in view
            usernameHolder.setText(ActivityUtils.TMP_USR.getuName());

            // Setup the rating bars (stars and colors)
            overallRatingBar.setNumStars(5);
            specificRatingBar.setNumStars(5);
            overallRatingBar.setMax(5);
            specificRatingBar.setMax(5);
            overallRatingBar.setIsIndicator(true);
            specificRatingBar.setIsIndicator(true);
            overallRatingBar.setStepSize(new Float(0.1));
            specificRatingBar.setStepSize(new Float(0.1));
            LayerDrawable stars1 = (LayerDrawable) overallRatingBar.getProgressDrawable();
            stars1.getDrawable(2).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            stars1.getDrawable(0).setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
            stars1.getDrawable(1).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            LayerDrawable stars2 = (LayerDrawable) specificRatingBar.getProgressDrawable();
            stars2.getDrawable(2).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            stars2.getDrawable(0).setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
            stars2.getDrawable(1).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);

            // Bind data to rating bar and text views which display rating.
            String overallRating = ActivityUtils.getFormattedFloat(ActivityUtils.TMP_USR.getTutorRating());
            String courseRating = ActivityUtils.getFormattedFloat(ActivityUtils.COURSE_OF_INTEREST.second.getRating().floatValue());
            avgRatingHolder.setText("Overall Rating (" + overallRating + ")");
            specificRatingHolder.setText(ActivityUtils.COURSE_OF_INTEREST.first + " Rating (" + courseRating + ")");
            overallRatingBar.setRating(Float.parseFloat(overallRating));
            specificRatingBar.setRating(Float.parseFloat(courseRating));

            // Set availability beacon
            if (ActivityUtils.TMP_USR.isAvail()) {
                this.beaconHolder.setBackground(activeBeacon);
            }
            else {
                this.beaconHolder.setBackground(inactiveBeacon);
            }

            // Set miles away in view (if available)
            Float milesAway = Float.valueOf(-1);
            if (ActivityUtils.CURRENT_USER_OBJECT.getLocation() != null) {
                milesAway = LocationService.getMilesAway(
                        ActivityUtils.CURRENT_USER_OBJECT.getLocation(),
                        ActivityUtils.TMP_USR.getLocation()
                );
            }
            if (milesAway.equals(Float.valueOf(-1))) {
                milesAwayHolder.setText("Location Unavailable");
            }
            else {
                milesAwayHolder.setText(ActivityUtils.getFormattedFloat(milesAway) + " miles away");
            }

            // Set total XP hours for the course (if available)
            String hours = ActivityUtils.getFormattedFloat(ActivityUtils.COURSE_OF_INTEREST.second.getHours().floatValue());
            totalHoursHolder.setText(ActivityUtils.COURSE_OF_INTEREST.first + " Hours: " + hours);

            // Set the avatar if it exists.
            if (!ActivityUtils.TMP_USR.getPicURL().isEmpty()) {
                GlideApp.with(this).load(storageReference
                        .child(ActivityUtils.TMP_USR.getPicURL())).into(avatarHolder);
            }

            // Set the text in the view above the actual pay rate price.
            String fName = ActivityUtils.TMP_USR.getfName();
            String lName = ActivityUtils.TMP_USR.getlName();
            tutorChargingViewHolder.setText(fName + " " + lName + "'s rate for " + ActivityUtils.COURSE_OF_INTEREST.first);

            // Set the price in the view
            String price = ActivityUtils.getFormattedFloat(ActivityUtils.TMP_USR
                    .getCourses().get(ActivityUtils.COURSE_OF_INTEREST.first)
                    .getPayRate().floatValue());
            tutorChargingAmountHolder.setText("$" + price + " per hour");

            // Set the text in the select prompt
            String text = selectPrompt.getText().toString();
            text = text.replace("%COURSE%", ActivityUtils.COURSE_OF_INTEREST.first);
            text = text.replace("%USER%", ActivityUtils.TMP_USR.getuName());
            selectPrompt.setText(text);
        }
        catch (Exception ex) {
            Log.d(TAG, "Something went wrong when initializing the view");
            String msg = "Failed to initialize view. Killing activity";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initYearSpinner() {
        this.yearSpinnerText = this.DEFAULT_YEAR_SPINNER_TEXT;
        ArrayList<String> years = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance();
        final int thisYear = calendar.get(Calendar.YEAR);
        for (int i = 0; i < 20; i++) {
            years.add(new Integer(thisYear + i).toString());
        }
        years.add(0, DEFAULT_YEAR_SPINNER_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                years
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.yearSpinner.setAdapter(adapter);
        this.yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearSpinnerText = parent.getItemAtPosition(position).toString();
                initDaySpinner(monthSpinnerText, yearSpinnerText);
                initStartTimeSpinner();
                initEndTimeSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void initMonthSpinner() {
        this.monthSpinnerText = this.DEFAULT_MONTH_SPINNER_TEXT;
        ArrayList<String> months = CalendarService.getMonths();
        months.add(0, DEFAULT_MONTH_SPINNER_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                months
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.monthSpinner.setAdapter(adapter);
        this.monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthSpinnerText = parent.getItemAtPosition(position).toString();
                initDaySpinner(monthSpinnerText, yearSpinnerText);
                initStartTimeSpinner();
                initEndTimeSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void initDaySpinner(String month, String year) {
        this.daySpinnerText = this.DEFAULT_DAY_SPINNER_TEXT;
        ArrayList<String> days = new ArrayList<String>();
        if (CalendarService.isMonth(month) && CalendarService.isYear(year)) {
            days = CalendarService.getDays(month, Integer.parseInt(year));
        }
        days.add(0, DEFAULT_DAY_SPINNER_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                days
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.daySpinner.setAdapter(adapter);
        this.daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySpinnerText = parent.getItemAtPosition(position).toString();
                initStartTimeSpinner();
                initEndTimeSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void initStartTimeSpinner() {
        this.startTimeSpinnerText = this.DEFAULT_START_TIME_SPINNER_TEXT;
        User u = ActivityUtils.TMP_USR;
        ArrayList<String> timeSlots = new ArrayList<String>();
        final Date selectedDate = CalendarService.getDateFromStrings(
                yearSpinnerText,
                monthSpinnerText,
                daySpinnerText
        );

        if (selectedDate != null) {
            timeSlots = u.findAvailableTimeSlots(selectedDate);
        }
        timeSlots.add(0, DEFAULT_START_TIME_SPINNER_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                timeSlots
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.startTimeSpinner.setAdapter(adapter);
        this.startTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                startTimeSpinnerText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void initEndTimeSpinner() {
        this.endTimeSpinnerText = this.DEFAULT_END_TIME_SPINNER_TEXT;
        User u = ActivityUtils.TMP_USR;
        ArrayList<String> timeSlots = new ArrayList<String>();
        final Date selectedDate = CalendarService.getDateFromStrings(
                yearSpinnerText,
                monthSpinnerText,
                daySpinnerText
        );

        if (selectedDate != null) {
            timeSlots = u.findAvailableTimeSlots(selectedDate);
        }
        timeSlots.add(0, DEFAULT_END_TIME_SPINNER_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                timeSlots
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.endTimeSpinner.setAdapter(adapter);
        this.endTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                endTimeSpinnerText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Based on this user's delivery options, populate this spinner with what is available.
     */
    public void initDeliveryMethodSpinner() {
        final String online = "Online";
        final String inPerson = "In-Person";
        ArrayList<String> options = new ArrayList<String>();
        options.add(DEFAULT_DELIVERY_METHOD_TEXT);
        if (ActivityUtils.TMP_USR.isOffersInPerson()) {
            options.add(inPerson);
        }
        if (ActivityUtils.TMP_USR.isOffersOnline()) {
            options.add(online);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                options
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.deliveryMethodSpinner.setAdapter(adapter);
        this.deliveryMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deliveryMethodText = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes sure that the assumptions outlined the activity description hold. If they do not
     * hold for some reason, then his activity should be killed immediately.
     *
     * @return TRUE if has integrity, FALSE otherwise (if false, kill the activity)
     */
    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            hasIntegrity = false;
        }
        else if (ActivityUtils.TMP_USR == null || !ActivityUtils.TMP_USR.isValidUser()) {
            hasIntegrity = false;
        }
        else if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.TMP_USR != null) {
            if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(ActivityUtils.TMP_USR.getuName())) {
                hasIntegrity = false;
            }
        }
        else if (ActivityUtils.COURSE_OF_INTEREST == null) {
            hasIntegrity = false;
        }
        else if (!ActivityUtils.TMP_USR.coursesOffered().containsKey(ActivityUtils.COURSE_OF_INTEREST.first)) {
            hasIntegrity = false;
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Code that executes when the submit button is pressed.
     */
    public void setSubmitRequestListener() {
        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deliveryMethodText.equals(DEFAULT_DELIVERY_METHOD_TEXT)) {
                    String msg = "Please specify your delivery method for the session.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else if (yearSpinnerText.equals(DEFAULT_YEAR_SPINNER_TEXT) ||
                        monthSpinnerText.equals(DEFAULT_MONTH_SPINNER_TEXT) ||
                        daySpinnerText.equals(DEFAULT_DAY_SPINNER_TEXT) ||
                        startTimeSpinnerText.equals(DEFAULT_START_TIME_SPINNER_TEXT) ||
                        endTimeSpinnerText.equals(DEFAULT_END_TIME_SPINNER_TEXT) ||
                        startTimeSpinnerText.equals(TAKEN_TIME)
                ) {
                    String msg = "Please select a date for this session request";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else if (startTimeSpinnerText.equals("-") ||
                        endTimeSpinnerText.equals("-") ||
                        !CalendarService.notOverlapping(
                                startTimeSpinnerText,
                                endTimeSpinnerText,
                                yearSpinnerText,
                                monthSpinnerText,
                                daySpinnerText)
                ) {
                    initStartTimeSpinner();
                    initEndTimeSpinner();
                    String msg = "This time conflicts with this tutor's schedule";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    if (!CalendarService.timesHaveIntegrity(startTimeSpinnerText, endTimeSpinnerText)) {
                        String msg = "The end date must be after the start date!";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (!CalendarService.isFutureDate(
                                yearSpinnerText,
                                monthSpinnerText,
                                daySpinnerText,
                                startTimeSpinnerText)
                        ) {
                            String msg = "Please select a date in the future for this session";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if (canSubmit) {
                                if (braintreeFragment != null) {
                                    server.initPayment(getUserAsCustomer(ActivityUtils.CURRENT_USER_OBJECT).getId(), getTotal());
                                }
                                else {
                                    toastOnUiThread("Please wait a second then try again.");
                                }
                            }
                            else {
                                ForbidSessionDialog dialog = new ForbidSessionDialog();
                                dialog.show(getSupportFragmentManager(), "ForbidSessionDialog");
                            }
                        }
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Calculates total amount from views and times.
     * @return Total
     */
    public Float getTotal() {
        try {
            String totalString = PaymentService.getTotalAsString(
                    CalendarService.getTimeInterval(
                            CalendarService.convertTimeToMilitaryTime(startTimeSpinnerText),
                            CalendarService.convertTimeToMilitaryTime(endTimeSpinnerText)
                    ),
                    ActivityUtils.COURSE_OF_INTEREST.second.getPayRate().floatValue()
            );
            return Float.parseFloat(totalString);
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
            return Float.valueOf(0);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sends a request to the tutor in a notification. NOTE: the session is not created nor are
     * the alarm reminders set. This method simply wraps the session data in a notification and
     * sends it to the tutor.
     */
    public void submitRequest() {
        User tutor = ActivityUtils.TMP_USR;
        User tutee = ActivityUtils.CURRENT_USER_OBJECT;
        String courseID = ActivityUtils.COURSE_OF_INTEREST.first;
        Course course = ActivityUtils.COURSE_OF_INTEREST.second;
        course.initNullValues();
        Session session = new Session();

        try {
            if (deliveryMethodText.equals("Online")) {
                session.setOnline(true);
            }
            session.setTutorUID(tutor.getuName());
            session.setTuteeUID(tutee.getuName());
            session.putInvolvedUsers();
            session.setStart(new Timestamp(CalendarService.getDateTimeFromStrings(
                    yearSpinnerText,
                    monthSpinnerText,
                    daySpinnerText,
                    startTimeSpinnerText))
            );
            session.setEnd(new Timestamp(CalendarService.getDateTimeFromStrings(
                    yearSpinnerText,
                    monthSpinnerText,
                    daySpinnerText,
                    endTimeSpinnerText))
            );
            session.setExpectedMinutes(CalendarService.getTimeInterval(
                    CalendarService.convertTimeToMilitaryTime(startTimeSpinnerText),
                    CalendarService.convertTimeToMilitaryTime(endTimeSpinnerText))
            );
            session.setHourlyPayRate(course.getPayRate().floatValue());
            session.setExpectedAmountOwed(PaymentService.getTotal(
                    session.getExpectedMinutes(),
                    session.getHourlyPayRate())
            );
            session.setCourseID(courseID);
            session.setTuteePicUrl(tutee.getPicURL());
            session.setTutorPicUrl(tutor.getPicURL());
            session.setIsPending(true);
            session.setWasAccepted(false);
            session.setTutorHasPaid(false);
            session.setPaymentTags(new HashMap<String, String>() {{
                put("NAME", paymentName);
                put("CANONICAL_NAME", canonicalPaymentName);
                put("DESCRIPTION", paymentDescription);
            }});
            session.initNullValues();

            // Get the tutor's current list of requests and add this request to that array.
            ArrayList<String> requestIds;
            if (tutor.getRequestIds() == null) {
                requestIds = new ArrayList<String>();
            }
            else {
                requestIds = (ArrayList<String>) tutor.getRequestIds();
            }
            requestIds.add(session.getSessionID());
            database.collection("users").document(tutor.getuName()).update("requestIds", requestIds)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                // Now put the session in the database
                                database.collection("sessions").document(session.getSessionID())
                                        .set(session).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    ActivityUtils.TMP_USR.setRequestIds(requestIds);

                                                    Gson gson = new Gson();
                                                    String sessionJson = gson.toJson(session);
                                                    Pair<String, String> pair = new Pair<String, String>("sessionRequest", sessionJson);
                                                    List<Pair<String, String>> sessionData = new ArrayList<Pair<String, String>>();
                                                    sessionData.add(pair);

                                                    final String title = "New session request for " + courseID + "!";
                                                    final String msg = tutee.getuName() + " has requested a session from you!";
                                                    final String topic = "/topics/" + MessageService.DM_TOPIC + tutor.getuName();
                                                    NotificationService notificationService = new NotificationService(getApplicationContext());
                                                    notificationService.setTopic(topic);
                                                    notificationService.setHighPriority(true);
                                                    notificationService.setTitle(title);
                                                    notificationService.setMessage(msg);

                                                    notificationService.sendNotification(sessionData);

                                                    finish();
                                                }
                                                else {
                                                    String msg = "Something went wrong. Could not send request to " + tutor.getuName();
                                                    Log.d(TAG, "Task failed. Could create session document : " + session.getSessionID());
                                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            }
                            else {
                                String msg = "Something went wrong. Could not send request to " + tutor.getuName();
                                Log.d(TAG, "Task failed. Could not update requestIds for : " + tutor.getuName());
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        }
        catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Failed to submit request", Toast.LENGTH_LONG).show();
            Log.d(TAG, ex + "");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * The current user is the tutee since they are the one trying to schedule the session.
     * This means that the other user is the tutor. We care about the tutee first, then the tutor.
     *
     * @param customer
     * @param onlyIncludePaypal
     * @return TRUE if the Braintree customer has payment methods.
     */
    public boolean hasPayments(Customer customer, boolean onlyIncludePaypal) {
        boolean hasPayments = true;
        if (customer == null) {
            return false;
        }
        else {
            final List<PayPalAccount> payPalAccounts = customer.getPayPalAccounts();
            final List<CreditCard> creditCards = customer.getCreditCards();

            if (onlyIncludePaypal) {
                if (payPalAccounts != null && payPalAccounts.size() > 0) {
                    hasPayments = true;
                }
                else {
                    hasPayments = false;
                }
            }
            else {
                if (payPalAccounts != null && payPalAccounts.size() > 0) {
                    hasPayments = true;
                }
                else {
                    if (creditCards != null && creditCards.size() > 0) {
                        hasPayments = true;
                    }
                    else {
                        hasPayments = false;
                    }
                }
            }
        }

        return hasPayments;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        if (isOnConfirmScreen) {
            setViewsClickable();
            isOnConfirmScreen = false;
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.confirmRequestFragmentContainer,
                    new EmptyFragment()
            ).commit();
        }
        else {
            super.onBackPressed();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the main thread.
     * @param msg
     */
    public void toastOnUiThread(String msg) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the user as a customer if they exist in this.customers
     * @param user
     * @return customer
     */
    public Customer getUserAsCustomer(User user) {
        Customer customer = null;
        if (user != null && user.isValidUser()) {
            if (this.customers != null && this.customers.size() > 0) {
                for (Customer c : this.customers) {
                    if (c != null && c.getId().equals(user.getuName())) {
                        customer = c;
                        break;
                    }
                }
            }
        }

        return customer;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This result will be used to execute the transaction after the user has specified the
     * amount they wish to pay and the payment method (Paypal OR credit card).
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Context context = this.getApplicationContext();

        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                try {
                    if (result != null) {
                        // For the payment method tags on the session.
                        this.paymentDescription = result.getPaymentMethodNonce().getDescription();
                        this.canonicalPaymentName = result.getPaymentMethodType().getCanonicalName();
                        this.paymentName = result.getPaymentMethodType().name();

                        setViewsUnclickable();
                        isOnConfirmScreen = true;
                        getSupportFragmentManager().beginTransaction().replace(
                                R.id.confirmRequestFragmentContainer,
                                new ConfirmSessionRequestFragment()
                        ).commit();
                    }
                    else {
                        Toast.makeText(context, "Failed to submit payment", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception ex) {
                    Log.d(TAG, ex.getLocalizedMessage());
                    Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(context, "Result = CANCELLED", Toast.LENGTH_LONG).show();
            }
            else {
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.d(TAG, error.getLocalizedMessage());
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * BRAINTREE CALLBACK METHODS
     * ============================================================================================
     */
    @Override
    public void onServerReady(boolean isReady) {
        if (isReady && this.server.isReady()) {
            try {
                final String tokenizationKey = server.getDataModel().getTokenizationKey();
                braintreeFragment = BraintreeFragment.newInstance(this, tokenizationKey);
            }
            catch (Exception ex) {
                Log.d(TAG, "Failed to initialize the Braintree fragment");
            }
            this.server.findCustomer(ActivityUtils.CURRENT_USER_OBJECT.getuName());
            this.server.findCustomer(ActivityUtils.TMP_USR.getuName());
        }
        else {
            Log.d(TAG, "Server was not ready when it was supposed to be ready!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called in the activity that implements this class. It is intended that the
     * server will do something, and once it is finished, it will call this method in the
     * activity that called the server method.
     *
     * @param amount - Amount to submit
     * @param clientToken - Token generated on the faux "server" that configures the transaction
     */
    public void submitPayment(Float amount, String clientToken) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken);
        Intent intent = dropInRequest.getIntent(this);
        startActivityForResult(intent, PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT);

        toastOnUiThread("Please select your desired payment method");
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the customer object is found.
     *
     * @param customer - The customer object retrieved from Braintree
     */
    public void onCustomerFound(Customer customer, boolean wasFound) {
        this.customers.add(customer);
        if (customer != null && wasFound) {
            Log.d(TAG, "Found customer : " + customer.getId());
        }
        else {
            Log.d(TAG, "Failed to find this customer!");
        }

        if (this.customers.size() == 2) {
            // Both customer callback methods have finished executing
            final String cUsername = ActivityUtils.CURRENT_USER_OBJECT.getuName();
            final String oUsername = ActivityUtils.TMP_USR.getuName();
            boolean tuteeIsCustomer = true;
            boolean tutorIsCustomer = true;
            Customer tuteeCustomer = null;
            Customer tutorCustomer = null;

            if (this.customers.get(0) == null && this.customers.get(1) == null) {
                // Neither user is a customer, but the fact that the current user is not
                // a customer takes priority.
                tuteeIsCustomer = false;
                tutorIsCustomer = false;
            }
            else if (this.customers.get(0) == null && this.customers.get(1) != null) {
                // One user is a customer. Who is it?
                if (this.customers.get(1).getId().equals(cUsername)) {
                    tuteeIsCustomer = true;
                    tutorIsCustomer = false;
                    tuteeCustomer = this.customers.get(1);
                    tutorCustomer = this.customers.get(0);
                }
                else {
                    tuteeIsCustomer = false;
                    tutorIsCustomer = true;
                    tuteeCustomer = this.customers.get(0);
                    tutorCustomer = this.customers.get(1);
                }
            }
            else if (this.customers.get(0) != null && this.customers.get(1) == null) {
                // One user is a customer. Who is it?
                if (this.customers.get(0).getId().equals(cUsername)) {
                    tuteeIsCustomer = true;
                    tutorIsCustomer = false;
                    tuteeCustomer = this.customers.get(0);
                    tutorCustomer = this.customers.get(1);
                }
                else {
                    tuteeIsCustomer = false;
                    tutorIsCustomer = true;
                    tuteeCustomer = this.customers.get(1);
                    tutorCustomer = this.customers.get(0);
                }
            }
            else {
                Log.d(TAG, "Both users are customers");
                tuteeIsCustomer = true;
                tutorIsCustomer = true;
                if (this.customers.get(0).getId().equals(cUsername)) {
                    tuteeCustomer = this.customers.get(0);
                    tutorCustomer = this.customers.get(1);
                }
                else {
                    tuteeCustomer = this.customers.get(1);
                    tutorCustomer = this.customers.get(0);
                }
            }

            boolean renderDialog = false;
            if (!tuteeIsCustomer || !this.hasPayments(tuteeCustomer, false)) {
                toastOnUiThread("You don't have any payment methods set up!");
                renderDialog = true;
                this.forbidSessionTitle = "Session Request Forbidden";
                this.forbidSessionReason = "You currently do not have any payment methods set up. " +
                        "If you would like to schedule a session with " + ActivityUtils.TMP_USR.getuName() +
                        ", then you must supply a PayPal or credit card to your account.";
            }
            else if (!tutorIsCustomer || !this.hasPayments(tutorCustomer, true)) {
                toastOnUiThread("This tutor doesn't have a PayPal set up!");
                renderDialog = true;
                this.forbidSessionTitle = "Session Request Forbidden";
                this.forbidSessionReason = "User " + ActivityUtils.TMP_USR.getuName() + " does not " +
                        "currently have a PayPal account setup, so they cannot accept tutees. We " +
                        "recommend sending " + ActivityUtils.TMP_USR.getuName() + " a message so that " +
                        "they know tutees are interested!";
            }

            if (renderDialog) {
                canSubmit = false;
                ForbidSessionDialog dialog = new ForbidSessionDialog();
                dialog.show(getSupportFragmentManager(), "ForbidSessionDialog");
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a customer is created in Braintree.
     *
     * @param customer
     * @param wasCreated
     */
    public void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a submitted transaction is completed.
     *
     * @param transaction
     * @param wasSuccessful
     */
    public void onTransactionComplete(BraintreeTransaction transaction, boolean wasSuccessful) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a submitted void for a transaction is completed.
     *
     * @param transactionID
     * @param wasVoided
     */
    public void onTransactionVoided(String transactionID, boolean wasVoided) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a transaction is found (from its ID).
     *
     * @param transaction
     * @param wasFound
     */
    public void onTransactionFound(Transaction transaction, boolean wasFound) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a payment method nonce is created.
     *
     * @param nonce
     * @param wasCreated
     */
    @Override
    public void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated) {

    }
    //---------------------------------------------------------------------------------------------
}
