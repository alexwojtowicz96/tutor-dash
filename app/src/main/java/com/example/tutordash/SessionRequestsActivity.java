package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import com.example.tutordash.adapter.SessionRequestsAdapter;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity displays all session requests that a user has yet to respond to.
 * When they click on a request, they will be redirected to the AcceptRequestActivity
 */
public class SessionRequestsActivity extends AppCompatActivity {

    public static final String TAG = "SessionRequestsActivity";

    Toolbar toolbar;
    RecyclerView recyclerView;
    FrameLayout noNotificationsWindow;

    FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_requests);

        if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            try {
                this.database = FirebaseFirestore.getInstance();

                this.toolbar = findViewById(R.id.sessionRequestsToolbar);
                this.recyclerView = findViewById(R.id.sessionRequestsRecyclerView);
                this.noNotificationsWindow = findViewById(R.id.noNotificationsWindow);
                initView();
            }
            catch (Exception ex) {
                String msg = "Something went wrong. Please try again later.";
                Log.d(TAG, ex.getMessage());
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                finish();
            }
        }
        else {
            String msg = "Something went wrong. Please restart the applicaion.";
            Log.d(TAG, "ActivityUtils.CURRENT_USER_OBJECT was not valid!");
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Performs logic to determine what renders in view.
     */
    public void initView() {
        User user = ActivityUtils.CURRENT_USER_OBJECT;

        toolbar.setTitle("Session Requests");
        if (user.getRequestIds() == null || user.getRequestIds().size() == 0) {
            recyclerView.setVisibility(View.INVISIBLE);
            noNotificationsWindow.setVisibility(View.VISIBLE);
        }
        else {
            recyclerView.setVisibility(View.VISIBLE);
            noNotificationsWindow.setVisibility(View.GONE);

            final int size = user.getRequestIds().size();
            ArrayList<Session> requestedSessions = new ArrayList<Session>();
            for (String sessionId : user.getRequestIds()) {
                database.collection("sessions").document(sessionId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            try {
                                Session session = task.getResult().toObject(Session.class);
                                requestedSessions.add(session);
                                if (requestedSessions.size() == size) {
                                    initRecyclerView(requestedSessions);
                                }
                            }
                            catch (Exception ex) {
                                Log.d(TAG, ex.getMessage());
                                Session invalidSession = new Session();
                                invalidSession.setSessionID(null);
                                requestedSessions.add(invalidSession);
                            }
                        }
                        else {
                            Log.d(TAG, "Could not retrieve session document : " + sessionId);
                            Session invalidSession = new Session();
                            invalidSession.setSessionID(null);
                            requestedSessions.add(invalidSession);
                        }
                    }
                });
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the recycler view from the list of sessions.
     *
     * @param sessions
     */
    public void initRecyclerView(List<Session> sessions) {
        if (sessions != null) {
            // Prune invalid sessions
            List<Session> requestedSessions = new ArrayList<Session>();
            for (Session s : sessions) {
                if (s.findIsValidSession()) {
                    requestedSessions.add(s);
                }
            }

            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            SessionRequestsAdapter adapter = new SessionRequestsAdapter(
                    getApplicationContext(),
                    (ArrayList<Session>) sessions
            );
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(manager);
        }
        else {
            Log.d(TAG, "sessions was null?");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
        initView();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //---------------------------------------------------------------------------------------------
}
