package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * The user will select "Send an Alert" and be navigated to this activity.
 * Based on their selection will determine where they go next.
 */
public class IAmLookingForActivity extends AppCompatActivity {

    public static final String TAG = "IAmLookingForActivity";

    public Button alertTutorsBtn;
    public Button alertTuteesBtn;
    public Button cancelBtn;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iam_looking_for);

        alertTuteesBtn = findViewById(R.id.seekingTuteeBtn);
        alertTutorsBtn = findViewById(R.id.seekingTutorBtn);
        cancelBtn = findViewById(R.id.seekingNobodyBtn);

        alertTutorsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AlertTutorActivity.class));
            }
        });

        alertTuteesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AlertTuteeActivity.class));
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
