package com.example.tutordash.base;

/**
 * This exists as a workaround for the Glide error (ClassCastException when calling
 * methods in fragments that live in the Main Discovery Activity).
 */
public interface MainDiscoveryFragment {

    /**
     * Use this to call TextFragment.notifyAdapter()
     */
    void notifyAdapter();

    /**
     * Use this to call MapFragment.addNearbyUsers()
     */
    void addNearbyUsers();
}
//-------------------------------------------------------------------------------------------------
