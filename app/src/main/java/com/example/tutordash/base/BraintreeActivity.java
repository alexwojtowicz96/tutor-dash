package com.example.tutordash.base;

import com.braintreegateway.Customer;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;

/**
 * Use this interface for classes that implement the Braintree components. The faux "server"
 * object that is needed to use the Braintree components inside of the activity needs these
 * methods to function.
 */
public interface BraintreeActivity {

    /**
     * Gets called when the faux "server" object is initialized.
     *
     * @param isReady
     */
    void onServerReady(boolean isReady);

    /**
     * Gets called in the activity that implements this class. It is intended that the
     * server will do something, and once it is finished, it will call this method in the
     * activity that called the server method.
     *
     * @param amount - Amount to submit
     * @param clientToken - Token generated on the faux "server" that configures the transaction
     */
    void submitPayment(Float amount, String clientToken);

    /**
     * Gets called when the customer object is found.
     *
     * @param customer - The customer object retrieved from Braintree
     */
    void onCustomerFound(Customer customer, boolean wasFound);

    /**
     * Gets called when a customer is created in Braintree.
     *
     * @param customer
     * @param wasCreated
     */
    void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated);

    /**
     * Gets called when a submitted transaction is completed.
     *
     * @param transaction
     * @param wasSuccessful
     */
    void onTransactionComplete(BraintreeTransaction transaction, boolean wasSuccessful);

    /**
     * Gets called when a submitted void for a transaction is completed.
     *
     * @param transactionID
     * @param wasVoided
     */
    void onTransactionVoided(String transactionID, boolean wasVoided);

    /**
     * Gets called when a transaction is found (from its ID).
     *
     * @param transaction
     * @param wasFound
     */
    void onTransactionFound(Transaction transaction, boolean wasFound);

    /**
     * Gets called when a payment method nonce is created.
     *
     * @param nonce
     * @param wasCreated
     */
    void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated);
}
//-------------------------------------------------------------------------------------------------
