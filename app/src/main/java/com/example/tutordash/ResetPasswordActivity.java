package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.core.FormatValidationUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * FYI, passwords are never "reset" in Auth. We are handling this by creating a new account
 * with a random email in auth. That random email is stored in the user's record as "authEmail"
 * and is subsequently updated to another random email when they "reset" their password.
 */
public class ResetPasswordActivity extends AppCompatActivity {

    public static final String TAG = "ResetPasswordActivity";

    Button submit;
    TextView welcome;
    EditText password1;
    EditText password2;

    FirebaseAuth auth;
    FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        auth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        submit = findViewById(R.id.resetPwdButton);
        welcome = findViewById(R.id.welcomeResetView);
        password1 = findViewById(R.id.password1EditText);
        password2 = findViewById(R.id.password2EditText);

        String promptMsg = "Hi " + ActivityUtils.TMP_USR.getfName() + ", " +
                "Please enter your new password below:";
        welcome.setText(promptMsg);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityUtils.TMP_USR != null) {
                    String p1 = password1.getText().toString().trim();
                    String p2 = password2.getText().toString().trim();

                    if (p1.isEmpty() || p2.isEmpty()) {
                        String msg = "Please fill in both password fields";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        if (p1.isEmpty()) {
                            password1.setError("Required Field");
                        }
                        if (p2.isEmpty()) {
                            password1.setError("Required Field");
                        }
                    }
                    else {
                        if (!p1.equals(p2)) {
                            String msg = "Passwords do not match";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            password1.setError("Passwords do not match");
                            password2.setError("Passwords do not match");
                        }
                        else {
                            if (!FormatValidationUtils.passwordIsValidFormat(
                                    p1,
                                    ActivityUtils.TMP_USR.getuName(),
                                    ActivityUtils.TMP_USR.getEmail())) {
                                String msg = "Invalid password format";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                password1.setError("Invalid Password");
                                password2.setError("Invalid Password");
                            }
                            else {
                                createNewAccount(p1);
                            }
                        }
                    }
                }
                else {
                    // Why is TMP_USR null? This shouldn't be...
                    Log.d(TAG, "TMP_USR was null!?");
                    Toast.makeText(getApplicationContext(), "INTERNAL FAILURE", Toast.LENGTH_LONG).show();
                    finish();
                    startActivity(ActivityUtils.makeIntent(getApplicationContext(), LoginActivity.class));
                }
            }
        });
    }
    //-------------------------------------------------------------------------------------------

    /**
     *  When users reset their passwords, a new account with a different auth email will
     *  be created. Subsequently, their document record in Firestore will be updated with
     *  this information so they can log in with this new auth email. The old auth account
     *  will still exist! This is a workaround since we can't update passwords for users
     *  who are not currently signed in.
     */
    public void createNewAccount(String password) {
        // My assumption is that the RNG will be good enough to give us a unique email,
        // but be aware, this has potential to fail!
        final String newEmail = FileUtils.generateRandomEmail();
        auth.createUserWithEmailAndPassword(newEmail, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    DocumentReference ref = database.collection("users").document(ActivityUtils.TMP_USR.getuName());
                    ref.update("authEmail", newEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                String msg = "Reset Successful!";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                finish();
                                startActivity(ActivityUtils.makeIntent(getApplicationContext(), LoginActivity.class));
                            }
                            else {
                                if (auth.getCurrentUser() != null) {
                                    auth.getCurrentUser().delete();
                                }
                                Log.d(TAG, "Could not update authEmail for user : " + ActivityUtils.TMP_USR.getuName());
                                String msg = "An unexpected error occurred. Please try again later.";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                else {
                    Log.d(TAG, "Could not create new account in auth for email : " + newEmail);
                    String msg = "An error occurred. Please try again in a moment.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    //-------------------------------------------------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityUtils.TMP_USR = null;
        ActivityUtils.PASSWORD_RESET_TOKEN = null;
    }
    //-------------------------------------------------------------------------------------------
}
