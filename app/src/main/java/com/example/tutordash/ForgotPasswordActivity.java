package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.EmailService;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.core.FormatValidationUtils;
import com.example.tutordash.model.Token;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;

public class ForgotPasswordActivity extends AppCompatActivity {

    static final String TAG = "ForgotPasswordActivity";
    LinearLayout loadingLayout;
    TextView notFoundView;
    Button submitBtn;
    EditText credentialForm;

    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        loadingLayout = findViewById(R.id.loadingLayout);
        notFoundView = findViewById(R.id.notFoundView);
        submitBtn = findViewById(R.id.submitBtn);
        credentialForm = findViewById(R.id.credentialForm);

        loadingLayout.setVisibility(LinearLayout.GONE);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                credentialForm.onEditorAction(EditorInfo.IME_ACTION_DONE);
                submitBtn.setClickable(false);
                final String userInput = credentialForm.getText().toString().trim();
                String msg;

                if (userInput.equals("")) {
                    msg = "Please Enter Your Username or Email";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    submitBtn.setClickable(true);
                }
                else {
                    if (FormatValidationUtils.usernameIsValidFormat(userInput) ||
                            FormatValidationUtils.emailIsValidFormat(userInput)) {
                        loadingLayout.setVisibility(LinearLayout.VISIBLE);
                        Handler handler = new Handler();
                        Runnable runnable = new Runnable() {
                            public void run() {
                                attemptPasswordReset(userInput);
                            }
                        };
                        handler.postDelayed(runnable, 2000);
                    }
                    else {
                        loadingLayout.setVisibility(LinearLayout.GONE);
                        msg = "Please Enter A Valid Username or Email Address";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        submitBtn.setClickable(true);
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to find a user with the supplied userInput. If it finds one, it attempts to
     * send a password reset email.
     *
     * @param userInput
     */
    public void attemptPasswordReset(final String userInput) {
        String found = "This user exists!";
        String failure = "An unexpected error occurred. Please try again later.";
        if (FormatValidationUtils.usernameIsValidFormat(userInput)) {
            String notFound = "A user with username '" + userInput + "' could not be found";
            DocumentReference docRef = database.collection("users").document(userInput);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    loadingLayout.setVisibility(View.INVISIBLE);
                    submitBtn.setClickable(true);
                    if (documentSnapshot != null && documentSnapshot.get("uName") != null) {
                        User user = documentSnapshot.toObject(User.class);
                        if (user != null && user.isValidUser()) {
                            generateTokenAndSendEmail(user);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), failure, Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), notFound, Toast.LENGTH_LONG).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    loadingLayout.setVisibility(View.INVISIBLE);
                    submitBtn.setClickable(true);
                    Toast.makeText(getApplicationContext(), failure, Toast.LENGTH_LONG).show();
                }
            });
        }
        else if (FormatValidationUtils.emailIsValidFormat(userInput)) {
            Query query = database.collection("users").whereEqualTo("email", userInput);
            String notFound = "A user with email '" + userInput + "' could not be found";
            query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                @Override
                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                    loadingLayout.setVisibility(View.INVISIBLE);
                    submitBtn.setClickable(true);
                    String msg;
                    if (queryDocumentSnapshots != null) {
                        if (queryDocumentSnapshots.size() == 0) {
                            msg = notFound;
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                        else if (queryDocumentSnapshots.size() > 1) {
                            // Honestly, this should NEVER happen
                            msg = "Multiple documents with email '" + userInput + "' were found!";
                            StringBuilder b = new StringBuilder();
                            for (DocumentSnapshot doc : queryDocumentSnapshots) {
                                b.append("DOC_ID: " + doc.getId() + "\n");
                            }
                            Toast.makeText(getApplicationContext(), msg + "\n" + b.toString(), Toast.LENGTH_LONG).show();
                        }
                        else {
                            // This is what we expect to get...
                            int i = 0;
                            for (DocumentSnapshot doc : queryDocumentSnapshots) {
                                User user = doc.toObject(User.class);
                                if (user != null && user.isValidUser()) {
                                    generateTokenAndSendEmail(user);
                                }
                                i += 1;
                                if (i == 1) {
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), notFound, Toast.LENGTH_LONG).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    loadingLayout.setVisibility(View.INVISIBLE);
                    submitBtn.setClickable(true);
                    Toast.makeText(getApplicationContext(), failure, Toast.LENGTH_LONG).show();
                }
            });
        }
        else {
            // This should never happen...
            String msg = "Something went wrong. Username and email passed validity checks, " +
                    "but still remain invalid when trying to reset password.";
            Log.d(TAG, msg + " => " + userInput);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to send the password reset email. On success, send user back to login.
     *
     * @param email
     */
    public void attemptSendEmail(String email) {
        if (firebaseAuth != null) {
            firebaseAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    String success = "Email Sent! Please check your inbox.";
                    Toast.makeText(getApplicationContext(), success, Toast.LENGTH_LONG).show();
                    startActivity(ActivityUtils.makeIntent(getApplicationContext(), LoginActivity.class));

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    String failure = "An error occurred preventing email to be sent. Please try again later.";
                    Toast.makeText(getApplicationContext(), failure, Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @param user
     */
    public void generateTokenAndSendEmail(User user) {
        final String key = FileUtils.generateToken(8);
        final Date now = new Date();
        final int timeout = 5;
        Token token = new Token(key, new Timestamp(now), timeout);

        String school = user.getSchool();
        String subject = "Tutor Dash Password Reset";
        String message = EmailService.passwordResetTemplate(token, user);

        EmailService emailService = new EmailService(getApplicationContext(), school, subject, message);
        emailService.sendEmail(user.getEmail(), true);

        ActivityUtils.PASSWORD_RESET_TOKEN = token;
        ActivityUtils.TMP_USR = user;
        //ActivityUtils.TMP_CONTEXT = getApplicationContext();
    }
    //---------------------------------------------------------------------------------------------
}
