package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.tutordash.adapter.SessionItemAdapter;
import com.example.tutordash.adapter.SessionRequestsAdapter;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.Session;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

/**
 * In the account settings, when a user clicks on "Pending Sessions", "Previous Sessions", or
 * "Cancelled Sessions", they will be redirected here.
 */
public class ShowSessionsActivity extends AppCompatActivity {

    public static final String TAG = "ShowSessionsActivity";

    public Toolbar toolbar;
    public RecyclerView recyclerView;
    public LinearLayout notFoundLayout;
    public ImageButton refreshBtn;

    public int mode;
    public FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_sessions);

        if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            this.mode = ActivityUtils.SESSIONS_VIEW_MODE;
            final int m1 = ActivityUtils.PENDING_SESSIONS_ID;
            final int m2 = ActivityUtils.PREVIOUS_SESSIONS_ID;
            final int m3 = ActivityUtils.CANCELLED_SESSIONS_ID;

            if (mode == m1 || mode == m2 || mode == m3) {
                this.database = FirebaseFirestore.getInstance();

                this.toolbar = findViewById(R.id.showSessionsToolbar);
                this.recyclerView = findViewById(R.id.showSessionsRecyclerView);
                this.notFoundLayout = findViewById(R.id.noSessionsFoundLayout);
                this.refreshBtn = findViewById(R.id.refreshSessionsBtn);
                initView(false);

                refreshBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        initView(true);
                    }
                });
            }
            else {
                String msg1 = "Something went wrong. Please try again later";
                String msg2 = "View mode was undefined : " + mode;
                showErrorAndExit(msg1, msg2);
            }
        }
        else {
            String msg1 = "Something went wrong. Please restart the application";
            String msg2 = "The current user object was not valid!";
            showErrorAndExit(msg1, msg2);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Using this.mode (pending/previous/cancelled), render the sessions in a recycler view
     * if they exist.
     */
    public void initView(boolean wasRefreshed) {
        String toolbarText = "";
        List<String> sessionIds = null;

        if (this.mode == ActivityUtils.PENDING_SESSIONS_ID) {
            toolbarText = "Pending Sessions";
            sessionIds = ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
        }
        else if (this.mode == ActivityUtils.PREVIOUS_SESSIONS_ID) {
            toolbarText = "Previous Sessions";
            sessionIds = ActivityUtils.CURRENT_USER_OBJECT.getSessionsFinishedIds();

        }
        else if (this.mode == ActivityUtils.CANCELLED_SESSIONS_ID) {
            toolbarText = "Cancelled Sessions";
            sessionIds = ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds();
        }
        else {
            String msg1 = "Something went wrong. Please try again later.";
            String msg2 = "Mode wasn't defined? => " + this.mode;
            showErrorAndExit(msg1, msg2);
        }

        if (sessionIds == null) {
            sessionIds = new ArrayList<String>();
        }
        this.toolbar.setTitle(toolbarText);

        if (sessionIds.size() == 0) {
            recyclerView.setVisibility(View.INVISIBLE);
            notFoundLayout.setVisibility(View.VISIBLE);
            if (wasRefreshed) {
                Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            recyclerView.setVisibility(View.VISIBLE);
            notFoundLayout.setVisibility(View.GONE);

            List<Session> sessionsToRender = new ArrayList<Session>();
            final int size = sessionIds.size();
            for (String id : sessionIds) {
                database.collection("sessions").document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            try {
                                Session s = task.getResult().toObject(Session.class);
                                sessionsToRender.add(s);
                                if (sessionsToRender.size() == size) {
                                    if (wasRefreshed) {
                                        Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();
                                    }
                                    renderSessions(sessionsToRender);
                                }
                            }
                            catch (Exception ex) {
                                Log.d(TAG, ex.getMessage());
                                Session invalidSession = new Session();
                                invalidSession.setSessionID(null);
                                sessionsToRender.add(invalidSession);
                            }
                        }
                        else {
                            Log.d(TAG, "Could not retrieve session document : " + id);
                            Session invalidSession = new Session();
                            invalidSession.setSessionID(null);
                            sessionsToRender.add(invalidSession);
                        }
                    }
                });
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void renderSessions(List<Session> sessionsToRender) {
        if (sessionsToRender != null && sessionsToRender.size() > 0) {
            ArrayList<Session> sessions = new ArrayList<Session>();
            for (Session s : sessionsToRender) {
                if (s.findIsValidSession()) {
                    sessions.add(s);
                }
            }

            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            SessionItemAdapter adapter = new SessionItemAdapter(
                    getApplicationContext(),
                    sessions,
                    this.mode
            );
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(manager);
        }
    }
    //---------------------------------------------------------------------------------------------

    public void showErrorAndExit(String userErrorMsg, String logErrorMsg) {
        final Context context = getApplicationContext();
        Log.d(TAG, logErrorMsg);
        Toast.makeText(context, userErrorMsg, Toast.LENGTH_LONG).show();
        finish();
    }
    //---------------------------------------------------------------------------------------------
}
