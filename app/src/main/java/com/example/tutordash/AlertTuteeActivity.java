package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.dialog.SendAlertDialog;
import com.example.tutordash.model.Course;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 * The user uses this activity to send alerts to tutees (they themselves are taking on
 * the role of a tutor).
 */
public class AlertTuteeActivity extends AppCompatActivity {

    public static final String TAG = "AlertTuteeActivity";

    Button submitAlertBtn;
    Spinner coursesSpinner;

    final String defaultSpinnerText = "PLEASE SELECT A COURSE";
    String spinnerText = defaultSpinnerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_tutee);

        this.submitAlertBtn = findViewById(R.id.submitAlert1);
        this.coursesSpinner = findViewById(R.id.spinnerSelect1);

        if (ActivityUtils.CURRENT_USER_OBJECT.coursesOffered() == null ||
                ActivityUtils.CURRENT_USER_OBJECT.coursesOffered().size() == 0) {
            String msg = "You do not currently offer any courses. You must become a tutor first!";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
        else {
            setupSpinner();

            this.submitAlertBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spinnerText.equals(defaultSpinnerText)) {
                        String msg = "Please select a course from your offered courses";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        SendAlertDialog dialog = new SendAlertDialog();
                        dialog.show(getSupportFragmentManager(), "SendAlertDialog");
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    public void setupSpinner() {
        Map<String, Course> courses = ActivityUtils.CURRENT_USER_OBJECT.coursesOffered();
        if (courses != null) {
            ArrayList<String> courseIds = new ArrayList<String>();
            for (String c : courses.keySet()) {
                courseIds.add(c);
            }
            Collections.sort(courseIds);
            ArrayList<String> spinnerOptions = new ArrayList<String>(courseIds);
            spinnerOptions.add(0, defaultSpinnerText);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    getApplicationContext(),
                    android.R.layout.simple_spinner_item,
                    spinnerOptions
            );
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            coursesSpinner.setAdapter(adapter);
            coursesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    spinnerText = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // ?
                }
            });
        }
        else {
            String msg = "An error occurred. Please try again later.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "The list of courses was null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sends alerts to all users who need help with the course ID held in spinnerText
     *
     * TODO
     * Users could just keep spamming alerts. Find a way to limit how many they can push.
     */
    public void alertTutees() {
        NotificationService notificationService = new NotificationService(getApplicationContext());

        notificationService.setTitle(ActivityUtils.CURRENT_USERNAME + " is looking for tutees in " + spinnerText);
        notificationService.setMessage("Need help with " + spinnerText + "? " + ActivityUtils.CURRENT_USERNAME + " is available!");
        notificationService.setTopic("/topics/" + MessageService.getTopic(
                spinnerText,
                ActivityUtils.CURRENT_USER_SCHOOL,
                true)
        );
        notificationService.setHighPriority(false);

        notificationService.sendNotification(null);
    }
    //---------------------------------------------------------------------------------------------
}
