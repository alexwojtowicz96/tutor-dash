package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.AlarmService;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.dialog.CancelSessionDialog;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * When the user clicks on a session in the ShowSessionsActivity, they end up here.
 */
public class SessionItemActivity extends AppCompatActivity {

    public static final String TAG = "SessionItemActivity";

    public ConstraintLayout layoutRoot;
    public Toolbar toolbar;
    public ImageButton refreshBtn;
    public Button contactBtn;
    public Button cancelBtn;
    public Button joinBtn;
    public TextView statusView;
    public TextView roleView;
    public TextView courseView;
    public TextView withUserView;
    public TextView deliveryView;
    public TextView dateView;
    public TextView timeView;
    public TextView totalView;

    public int defaultBgColor;
    public int activeBgColor;
    public boolean isPending = false;
    public boolean isReady = false;
    public boolean isCancelled = false;
    public boolean isPrevious = false;

    private FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_item);

        if (hasIntegrity()) {
            this.defaultBgColor = this.getResources().getColor(R.color.tutor_blue);
            this.activeBgColor = this.getResources().getColor(R.color.leaf_green);

            this.layoutRoot = findViewById(R.id.rootSessionItem);
            this.toolbar = findViewById(R.id.mySessionToolbar);
            this.refreshBtn = findViewById(R.id.refreshMySessionBtn);
            this.contactBtn = findViewById(R.id.mySessionContactBtn);
            this.cancelBtn = findViewById(R.id.mySessionCancelBtn);
            this.joinBtn = findViewById(R.id.mySessionJoinBtn);
            this.statusView = findViewById(R.id.field1);
            this.roleView = findViewById(R.id.field2);
            this.courseView = findViewById(R.id.field3);
            this.withUserView = findViewById(R.id.field4);
            this.deliveryView = findViewById(R.id.field5);
            this.dateView = findViewById(R.id.field6);
            this.timeView = findViewById(R.id.field7);
            this.totalView = findViewById(R.id.field8);

            this.toolbar.setTitle("My Session");
            bindDataToView(false);
            setClickListeners();
        }
        else {
            String msg = "Something went wrong. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        List<Integer> allowedModes = new ArrayList<Integer>();
        allowedModes.add(ActivityUtils.PREVIOUS_SESSIONS_ID);
        allowedModes.add(ActivityUtils.CANCELLED_SESSIONS_ID);
        allowedModes.add(ActivityUtils.PENDING_SESSIONS_ID);

        if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "Current user was not valid! (User may have been null)");
        }
        if (ActivityUtils.SESSION_REQUESTED == null || !ActivityUtils.SESSION_REQUESTED.findIsValidSession()) {
            hasIntegrity = false;
            Log.d(TAG, "The session requested was not valid! (Session may have been null)");
        }
        if (!allowedModes.contains(ActivityUtils.SESSIONS_VIEW_MODE)) {
            hasIntegrity = false;
            Log.d(TAG, "This view mode was not an option! : " + ActivityUtils.SESSIONS_VIEW_MODE);
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    public void bindDataToView(boolean wasRefreshed) {
        bindMode();
        Session session = ActivityUtils.SESSION_REQUESTED;
        contactBtn.setVisibility(View.VISIBLE);
        cancelBtn.setVisibility(View.GONE);
        joinBtn.setVisibility(View.GONE);

        layoutRoot.setBackgroundColor(defaultBgColor);
        if (isReady) {
            statusView.setText("Ready");
            layoutRoot.setBackgroundColor(activeBgColor);
            joinBtn.setVisibility(View.VISIBLE);
        }
        else if (isPending) {
            statusView.setText("Pending");
            cancelBtn.setVisibility(View.VISIBLE);
        }
        else if (isPrevious) {
            statusView.setText("Finished");
        }
        else if (isCancelled) {
            statusView.setText("Cancelled");
        }

        String usr;
        if (session.getTuteeUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
            roleView.setText("TUTEE");
            withUserView.setText(session.getTutorUID());
            usr = session.getTutorUID();
        }
        else {
            roleView.setText("TUTOR");
            withUserView.setText(session.getTuteeUID());
            usr = session.getTuteeUID();
        }
        courseView.setText(session.getCourseID());
        if (session.isOnline()) {
            deliveryView.setText("Online");
        }
        else {
            deliveryView.setText("In-Person");
        }
        dateView.setText(CalendarService.getFormattedDateFromDate(session.getStart().toDate()));
        timeView.setText(CalendarService.getFormattedTimeFromDate(session.getStart().toDate()) +
                " - " + CalendarService.getFormattedTimeFromDate(session.getEnd().toDate()));
        totalView.setText("$" + ActivityUtils.getFormattedFloat(session.getExpectedAmountOwed()));
        contactBtn.setText(contactBtn.getText().toString().replace("%USER%", usr));

        if (wasRefreshed) {
            Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void bindMode() {
        this.isReady = false;
        this.isCancelled = false;
        this.isPending = false;
        this.isPrevious = false;
        int mode = ActivityUtils.SESSIONS_VIEW_MODE;

        if (mode == ActivityUtils.PENDING_SESSIONS_ID) {
            if (ActivityUtils.SESSION_REQUESTED.findIsWithinWaitingWindow()) {
                isReady = true;
            }
            else {
                isPending = true;
            }
        }
        else if (mode == ActivityUtils.CANCELLED_SESSIONS_ID) {
            isCancelled = true;
        }
        else if (mode == ActivityUtils.PREVIOUS_SESSIONS_ID) {
            isPrevious = true;
        }
    }
    //---------------------------------------------------------------------------------------------

    public void setClickListeners() {
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindDataToView(true);
            }
        });

        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otherUser;
                if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(ActivityUtils.SESSION_REQUESTED.getTuteeUID())) {
                    otherUser = ActivityUtils.SESSION_REQUESTED.getTutorUID();
                }
                else {
                    otherUser = ActivityUtils.SESSION_REQUESTED.getTuteeUID();
                }
                Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
                intent.putExtra("MESSAGING", otherUser);
                startActivity(intent);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelSessionDialog dialog = new CancelSessionDialog();
                dialog.show(getSupportFragmentManager(), "CancelSessionDialog");
            }
        });

        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Entering Session...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), HandshakeActivity.class));
                finish();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Cancels a session. Sessions are never deleted once they are created. This will send an
     * alert to the other user and move the session from PENDING to CANCELLED, while setting
     * the cancellerUID to ActivityUtils.CURRENT_USER_OBJECT.getuName()
     *
     * The assumption is that you clicked on the session when it wasn't ready and WAS pending.
     */
    public void cancelSession() {
        if (!ActivityUtils.SESSION_REQUESTED.findIsWithinWaitingWindow()) {
            if (ActivityUtils.SESSION_REQUESTED.findIsUpcoming()) {
                database = FirebaseFirestore.getInstance();
                final Session sessionBeforeCancel = ActivityUtils.SESSION_REQUESTED;
                ActivityUtils.SESSION_REQUESTED.setIsPending(false);
                ActivityUtils.SESSION_REQUESTED.setWasCancelled(true);
                ActivityUtils.SESSION_REQUESTED.setCancellerUID(ActivityUtils.CURRENT_USER_OBJECT.getuName());
                Session s = ActivityUtils.SESSION_REQUESTED;

                // Update the session's cancellerUID and status in Firestore by overwriting
                database.collection("sessions").document(s.getSessionID()).set(s).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            // Update the current user's pending session id's array
                            ArrayList<String> updatedPendingIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
                            updatedPendingIds.remove(s.getSessionID());
                            DocumentReference uRef = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
                            uRef.update("sessionsPendingIds", updatedPendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ActivityUtils.CURRENT_USER_OBJECT.setSessionsPendingIds(updatedPendingIds);

                                        // Update the current user's cancelled session id's array
                                        ArrayList<String> updatedCancelledIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds();
                                        updatedCancelledIds.add(s.getSessionID());
                                        uRef.update("sessionsCancelledIds", updatedCancelledIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    ActivityUtils.CURRENT_USER_OBJECT.setSessionsCancelledIds(updatedCancelledIds);

                                                    // Get the other user as an object (we will use this in a second)
                                                    String otherUID = s.getTuteeUID();
                                                    if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(s.getTuteeUID())) {
                                                        otherUID = s.getTutorUID();
                                                    }
                                                    database.collection("users").document(otherUID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                            if (task.isSuccessful() && task.getResult() != null) {
                                                                User otherUser = task.getResult().toObject(User.class);
                                                                if (otherUser != null && otherUser.isValidUser()) {
                                                                    final User oUser = otherUser.initNullValues();

                                                                    // Update the other user's pending session ids array
                                                                    List<String> otherUserUpdatedPendingIds = oUser.getSessionsPendingIds();
                                                                    otherUserUpdatedPendingIds.remove(s.getSessionID());
                                                                    DocumentReference oRef = database.collection("users").document(oUser.getuName());
                                                                    oRef.update("sessionsPendingIds", otherUserUpdatedPendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                            if (task.isSuccessful()) {

                                                                                // Update the other user's cancelled session ids array
                                                                                List<String> otherUserUpdatedCancelledIds = oUser.getSessionsCancelledIds();
                                                                                otherUserUpdatedCancelledIds.add(s.getSessionID());
                                                                                oRef.update("sessionsCancelledIds", otherUserUpdatedCancelledIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                        if (task.isSuccessful()) {
                                                                                            sendCancelAlert();
                                                                                            AlarmService.cancelSessionAlarms(getApplicationContext(), s);
                                                                                        }
                                                                                        else {
                                                                                            // Don't undo because at this point our current user has been updated successfully.
                                                                                            String m1 = "Failed to update cancel session. Please try again later.";
                                                                                            String m2 = "Could not update sessionsCancelledIds for user : " + oUser.getuName();
                                                                                            Log.d(TAG, m2);
                                                                                            Toast.makeText(getApplicationContext(), m1, Toast.LENGTH_LONG).show();
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                // Don't undo because at this point our current user has been updated successfully.
                                                                                String m1 = "Failed to update cancel session. Please try again later.";
                                                                                String m2 = "Could not update sessionsPendingIds for user : " + oUser.getuName();
                                                                                Log.d(TAG, m2);
                                                                                Toast.makeText(getApplicationContext(), m1, Toast.LENGTH_LONG).show();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                else {
                                                                    // Don't undo because at this point our current user has been updated successfully.
                                                                    String m1 = "Failed to update cancel session. Please try again later.";
                                                                    String m2 = "Could not convert user document to User object";
                                                                    Log.d(TAG, m2);
                                                                    Toast.makeText(getApplicationContext(), m1, Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                            else {
                                                                // Don't undo because at this point our current user has been updated successfully.
                                                                String m1 = "Failed to update cancel session. Please try again later.";
                                                                String m2 = "Could not retrieve the other user document";
                                                                String ex = "";
                                                                if (task.getException() != null) {
                                                                    ex = task.getException().getMessage();
                                                                }
                                                                Log.d(TAG, m2 + "\n" + ex);
                                                                Toast.makeText(getApplicationContext(), m1, Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    String m1 = "Something went wrong. Could not cancel session";
                                                    String m2 = "Failed to update sessionsCancelledIds for user : " + ActivityUtils.CURRENT_USER_OBJECT.getuName();
                                                    alertAndUndo(sessionBeforeCancel, m1, m2);
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        String m1 = "Something went wrong. Could not cancel session";
                                        String m2 = "Failed to update sessionsPendingIds for user : " + ActivityUtils.CURRENT_USER_OBJECT.getuName();
                                        alertAndUndo(sessionBeforeCancel, m1, m2);
                                    }
                                }
                            });
                        }
                        else {
                            String m1 = "Something went wrong. Could not cancel session";
                            String m2 = "Failed to overwrite session : " + s.getSessionID();
                            alertAndUndo(sessionBeforeCancel, m1, m2);
                        }
                    }
                });
            }
            else {
                // This shouldn't happen since the waiting window check should handle errors
                String msg = "This session is not upcoming. Failed to cancel";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Session is not upcoming => Session started at : " + ActivityUtils.SESSION_REQUESTED.getStart().toDate());
            }
        }
        else {
            String msg = "Could not cancel session. This session is now active!";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            bindDataToView(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This can only undo so much... Hopefully this never gets called.
     *
     * @param sessionBeforeCancel
     * @param userErrorMsg
     * @param logErrorMsg
     */
    public void alertAndUndo(Session sessionBeforeCancel, String userErrorMsg, String logErrorMsg) {
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        ActivityUtils.SESSION_REQUESTED = sessionBeforeCancel;

        if (!cUser.getSessionsPendingIds().contains(ActivityUtils.SESSION_REQUESTED.getSessionID())) {
            ActivityUtils.CURRENT_USER_OBJECT.putNewPendingSessionId(ActivityUtils.SESSION_REQUESTED.getSessionID());
        }
        if (cUser.getSessionsCancelledIds().contains(ActivityUtils.SESSION_REQUESTED.getSessionID())) {
            ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds().remove(ActivityUtils.SESSION_REQUESTED.getSessionID());
        }
        Log.d(TAG, logErrorMsg);
        Toast.makeText(getApplicationContext(), userErrorMsg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sends a notification to the other user saying that the current user has cancelled the
     * session.
     */
    public void sendCancelAlert() {
        Session session = ActivityUtils.SESSION_REQUESTED;
        String otherUID;
        if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(session.getTuteeUID())) {
            otherUID = session.getTutorUID();
        }
        else {
            otherUID = session.getTuteeUID();
        }

        final String title = ActivityUtils.CURRENT_USER_OBJECT.getuName() + " has cancelled an upcoming session";
        final String msg = "Session for " + session.getCourseID() + " on "
                + CalendarService.getFormattedDateFromDate(session.getStart().toDate()) + " at "
                + CalendarService.getFormattedTimeFromDate(session.getStart().toDate())
                + " has been cancelled";
        final String topic = "/topics/" + MessageService.DM_TOPIC + otherUID;
        NotificationService notificationService = new NotificationService(getApplicationContext());
        notificationService.setTopic(topic);
        notificationService.setHighPriority(true);
        notificationService.setTitle(title);
        notificationService.setMessage(msg);

        Gson gson = new Gson();
        session.initNullValues();
        String sessionString = gson.toJson(session);
        Pair<String, String> data = new Pair<String, String>("sessionCancelled", sessionString);
        List<Pair<String, String>> dataList = new ArrayList<Pair<String, String>>();
        dataList.add(data);

        notificationService.sendNotification(dataList);

        finish();
        ActivityUtils.SESSION_REQUESTED = null;
    }
    //---------------------------------------------------------------------------------------------
}
