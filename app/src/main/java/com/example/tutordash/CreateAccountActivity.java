package com.example.tutordash;

import static android.text.TextUtils.isEmpty;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.core.FormatValidationUtils;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateAccountActivity extends AppCompatActivity {

    private static final String TAG = "CreateAccountActivity";
    public final String SPINNER_SELECT_PROMPT = " ";
    public String spinnerText = SPINNER_SELECT_PROMPT;
    public Map<String, String> emailSuffixes;

    private EditText uNameEdit;
    private EditText passwordEdit;
    private EditText confirmPasswordEdit;
    private EditText fNameEdit;
    private EditText lNameEdit;
    private EditText emailEdit;
    private CheckBox isTutorEdit;
    private Button submitButton;
    private Spinner spinner;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore database;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        emailEdit = findViewById(R.id.emailForm);
        fNameEdit = findViewById(R.id.firstNameForm);
        lNameEdit = findViewById(R.id.lastNameForm);
        uNameEdit = findViewById(R.id.usernameForm);
        passwordEdit = findViewById(R.id.passwordForm);
        confirmPasswordEdit = findViewById(R.id.passwordConfirmForm);
        isTutorEdit = findViewById(R.id.tutorCheck);
        submitButton = findViewById(R.id.submitBtn);
        spinner = findViewById(R.id.schoolSelection);
        emailSuffixes = new HashMap<String, String>();
        initializeSpinner(spinner);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String password1 = passwordEdit.getText().toString();
                final String password2 = confirmPasswordEdit.getText().toString();
                final String email = emailEdit.getText().toString().trim();
                final String uName = uNameEdit.getText().toString().trim();

                final String errorMsg = "Something went wrong. Please try again later.";
                CollectionReference usersCollection = database.collection("users");
                usersCollection.whereEqualTo("email", email).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots != null && queryDocumentSnapshots.size() == 0) {
                            usersCollection.whereEqualTo("uName", uName).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    if (queryDocumentSnapshots != null && queryDocumentSnapshots.size() == 0) {
                                        if (password1.equals(password2)) {
                                            writeUserObject();
                                            if (validateUser(user)) {
                                                authenticate(password1);
                                            }
                                        }
                                        else {
                                            String error = "Passwords Do Not Match";
                                            passwordEdit.setText("");
                                            passwordEdit.setError(error);
                                            confirmPasswordEdit.setText("");
                                            confirmPasswordEdit.setError(error);
                                            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else {
                                        String msg;
                                        if (queryDocumentSnapshots == null) {
                                            Log.d(TAG, "Query returned null result");
                                            msg = errorMsg;
                                        }
                                        else {
                                            Log.d(TAG, "A user with uName '" + uName + "' already exists : uName="
                                                    + queryDocumentSnapshots.toString());
                                            msg = "A user with username '" + uName + "' already exists!";
                                        }
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG, "Failed to get results when querying uName whereEqualTo : " + uName);
                                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        else {
                            String msg;
                            if (queryDocumentSnapshots == null) {
                                Log.d(TAG, "Query returned null result");
                                msg = errorMsg;
                            }
                            else {
                                Log.d(TAG, "A user with email '" + email + "' already exists : uName="
                                        + queryDocumentSnapshots.toString());
                                msg = "A user with email '" + email + "' already exists!";
                            }
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Failed to validate email for new user: " + uName + ", " + email);
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void writeUserObject() {
        user = new User();
        user.setPicURL("avatar/DEFAULT_USER.jpg");
        user.setuName(uNameEdit.getText().toString().trim());
        user.setfName(fNameEdit.getText().toString().trim());
        user.setlName(lNameEdit.getText().toString().trim());
        user.setEmail(emailEdit.getText().toString().trim());
        user.setEmail(emailEdit.getText().toString().trim());
        user.setTutor(isTutorEdit.isChecked());
        user.setSchool(spinner.getSelectedItem().toString());
        user.setLocation(new GeoPoint(34.137355, -118.12502));

    }
    //---------------------------------------------------------------------------------------------

    private void authenticate(String password) {
        String randEmail = FileUtils.generateRandomEmail();
        user.setAuthEmail(randEmail);
        user.setBlacklisted(false);
        user.setDateCreated(new Timestamp(new Date()));
        user.setHasTuteeNotifications(true);
        user.setHasTutorNotifications(true);
        user.setOffersOnline(true);
        user.setOffersInPerson(true);

        firebaseAuth.createUserWithEmailAndPassword(user.getAuthEmail(), password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            addUserToDatabase(user);
                            ActivityUtils.CURRENT_USERNAME = user.getuName();
                            ActivityUtils.CURRENT_USER_EMAIL = user.getEmail();
                            ActivityUtils.CURRENT_USER_SCHOOL = user.getSchool();

                            if (user.isTutor()) {
                                toUploadTranscriptActivity();
                            }
                            else {
                                toLoginActivity();
                            }
                        }
                        else {
                            Log.d(TAG, "Failed to create user in auth: " + task.getException());
                            String msg = "Authentication Failed. Please Try Again Later.";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Adds new document to users collection. Adds two new subcollections (courses and sessions)
     * to this new document each with a BLANK entry.
     *
     * @param user - new user to add
     */
    public void addUserToDatabase(User user) {
        database.collection("users").document(user.getuName()).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DocumentReference userRef = database.collection("users").document(user.getuName());
                userRef.collection("courses").document("BLANK").set(new Course()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                            ActivityUtils.CURRENT_USER_OBJECT = user.initNullValues();
                            finish();
                        }
                        else {
                            if (firebaseAuth.getCurrentUser() != null) {
                                firebaseAuth.getCurrentUser().delete();
                            }
                            Log.d(TAG, "Failed to create blank courses document");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (firebaseAuth.getCurrentUser() != null) {
                            firebaseAuth.getCurrentUser().delete();
                        }
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        if (firebaseAuth.getCurrentUser() != null) {
                            firebaseAuth.getCurrentUser().delete();
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                firebaseAuth.signOut(); // Can't use app until confirmed email
                Toast.makeText(getApplicationContext(), "Failure: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Could not add user to collection: " + e.getCause());
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void toLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------

    public void toUploadTranscriptActivity() {
        Intent intent = new Intent(this, UploadTranscriptActivity.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------

    public boolean validateUser(User user) {
        boolean isValid = true;
        final String password = passwordEdit.getText().toString();

        // check spinner value
        if (user.getSchool().equals(SPINNER_SELECT_PROMPT)) {
            isValid = false;
            String msg = "Please Select Your University";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }

        // validate username
        if (isEmpty(user.getuName()) || !FormatValidationUtils.usernameIsValidFormat(user.getuName())) {
            isValid = false;
            if (isEmpty(user.getuName())) {
                uNameEdit.setError("Username Required");
            }
            else {
                uNameEdit.setError("Invalid Username");
            }
        }
        else {
            uNameEdit.setError(null);
        }

        // validate password
        if (isEmpty(password) || !FormatValidationUtils.passwordIsValidFormat(
                password, user.getuName(), user.getEmail())) {
            isValid = false;
            if (isEmpty(password)) {
                passwordEdit.setError("Password Required");
            }
            else {
                passwordEdit.setError("Invalid Password");
            }
        }
        else {
            passwordEdit.setError(null);
        }

        // make sure first name != null
        if (isEmpty(user.getfName())) {
            isValid = false;
            fNameEdit.setError("First Name Required");
        }
        else {
            fNameEdit.setError(null);
        }

        // make sure last name != null
        if (isEmpty(user.getlName())) {
            isValid = false;
            lNameEdit.setError("Last Name Required");
        }
        else {
            lNameEdit.setError(null);
        }

        // validate email
        if (isEmpty(user.getEmail()) || !FormatValidationUtils.emailIsValidFormat(user.getEmail())) {
            isValid = false;
            if (isEmpty(user.getEmail())) {
                emailEdit.setError("Email Required");
            }
            else {
                emailEdit.setError("Please Enter A Valid '.edu' Email Address");
            }
        }
        else {
            String emailSuffix = emailSuffixes.get(spinnerText);
            if (emailSuffix != null) {
                if (user.getEmail().trim().endsWith(emailSuffix) && !spinnerText.equals(SPINNER_SELECT_PROMPT)) {
                    emailEdit.setError(null);
                }
                else {
                    String word = "A";
                    if (emailSuffix.startsWith("a") || emailSuffix.startsWith("e") || emailSuffix.startsWith("i") ||
                        emailSuffix.startsWith("o") || emailSuffix.startsWith("u")) {
                        word += "n";
                    }
                    String msg = word + " " + emailSuffix + " Email Required";
                    emailEdit.setError(msg);
                }
            }
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    public void initializeSpinner(Spinner spinner) {
        CollectionReference schools = database.collection("schoolList");
        schools.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                ArrayList<String> schoolList = new ArrayList<String>();
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String universityName = document.get("name").toString();
                        schoolList.add(universityName);
                        emailSuffixes.put(universityName, document.get("suffix").toString());
                    }
                    Collections.sort(schoolList);
                    schoolList.add(0, SPINNER_SELECT_PROMPT);
                    String[] schoolListArray = new String[schoolList.size()];
                    for (int i = 0; i < schoolList.size(); i++) {
                        schoolListArray[i] = schoolList.get(i);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            getApplicationContext(), android.R.layout.simple_spinner_item, schoolListArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            spinnerText = parent.getItemAtPosition(position).toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // ?
                        }
                    });
                }
                else {
                    String msg = "Could Load Load University Options. Please Try Again Later.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Toast.makeText(getApplicationContext(), "Cancelled!", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
