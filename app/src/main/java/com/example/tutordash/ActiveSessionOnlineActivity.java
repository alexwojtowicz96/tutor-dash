package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.PaymentService;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.core.VidyoTokenGenerator;
import com.example.tutordash.dialog.FinishSessionDialog;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.example.tutordash.model.VidyoIoDataModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.vidyo.VidyoClient.Connector.Connector;
import com.vidyo.VidyoClient.Connector.ConnectorPkg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This activity's logic was derived from ActiveSessionInPersonActivity.class
 * Active session activity for ONLINE sessions.
 */
public class ActiveSessionOnlineActivity extends AppCompatActivity implements Connector.IConnect {

    public static final String TAG = "ActiveSession_Online";

    public Chronometer timer;
    public TextView courseView;
    public ConstraintLayout connectionStatusLayout;
    public FrameLayout statusBeacon;
    public TextView statusText;
    public Button endSessionBtn;
    public Button startBtn;
    public Button connectBtn;
    public Button disconnectBtn;
    public FrameLayout videoLayout;
    public ConstraintLayout iHaveFinishedLayout;
    public TextView iHaveFinishedTextView;
    public Button iHaveFinishedLayoutButton;
    public ConstraintLayout theyHaveFinishedLayout;
    public TextView theyHaveFinishedTextView;
    public Button theyHaveFinishedLayoutButton;

    public boolean hasDisplayedEndPrompt = false;
    public boolean isDone = false;

    public FirebaseFirestore database;
    public CollectionReference sessionsColReference;
    public DocumentReference thisSessionRef;
    public StorageReference storageReference;

    public final String mTrue = "true";
    public final String mFalse = "false";
    public final String mNull = "";
    public final String mReady = "ready";
    public final String mNotReady = "not_ready";
    public final String mConnected = "connected";
    public final String mNotConnected = "not_connected";
    public final String mConnecting = "connecting";

    public Context context;
    public Connector connector;
    public Connector.IConnect iConnect;
    public VidyoTokenGenerator tokenGenerator;
    public String roomName = VidyoIoDataModel.DEFAULT_ROOM;
    public boolean isConnected = false;
    public String currConnectionStatus = "";
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called onCreate(). The data MUST have integrity.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_session_online);

        if (this.hasIntegrity()) {
            this.context = this.getApplicationContext();
            this.roomName = ActivityUtils.SESSION_REQUESTED.getSessionID() + "_ROOM";

            this.database = FirebaseFirestore.getInstance();
            this.sessionsColReference = database.collection("sessions");
            this.thisSessionRef = sessionsColReference.document(ActivityUtils.SESSION_REQUESTED.getSessionID());
            this.storageReference = FirebaseStorage.getInstance().getReference();

            this.timer = findViewById(R.id.session_online_chronometer);
            this.courseView = findViewById(R.id.session_online_course_display);
            this.connectionStatusLayout = findViewById(R.id.session_online_status_bubble);
            this.statusBeacon = findViewById(R.id.session_online_status_beacon);
            this.statusText = findViewById(R.id.session_online_status_label);
            this.endSessionBtn = findViewById(R.id.session_online_end_btn);
            this.connectBtn = findViewById(R.id.session_online_connect_btn);
            this.startBtn = findViewById(R.id.session_online_start_btn);
            this.disconnectBtn = findViewById(R.id.session_online_disconnect_btn);
            this.videoLayout = findViewById(R.id.session_online_video_frame);
            this.iHaveFinishedLayout = findViewById(R.id.session_online_awaitingFinishLayout);
            this.iHaveFinishedTextView = findViewById(R.id.session_online_awaitingFinishView);
            this.iHaveFinishedLayoutButton = findViewById(R.id.session_online_undoFinishButton);
            this.theyHaveFinishedLayout = findViewById(R.id.session_online_alertOtherUserHasFinishedLayout);
            this.theyHaveFinishedTextView = findViewById(R.id.session_online_alertOtherUserHasFinishedView);
            this.theyHaveFinishedLayoutButton = findViewById(R.id.session_online_confirmFinishButton);

            this.initView();
            this.initTimer();
            this.setVidyoIoButtonListeners(mFalse, mFalse, mFalse); // wait until conference initializes
            this.setEndSessionButtonListener(true);
            this.setFragmentButtonListeners(true);
            this.setSessionDocumentListener();

            if (hasRequiredPermissions()) {
                this.initConference();
            }
            else {
                this.promptForPermissions();
            }
        }
        else {
            String msg = "Something went wrong. This is a problem!";
            Log.d(TAG, "Data did not have integrity!");
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if the CAMERA and MICROPHONE permissions are allowed.
     */
    public boolean hasRequiredPermissions() {
        final int permissionGranted = PackageManager.PERMISSION_GRANTED;

        return (PermissionUtils.getAudioPermissionValue(context) == permissionGranted
                && PermissionUtils.getCameraPermissionValue(context) == permissionGranted);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Wrapper method around the prompting of permissions (CAMERA and MICROPHONE).
     */
    public void promptForPermissions() {
        if (PermissionUtils.getAudioPermissionValue(context) != PackageManager.PERMISSION_GRANTED) {
            requestMicrophonePermission();
        }
        else if (PermissionUtils.getCameraPermissionValue(context) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Renders the UI components so that the user can allow the microphone permission.
     */
    public void requestMicrophonePermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.RECORD_AUDIO },
                PermissionUtils.REQUEST_CODE_FOR_MICROPHONE
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Renders the UI components so that the user can allow the camera permission.
     */
    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.CAMERA },
                PermissionUtils.REQUEST_CODE_FOR_CAMERA
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the vidyo.io components ready for the conference.
     */
    public void initConference() {
        this.connector = null;
        this.iConnect = this;
        this.tokenGenerator = new VidyoTokenGenerator(context);
        ConnectorPkg.setApplicationUIContext(this);
        ConnectorPkg.initialize();

        setVidyoIoButtonListeners(mTrue, mTrue, mTrue);
    }
    //---------------------------------------------------------------------------------------------


    /**
     * Gets called when the users selects "ACCEPT" or "DENY" on the popup window asking them
     * to allow permission.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        final int granted = PackageManager.PERMISSION_GRANTED;
        if (grantResults.length > 0) {
            if (requestCode == PermissionUtils.REQUEST_CODE_FOR_CAMERA && grantResults[0] == granted) {
                String msg = "Camera Permission Enabled";
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
            else if (requestCode == PermissionUtils.REQUEST_CODE_FOR_MICROPHONE && grantResults[0] == granted) {
                String msg = "Microphone Permission Enabled";
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
            else {
                String msg = "You must allow these permissions for the video conference";
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }

            // Make sure BOTH permissions have been granted on request result.
            if (hasRequiredPermissions()) {
                initConference();
            }
            else {
                promptForPermissions();
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the UI thread. By default, the IConnect methods run in the
     * background, so we can't do a toast on them since they are UI components.
     *
     * @param msg
     */
    public void toastOnUiThread(String msg) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Updates the connection bubble with the appropriate status. Runs on the UI thread.
     *
     * @param status
     */
    public void updateConnectionStatus(String status) {
        final String mStatus = status.toLowerCase().trim();
        final String notReady = "Not Ready";
        final String ready = "Ready";
        final String connected = "Connected";
        final String notConnected = "Not Connected";
        final String connecting = "Connecting...";

        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (mStatus.equals(mNotReady)) {
                            statusBeacon.setBackgroundResource(R.drawable.status_not_ready);
                            statusText.setText(notReady);
                            currConnectionStatus = mNotReady;
                        }
                        else if (mStatus.equals(mReady)) {
                            statusBeacon.setBackgroundResource(R.drawable.status_ready);
                            statusText.setText(ready);
                            currConnectionStatus = mReady;
                        }
                        else if (mStatus.equals(mNotConnected)) {
                            statusBeacon.setBackgroundResource(R.drawable.status_not_connected);
                            statusText.setText(notConnected);
                            currConnectionStatus = mNotConnected;
                        }
                        else if (mStatus.equals(mConnected)) {
                            statusBeacon.setBackgroundResource(R.drawable.status_connected);
                            statusText.setText(connected);
                            currConnectionStatus = mConnected;
                        }
                        else if (mStatus.equals(mConnecting)) {
                            statusBeacon.setBackgroundResource(R.drawable.status_connecting);
                            statusText.setText(connecting);
                            currConnectionStatus = mConnecting;
                        }
                        else {
                            Log.d(TAG, "Not a valid connection status : " + mStatus);
                            currConnectionStatus = "";
                        }
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Connects to the vidyo.io server.
     */
    public void connect() {
        if (this.connector != null) {
            if (tokenGenerator.isValid()) {
                String msg = "Connecting...";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

                final User user = ActivityUtils.CURRENT_USER_OBJECT;
                final String uName = user.getuName();
                String token = tokenGenerator.generateToken(uName);
                connector.connect(
                        VidyoIoDataModel.HOST,
                        token,
                        uName,
                        roomName,
                        iConnect
                );
            }
            else {
                String msg = "Your web conference is preparing... Please try again in a few seconds.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            String msg = "Failed to connect : Connector was null!";
            toastOnUiThread(msg);
            Log.d(TAG, msg);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Disconnects from the vidyo.io server. If isConnectedOption is specified, then isConnected
     * will be set to that option.
     */
    public void disconnect(String isConnectedOption) {
        if (this.connector != null) {
            connector.disconnect();
            if (isConnectedOption.trim().toLowerCase().equals(mFalse)) {
                this.isConnected = false;
            }
            else if (isConnectedOption.trim().toLowerCase().equals(mTrue)) {
                this.isConnected = true;
            }
        }
        else {
            String msg = "Failed to disconnect : Connector was null!";
            toastOnUiThread(msg);
            Log.d(TAG, msg);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the vidyo.io connection ready.
     */
    public void initConnector() {
        final String logFileFilter = "";
        final String logFileName = "";
        final int userData = 0;
        connector = new Connector(
                videoLayout,
                Connector.ConnectorViewStyle.VIDYO_CONNECTORVIEWSTYLE_Default,
                VidyoIoDataModel.DEFAULT_PARTICIPANTS,
                logFileFilter,
                logFileName,
                userData
        );
        connector.showViewAt(
                videoLayout,
                0,
                0,
                videoLayout.getWidth(),
                videoLayout.getHeight()
        );
        this.updateConnectionStatus(mReady);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if the data has integrity.
     *
     * @return TRUE if hasIntegrity, FALSE otherwise
     */
    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        Session cSession = ActivityUtils.SESSION_REQUESTED;
        List<String> logMessages = new ArrayList<String>();

        if (cUser == null || !cUser.isValidUser() || cSession == null || !cSession.findIsValidSession()) {
            hasIntegrity = false;
            if (cUser == null) {
                logMessages.add("ActivityUtils.CURRENT_USER_OBJECT was null!");
            }
            else if (!cUser.isValidUser()) {
                logMessages.add("The current user was not in a valid state (.isValidUser() returned FALSE)");
            }
            if (cSession == null) {
                logMessages.add("ActivityUtils.SESSION_REQUESTED was null!");
            }
            else if (!cSession.findIsValidSession()) {
                logMessages.add("The current session was not in a valid state (.findIsValidSession() returned FALSE)");
            }
        }
        if (logMessages.size() > 0) {
            for (String msg : logMessages) {
                Log.d(TAG, msg);
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Binds all of the initial data to the view (Not much to initialize in this activity).
     */
    public void initView() {
        final String otherUName = this.getOtherUsername();
        this.courseView.setText(ActivityUtils.SESSION_REQUESTED.getCourseID());
        this.updateConnectionStatus(mNotReady);
        this.theyHaveFinishedTextView.setText(theyHaveFinishedTextView.getText().toString().replace("%USER%", otherUName));
        this.iHaveFinishedTextView.setText(iHaveFinishedTextView.getText().toString().replace("%USER%", otherUName));

        // Hide these by default
        this.iHaveFinishedLayout.setVisibility(View.GONE);
        this.theyHaveFinishedLayout.setVisibility(View.GONE);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the username of the other user involved in this session.
     * @return otherUsername
     */
    public String getOtherUsername() {
        Session session = ActivityUtils.SESSION_REQUESTED;
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;

        if (session.getTutorUID().equals(cUser.getuName())) {
            return session.getTuteeUID();
        }
        else {
            return session.getTutorUID();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This sets the START, CONNECT, and DISCONNECT click listeners on the buttons in the UI.
     *
     * @param startOn      - option for START button to be on
     * @param connectOn    - option for CONNECT button to be on
     * @param disconnectOn - option for DISCONNECT button to be on
     */
    public void setVidyoIoButtonListeners(String startOn, String connectOn, String disconnectOn) {
        // START BUTTON ***************************************************************************
        if (startOn.trim().toLowerCase().equals(mTrue)) {
            this.startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currConnectionStatus.equals(mNotReady)) {
                        initConnector();
                        updateConnectionStatus(mReady);
                    }
                    else {
                        String msg = "Already Started!";
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if (startOn.trim().toLowerCase().equals(mFalse)) {
            this.startBtn.setOnClickListener(null);
        }

        // CONNECT BUTTON *************************************************************************
        if (connectOn.trim().toLowerCase().equals(mTrue)) {
            this.connectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tokenGenerator.getDataModel().findIfMembershipHasExpired()) {
                        if (currConnectionStatus.equals(mConnected)) {
                            String msg = "You are already connected!";
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                        else if (currConnectionStatus.equals(mConnecting)) {
                            String msg = "Please wait. Being connected...";
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                        else if (currConnectionStatus.equals(mNotReady)) {
                            String msg = "Not ready!";
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            connect();
                            updateConnectionStatus(mConnecting);
                        }
                    }
                    else {
                        // If vidyo.io membership has expired, make sure the user knows they can't connect
                        String msg = "Online sessions are currently unavailable. Sorry for the inconvenience.";
                        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else if (connectOn.trim().toLowerCase().equals(mFalse)) {
            this.connectBtn.setOnClickListener(null);
        }

        // DISCONNECT BUTTON **********************************************************************
        if (disconnectOn.trim().toLowerCase().equals(mTrue)) {
            this.disconnectBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currConnectionStatus.equals(mConnected)) {
                        disconnect(mNull);
                    }
                    else {
                        String msg = "Cannot disconnect... Not connected!";
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if (disconnectOn.trim().toLowerCase().equals(mFalse)) {
            this.disconnectBtn.setOnClickListener(null);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the listener for the End Session button in the UI as long as 'on' == TRUE. Otherwise,
     * this sets the listener to NULL.
     *
     * @param on
     */
    public void setEndSessionButtonListener(boolean on) {
        if (on) {
            this.endSessionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FinishSessionDialog dialog = new FinishSessionDialog();
                    dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
                }
            });
        }
        else {
            this.endSessionBtn.setOnClickListener(null);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets listeners on the hidden "fragment" buttons that pop up when either user wants to
     * end the session when 'on' == TRUE. Otherwise, sets the listener to NULL.
     *
     * @param on
     */
    public void setFragmentButtonListeners(boolean on) {
        if (on) {
            this.theyHaveFinishedLayoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickableEndButton(false);
                    FinishSessionDialog dialog = new FinishSessionDialog();
                    dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
                }
            });
            this.iHaveFinishedLayoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    endSession(false);
                    clickableEndButton(true);
                }
            });
        }
        else {
            this.theyHaveFinishedLayoutButton.setOnClickListener(null);
            this.iHaveFinishedLayoutButton.setOnClickListener(null);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets a listener on the document associated with ActivityUtils.SESSION_REQUESTED to listen
     * for changes in data.
     */
    public void setSessionDocumentListener() {
        thisSessionRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(
                    @androidx.annotation.Nullable DocumentSnapshot documentSnapshot,
                    @androidx.annotation.Nullable FirebaseFirestoreException e
            ) {
                if (!isDone) {
                    if (e != null) {
                        String msg = "Something went wrong on the database server";
                        Log.d(TAG, e.getMessage());
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                    else if (documentSnapshot != null && documentSnapshot.exists()) {
                        if (documentSnapshot.contains("tutorHasConcluded")) {
                            ActivityUtils.SESSION_REQUESTED.setTutorHasConcluded(
                                    (boolean) documentSnapshot.get("tutorHasConcluded"));
                        }
                        if (documentSnapshot.contains("tuteeHasConcluded")) {
                            ActivityUtils.SESSION_REQUESTED.setTuteeHasConcluded(
                                    (boolean) documentSnapshot.get("tuteeHasConcluded"));
                        }
                        checkConclusionStatus();
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks the status of the current session. If either the tutor or tutee have opted to
     * end the session, action must be taken. If both users have ended, then this activity must
     * end ASAP.
     */
    public void checkConclusionStatus() {
        Session session = ActivityUtils.SESSION_REQUESTED;

        if (session.getTutorHasConcluded() && session.getTuteeHasConcluded()) {
            // Both users have decided to end the session
            clickableEndButton(false);
            this.iHaveFinishedLayout.setVisibility(View.GONE);
            this.theyHaveFinishedLayout.setVisibility(View.GONE);
            finishConcludingSession();
        }
        else {
            if (session.getTuteeHasConcluded() || session.getTutorHasConcluded()) {
                // Only one user has decided to end the session
                clickableEndButton(false);
                if (session.getTutorUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                    // I am the tutor
                    if (session.getTutorHasConcluded()) {
                        this.iHaveFinishedLayout.setVisibility(View.VISIBLE);
                        this.theyHaveFinishedLayout.setVisibility(View.GONE);
                    }
                    else {
                        this.iHaveFinishedLayout.setVisibility(View.GONE);
                        this.theyHaveFinishedLayout.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    // I am the tutee
                    if (session.getTuteeHasConcluded()) {
                        this.iHaveFinishedLayout.setVisibility(View.VISIBLE);
                        this.theyHaveFinishedLayout.setVisibility(View.GONE);
                    }
                    else {
                        this.iHaveFinishedLayout.setVisibility(View.GONE);
                        this.theyHaveFinishedLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
            else {
                // Neither user has decided to end the session
                clickableEndButton(true);
                this.iHaveFinishedLayout.setVisibility(View.GONE);
                this.theyHaveFinishedLayout.setVisibility(View.GONE);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This is a very important method!
     *
     * Essentially, it will update the session time based on the time upon both users concluding.
     * It will also update the expected amount owed and expected minutes of this session to
     * reflect the actual values. Once this is done, it will actually transfer the money from
     * the tutee's outgoing payment account into the tutor's incoming payment account. If all
     * of this goes well, then it will finish the activity and move onto the rating activity.
     */
    public void finishConcludingSession() {
        this.timer.stop();
        String rawTimeString = this.timer.getText().toString().trim();
        final int minutesElapsed = CalendarService.getMinutesFromTimeString(rawTimeString);
        final Float amountOwed = PaymentService.getTotal(minutesElapsed, ActivityUtils.SESSION_REQUESTED.getHourlyPayRate());
        final Session session = ActivityUtils.SESSION_REQUESTED;

        ActivityUtils.SESSION_REQUESTED.setExpectedAmountOwed(amountOwed);
        ActivityUtils.SESSION_REQUESTED.setHasHappened(true);
        ActivityUtils.SESSION_REQUESTED.setExpectedMinutes(minutesElapsed);
        database.collection("sessions").document(session.getSessionID())
                .set(ActivityUtils.SESSION_REQUESTED).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName())
                            .update("pendingSessionRatingID", session.getSessionID())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ActivityUtils.CURRENT_USER_OBJECT.setPendingSessionRatingID(session.getSessionID());
                                        makeSessionIDFinished();
                                    }
                                    else {
                                        String msg = "Something went wrong. This is bad!";
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                        String ex = "";
                                        if (task.getException() != null) {
                                            ex = task.getException().getMessage();
                                        }
                                        Log.d(TAG, ex);
                                    }
                                    finish();
                                }
                            });
                }
                else {
                    ActivityUtils.SESSION_REQUESTED = session; // revert
                    String msg = "Something went wrong";
                    String ex = "";
                    if (task.getException() != null) {
                        ex = " : " + task.getException().getMessage();
                    }
                    Toast.makeText(getApplicationContext(), msg + ex, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Could not override session document : " + session.getSessionID()
                            + "\n" + ex);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Moves the session ID from PENDING to FINISHED locally and in Firestore.
     */
    public void makeSessionIDFinished() {
        List<String> pendingSessions = ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
        List<String> finishedSessions = ActivityUtils.CURRENT_USER_OBJECT.getSessionsFinishedIds();
        finishedSessions.add(ActivityUtils.SESSION_REQUESTED.getSessionID());
        pendingSessions.remove(ActivityUtils.SESSION_REQUESTED.getSessionID());

        // Update those two fields separately
        DocumentReference uRef = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        uRef.update("sessionsPendingIds", pendingSessions).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    uRef.update("sessionsFinishedIds", finishedSessions).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ActivityUtils.CURRENT_USER_OBJECT.setSessionsPendingIds(pendingSessions);
                                ActivityUtils.CURRENT_USER_OBJECT.setSessionsFinishedIds(finishedSessions);
                                Intent intent = new Intent(getApplicationContext(), RatingActivity.class);
                                startActivity(intent);
                                isDone = true;
                            }
                            else {
                                String msg = "Something bad just happened.";
                                String ex = "";
                                if (task.getException() != null) {
                                    ex = task.getException().getMessage();
                                }
                                Toast.makeText(getApplicationContext(), msg + " : " + ex, Toast.LENGTH_LONG).show();
                                Log.d(TAG, "Failed to update sessionsFinishedIds\n" + ex);
                            }
                        }
                    });
                }
                else {
                    String msg = "Something bad just happened.";
                    String ex = "";
                    if (task.getException() != null) {
                        ex = task.getException().getMessage();
                    }
                    Toast.makeText(getApplicationContext(), msg + " : " + ex, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Failed to update sessionsPendingIds\n" + ex);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Updates local and remote data with the desireToEnd option. If you want to end the session,
     * pass in TRUE. If you want to undo the session ending, pass in FALSE.
     *
     * @param desireToEnd
     */
    public void endSession(boolean desireToEnd) {
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        Session session = ActivityUtils.SESSION_REQUESTED;

        if (cUser.getuName().equals(session.getTutorUID())) {
            thisSessionRef.update("tutorHasConcluded", desireToEnd).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        ActivityUtils.SESSION_REQUESTED.setTutorHasConcluded(desireToEnd);
                    }
                    else {
                        String msg = "Failed to conclude session for tutee";
                        Log.d(TAG, msg);
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else if (cUser.getuName().equals(session.getTuteeUID())) {
            thisSessionRef.update("tuteeHasConcluded", desireToEnd).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        ActivityUtils.SESSION_REQUESTED.setTuteeHasConcluded(desireToEnd);
                    }
                    else {
                        String msg = "Failed to conclude session for tutor";
                        Log.d(TAG, msg);
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else {
            clickableEndButton(true);
            String msg = "Error: Username does not match tutor or tutee";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given an option, make the END SESSION button clickable or non-clickable.
     * @param option
     */
    public void clickableEndButton(boolean option) {
        if (option) {
            setEndSessionButtonListener(true);
        }
        else {
            setEndSessionButtonListener(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets up the timer to start counting up from zero seconds. Also adds a listener that
     * will trigger when the timer has reached the expected length of the session.
     */
    public void initTimer() {
        this.timer.setBase(SystemClock.elapsedRealtime());
        this.timer.setText("00:00:00");
        this.timer.start();

        this.timer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - (h * 3600000)) / (60000);
                int s = (int) (time - (h * 3600000) - (m * 60000)) / 1000;
                String t = (h < 10 ? "0" + h : h)
                        + ":" + (m < 10 ? "0" + m : m)
                        + ":" + (s < 10 ? "0" + s : s);
                chronometer.setText(t);

                int minutesElapsed = CalendarService.getMinutesFromTimeString(chronometer.getText().toString().trim());
                if (minutesElapsed == ActivityUtils.SESSION_REQUESTED.getExpectedMinutes()) {
                    if (!hasDisplayedEndPrompt) {
                        hasDisplayedEndPrompt = true;
                        // TODO display a dialog that asks the users if they want to end.
                        //  Why? Because the expected length of the session has been reached.
                        //  If possible, output a sound as well.
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * VIDYO.IO ICONNECTOR IMPLEMENTATION METHODS
     * ============================================================================================
     */
    public void onSuccess() {
        toastOnUiThread("You are connected!");
        this.isConnected = true;
        this.updateConnectionStatus(mConnected);
    }
    //---------------------------------------------------------------------------------------------

    public void onFailure(Connector.ConnectorFailReason reason) {
        toastOnUiThread("Failed to connect : " + reason.toString());
        Log.d(TAG, "Failed to connect with vidyo.io : " + reason.toString());
        this.isConnected = false;
        this.updateConnectionStatus(mNotConnected);
    }
    //---------------------------------------------------------------------------------------------

    public void onDisconnected(Connector.ConnectorDisconnectReason reason) {
        toastOnUiThread("You have disconnected : " + reason.toString());
        Log.d(TAG, "vidyo.io disconnected : " + reason.toString());
        this.updateConnectionStatus(mReady);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * ADDITIONAL ANDROID LIFECYCLE METHODS
     * ============================================================================================
     */
    @Override
    public void onStart() {
        super.onStart();
        if (isConnected) {
            connect();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRestart() {
        super.onRestart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
        if (isConnected) {
            connect();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        super.onPause();
        disconnect(mTrue);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
        disconnect(mTrue);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
        disconnect(mFalse);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        clickableEndButton(false);
        FinishSessionDialog dialog = new FinishSessionDialog();
        dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
    }
    //---------------------------------------------------------------------------------------------
}
