package com.example.tutordash;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tutordash.adapter.CalendarAdapter;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.CalendarEvent;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class CalendarActivity extends AppCompatActivity {

    public static final String TAG = "CalendarActivity";

    public GregorianCalendar calendarMonth, calendarMonth2;
    public ImageButton previousMonth;
    public ImageButton nextMonth;
    private CalendarAdapter calendarAdapter;
    private TextView calendarCurrentMonthYear;
    private TextView currentDate;
    public Boolean isMe;
    User usr;

    public FirebaseFirestore database;

    private Handler handler;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_calendar);

        this.database = FirebaseFirestore.getInstance();
        this.currentDate = findViewById(R.id.currentDate);
        this.isMe = true;
        this.usr = new User();
        this.usr = ActivityUtils.CURRENT_USER_OBJECT;

        // Check if user is current or another selected user
        Bundle intentExtras = getIntent().getExtras();
        if (intentExtras != null) {
            String isCurrUser = intentExtras.getString("IS_CURRENT_USER");
            if (isCurrUser != null && isCurrUser.equals("false")) {
                this.isMe = false;
                this.usr.initNullValues();
                this.usr = ActivityUtils.TMP_USR;
            }
        }

        Date d = GregorianCalendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US);
        String s = "Current Date: " + dateFormat.format(d);
        currentDate.setText(s);

        CalendarEvent.calendarListings = new ArrayList<CalendarEvent>();
        String msg = "Loading calendar...";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

        // Add sessions based of IDs from user here
        // Sample Event:
        //      CalendarEvent.calendarListings.add(new CalendarEvent("2019-11-05", "Tom", "CS410",
        //      "Tutor", "10:00am to 01:00pm", "Cancelled"));
        getUserSessions();

        calendarMonth = (GregorianCalendar) GregorianCalendar.getInstance();
        calendarMonth2 = (GregorianCalendar) calendarMonth.clone();
        calendarAdapter = new CalendarAdapter(
                this,
                calendarMonth,
                CalendarEvent.calendarListings,
                isMe
        );

        calendarCurrentMonthYear = findViewById(R.id.currentMonthYear);
        calendarCurrentMonthYear.setText(android.text.format.DateFormat.format("MMMM yyyy", calendarMonth));

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                refresh();
                String msg = "";
                if (!isMe)
                    msg = "Viewing " + usr.getuName() + "'s Sessions.";
                else
                    msg = "Viewing your sessions.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        }, 2000); // Add delay to compensate for async-related tasks

        // Previous Month button
        previousMonth = findViewById(R.id.calendar_previous);
        previousMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPrevious();
                refresh();
            }
        });

        // Next Month button
        nextMonth = findViewById(R.id.calendar_next);
        nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNext();
                refresh();
            }
        });

        // Calendar created via GridView
        GridView gv = findViewById(R.id.calendarDays);
        gv.setAdapter(calendarAdapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
                String selection = CalendarAdapter.day_string.get(pos);
                ((CalendarAdapter) parent.getAdapter()).getPositionList(selection, CalendarActivity.this);
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets calendar one month prior of the current month
     */
    private void setPrevious() {
        // If current month is January, go back one year and set month to December
        if (calendarMonth.get(GregorianCalendar.MONTH) == calendarMonth.
                getActualMinimum(GregorianCalendar.MONTH)) {
            calendarMonth.set((calendarMonth.get(GregorianCalendar.YEAR) - 1), calendarMonth.
                    getActualMaximum(GregorianCalendar.MONTH), 1);
        }
        // For all other months, subtract one month
        else {
            calendarMonth.set(GregorianCalendar.MONTH, calendarMonth.get(GregorianCalendar.MONTH) - 1);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets calendar one month ahead of the current month
     */
    private void setNext() {
        // If current month is December, add one year and set month to January
        if (calendarMonth.get(GregorianCalendar.MONTH) ==
                calendarMonth.getActualMaximum(GregorianCalendar.MONTH)) {
            calendarMonth.set((calendarMonth.get(GregorianCalendar.YEAR) + 1),
                    calendarMonth.getActualMinimum(GregorianCalendar.MONTH), 1);
        }
        // For all other months, add one month
        else {
            calendarMonth.set(GregorianCalendar.MONTH, calendarMonth.get(GregorianCalendar.MONTH) + 1);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Refreshes calendar
     */
    private void refresh() {
        calendarAdapter.refreshDays();
        calendarAdapter.notifyDataSetChanged();
        calendarCurrentMonthYear.setText(android.text.format.DateFormat.format("MMMM yyyy", calendarMonth));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Collects id strings (cancelled, pending, and finished) of a user's account, concatenates
     * into one list, and searches the database for matches. Matches are collected into a list of
     * sessions and are passed into a function that converts the sessions into events and adds
     * them to the calendar.
     */
    private void getUserSessions() {
        List<String> cancelledIds = null;
        List<String> pendingIds = null;
        List<String> finishedIds = null;
        List<Session> userSessions = new ArrayList<Session>();

        // Get all Ids
        cancelledIds = usr.getSessionsCancelledIds();
        pendingIds = usr.getSessionsPendingIds();
        finishedIds = usr.getSessionsFinishedIds();

        // and concatenate
        List<String> sessionIds = new ArrayList<>(cancelledIds.size() + pendingIds.size() + finishedIds.size());
        sessionIds.addAll(cancelledIds);
        sessionIds.addAll(pendingIds);
        sessionIds.addAll(finishedIds);

        // Collect sessions
        final int size = sessionIds.size();
        for (String id : sessionIds) {
            database.collection("sessions").document(id).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                try {
                                    Session s = task.getResult().toObject(Session.class);
                                    userSessions.add(s);
                                    if (userSessions.size() == size) {
                                        addSessionsToCalendar(userSessions);
                                }
                            }
                                catch (Exception ex) {
                                    Log.d(TAG, ex.getMessage());
                                    Session invalidSession = new Session();
                                    invalidSession.setSessionID(null);
                                    userSessions.add(invalidSession);
                                }
                            }
                            else {
                                Log.d(TAG, "Could not retrieve session document : " + id);
                                Session invalidSession = new Session();
                                invalidSession.setSessionID(null);
                                userSessions.add(invalidSession);
                            }
                        }
                    });
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Add sessions to the calendar as events
     * @param usrSessions All associated sessions tied to the user's account
     */
    private void addSessionsToCalendar(List<Session> usrSessions) {
        if (usrSessions != null && usrSessions.size() > 0) {
            for (Session s : usrSessions) {
                if (s.findIsValidSession()){
                    SimpleDateFormat timeOnly = new SimpleDateFormat("hh:mm aa", Locale.US);
                    SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    Timestamp startTime = s.getStart();
                    Timestamp endTime = s.getEnd();

                    String date, title, subj, role, timeframe, status;
                    date = title = subj = role = timeframe = status = "";

                    // Get date
                    date = dateOnly.format(startTime.toDate());

                    // Get peer and role
                    if (s.getTutorUID().equals(usr.getuName())) {
                        // If the user is the tutor of the session
                        title = s.getTuteeUID();
                        role = "Tutor";
                    }
                    else {
                        // If the user is the tutee of the session
                        title = s.getTutorUID();
                        role = "Tutee";
                    }

                    // Get subject
                    subj = s.getCourseID();

                    // Get timeframe
                    timeframe = timeOnly.format(startTime.toDate()) + " to "
                            + timeOnly.format(endTime.toDate());

                    // Get status
                    if (s.getIsPending() && !s.getHasHappened()) {
                        status = "Pending";
                    }
                    else if (s.getWasCancelled()) {
                        status = "Cancelled";
                    }
                    else if (s.getHasHappened() && s.getTuteeHasConcluded()
                            && s.getTutorHasConcluded()) {
                        status = "Completed";
                    }
                    else {
                        status = "Error";
                    }

                    CalendarEvent ce = new CalendarEvent(date, title, subj, role, timeframe, status, s.getSessionID());

                    CalendarEvent.calendarListings.add(ce);
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------
}
