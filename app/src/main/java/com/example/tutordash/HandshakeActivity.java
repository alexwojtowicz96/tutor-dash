package com.example.tutordash;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.AlarmService;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.dialog.SessionCancelDialog;
import com.example.tutordash.dialog.SessionStartDialog;
import com.example.tutordash.model.Session;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Users enter this activity when they have a session that is awaiting start.
 */
public class HandshakeActivity extends AppCompatActivity {

    public static final String TAG = "HandshakeActivity";

    public Button confirmButton;
    public Button cancelButton;
    public TextView classText;
    public TextView leftNameText;
    public TextView rightNameText;
    public TextView timeText;
    public TextView amountOwedText;
    public TextView deliveryTypeText;
    public CircleImageView tuteeAvatar;
    public CircleImageView tutorAvatar;
    public ConstraintLayout waitingLayout;
    public CircleImageView waitingOnAvatar;
    public TextView waitingOnTextView;

    public boolean hasEntered = false;

    public StorageReference storageReference;
    public FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handshake);

        if (!hasIntegrity()) {
            String msg = "FATAL ERROR";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            this.db = FirebaseFirestore.getInstance();
            this.storageReference = FirebaseStorage.getInstance().getReference();

            this.confirmButton = findViewById(R.id.leftButtonww);
            this.cancelButton = findViewById(R.id.rightButtonww);
            this.classText = findViewById(R.id.classNameww);
            this.leftNameText = findViewById(R.id.leftNameww);
            this.rightNameText = findViewById(R.id.rightNameww);
            this.timeText = findViewById(R.id.handshakeTimeViewww);
            this.amountOwedText = findViewById(R.id.costww);
            this.deliveryTypeText = findViewById(R.id.sessionTypeww);
            this.tutorAvatar = findViewById(R.id.leftAvatarww);
            this.tuteeAvatar = findViewById(R.id.rightAvatarww);
            this.waitingLayout = findViewById(R.id.waitingViewww);
            this.waitingOnAvatar = findViewById(R.id.waitingOnUserAvatarww);
            this.waitingOnTextView = findViewById(R.id.waitingOnUserww);

            this.waitingLayout.setVisibility(View.GONE);

            db.collection("sessions").document(ActivityUtils.SESSION_REQUESTED.getSessionID())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        if (documentSnapshot.contains("tutorPresent")) {
                            ActivityUtils.SESSION_REQUESTED.setTutorPresent((boolean) documentSnapshot.get("tutorPresent"));
                        }
                        if (documentSnapshot.contains("tuteePresent")) {
                            ActivityUtils.SESSION_REQUESTED.setTuteePresent((boolean) documentSnapshot.get("tuteePresent"));
                        }
                        initDataInViews();
                        setClickListeners();
                    }
                    else {
                        toastFalure("Could not find your session!");
                        Log.d(TAG, "No session found in Firestore : " + ActivityUtils.SESSION_REQUESTED.getSessionID());
                        finish();
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();

        if (hasIntegrity()) {
            this.waitingLayout.setVisibility(View.GONE);
            CollectionReference cr = db.collection("sessions");
            DocumentReference sessionRef = cr.document(ActivityUtils.SESSION_REQUESTED.getSessionID());
            checkHandshakeStatus();

            sessionRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(
                        @Nullable DocumentSnapshot documentSnapshot,
                        @Nullable FirebaseFirestoreException e)
                {
                    if (e != null) {
                        String msg = "Error loading session document";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, e.getMessage());
                    }
                    else if (documentSnapshot != null && documentSnapshot.exists()) {
                        if (documentSnapshot.contains("tutorPresent")) {
                            ActivityUtils.SESSION_REQUESTED.setTutorPresent((boolean) documentSnapshot.get("tutorPresent"));
                        }
                        if (documentSnapshot.contains("tuteePresent")) {
                            ActivityUtils.SESSION_REQUESTED.setTuteePresent((boolean) documentSnapshot.get("tuteePresent"));
                        }
                        checkHandshakeStatus();
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initDataInViews() {
        Session currentSession = ActivityUtils.SESSION_REQUESTED;

        classText.setText(currentSession.getCourseID());
        leftNameText.setText(currentSession.getTutorUID());
        rightNameText.setText(currentSession.getTuteeUID());
        timeText.setText(CalendarService.getFormattedTimeFromDate(currentSession.getStart().toDate())
                + " - " + CalendarService.getFormattedTimeFromDate(currentSession.getEnd().toDate()));
        amountOwedText.setText("$" + ActivityUtils.getFormattedFloat(currentSession.getExpectedAmountOwed()));

        if (currentSession.isOnline()) {
            deliveryTypeText.setText("Online");
        }
        else {
            deliveryTypeText.setText("In-Person");
        }

        if (currentSession.getTutorPicUrl() != null && !currentSession.getTutorPicUrl().isEmpty()) {
            GlideApp.with(getApplicationContext()).load(storageReference
                    .child(currentSession.getTutorPicUrl())).into(this.tutorAvatar);
        }
        if (currentSession.getTuteePicUrl() != null && !currentSession.getTuteePicUrl().isEmpty()) {
            GlideApp.with(getApplicationContext()).load(storageReference
                    .child(currentSession.getTuteePicUrl())).into(this.tuteeAvatar);
        }

        // Initialize the hidden view for waiting
        if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(currentSession.getTuteeUID())) {
            this.waitingOnTextView.setText("Waiting for " + currentSession.getTutorUID());
            if (currentSession.getTutorPicUrl() != null && !currentSession.getTutorPicUrl().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference
                        .child(currentSession.getTutorPicUrl())).into(this.waitingOnAvatar);
            }
        }
        else {
            this.waitingOnTextView.setText("Waiting for " + currentSession.getTuteeUID());
            if (currentSession.getTuteePicUrl() != null && !currentSession.getTuteePicUrl().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference
                        .child(currentSession.getTuteePicUrl())).into(this.waitingOnAvatar);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void setClickListeners() {
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setButtonsClickable(false);
                SessionStartDialog dialog = new SessionStartDialog();
                dialog.show(getSupportFragmentManager(), "SessionStartDialog");
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setButtonsClickable(false);
                String msg = "This is broken. It's TODO right now.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                //SessionCancelDialog dialog = new SessionCancelDialog();
                //dialog.show(getSupportFragmentManager(), "SessionCancelDialog");
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Not sure what is supposed to happen here? When they click ACCEPT, they go the the
     * ActiveSession activity, but this indicates that if both users are ready, then the
     * ActiveSession activity starts for both users... Doesn't make sense.
     *
     * For whatever reason, after the activity is destroyed, the document snapshot listeners
     * are still active... So to workaround this issue, I am using a hasEntered flag localized
     * to the activity. Not the best idea, but it will work for now.
     */
    public void checkHandshakeStatus() {
        if (ActivityUtils.SESSION_REQUESTED.getTutorPresent() &&
                ActivityUtils.SESSION_REQUESTED.getTuteePresent())
        {
            if (!hasEntered) {
                AlarmService.cancelSessionAlarms(getApplicationContext(), ActivityUtils.SESSION_REQUESTED);
                removeSnapshotListener();

                Intent intent;
                if (ActivityUtils.SESSION_REQUESTED.isOnline()) {
                    intent = new Intent(getApplicationContext(), ActiveSessionOnlineActivity.class);
                }
                else {
                    intent = new Intent(getApplicationContext(), ActiveSessionInPersonActivity.class);
                }
                startActivity(intent);
                finish();

                String msg = "Both users are now ready!";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                hasEntered = true;
            }
        }
        else {
            String msg = "Waiting for both users to get ready";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            if (ActivityUtils.SESSION_REQUESTED.getTutorUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                // This user is the tutor
                if (ActivityUtils.SESSION_REQUESTED.getTutorPresent()) {
                    this.waitingLayout.setVisibility(View.VISIBLE);
                    setButtonsClickable(false);
                }
                else {
                    this.waitingLayout.setVisibility(View.GONE);
                    setButtonsClickable(true);
                }
            }
            else {
                // This user is the tutee
                if (ActivityUtils.SESSION_REQUESTED.getTuteePresent()) {
                    this.waitingLayout.setVisibility(View.VISIBLE);
                    setButtonsClickable(false);
                }
                else {
                    this.waitingLayout.setVisibility(View.GONE);
                    setButtonsClickable(true);
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Not sure if this works, but the idea is that we need to "remove" the listener on this
     * session document before proceeding. I'm doing this by attaching a new listener which does
     * nothing.
     */
    public void removeSnapshotListener() {
        CollectionReference cr = db.collection("sessions");
        DocumentReference sessionRef = cr.document(ActivityUtils.SESSION_REQUESTED.getSessionID());

        sessionRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(
                    @androidx.annotation.Nullable DocumentSnapshot documentSnapshot,
                    @androidx.annotation.Nullable FirebaseFirestoreException e)
            {
                // Do nothing
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        if (ActivityUtils.SESSION_REQUESTED == null || !ActivityUtils.SESSION_REQUESTED.findIsValidSession()) {
            hasIntegrity = false;
            Log.d(TAG, "Current session is not in a valid state!");
        }
        if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "Current user is not in a valid state!");
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    public void toastFalure(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    public void setButtonsClickable(boolean option) {
        if (option) {
            setClickListeners();
        }
        else {
            this.confirmButton.setOnClickListener(null);
            this.cancelButton.setOnClickListener(null);
        }
    }
    //---------------------------------------------------------------------------------------------
}
