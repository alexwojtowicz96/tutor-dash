package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.model.ChatUser;
import com.example.tutordash.model.Message;
import com.example.tutordash.model.NewConvo;
import com.example.tutordash.model.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {

    private static final String TAG = "MessageActivity";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private EditText newMessage;
    private ImageButton send;
    private Toolbar toolbar;

    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter adapter;

    private User user;
    private String otherUser;

    private Message lastMessage;
    private boolean isReadyForMessageUpdates = false;
    private boolean createdConversation = false;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        recyclerView = findViewById(R.id.convo);
        newMessage = findViewById(R.id.new_message);
        send = findViewById(R.id.post_button);
        Bundle intentExtras = getIntent().getExtras();
        otherUser = intentExtras.getString("MESSAGING");

        toolbar = findViewById(R.id.message_toolbar);
        toolbar.setTitle(otherUser);
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageActivity.super.onBackPressed();
            }
        });

        init();
        getMessageList();
    }
    //---------------------------------------------------------------------------------------------

    private void init() {
        linearLayoutManager = new LinearLayoutManager(
                getApplicationContext(),
                RecyclerView.VERTICAL,
                false
        );
        recyclerView.setLayoutManager(linearLayoutManager);
        database = FirebaseFirestore.getInstance();
        user = ActivityUtils.CURRENT_USER_OBJECT;
    }
    //---------------------------------------------------------------------------------------------

    private void getMessageList() {
        Query query = database.collection("messages")
                .document(getMessageTag())
                .collection("messages");

        FirestoreRecyclerOptions<Message> messageList = new FirestoreRecyclerOptions
                .Builder<Message>()
                .setQuery(query, Message.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Message, MessageActivity.MessageHolder>(messageList) {
            public List<Message> allMessages;

            @NonNull
            @Override
            public MessageActivity.MessageHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.model_messages, group, false);
                allMessages = new ArrayList<Message>();
                for (Message message : this.getSnapshots()) {
                    allMessages.add(message);
                }
                lastMessage = this.getLastMessage();

                return new MessageActivity.MessageHolder(view);
            }
            //-------------------------------------------------------------------------------------

            @Override
            public void onBindViewHolder(
                    @NonNull MessageActivity.MessageHolder holder,
                    int position,
                    @NonNull Message model
            ) {
                if (user.getuName().equals(model.getName())) {
                    holder.thisUserLayout.setVisibility(View.VISIBLE);
                    holder.otherUserLayout.setVisibility(View.GONE);

                    holder.getUserAvatar(model.getName(), true);
                    holder.thisUserMessage.setText(model.getMessage());
                    holder.thisUserMsgDate.setText(model.getDateTime());
                }
                else {
                    holder.otherUserLayout.setVisibility(View.VISIBLE);
                    holder.thisUserLayout.setVisibility(View.GONE);

                    holder.getUserAvatar(model.getName(), false);
                    holder.otherUserMessage.setText(model.getMessage());
                    holder.otherUserMsgDate.setText(model.getDateTime());
                }
            }
            //-------------------------------------------------------------------------------------

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
            //-------------------------------------------------------------------------------------

            public Message getLastMessage() {
                Message message = this.allMessages.get(this.allMessages.size() - 1);

                return message;
            }
            //-------------------------------------------------------------------------------------

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (this.getSnapshots().size() > 0) {
                    allMessages = new ArrayList<Message>();
                    for (Message message : this.getSnapshots()) {
                        allMessages.add(message);
                    }
                    lastMessage = this.getLastMessage();

                    if (isReadyForMessageUpdates) {
                        updateLastMessage();
                    }
                    isReadyForMessageUpdates = true; // The first onDataChanged will be ignored
                }
                else {
                    // Create the conversation (no messages currently)
                    if (!createdConversation) {
                        createNewConversation();
                    }
                }
            }
            //-------------------------------------------------------------------------------------
        };

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int messageCount = adapter.getItemCount();
                int lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (messageCount - 1) && lastVisiblePosition == (positionStart - 1))
                ) {
                    recyclerView.scrollToPosition(positionStart);
                }
            }
        });

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String timestamp = getCurrentTime();
                Message message = new Message(
                        user.getuName(),
                        newMessage.getText().toString(),
                        timestamp
                );
                Map<String, Object> convo = new HashMap<>();
                String[] msgTag = getMessageTag().split("_");
                convo.put("user1", msgTag[1]);
                convo.put("user2", msgTag[0]);

                database.collection("messages")
                        .document(getMessageTag())
                        .set(convo)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Log.d(TAG, "Could not write the document : " + getMessageTag());
                                }
                            }
                        });

                database.collection("messages")
                        .document(getMessageTag())
                        .collection("messages")
                        .document(timestamp)
                        .set(message)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Log.d(TAG, "Could not write message to Firestore");
                                }
                            }
                        });

                sendNotification(newMessage.getText().toString());
                newMessage.setText("");
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sends notification to other user with the message. They won't get this notification
     * if they are currently viewing this conversation.
     *
     * @param msg
     */
    private void sendNotification(String msg) {
        final String title = "New Message From " + user.getuName() + "!";
        final String message = ChatUser.returnTrimmedMessage(msg);
        final String topic = "/topics/" + MessageService.DM_TOPIC + otherUser;
        NotificationService notificationService = new NotificationService(getApplicationContext());
        notificationService.setTopic(topic);
        notificationService.setHighPriority(true);
        notificationService.setTitle(title);
        notificationService.setMessage(msg);

        List<Pair<String, String>> extraData = new ArrayList<Pair<String, String>>();
        Pair<String, String> chatNotifier = new Pair<String, String>("chatMessage", "true");
        Pair<String, String> sender = new Pair<String, String>("sender", user.getuName());
        extraData.add(chatNotifier);
        extraData.add(sender);

        notificationService.sendNotification(extraData);
    }
    //---------------------------------------------------------------------------------------------

    public String getCurrentTime() {
        Date now = new Date();
        return String.valueOf(now.getTime() / 1000);
    }
    //---------------------------------------------------------------------------------------------

    public String getMessageTag() {
        ArrayList<String> users = new ArrayList<String>();
        users.add(user.getuName());
        users.add(otherUser);
        Collections.sort(users);

        return users.get(1) + "_" + users.get(0);
    }
    //---------------------------------------------------------------------------------------------

    public void createNewConversation() {
        List<String> uNames = new ArrayList<String>();
        uNames.add(user.getuName());
        uNames.add(otherUser);
        for (int i = 0; i < uNames.size(); i++) {
            String tmp1 = "";
            String tmp2 = "";
            if (i == 0) {
                tmp1 = uNames.get(0);
                tmp2 = uNames.get(1);
            }
            else if (i == 1) {
                tmp1 = uNames.get(1);
                tmp2 = uNames.get(0);
            }
            final String uName = tmp1;
            final String otherUName = tmp2;

            database.collection("chat").document(uName).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        if (!task.getResult().contains("name")) {
                            // Could not find document. Create one.
                            database.collection("chat").document(uName).set(new NewConvo(uName))
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                ChatUser invalidModel = new ChatUser();
                                                invalidModel.setValid(false);
                                                invalidModel.initNullValues();
                                                database.collection("chat").document(uName).collection("userList")
                                                        .document(otherUName).set(invalidModel)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    createdConversation = true;
                                                                }
                                                                else {
                                                                    Log.d(TAG, "Could not create document!");
                                                                    createdConversation = false;
                                                                }
                                                            }
                                                        });
                                            }
                                            else {
                                                String ex = "";
                                                if (task.getException() != null) {
                                                    ex = task.getException().getMessage();
                                                }
                                                Log.d(TAG, "There was a problem creating chat/" + uName + "\n" + ex);
                                                createdConversation = false;
                                            }
                                        }
                                    });
                        }
                        else {
                            // Document exists, but does a document with other user's uName exist in userList?
                            database.collection("chat").document(uName).collection("userList").document(otherUName)
                                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful() && task.getResult() != null) {
                                        if (task.getResult().contains("lastMessage")) {
                                            Log.d(TAG, "There is already a document for '" + otherUName
                                                    + "' in chat/" + uName + "/userList/\nNOT overwriting!");
                                        }
                                        else {
                                            ChatUser invalidModel = new ChatUser();
                                            invalidModel.setValid(false);
                                            invalidModel.initNullValues();
                                            database.collection("chat").document(uName).collection("userList")
                                                    .document(otherUName).set(invalidModel)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (!task.isSuccessful()) {
                                                                Log.d(TAG, "Could not create document!");
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                    else {
                                        Log.d(TAG, "Failed to query document!");
                                    }
                                }
                            });
                        }
                    }
                    else {
                        Log.d(TAG, "Could not retrieve document : chat/" + uName);
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    public void updateLastMessage() {
        if (lastMessage != null) {
            DocumentReference refForThisUser = database.collection("chat")
                    .document(user.getuName())
                    .collection("userList")
                    .document(otherUser);
            DocumentReference refForOtherUser = database.collection("chat")
                    .document(otherUser)
                    .collection("userList")
                    .document(user.getuName());

            lastMessage.initNullValues();
            ChatUser chatUserDoc = new ChatUser(
                    otherUser,
                    lastMessage.getDateTime(),
                    lastMessage.getMessage(),
                    otherUser,
                    lastMessage.getName(),
                    true,
                    true
            );

            refForThisUser.set(chatUserDoc).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (!task.isSuccessful()) {
                        Log.d(TAG, "Could not write document : " + refForThisUser.getPath());
                    }
                }
            });

            chatUserDoc.setHasBeenRead(false);
            chatUserDoc.setName(user.getuName());
            chatUserDoc.setUserName(user.getuName());
            refForOtherUser.set(chatUserDoc).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (!task.isSuccessful()) {
                        Log.d(TAG, "Could not write document : " + refForOtherUser.getPath());
                    }
                }
            });
        }
        else {
            Log.d(TAG, "lastMessage was null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
        isReadyForMessageUpdates = false;
        ActivityUtils.IN_CHAT_WITH = otherUser;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
        ActivityUtils.IN_CHAT_WITH = null;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        super.onPause();
        ActivityUtils.IN_CHAT_WITH = null;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityUtils.IN_CHAT_WITH = null;
    }
    //---------------------------------------------------------------------------------------------

    public class MessageHolder extends RecyclerView.ViewHolder {
        ConstraintLayout otherUserLayout;
        ConstraintLayout thisUserLayout;
        CircleImageView otherUserAvatar;
        CircleImageView thisUserAvatar;
        TextView otherUserMessage;
        TextView thisUserMessage;
        TextView otherUserMsgDate;
        TextView thisUserMsgDate;

        public MessageHolder(@NonNull View itemView) {
            super(itemView);
            otherUserLayout = itemView.findViewById(R.id.otherUserInChatLayout);
            thisUserLayout = itemView.findViewById(R.id.thisUserInChatLayout);
            otherUserAvatar = itemView.findViewById(R.id.otherUserInChatAvatar);
            thisUserAvatar = itemView.findViewById(R.id.thisUserInChatAvatar);
            otherUserMessage = itemView.findViewById(R.id.otherUserInChatMessage);
            thisUserMessage = itemView.findViewById(R.id.thisUserInChatMessage);
            otherUserMsgDate = itemView.findViewById(R.id.otherUserInChatDate);
            thisUserMsgDate = itemView.findViewById(R.id.thisUserInChatDate);

            otherUserLayout.setVisibility(View.GONE);
            thisUserLayout.setVisibility(View.GONE);
        }

        public void getUserAvatar(String uName, boolean isThisUser) {
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            FirebaseFirestore.getInstance().collection("users").document(uName).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                if (task.getResult().get("picURL") != null) {
                                    String url = (String) task.getResult().get("picURL");
                                    if (url != null && !url.isEmpty()) {
                                        if (isThisUser) {
                                            GlideApp.with(getApplicationContext()).load(storageRef
                                                    .child(url)).into(thisUserAvatar);
                                        }
                                        else {
                                            GlideApp.with(getApplicationContext()).load(storageRef
                                                    .child(url)).into(otherUserAvatar);
                                        }
                                    }
                                }
                            }
                            else {
                                String ex = "";
                                if (task.getException() != null) {
                                    ex = task.getException().getMessage();
                                }
                                Log.d(TAG, "Could not complete retrieval of user : " + uName + "\n" + ex);
                            }
                        }
                    });
        }
    }
    //---------------------------------------------------------------------------------------------
}
