package com.example.tutordash;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.PaymentService;
import com.example.tutordash.core.PayRateService;
import com.example.tutordash.dialog.SubmitRatingDialog;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * When users finish their sessions, they end up here. If they log back in without completing
 * this activity, they will be redirected here upon sign in.
 */
public class RatingActivity extends AppCompatActivity {

    public static final String TAG = "RatingActivity";

    public TextView courseView;
    public TextView otherUserNameView;
    public CircleImageView otherUserAvatar;
    public RatingBar ratingBar;
    public TextView ratingPrompt;
    public Button submitBtn;

    public FirebaseFirestore database;
    public StorageReference storageReference;
    public boolean thisUserIsTutor;
    //---------------------------------------------------------------------------------------------

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        this.database = FirebaseFirestore.getInstance();
        this.storageReference = FirebaseStorage.getInstance().getReference();

        if (hasIntegrity()) {
            database.collection("sessions").document(ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    try {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Session session = task.getResult().toObject(Session.class);
                            if (session == null || !session.findIsValidSession()) {
                                throw new Exception("This session object could not be deserialized : "
                                        + task.getResult().getId());
                            }
                            else {
                                ActivityUtils.SESSION_REQUESTED = session;
                                thisUserIsTutor = findIfUserIsTutor();

                                courseView = findViewById(R.id.className);
                                otherUserNameView = findViewById(R.id.leftNamett);
                                otherUserAvatar = findViewById(R.id.ratedAvatar);
                                ratingBar = findViewById(R.id.ratingBaryyr);
                                submitBtn = findViewById(R.id.submitButtonvbn);
                                ratingPrompt = findViewById(R.id.ratingPrompt);
                                bindDataToView();

                                // Check if the current user is the tutee. If they are, make them pay!
                                final Float amountOwed = PaymentService.getTotal(session.getExpectedMinutes(), session.getHourlyPayRate());
                                if (!session.getTutorHasPaid()) {
                                    if (session.getTuteeUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                                        submitTransaction(session, amountOwed);

                                        // If the transaction fails, then this still updates...
                                        // Not too good, but it's alright for now I suppose.
                                        database.collection("sessions").document(session.getSessionID()).update("tutorHasPaid", true)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            ActivityUtils.SESSION_REQUESTED.setTutorHasPaid(true);
                                                        }
                                                        else {
                                                            Log.d(TAG, "Failed to update tutorHasPaid for session : " + session.getSessionID());
                                                        }
                                                    }
                                                });
                                    }
                                }
                            }
                        }
                        else {
                            throw new Exception("Task did not complete successfully." +
                                    " Does this session document exist? => " +
                                    ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID());
                        }
                    }
                    catch (Exception ex) {
                        String msg = "Something went wrong. Please try again later.";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }
        else {
            String msg = "An error occurred. Please try again later.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void bindDataToView() {
        final Session cSession = ActivityUtils.SESSION_REQUESTED;

        String partner;
        if (!thisUserIsTutor) {
            partner = "Tutor";
            this.otherUserNameView.setText(cSession.getTutorUID());
            if (cSession.getTutorPicUrl() != null && !cSession.getTutorPicUrl().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference
                        .child(cSession.getTutorPicUrl())).into(this.otherUserAvatar);
            }
        }
        else {
            partner = "Tutee";
            this.otherUserNameView.setText(cSession.getTuteeUID());
            if (cSession.getTuteePicUrl() != null && !cSession.getTuteePicUrl().isEmpty()) {
                GlideApp.with(getApplicationContext()).load(storageReference
                        .child(cSession.getTuteePicUrl())).into(this.otherUserAvatar);
            }
        }
        this.courseView.setText(courseView.getText().toString().replace("%COURSE%", cSession.getCourseID()));
        this.ratingPrompt.setText(ratingPrompt.getText().toString().replace("%PARTNER%", partner));

        this.ratingBar.setStepSize(Float.valueOf(1 / 2));
        this.ratingBar.setRating(0);
        this.ratingBar.setMax(5);
        this.ratingBar.setNumStars(5);

        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingBar.getRating() == Float.valueOf(0) || ratingBar.getRating() > Float.valueOf(5)) {
                    String msg = "You must supply a valid rating for " + getOtherUsername() + "!";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    SubmitRatingDialog dialog = new SubmitRatingDialog();
                    dialog.show(getSupportFragmentManager(), "SubmitRatingDialog");
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Coming into this activity, users must have a pendingSessionRatingID (indicating which
     * session they need to supply a rating for).
     *
     * @return TRUE if has integrity, FALSE otherwise
     */
    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        List<String> logMessages = new ArrayList<String>();

        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            logMessages.add("The current user was null!");
        }
        else if (!ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            logMessages.add("The current user was not null, but invalid!");
        }
        else if (ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID() == null
                || ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID().isEmpty())
        {
            logMessages.add("No pendingSessionRatingID was found for user : "
                    + ActivityUtils.CURRENT_USER_OBJECT.getuName());
        }

        if (logMessages.size() > 0) {
            hasIntegrity = false;
            for (String msg : logMessages) {
                Log.d(TAG, msg);
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * If a tutor is being rated, then this gets the rating, compounds it into the tutor's average
     * for this course, then proceeds to compute the overall rating's new average by getting
     * all ratings from all tutored courses (hours are critical!).
     *
     * If a tutee is being rated, then the only rating updated will be the overall tutee rating.
     * This will be recalculated based on hoursAsTutee.
     *
     * If all goes well, users will end up back on the main discovery activity.
     */
    public void submitRating() {
        final Float rating = Float.parseFloat(getRating());
        final Float hours = getHours();
        final String partner = getOtherUsername();
        DocumentReference uRef = database.collection("users").document(partner);

        if (thisUserIsTutor) {  // Then the other user is a tutee
            uRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        User user = task.getResult().toObject(User.class);
                        if (user != null && user.isValidUser()) {
                            user = user.initNullValues();
                            final Float currRating = user.getTuteeRating();
                            final Float currHours = user.getHoursAsTutee();
                            final Float newRating = calculateNewRating(currRating, currHours, rating, hours);
                            final Float newHours = currHours + hours;

                            user.setHoursAsTutee(newHours);
                            user.setTuteeRating(newRating);

                            // Data has been updated. Now write it back to Firestore (overrides existing document)
                            uRef.set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        removeRatingHold();
                                    }
                                    else {
                                        errorOut("Failed write user back to Firestore : " + partner);
                                    }
                                }
                            });
                        }
                        else {
                            errorOut("Failed to deserialize user : " + partner);
                        }
                    }
                    else {
                        errorOut("This user was not retrieve successfully : " + partner);
                    }
                }
            });
        }
        else {  // Then the other user is a tutor
            uRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        User user = task.getResult().toObject(User.class);
                        if (user != null && user.isValidUser()) {
                            user = user.initNullValues();
                            final Float currRating = user.getTutorRating();
                            final Float currHours = user.getHoursAsTutor();
                            final Float newRating = calculateNewRating(currRating, currHours, rating, hours);
                            final Float newHours = currHours + hours;

                            user.setHoursAsTutor(newHours);
                            user.setTutorRating(newRating);

                            // Data has been updated. Now write it back to Firestore (overrides existing document)
                            uRef.set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        // Now update the course rating
                                        final String courseID = ActivityUtils.SESSION_REQUESTED.getCourseID();
                                        uRef.collection("courses").document(courseID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful() && task.getResult() != null) {
                                                    Course course = ConversionUtils.getCourseFromDocumentSnapshot(task.getResult());
                                                    if (course != null) {
                                                        final Float currCourseRating = course.getRating().floatValue();
                                                        final Float currCourseHours = course.getHours().floatValue();
                                                        final Float newCourseRating = calculateNewRating(currCourseRating, currCourseHours, rating, hours);
                                                        final Float newCourseHours = currCourseHours + hours;

                                                        course.setHours(newCourseHours);
                                                        course.setRating(newCourseRating);

                                                        // Adjust database values for the school, course, and tutor
                                                        try {
                                                            PayRateService payrateService = new PayRateService(
                                                                    newRating,
                                                                    course,
                                                                    courseID
                                                            );
                                                            course = payrateService.calculateNewPayrate();
                                                        }
                                                        catch (Exception ex) {
                                                            Log.d(TAG, "Failed to complete PayRateService functionality.\n" + ex.getLocalizedMessage());
                                                        }

                                                        // Write this course back to Firestore
                                                        uRef.collection("courses").document(courseID).set(course)
                                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    removeRatingHold();
                                                                }
                                                                else {
                                                                    errorOut("Failed to update course : " + courseID);
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        errorOut("Could not deserialize course : " + courseID);
                                                    }
                                                }
                                                else {
                                                    errorOut("Could not find course '" + courseID
                                                            + "' for user : " + partner);
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        errorOut("Failed write user back to Firestore : " + partner);
                                    }
                                }
                            });
                        }
                        else {
                            errorOut("Failed to deserialize user : " + partner);
                        }
                    }
                    else {
                        errorOut("This user was not retrieve successfully : " + partner);
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    private Float getHours() {
        return (Float.valueOf(ActivityUtils.SESSION_REQUESTED.getExpectedMinutes()) / Float.valueOf(60));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Calculates a new rating based on current values and new values.
     *
     * @param currRating
     * @param currHours
     * @param thisRating - rating we are adding into the currRating
     * @param theseHours - hours we want to add to the currHours
     * @return new average rating
     */
    private Float calculateNewRating(Float currRating, Float currHours, Float thisRating, Float theseHours) {
        // [(currRating)(currHours) + (thisRating)(theseHours)] / (currHours + theseHours)
        Float numeratorTerm1 = currRating * currHours;
        Float numeratorTerm2 = thisRating * theseHours;
        Float denominator = currHours + theseHours;

        return (numeratorTerm1 + numeratorTerm2) / denominator;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Removes the current user's "hold" on their account since now they have supplied the
     * mandatory rating for this session.
     */
    public void removeRatingHold() {
        final String currUID = ActivityUtils.CURRENT_USER_OBJECT.getuName();
        database.collection("users").document(currUID)
                .update("pendingSessionRatingID", null)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            ActivityUtils.CURRENT_USER_OBJECT.setPendingSessionRatingID(null);
                            exit();
                        }
                        else {
                            errorOut("Failed to remove rating hold for current user : " + currUID);
                        }
                    }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void errorOut(String logMsg) {
        final String msg = "Something went wrong. Please try again later.";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        Log.d(TAG, logMsg);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * If the user was the tutor for this session, this returns TRUE, FALSE otherwise.
     *
     * @return TRUE if tutor, FALSE otherwise
     */
    public boolean findIfUserIsTutor() {
        boolean isTutor = false;
        final User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        final Session cSession = ActivityUtils.SESSION_REQUESTED;

        if (cSession.getTutorUID().equals(cUser.getuName())) {
            isTutor = true;
        }

        return isTutor;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns the other user's username (for this session).
     *
     * @return other user's username
     */
    public String getOtherUsername() {
        String otherUsername;
        if (thisUserIsTutor) {
            otherUsername = ActivityUtils.SESSION_REQUESTED.getTuteeUID();
        }
        else {
            otherUsername = ActivityUtils.SESSION_REQUESTED.getTutorUID();
        }

        return otherUsername;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the current rating in this.ratingBar as a string (formatted to have 2 points
     * of precision after the decimal point).
     *
     * @return rating as a string
     */
    public String getRating() {
        return ActivityUtils.getFormattedFloat(this.ratingBar.getRating());
    }
    //---------------------------------------------------------------------------------------------

    private void exit() {
        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), MainDiscoveryActivity.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        String msg = "You must supply a rating for " + getOtherUsername() + "!";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Submits the transaction (hopefully)...
     * The transaction ID is left blank until we actually execute the transaction then find it again.
     *
     * @param session
     * @param amount
     */
    public void submitTransaction(Session session, Float amount) {
        final Timestamp now = new Timestamp(new Date());
        BraintreeTransaction transaction = new BraintreeTransaction();

        String cName = "CREDIT_CARD";
        String desc = "DEFAULT";
        String mName = "CC";
        if (session.getPaymentTags() != null && session.getPaymentTags().size() > 0) {
            cName = session.getPaymentTags().get("CANONICAL_NAME");
            desc = session.getPaymentTags().get("DESCRIPTION");
            mName = session.getPaymentTags().get("NAME");

            ArrayList<String> tags = new ArrayList<String>();
            if (cName != null && !cName.isEmpty()) {
                tags.add(cName);
            }
            if (desc != null && !desc.isEmpty()) {
                tags.add(desc);
            }
            if (mName != null && !mName.isEmpty()) {
                tags.add(mName);
            }
            if (tags.size() > 0) {
                transaction.setTags(tags);
            }
        }

        transaction.setInitiatorUID(session.getTuteeUID());
        transaction.setReceiverUID(session.getTutorUID());
        transaction.setAmount(amount);
        transaction.setDateUpdated(now);

        transaction.setDateSubmitted(now);
        transaction.setPaymentMethod(mName);
        transaction.setIsVoid(false);
        transaction.setInvolved(new ArrayList<String>() {{
            add(session.getTuteeUID());
            add(session.getTutorUID());
            add(BraintreeTransaction.DEFAULT_RECEIVER); // TODO : get rid of this if tutors can ever get paid...
        }});

        PaymentService paymentService = new PaymentService(this);
        final String success = "Transaction completed successfully!";
        final String failure = "Transaction failed to complete";
        paymentService.executeTransaction(transaction, getApplicationContext(), success, failure);
    }
    //---------------------------------------------------------------------------------------------
}
