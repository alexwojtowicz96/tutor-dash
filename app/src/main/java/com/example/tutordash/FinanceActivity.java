package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.PayPalAccount;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.example.tutordash.adapter.PaymentMethodsAdapter;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.BraintreeFauxServer;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.List;

/**
 * This activity is the landing for the finance section of the member profile (only for
 * the currently logged-in user). Users can see their payment methods on this screen, and they
 * have the option to view transactions, but that is handled in a separate activity.
 */
public class FinanceActivity extends AppCompatActivity implements BraintreeActivity {

    public static final String TAG = "FinanceActivity";

    public FirebaseFirestore database;
    public BraintreeFauxServer server;
    public Context context;
    public User user;
    public Customer uCustomer;
    public BraintreeFragment mBraintreeFragment;

    public LinearLayoutManager layoutManager1;
    public LinearLayoutManager layoutManager2;

    public Toolbar toolbar;
    public RecyclerView paypalAccountsRecyclerView;
    public RecyclerView creditCardsRecylerView;
    public LinearLayout noMethodsView;
    public LinearLayout paypalLayout;
    public LinearLayout creditCardLayout;
    //---------------------------------------------------------------------------------------------

    /**
     * On create of this activity, the current user must not be null.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance);
        this.initalizeView();

        if (hasDataIntegrity()) {
            this.user = ActivityUtils.CURRENT_USER_OBJECT;
            this.uCustomer = null;
            this.context = this.getApplicationContext();
            this.database = FirebaseFirestore.getInstance();
            this.mBraintreeFragment = null;
            this.server = new BraintreeFauxServer(this);
            this.layoutManager1 = new LinearLayoutManager(context);
            this.layoutManager2 = new LinearLayoutManager(context);
        }
        else {
            String msg = "Something went wrong. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.finance_menu, menu);

        return true;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finance_menu_add_payment:
                if (this.mBraintreeFragment == null) {
                    String msg = "Not ready yet. Try again in a few seconds.";
                    Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    String uName = ActivityUtils.CURRENT_USER_OBJECT.getuName();
                    this.server.initPayment(uName, new Double(0.01).floatValue());
                }
                break;

            case R.id.finance_menu_transactions:
                Gson gson = new Gson();
                Intent intent = new Intent(this, FinanceTransactionsActivity.class);
                if (this.uCustomer == null) {
                    String msg = "The customer object is null!";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                intent.putExtra("U_CUSTOMER", gson.toJson(this.uCustomer));
                startActivity(intent);
                break;
        }

        return true;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes all the views and hides necessary information by default.
     */
    public void initalizeView() {
        this.toolbar = findViewById(R.id.finance_toolbar);
        this.noMethodsView = findViewById(R.id.finance_noPaymentsFound);
        this.paypalAccountsRecyclerView = findViewById(R.id.finance_paypal_recyclerView);
        this.creditCardsRecylerView = findViewById(R.id.finance_creditCards_recyclerView);
        this.creditCardLayout = findViewById(R.id.finance_creditCards_container);
        this.paypalLayout = findViewById(R.id.finance_paypal_container);

        this.toolbar.setTitle("My Payment Methods");
        this.toolbar.setNavigationIcon(R.drawable.back_arrow);
        this.setSupportActionBar(this.toolbar);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.noMethodsView.setVisibility(View.VISIBLE);
        this.paypalLayout.setVisibility(View.GONE);
        this.creditCardLayout.setVisibility(View.GONE);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the data has integrity.
     * @return TRUE if has integrity, FALSE otherwise
     */
    public boolean hasDataIntegrity() {
        boolean hasIntegrity = true;
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            hasIntegrity = false;
            Log.d(TAG, "Firebase Auth : current user is null!");
        }
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            hasIntegrity = false;
            Log.d(TAG, "Current user is null!");
        }
        else {
            if (!ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                hasIntegrity = false;
                Log.d(TAG, "Current user is not valid!");
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Puts the required data in the views. Keep in mind, this method does not run on the main thread.
     * @param customerFound - If the Braintree customer is found, then we have additional considerations.
     */
    public void initDataInView(boolean customerFound) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (customerFound && uCustomer != null) {
                                final List<PayPalAccount> payPalAccounts = uCustomer.getPayPalAccounts();
                                final List<CreditCard> creditCards = uCustomer.getCreditCards();
                                PaymentMethodsAdapter paypalAdapter = new PaymentMethodsAdapter(
                                        creditCards,
                                        payPalAccounts,
                                        true,
                                        context
                                );
                                PaymentMethodsAdapter creditCardsAdapter = new PaymentMethodsAdapter(
                                        creditCards,
                                        payPalAccounts,
                                        false,
                                        context
                                );

                                if (payPalAccounts != null && payPalAccounts.size() > 0) {
                                    paypalAccountsRecyclerView.setAdapter(paypalAdapter);
                                    paypalAccountsRecyclerView.setLayoutManager(layoutManager1);
                                    paypalLayout.setVisibility(View.VISIBLE);
                                    noMethodsView.setVisibility(View.GONE);
                                }
                                if (creditCards != null && creditCards.size() > 0) {
                                    creditCardsRecylerView.setAdapter(creditCardsAdapter);
                                    creditCardsRecylerView.setLayoutManager(layoutManager2);
                                    creditCardLayout.setVisibility(View.VISIBLE);
                                    noMethodsView.setVisibility(View.GONE);
                                }
                            }
                            else {
                                // Create a new customer with no payment methods so they can add methods to Braintree
                                server.createCustomer(new BraintreeCustomer(user));
                            }
                        }
                        catch (Exception ex) {
                            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                            Log.d(TAG, ex.getMessage());
                        }
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the main thread.
     * @param msg
     */
    public void toastOnUiThread(String msg) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * BRAINTREE SERVER CALLBACK METHODS
     * ============================================================================================
     */
    @Override
    public void onServerReady(boolean isReady) {
        if (isReady && this.server.isReady()) {
            this.server.findCustomer(user.getuName());

            try {
                final String tokenizationKey = this.server.getDataModel().getTokenizationKey();
                this.mBraintreeFragment = BraintreeFragment.newInstance(this, tokenizationKey);
            }
            catch (Exception ex) {
                Log.d(TAG, "Braintree fragment could not be initialized :\n" + ex.getLocalizedMessage());
            }
        }
        else {
            Log.d(TAG, "The server was not initialized properly");
            Toast.makeText(context, "Something went wrong here...", Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This gets hit when the current user has never been here before. They become a customer
     * automatically once they enter the activity (as long as everything goes accordingly).
     * Once they are a customer, we will refresh the view so that we can initialize this.uCustomer.
     * Even though the view won't change, we still get to initialize our local customer object,
     * so all is well.
     *
     * @param customer
     * @param wasCreated
     */
    @Override
    public void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated) {
        if (wasCreated && customer != null) {
            Log.d(TAG, "New customer created : " + customer.getCustomerId());
            this.server.findCustomer(customer.getCustomerId());
        }
        else {
            Log.d(TAG, "There was a problem creating a new customer");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onCustomerFound(Customer customer, boolean wasFound) {
        if (wasFound && customer != null) {
            this.uCustomer = customer;
            this.initDataInView(true);
        }
        else {
            this.uCustomer = null;
            this.initDataInView(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onTransactionComplete(BraintreeTransaction transaction, boolean wasSuccessful) {
        if (transaction != null && wasSuccessful) {
            this.server.voidTransaction(transaction);
        }
        else {
            Log.d(TAG, "Failed to complete transaction");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onTransactionVoided(String transactionID, boolean wasVoided) {
        if (transactionID != null && wasVoided) {
            toastOnUiThread("Successfully added new payment method!");
            this.server.findCustomer(this.user.getuName());
        }
        else {
            Log.d(TAG, "Failed to void transaction");
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void submitPayment(Float amount, String clientToken) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken);
        Intent intent = dropInRequest.getIntent(this);
        startActivityForResult(intent, PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onTransactionFound(Transaction transaction, boolean wasFound) {
        // Do nothing!
    }
    //---------------------------------------------------------------------------------------------


    /**
     * Gets called when a payment method nonce is created.
     *
     * @param nonce
     * @param wasCreated
     */
    @Override
    public void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * ANDROID LIFECYCLE METHODS
     * ============================================================================================
     */
    @Override
    public void onStart() {
        super.onStart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRestart() {
        super.onRestart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        super.onPause();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This result will be used to execute the transaction after the user has specified the
     * amount they wish to pay and the payment method (Paypal OR credit card).
     *
     * So what's going on here? I am making the user submit a payment of $0.01 so this payment
     * method will be added to their account. Once the payment submits, I will immediately void
     * it, so they shouldn't get charged... Though I am not sure if this will work.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                try {
                    if (result != null) {
                        final Float amount = new Double(0.01).floatValue();
                        final String nonce = result.getPaymentMethodNonce().getNonce();

                        this.server.executeTransaction(nonce, amount);
                    }
                    else {
                        Toast.makeText(context, "Failed to add payment method", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception ex) {
                    Log.d(TAG, ex.getLocalizedMessage());
                    Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(context, "You must select a payment method to add a new one", Toast.LENGTH_LONG).show();
            }
            else {
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.d(TAG, error.getLocalizedMessage());
            }
        }
    }
    //---------------------------------------------------------------------------------------------
}
