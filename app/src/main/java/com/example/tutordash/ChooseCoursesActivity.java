package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.adapter.EligibleCoursesAdapter;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.dialog.DataDisappearDialog;
import com.example.tutordash.dialog.SubmitCoursesDialog;
import com.example.tutordash.model.CourseEntry;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This activity is designed to update the courses that a tutor offers.
 */
public class ChooseCoursesActivity extends AppCompatActivity {

    public final String TAG = "ChooseCoursesActivity";

    TextView promptClassesView;
    RecyclerView eligibleCoursesRecyclerView;
    Button submitCoursesBtn;
    Button selectAllBtn;
    Button deselectAllBtn;

    DocumentReference userRef;
    CollectionReference coursesRef;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore database;

    public List<CourseEntry> coursesToSubmit;
    public EligibleCoursesAdapter viewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_courses);

        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        userRef = database.collection("users").document(ActivityUtils.CURRENT_USERNAME);
        coursesRef = userRef.collection("courses");

        promptClassesView = findViewById(R.id.promptClassesView);
        eligibleCoursesRecyclerView = findViewById(R.id.eligibleCoursesRecyclerView);
        submitCoursesBtn = findViewById(R.id.submitCoursesBtn);
        selectAllBtn = findViewById(R.id.selectAllBtn);
        deselectAllBtn = findViewById(R.id.deselectAllBtn);

        final ArrayList<String> ELIGIBLE_COURSES = ActivityUtils.ELIGIBLE_COURSES;
        if (ActivityUtils.ELIGIBLE_COURSES.size() == 0) {
            String msg = "Sorry, you are not eligible to tutor any of your previously taken courses.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
        else {
            ActivityUtils.ELIGIBLE_COURSES = new ArrayList<String>(); // Reset in case they go back...
            List<CourseEntry> courses = new ArrayList<CourseEntry>();
            try {
                for (String course : ELIGIBLE_COURSES) {
                    courses.add(new CourseEntry(course));
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            ArrayList<String> courseIds = new ArrayList<String>();
            for (CourseEntry course : courses) {
                courseIds.add(course.getCourseId());
            }

            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            EligibleCoursesAdapter adapter;
            if (firebaseAuth.getCurrentUser() != null) {
                ArrayList<String> alreadyOffered = new ArrayList<String>();
                for (String courseId : ActivityUtils.CURRENT_USER_OBJECT.coursesOffered().keySet()) {
                    alreadyOffered.add(courseId);
                }
                adapter = new EligibleCoursesAdapter(
                        getApplicationContext(),
                        courseIds,
                        alreadyOffered,
                        ActivityUtils.IS_TRANSCRIPT_MODE
                );
            }
            else {
                adapter = new EligibleCoursesAdapter(
                        getApplicationContext(),
                        courseIds,
                        new ArrayList<String>(),
                        ActivityUtils.IS_TRANSCRIPT_MODE
                );
            }
            eligibleCoursesRecyclerView.setAdapter(adapter);
            eligibleCoursesRecyclerView.setLayoutManager(manager);

            setSubmitButtonListener(adapter, courses);

            selectAllBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.selectAll();
                }
            });

            deselectAllBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.deselectAll();
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generic toast for failure of sorts.
     */
    public void alertFailure() {
        String msg = "An unexpected error occurred. Please restart the application.";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * First writes the courses to the user, and if this succeeds, it writes the courses to their
     * school in schoolList.
     *
     * What is transcript mode? So, when users get to this activity, they either got here by
     * uploading a transcript (first one or just a new one), OR they navigated here from their
     * account settings. If they are in transcript mode, then they can only add classes.
     * Classes that they already offer will be greyed out, and they WILL NOT be able to
     * select them (hence why they are disabled).
     *
     * If they are not in transcript mode, then these classes will appear selected rather
     * than disabled. In this mode, they can deselect previously-selected courses. So, when
     * they are NOT in transcript mode, it is more complicated since we have to deal with the
     * fact that they may be removing previously offered courses. We DO NOT want to delete the
     * data though! If they decide later that they want to offer CS150 again, their hours,
     * pay-rate, and rating should all be preserved.
     *
     * @param allCourses -- All the courses in view from the adapter (eligible courses).
     * @param selectedCourses -- Either they are new courses we want to add or are already offering.
     * @param disabledCourses -- Courses that we are already offering.
     */
    public void writeCourses(
            ArrayList<String> allCourses,
            ArrayList<String> selectedCourses,
            ArrayList<String> disabledCourses)
    {
        // First, update the user's courses to be what they want based on selections.
        // Unsubscribe since things may have changed (re-sub later)
        ActivityUtils.CURRENT_USER_OBJECT.unsubscribeToDisableTuteeNotifications();
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        if (cUser.getCourses() == null || cUser.getCourses().size() == 0) {
            cUser.setCourses(new HashMap<String, Course>());
            for (String c : allCourses) {
                Course course = new Course();
                course.setIsEligible(true);
                if (selectedCourses.contains(c) || disabledCourses.contains(c)) {
                    course.setIsOffered(true);
                }
                cUser.putNewCourse(c, course);
            }
        }
        else {
            for (String c : allCourses) {
                if (!cUser.getCourses().containsKey(c)) {
                    // It's a new course on our transcript
                    Course course = new Course();
                    course.setIsEligible(true);
                    if (selectedCourses.contains(c) || disabledCourses.contains(c)) {
                        course.setIsOffered(true);
                    }
                    cUser.putNewCourse(c, course);
                }
                else {
                    // Course already exists in our collection.
                    if (selectedCourses.contains(c) || disabledCourses.contains(c)) {
                        // It's either selected or previously offered. Make it offered.
                        cUser.getCourses().get(c).setIsOffered(true);
                    }
                    else {
                        // It's not selected and hasn't been offered. Make it not offered.
                        cUser.getCourses().get(c).setIsOffered(false);
                    }
                }
            }
        }
        cUser.setOfferedCourseStrings(new ArrayList<String>(cUser.coursesOffered().keySet()));
        ActivityUtils.CURRENT_USER_OBJECT = cUser; // Update CURRENT_USER_OBJECT
        // Re-subscribe to sync changes.
        ActivityUtils.CURRENT_USER_OBJECT.subscribeToReceiveTuteeNotifications();

        // Update cUser.uName/offeredCourseStrings field (overrides current data)
        database.collection("users").document(cUser.getuName())
                .update("offeredCourseStrings", cUser.getOfferedCourseStrings())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "Failed to update offeredCourseStrings for " + cUser.getuName());
                            alertFailure();
                        }
                        else {
                            String msg = "Thanks for signing up to tutor!";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                });

        // Update the collection of course documents in cUser.uName (need to get
        // what is currently there to decide what to write)
        database.collection("users").document(cUser.getuName()).collection("courses")
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    final Map<String, Course> coursesInDb =
                            ConversionUtils.getCoursesFromQuerySnapshot(task.getResult());

                    if (coursesInDb.size() == 1) { // BLANK document entry
                        // Empty course collection? Upload everything!
                        for (String c : cUser.getCourses().keySet()) {
                            database.collection("users").document(cUser.getuName()).collection("courses")
                                    .document(c).set(cUser.getCourses().get(c))
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (!task.isSuccessful()) {
                                                Log.d(TAG, "Could not create document : "
                                                        + "users/" + cUser.getuName()
                                                        + "/courses/" + c);
                                            }
                                        }
                                    });
                        }
                    }
                    else if (coursesInDb.size() != 0) {
                        // Some courses already exist. We need to cherry-pick
                        for (String c : cUser.getCourses().keySet()) {
                            if (!coursesInDb.containsKey(c)) {
                                // It's a new course, so we need to add it to DB
                                database.collection("users").document(cUser.getuName()).collection("courses")
                                        .document(c).set(cUser.getCourses().get(c))
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (!task.isSuccessful()) {
                                                    Log.d(TAG, "Could not create document : "
                                                            + "users/" + cUser.getuName()
                                                            + "/courses/" + c);
                                                }
                                            }
                                        });
                            }
                            else {
                                // It's an existing course. Does it need updates?
                                if (!(cUser.getCourses().get(c).isOffered() && coursesInDb.get(c).isOffered())) {
                                    // The course either was offered before, or is being offered now.
                                    database.collection("users").document(cUser.getuName()).collection("courses")
                                            .document(c).update("offered", cUser.getCourses().get(c).isOffered())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (!task.isSuccessful()) {
                                                        Log.d(TAG, "Could not update 'offered' "
                                                                + "for document : " + "users/"
                                                                + cUser.getuName() + "/courses/"
                                                                + c);
                                                    }
                                                }
                                            });
                                }
                                // Else? The isOffered status was the same. No need to update.
                            }
                        }
                    }
                    else {
                        Log.d(TAG, "Returned EMPTY course collection for " + cUser.getuName());
                        alertFailure();
                    }
                }
                else {
                    Log.d(TAG, "Failed to retrieve course collection for " + cUser.getuName());
                    alertFailure();
                }
            }
        });

        // Now that we have the user updated, sync the school courses array to ensure integrity.
        syncCoursesForSchool();

        if (ActivityUtils.IS_TRANSCRIPT_MODE) {
            startActivity(new Intent(getApplicationContext(), MainDiscoveryActivity.class));
        }
        ActivityUtils.IS_TRANSCRIPT_MODE = false; // Restore to default.
        finish();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Syncs the courses for the current user's school. This makes sure that all courses
     * in the courses array exist as long as at least one tutor is offering it.
     */
    public void syncCoursesForSchool() {
        final String school = ActivityUtils.CURRENT_USER_OBJECT.getSchool();

        // Go through all tutors at school, and add their offered courses to a global set.
        Set<String> tutoredCourses = new HashSet<String>();
        database.collection("users").whereEqualTo("school", school).whereEqualTo("tutor", true)
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    // No errors, but are we need to check if there are any tutors
                    if (task.getResult().size() == 0) {
                        // I don't think this should be possible since the current user
                        // just signed up to offer courses.
                        Log.d(TAG, "No tutors found at " + school + ". Is this correct?");
                    }
                    else {
                        // We have tutors! Get their courses that they are tutoring.
                        for (DocumentSnapshot tutor : task.getResult()) {
                            if (tutor.get("offeredCourseStrings") != null) {
                                List<String> tutorsCourses = (List<String>) tutor.get("offeredCourseStrings");
                                if (tutorsCourses != null && tutorsCourses.size() > 0) {
                                    tutoredCourses.addAll(tutorsCourses);
                                }
                            }
                            else {
                                // Are you using an old account without this attribute?
                                Log.d(TAG, "offeredCourseStrings field not found for user : " + tutor.getId());
                            }
                        }

                        // We have our set. Update that for the school. First we need to get
                        // that document though.
                        database.collection("schoolList").whereEqualTo("name", school)
                                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful() && task.getResult() != null && task.getResult().size() == 1) {
                                            // Need FOR loop to access data even though there's only 1 school.
                                            for (DocumentSnapshot university : task.getResult()) {
                                                // Perform update
                                                database.collection("schoolList").document(university.getId())
                                                        .update("courses", new ArrayList<String>(tutoredCourses))
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (!task.isSuccessful()) {
                                                                    alertFailure();
                                                                    Log.d(TAG, "Failed to update courses for "
                                                                            + "document : " + "schoolList/"
                                                                            + university.getId());
                                                                }
                                                                else {
                                                                    String msg = "Successfully updated courses"
                                                                            + " for " + school + "!";
                                                                    Toast.makeText(
                                                                            getApplicationContext(),
                                                                            msg,
                                                                            Toast.LENGTH_SHORT
                                                                    ).show();
                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                        else {
                                            alertFailure();
                                            Log.d(TAG, "There was an error retrieving a document " +
                                                    "with name \"" + school + "\" in schoolList");
                                        }
                                    }
                                });
                    }
                }
                else {
                    alertFailure();
                    Log.d(TAG, "Failed to retrieve tutors at " + school);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the listener for the courses submit button.
     *
     * @param adapter
     * @param courses
     */
    public void setSubmitButtonListener(EligibleCoursesAdapter adapter, List<CourseEntry> courses) {
        submitCoursesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coursesToSubmit = courses;
                viewAdapter = adapter;
                SubmitCoursesDialog dialog = new SubmitCoursesDialog();
                dialog.show(getSupportFragmentManager(), "SubmitCoursesDialog");
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Code that executes in order to submit the courses.
     */
    public void submitCourses() {
        final ArrayList<String> allCourses =
                ConversionUtils.renderCourseListToStringArrayList(this.viewAdapter.getAllCourses());
        final ArrayList<String> selectedCourses =
                ConversionUtils.renderCourseListToStringArrayList(this.viewAdapter.getSelectedCourses());
        final ArrayList<String> disabledCourses =
                ConversionUtils.renderCourseListToStringArrayList(this.viewAdapter.getDisabledCourses());

        // This follows the policy of "once you are a tutor, you are always a tutor".
        // If this changes in the future, it may be difficult to implement.
        if (selectedCourses.size() == 0) {
            String msg = "Please select at least one course that you would like to tutor.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
        else {
            if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                String msg = "An error occurred while trying to identify current user. Please try again later.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
            else {
                // They just submitted courses to tutor. Update isTutor status.
                if (!ActivityUtils.CURRENT_USER_OBJECT.isTutor()) {
                    database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName())
                            .update("tutor", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ActivityUtils.CURRENT_USER_OBJECT.setTutor(true);
                                    }
                                    else {
                                        Log.d(TAG, "Could not update tutor field for current user");
                                    }
                                }
                            });
                }
                writeCourses(allCourses, selectedCourses, disabledCourses);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Warns the user that the data will disappear if they go back. By the time this data
     * renders, it is already gone from ActivityUtils, so no need to delete it onBackPressed.
     */
    @Override
    public void onBackPressed() {
        DataDisappearDialog dialog = new DataDisappearDialog();
        dialog.show(getSupportFragmentManager(), "DataDisappearDialog");
    }
    //---------------------------------------------------------------------------------------------
}
