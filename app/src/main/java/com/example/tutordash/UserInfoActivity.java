package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.User;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * When the user clicks on the "User Info" card on the member profile, they will be
 * navigated here. Depending on whose profile is in view will determine how it renders.
 */
public class UserInfoActivity extends AppCompatActivity {

    public static final String TAG = "UserInfoActivity";

    // TODO define view types here

    private User user;
    private boolean isCurrentUser;
    private Context context;

    private FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        this.context = this.getApplicationContext();
        this.database = FirebaseFirestore.getInstance();

        setCurrentUserData();
        if (hasIntegrity()) {
            // TODO bind view types to XML

            bindDataToView();
        }
        else {
            String msg = "Something went wrong. Please try again later.";
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * By this point, this.user has been initialized with the proper object depending on the
     * string extra passed to this activity. This method checks for the integrity before
     * trying to initialize the view.
     *
     * @return TRUE if data has integrity, FALSE otherwise.
     */
    private boolean hasIntegrity() {
        boolean hasIntegrity = true;
        if (this.user == null) {
            hasIntegrity = false;
            Log.d(TAG, "The current user was null!");
        }
        else if (!this.user.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "The current user was not valid!");
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines which user's data to user (Current user or another user?).
     * Updates this.isCurrentUser and this.user object.
     */
    private void setCurrentUserData() {
        this.isCurrentUser = false;
        this.user = ActivityUtils.TMP_USR;

        Bundle intentExtras = getIntent().getExtras();
        if (intentExtras != null) {
            String isCurrUser = intentExtras.getString("IS_CURRENT_USER");
            if (isCurrUser != null && isCurrUser.equals("true")) {
                this.isCurrentUser = true;
                this.user = ActivityUtils.CURRENT_USER_OBJECT;
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    // TODO bind data to view
    private void bindDataToView() {
        if (this.isCurrentUser) {

        }
        else {

        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        //setCurrentUserData();
        //indDataToView();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        super.onPause();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //---------------------------------------------------------------------------------------------
}
