package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.FormatValidationUtils;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

public class ChangePasswordActivity extends AppCompatActivity {

    public static final String TAG = "ChangePasswordActivity";

    FirebaseAuth auth;
    EditText oldPasswordEdit;
    EditText newPasswordEdit1;
    EditText newPasswordEdit2;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        auth = FirebaseAuth.getInstance();
        checkUserIntegrity();

        oldPasswordEdit = findViewById(R.id.oldPasswordEdit);
        newPasswordEdit1 = findViewById(R.id.newPasswordEdit);
        newPasswordEdit2 = findViewById(R.id.confirmNewPasswordEdit);
        submit = findViewById(R.id.submitPasswordChange);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setClickable(false);
                validateData();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Performs integrity and validation checks on the data.
     */
    private void validateData() {
        boolean isValid = true;

        final User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        final String email = cUser.getEmail();
        final String uName = cUser.getuName();

        final String oldPassword = oldPasswordEdit.getText().toString().trim();
        final String newPassword1 = newPasswordEdit1.getText().toString().trim();
        final String newPassword2 = newPasswordEdit2.getText().toString().trim();

        if (!newPassword1.equals(newPassword2)) {
            isValid = false;
            newPasswordEdit1.setError("Passwords Do Not Match");
            newPasswordEdit2.setError("Passwords Do Not Match");
        }
        if (oldPassword.isEmpty() || !FormatValidationUtils.passwordIsValidFormat(oldPassword, uName, email)) {
            isValid = false;
            if (oldPassword.isEmpty()) {
                oldPasswordEdit.setError("Required Field");
            }
            else {
                oldPasswordEdit.setError("Invalid Password");
            }
        }
        if (newPassword1.isEmpty() || !FormatValidationUtils.passwordIsValidFormat(newPassword1, uName, email)) {
            isValid = false;
            if (newPassword1.isEmpty()) {
                newPasswordEdit1.setError("Required Field");
            }
            else {
                newPasswordEdit1.setError("Invalid Password");
            }
        }
        if (newPassword2.isEmpty() || !FormatValidationUtils.passwordIsValidFormat(newPassword2, uName, email)) {
            isValid = false;
            if (newPassword2.isEmpty()) {
                newPasswordEdit2.setError("Required Field");
            }
            else {
                newPasswordEdit2.setError("Invalid Password");
            }
        }

        if (isValid) {
            submitPasswordChange(oldPassword, newPassword1);
        }
        else {
            submit.setClickable(true);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to reset the password.
     * @param newPassword
     */
    private void submitPasswordChange(String oldPassword, String newPassword) {
        AuthCredential credential = EmailAuthProvider.getCredential(
                ActivityUtils.CURRENT_USER_OBJECT.getAuthEmail(),
                oldPassword
        );

        auth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    if (!oldPassword.equals(newPassword)) {
                        auth.getCurrentUser().updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    String msg = "Password Successfully Updated!";
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                                else {
                                    // I can't think of a reason why this would fail
                                    String msg = "An error occurred. Please try again later.";
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    String ex = "";
                                    if (task.getException() != null) {
                                        ex = task.getException().getMessage();
                                    }
                                    Log.d(TAG, "Failed to complete password update :\n" + ex);
                                    submit.setClickable(true);
                                }
                            }
                        });
                    }
                    else {
                        // Can't change password if old == new.
                        String msg = "Your new password cannot be your current password!";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        newPasswordEdit1.setError("New Password Cannot Be Current Password");
                        newPasswordEdit2.setError("New Password Cannot Be Current Password");
                        submit.setClickable(true);
                    }
                }
                else {
                    // Could not re-authenticate current user. Is their password current password correct?
                    String msg = "Incorrect Password";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Failed to re-authenticate current user");
                    oldPasswordEdit.setError("Incorrect Password");
                    submit.setClickable(true);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes sure current user is not null.
     */
    private void checkUserIntegrity() {
        if (auth.getCurrentUser() != null) {
            if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                String msg = "An error occurred. Please logout and restart the application.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Current user object was not valid. Object may have been null.");
                finish();
            }
        }
        else {
            String msg = "An error occurred. Please logout and restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "auth.getCurrentUser was null!");
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------
}
