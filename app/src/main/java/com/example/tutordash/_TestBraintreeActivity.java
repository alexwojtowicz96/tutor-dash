package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.braintreegateway.Customer;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.BraintreeFauxServer;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * This activity is used as a demo for the Braintree functionality. FYI, we are supposed to
 * have a server. If this ends up working, it is EXTREMELY insecure since all sensitive data
 * gets passed around on the client rather than on the server!
 *
 * Here is some test data that can be used for the credit card validation. All expiration dates
 * need to be anything set in the future:
 *
 * VALID CARDS
 * -----------
 * 3782 8224 6310 005  - AMERICAN EXPRESS
 * 3625 9600 0000 04   - DINER'S CLUB
 * 6011 1111 1111 1117 - DISCOVER
 * 3530 1113 3330 0000 - JCB
 * 6304 0000 0000 0000 - MAESTRO
 * 5555 5555 5555 4444 - MASTERCARD
 * 4111 1111 1111 1111 - VISA
 * -----------
 *
 * INVALID CARDS
 * -------------
 * 3787 3449 3671 000  - AMERICAN EXPRESS (Processor declined)
 * 4000 1111 1111 1115 - VISA             (Processor declined)
 * 5105 1051 0510 5100 - MASTERCARD       (Processor declined)
 * 6011 0009 9013 9424 - DISCOVER         (Processor declined)
 * 3566 0020 2036 0505 - JCB              (Failed [3000])
 * -------------
 *
 * From what I understand, we can just use the sandbox paypal for all paypal transactions. I'm
 * not sure if this means that every user is using the same paypal account or if it means
 * that it is just a random paypal for each user when they click on the submit the transaction.
 */
public class _TestBraintreeActivity extends AppCompatActivity implements BraintreeActivity {

    public static final String TAG = "_TestBraintreeActivity";

    public BraintreeFragment mBraintreeFragment;    // Popup fragment with the payment options
    public Button submitBtn;                        // Triggers the transaction to be initialized
    public EditText amountView;                     // Where the user types in the amount ($dollars)

    public FirebaseFirestore database;              // Database reference
    public Context context;                         // this.getApplicationContext() shorthand
    public BraintreeFauxServer server;              // The fake "server" that handles transactions
    private Float amountToPay = Float.valueOf(-1);  // It is extremely important that is is correct!
    //---------------------------------------------------------------------------------------------

    /**
     * onCreate() just try to initialize the basic stuff. Don't worry about the data integrity.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity___test_braintree);

        this.database = FirebaseFirestore.getInstance();
        this.context = this.getApplicationContext();
        this.server = new BraintreeFauxServer(this);
        this.submitBtn = findViewById(R.id.payment_demo_submit);
        this.amountView = findViewById(R.id.payment_demo_amount);

        setSubmitListener();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * From BraintreeActivity interface.
     *
     * Called in this.server when onServerReady() is called as long as the server is
     * in fact ready to be used.
     */
    @Override
    public void onServerReady(boolean isReady) {
        if (isReady) {
            Toast.makeText(context, "Server is ready", Toast.LENGTH_SHORT).show();
            try {
                final String tokenizationKey = server.getDataModel().getTokenizationKey();
                mBraintreeFragment = BraintreeFragment.newInstance(this, tokenizationKey);

                // TODO : TEMPORARY
                //server.createCustomer(new BraintreeCustomer(ActivityUtils.CURRENT_USER_OBJECT));
                //server.findCustomer(ActivityUtils.CURRENT_USERNAME);
            }
            catch (Exception ex) {
                Log.d(TAG, ex.getLocalizedMessage());
                String msg = "There was a problem : " + ex.getLocalizedMessage();
                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(context, "Server could not be prepared", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onServerReady() was called, and the response was isReady = FALSE");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the submit listener. Values typed in the box must be valid.
     */
    public void setSubmitListener() {
        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (server.isReady()) {
                    String rawAmount = amountView.getText().toString();
                    if (rawAmount.isEmpty()) {
                        Toast.makeText(context, "No amount specified", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        try {
                            Float serialAmount = Float.parseFloat(ActivityUtils.getFormattedFloat(Float.parseFloat(rawAmount)));
                            if (serialAmount.equals(Float.valueOf(0)) || serialAmount < 0) {
                                Toast.makeText(context, "Not a valid amount!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                String uName = ActivityUtils.CURRENT_USERNAME;
                                amountToPay = serialAmount;
                                server.initPayment(uName, serialAmount);
                            }
                        }
                        catch (Exception ex) {
                            Log.d(TAG, "Failed to get amount : " + ex.getLocalizedMessage());
                            Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else {
                    String msg = "Please wait... The server is not ready yet";
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * From BraintreeActivity interface
     *
     * @param amount - Amount to submit
     * @param clientToken - Token generated on the faux "server" that configures the transaction
     */
    public void submitPayment(Float amount, String clientToken) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken);
        Intent intent = dropInRequest.getIntent(this);
        startActivityForResult(intent, PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This result will be used to execute the transaction after the user has specified the
     * amount they wish to pay and the payment method (Paypal OR credit card).
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_PAYMENT_SUBMIT) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                Toast.makeText(context, "Result = OK", Toast.LENGTH_LONG).show();
                try {
                    if (result != null && this.amountToPay > 0) {
                        final Float amount = this.amountToPay;
                        final String nonce = result.getPaymentMethodNonce().getNonce();
                        this.amountToPay = Float.valueOf(-1);

                        server.executeTransaction(nonce, amount);
                    }
                    else {
                        Toast.makeText(context, "Failed to submit payment", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception ex) {
                    Log.d(TAG, ex.getLocalizedMessage());
                    Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(context, "Result = CANCELLED", Toast.LENGTH_LONG).show();
            }
            else {
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.d(TAG, error.getLocalizedMessage());
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the a customer object called in server.findCustomer() is available.
     * @param customer
     * @param wasFound
     */
    @Override
    public void onCustomerFound(Customer customer, boolean wasFound) {
        // Do nothing!
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a customer is created in Braintree.
     * @param customer
     * @param wasCreated
     */
    @Override
    public void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated) {
        // Do nothing!
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a transaction completes.
     * @param transaction
     * @param wasCompleted
     */
    @Override
    public void onTransactionComplete(BraintreeTransaction transaction, boolean wasCompleted) {
        if (wasCompleted && transaction != null && transaction.findIfIsValidModel()) {
            CollectionReference transactions = database.collection("transactions");
            transactions.document(transaction.getTransactionID()).set(transaction)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                String msg = "Successfully added new transaction document to Firestore";
                                Log.d(TAG, msg + " : " + transaction.getTransactionID());
                                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                String msg = "Failed to add new document to transactions collection";
                                Log.d(TAG, msg + " : " + transaction.getTransactionID());
                                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        else {
            Log.d(TAG, "This newly created transaction model is not valid for some reason");
            String msg = "Failed to create new transaction in Firestore";
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a transaction is voided.
     * @param transactionID
     * @param wasVoided
     */
    @Override
    public void onTransactionVoided(String transactionID, boolean wasVoided) {
        if (transactionID != null && !transactionID.isEmpty() && wasVoided) {
            DocumentReference ref = database.collection("transactions").document(transactionID);
            ref.update("isVoid", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (!task.isSuccessful()) {
                        Log.d(TAG, "Could not update 'isVoid' for transactions document : " + transactionID);
                    }
                }
            });
        }
        else {
            String msg = "There was a problem. Could not void transaction";
            Log.d(TAG, msg + " : " + transactionID);
            Toast.makeText(context, msg + ".", Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onTransactionFound(Transaction transaction, boolean wasFound) {
        // Do nothing!
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a payment method nonce is created.
     *
     * @param nonce
     * @param wasCreated
     */
    @Override
    public void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated) {

    }
    //-----------------------------------------------------------------------------------------
}
