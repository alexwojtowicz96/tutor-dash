package com.example.tutordash.core;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.tutordash.EnterTokenActivity;
import com.example.tutordash.model.Token;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Use this to send emails. It will only work as long as our sender is a GMail with
 * valid creds and "Allow Less Secure Apps" option turned on.
 */
public class EmailService {

    public static final String TAG = "EmailService";

    private Context context;
    private String school;
    private String subject;
    private String message;

    public EmailService(
            Context context,
            String school,
            String subject,
            String message
    ) {
        this.context = context;
        this.school = school;
        this.subject = subject;
        this.message = message;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sends an email to a specified user from tutor dash support email for the specified
     * school defined in the email service object. If the school is not found to have
     * a support email, then this will not send the email obviously, and the user will be
     * prompted to restart the app. Honestly, if we offer support for a university, we
     * better have a support email in the database...
     *
     * @param to
     * @param isReset : Either it is a reset email or a confirmation email...
     */
    public void sendEmail(String to, boolean isReset) {
        FirebaseFirestore database = FirebaseFirestore.getInstance();

        DocumentReference supportEmail = database.collection("supportCreds").document(school);
        supportEmail.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot result = task.getResult();
                    if (result != null) {
                        if (result.get("email") != null && result.get("pwd") != null) {
                            String email = result.get("email").toString();
                            String pwd = result.get("pwd").toString();

                            Class destination;
                            if (isReset) {
                                destination = EnterTokenActivity.class;
                            }
                            else {
                                destination = null;
                            }
                            SendEmail sendEmail = new SendEmail(
                                    email,
                                    pwd,
                                    to,
                                    subject,
                                    message,
                                    context,
                                    destination
                            );
                            sendEmail.execute();
                        }
                        else {
                            Log.d(TAG, "Support creds for school '" + school + "' had a null value!");
                            toastError();
                        }
                    }
                    else {
                        Log.d(TAG, "Result was successful, but returned null for school : " + school);
                        toastError();
                    }
                }
                else {
                    Log.d(TAG, "Task failed. Could not fetch support creds for school : " + school);
                    toastError();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts an error to user.
     */
    private void toastError() {
        String msg = "An error occurred while trying to send email." +
                " Please restart the application and try again.";
        Toast.makeText(this.context, msg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This inner class will create serve as an async task to actually send the email.
     */
    private static class SendEmail extends AsyncTask<Void, Void, Void> {

        public final String TAG = "SendEmail_ES";

        private String email;
        private String pwd;
        private String to;
        private String subject;
        private String message;
        private Session session;
        private Context mContext;
        private Class destination;

        public SendEmail(
                String email,
                String pwd,
                String to,
                String subject,
                String message,
                Context context,
                Class destination
        ) {
            this.email = email;
            this.pwd = pwd;
            this.to = to;
            this.subject = subject;
            this.message = message;
            this.mContext = context;
            this.destination = destination;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String msg = "Message Sent! Please check your inbox.";
            Toast.makeText(this.mContext, msg, Toast.LENGTH_LONG).show();

            try {
                if (destination != null && this.mContext != null) {
                    Intent intent = new Intent(this.mContext, destination);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    this.mContext.startActivity(intent);
                }
                else {
                    Toast.makeText(this.mContext, "ERROR", Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception ex) {
                Log.d(this.TAG, ex.getMessage());
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            Properties props = new Properties();

            // This is specific to GMail (needed for sender's email)
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            try {
                this.session = Session.getDefaultInstance(
                        props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(email, pwd);
                            }
                        });

                if (this.session != null) {
                    MimeMessage mm = new MimeMessage(session);
                    mm.setFrom(new InternetAddress(this.email));
                    mm.addRecipient(Message.RecipientType.TO, new InternetAddress(this.to));
                    mm.setSubject(this.subject);
                    mm.setText(this.message);
                    Transport.send(mm);
                }
                else {
                    Log.d(this.TAG, "This session was null (not tutordash.core.Session)");
                }
            }
            catch (MessagingException e) {
                Log.d(this.TAG, e.toString());
                String msg = "An error occurred while sending email. Please try again later.";
                Toast.makeText(this.mContext, msg, Toast.LENGTH_LONG).show();
            }
            catch (Exception ex) {
                Log.d(TAG, ex.toString());
                String msg = "An unknown error occurred while sending email. Please try again later.";
                Toast.makeText(this.mContext, msg, Toast.LENGTH_LONG).show();
            }

            return null;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This is an email template that prompts the user to enter the specified token into the
     * app for password resetting purposes.
     *
     * @param token
     * @param user
     * @return
     */
    public static String passwordResetTemplate(Token token, User user) {
        StringBuilder b = new StringBuilder();

        String displayName = "Friend";
        if (user != null && user.isValidUser()) {
            displayName = user.getFullName();
        }
        b.append("Hello, " + displayName + "!\n\n");
        b.append("We received your request to reset your password at " + token.getTimestamp().toDate() + ". ");
        b.append("Please enter this token to continue with the reset. It will expire in " +
                token.getTimeout() + " minutes:\n\n" + token.getKey() + "\n\n");
        b.append("If you did not make this change, then please reply to this email with your ");
        b.append("Tutor Dash username, and we will take care of the matter for you.\n\n");
        b.append("Keep in mind that not all passwords are acceptable. Below is a list of the criteria:\n");
        b.append("\t*  Must be 8 - 20 characters long\n");
        b.append("\t*  Must Contain at least 1 capital letter\n");
        b.append("\t*  Must contain at least 1 lowercase letter\n");
        b.append("\t*  Must contain at least 1 numeric digit\n");
        b.append("\t*  Must not contain your username or email\n\n");
        b.append("- The Tutor Dash Team");

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Use this as the email confirmation template.
     *
     * @param token
     * @param user
     * @return
     */
    public static String emailConfirmationTemplate(Token token, User user) {
        StringBuilder b = new StringBuilder();

        String displayName = "Friend";
        if (user != null && user.isValidUser()) {
            displayName = user.getFullName();
        }
        b.append("Hi, " + displayName + "!\n\n");
        b.append("Welcome to Tutor Dash! We're excited to have you join our community. ");
        b.append("In order for you to use our service, you are going to need to confirm your ");
        b.append("email address by entering the token below:\n\n");
        b.append(token.getKey() + "\n\n");
        b.append("Please keep in mind that this token is only temporary and it will expire ");
        b.append("in " + token.getTimeout() + " minutes. We look forward to your participation!");
        b.append("\n\n- The Tutor Dash Team");

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------
}
