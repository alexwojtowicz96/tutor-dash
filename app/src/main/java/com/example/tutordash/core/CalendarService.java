package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Use this kind of like a calendar utility to convert dates and times to user-friendly formats.
 */
public class CalendarService {

    public static final String TAG = "CalendarService";

    public CalendarService() {}
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the months in a year (strings)
     * @return months
     */
    public static ArrayList<String> getMonths() {
        return new ArrayList<String>(Arrays.asList(
                "January",     // Month: 1;  Index: 0;
                "February",    // Month: 2;  Index: 1;
                "March",       // Month: 3;  Index: 2;
                "April",       // Month: 4;  Index: 3;
                "May",         // Month: 5;  Index: 4;
                "June",        // Month: 6;  Index: 5;
                "July",        // Month: 7;  Index: 6;
                "August",      // Month: 8;  Index: 7;
                "September",   // Month: 9;  Index: 8;
                "October",     // Month: 10; Index: 9;
                "November",    // Month: 11; Index: 10;
                "December"));  // Month: 12; Index: 11;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a string is a month.
     */
    public static boolean isMonth(String month) {
        ArrayList<String> months = getMonths();
        for (String m : months) {
            if (m.toUpperCase().trim().equals(month.toUpperCase().trim())) {
                return true;
            }
        }

        return false;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a string is a year (a recent year within the next or past thousand years).
     */
    public static boolean isYear(String year) {
        try {
            int yearInt = Integer.parseInt(year);
            if (yearInt > 1000 && yearInt < 10000) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception ex) {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets a list of days from 1 to max_days_in_month for a given month and year. We need year
     * because February could be a leap year.
     *
     * @param month
     * @param year
     * @return list of strings (1 to max_days_in_month). Each string is a number.
     */
    public static ArrayList<String> getDays(String month, int year) {
        ArrayList<String> days = new ArrayList<String>();

        month = month.toLowerCase().trim();
        char[] m = month.toCharArray();
        String firstLetter = new Character(m[0]).toString().toUpperCase();
        month = firstLetter;
        for (int i = 1; i < m.length; i++) {
            month += m[i];
        }

        GregorianCalendar calendar = new GregorianCalendar();
        final ArrayList<String> months = getMonths();
        final ArrayList<String> monthsWith30Days = new ArrayList<String>(Arrays.asList(
                months.get(3),
                months.get(5),
                months.get(8),
                months.get(10)
        ));
        final ArrayList<String> monthsWith31Days = new ArrayList<String>(Arrays.asList(
                months.get(0),
                months.get(2),
                months.get(4),
                months.get(6),
                months.get(7),
                months.get(9),
                months.get(11)
        ));

        if (monthsWith30Days.contains(month)) {
            for (int i = 1; i < 31; i ++) {
                days.add(new Integer(i).toString());
            }
        }
        else if (monthsWith31Days.contains(month)) {
            for (int i = 1; i < 32; i ++) {
                days.add(new Integer(i).toString());
            }
        }
        else if (month.equals(months.get(1))) {
            for (int i = 1; i < 29; i ++) {
                days.add(new Integer(i).toString());
            }
            if (calendar.isLeapYear(year)) {
                days.add("29");
            }
        }

        return days;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a year, month (full name of month), and day as strings, try to convert this
     * to a date object. If it doesn't work, then return null.
     *
     * @param year
     * @param month
     * @param day
     * @return date (or NULL if invalid format).
     */
    public static Date getDateFromStrings(String year, String month, String day) {
        Date date;
        try {
            String monthString;
            int monthInt = monthToInt(month);
            if (monthInt > 9) {
                monthString = new Integer(monthInt).toString();
            }
            else {
                monthString = "0" + new Integer(monthInt).toString();
            }
            final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            final String dateString = day.trim() + "-" + monthString + "-" + year.trim();

            date = formatter.parse(dateString);
        }
        catch (Exception ex) {
            Log.d(TAG, "Could not parse date : " + ex);
            return null;
        }

        return date;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This is the same as getDateFromStrings() except time is a parameter as well. Time is assumed
     * to take on the form of 1:15am (NOT serial military time).
     */
    public static Date getDateTimeFromStrings(String year, String month, String day, String time) {
        Date date;
        try {
            String monthString;
            int monthInt = monthToInt(month);
            if (monthInt > 9) {
                monthString = new Integer(monthInt).toString();
            }
            else {
                monthString = "0" + new Integer(monthInt).toString();
            }
            final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            final String dateString = day.trim() + "-" + monthString + "-" + year.trim();

            date = formatter.parse(dateString + " " + convertTimeToMilitaryTime(time));
        }
        catch (Exception ex) {
            Log.d(TAG, "Could not parse date time : " + ex);
            return null;
        }

        return date;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given all the parameters of time in strings, determine if this date is in the future.
     *
     * @param year
     * @param month
     * @param day
     * @param startTime
     * @return TRUE if future date, FALSE otherwise
     */
    public static boolean isFutureDate(
            String year,
            String month,
            String day,
            String startTime
    ) {
        boolean isFutureDate = false;

        String serialStartTime = convertTimeToMilitaryTime(startTime);
        String monthString;
        int monthInt = monthToInt(month);
        if (monthInt > 9) {
            monthString = new Integer(monthInt).toString();
        }
        else {
            monthString = "0" + new Integer(monthInt).toString();
        }

        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        final String dateStringBase = day.trim() + "-" + monthString + "-" + year.trim() + " ";
        final String dateStringStart = dateStringBase + serialStartTime;

        try {
            Date startDate = formatter.parse(dateStringStart);
            isFutureDate = startDate.after(new Date());
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
            return false;
        }

        return isFutureDate;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if two times on the same day have integrity.
     *
     * @param startTime
     * @param endTime
     * @return TRUE if startTime is before endTime
     */
    public static boolean timesHaveIntegrity(String startTime, String endTime) {
        try {
            String serialStartString = convertTimeToMilitaryTime(startTime);
            String serialEndString = convertTimeToMilitaryTime(endTime);
            long startMillis = getMillisecondsFromTimeString(serialStartString);
            long endMillis = getMillisecondsFromTimeString(serialEndString);

            return (endMillis - startMillis > 0);
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
            return false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a string formatted like 13:15:00, convert it to 1:15pm.
     *
     * @param militaryTimeString
     * @return formatted time string
     */
    public static String convertFromMilitaryTime(String militaryTimeString) {
        String timeString = "";
        try {
            String[] components = militaryTimeString.trim().split(":");
            if (components.length != 3) {
                throw new Exception("Unexpected input : " + militaryTimeString);
            }
            else {
                Integer hours = Integer.parseInt(components[0].trim());
                Integer minutes = Integer.parseInt(components[1].trim());
                String xm = "am";

                if (hours > 12) {
                    hours = hours - 12;
                    xm = "pm";
                }
                if (minutes.toString().length() == 1) {
                    xm = "0" + xm;
                }
                if (hours.equals(0)) {
                    hours = 12;
                }
                timeString = "" + hours + ":" + minutes + xm;
            }
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
            return "12:00am";
        }

        return timeString;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets a string from a date object (returns string formatted like "September 30, 2010").
     *
     * @param date
     * @return date as a string
     */
    public static String getFormattedDateFromDate(Date date) {
        String dateString = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy / HH:mm:ss");
            dateString = formatter.format(date).split("/")[0].trim();
        }
        catch (Exception ex) {
            Log.d(TAG, "Could not parse date to get date : " + ex);
            return "ERROR";
        }

        return dateString;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the time from a date object (returns string formatted like "1:30pm").
     *
     * @param date
     * @return time as a string
     */
    public static String getFormattedTimeFromDate(Date date) {
        String timeString = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy / HH:mm:ss");
            timeString = formatter.format(date).split("/")[1].trim();
            timeString = convertFromMilitaryTime(timeString);
        }
        catch (Exception ex) {
            Log.d(TAG, "Could not parse date to get time : " + ex);
            return "ERROR";
        }

        return timeString;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Takes a string like 13:15:00, and converts that to milliseconds. It MUST be in this
     * military time format.
     *
     * @param timeString
     * @return milliseconds
     */
    public static long getMillisecondsFromTimeString(String timeString) {
        try {
            String[] components = timeString.split(":");
            if (components.length != 3) {
                throw new Exception("Invalid timeString : " + timeString);
            }
            else {
                Long hours = Long.parseLong(components[0]);
                Long minutes = Long.parseLong(components[1]);
                Long seconds = Long.parseLong(components[2]);

                return (hours * 3600000) + (minutes * 60000) + (seconds * 1000);
            }
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
            return 0;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given two times in military time, get the time interval (in minutes) between start
     * and end.
     *
     * @param start
     * @param end
     * @return time in minutes (as integer)
     */
    public static int getTimeInterval(String start, String end) {
        long startMilliseconds = getMillisecondsFromTimeString(start);
        long endMilliseconds = getMillisecondsFromTimeString(end);
        long difference = endMilliseconds - startMilliseconds;

        return ((int) (difference / 60000));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns the number of seconds between two dates.
     *
     * @param date1
     * @param date2
     */
    public static long getSecondsBetweenDates(Date date1, Date date2) {
        long date1Millis = date1.getTime();
        long date2Millis = date2.getTime();
        long difference = Math.abs(date1Millis - date2Millis);

        return (Long.valueOf(difference) / Long.valueOf(1000));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a string like 1:15pm, convert it to 13:15:00.
     *
     * @param timeString
     * @return time string converted to miltary time string
     */
    public static String convertTimeToMilitaryTime(String timeString) {
        String militaryTime = "";
        try {
            if (timeString.endsWith("am") || timeString.endsWith("pm")) {
                String xm;
                if (timeString.endsWith("am")) {
                    xm = "am";
                }
                else {
                    xm = "pm";
                }
                timeString = timeString.replaceAll(xm, "");
                String[] pieces = timeString.split(":");
                if (pieces.length != 2) {
                    throw new Exception("Length of pieces was not 2 : " + pieces.length);
                }
                else {
                    // Convert HOURS
                    if (xm.equals("am")) {
                        int hours = Integer.parseInt(pieces[0]);
                        if (hours > 12 || hours < 0) {
                            throw new Exception("HOURS was not valid : " + pieces[0]);
                        }
                        else {
                            if (hours == 12) { // 12:00am is 00:00:00
                                pieces[0] = "00";
                            }
                            if (pieces[0].length() == 1) {
                                pieces[0] = "0" + pieces[0];
                            }
                            militaryTime += pieces[0] + ":";
                        }
                    }
                    else {
                        int hours = Integer.parseInt(pieces[0]);
                        if (hours > 12 || hours < 0) {
                            throw new Exception("HOURS was not valid : " + pieces[0]);
                        }
                        else {
                            if (Integer.parseInt(pieces[0]) != 12) {
                                pieces[0] = new Integer(Integer.parseInt(pieces[0]) + 12).toString();
                            }
                            militaryTime += pieces[0] + ":";
                        }
                    }
                    // Convert MINUTES
                    int minutes = Integer.parseInt(pieces[1]);
                    if (minutes > 60 || minutes < 0) {
                        throw new Exception("MINUTES was not valid : " + pieces[1]);
                    }
                    else {
                        if (pieces[1].length() == 1) {
                            pieces[1] = "0" + pieces[1];
                        }
                        militaryTime += pieces[1];
                    }
                    // Add SECONDS
                    militaryTime += ":00";
                }
            }
            else {
                throw new Exception("Could not find am or pm units");
            }
        }
        catch (Exception ex) {
            Log.d(TAG, "Failed to create military time from string : " + timeString + "\n" + ex);
            return ("00:00:00"); // 12:00am
        }

        return militaryTime;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts month string to integer.
     *
     * @param month
     * @return integer representing month
     */
    public static int monthToInt(String month) {
        month = month.toUpperCase().trim();
        int monthInt = 1;
        ArrayList<String> months = getMonths();

        for (int i = 0; i < months.size(); i++) {
            if (month.equals(months.get(i).trim().toUpperCase())) {
                monthInt = i + 1;
                break;
            }
        }

        return monthInt;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if these two dates fall on the same day (time is irrelevant).
     *
     * @param date1
     * @param date2
     * @return TRUE if same day, FALSE otherwise
     */
    public static boolean isSameDay(Date date1, Date date2) {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date1String = dateFormat.format(date1);
        String date2String = dateFormat.format(date2);

        return (date1String.trim().toLowerCase().equals(date2String.trim().toLowerCase()));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO : I need to look at this
     * Used in the Schedule Session Activity to check if a time range has conflicting slots.
     *
     * @param time1
     * @param time2
     * @param year
     * @param month
     * @param day
     * @return TRUE if not overlapping, FALSE otherwise.
     */
    public static boolean notOverlapping(
            String time1,
            String time2,
            String year,
            String month, String day
    ) {
        User u = ActivityUtils.TMP_USR;
        final Date selectedDate = getDateFromStrings(
                year,
                month,
                day
        );
        ArrayList<String> timeSlots = u.findAvailableTimeSlots(selectedDate);
        int itr = 0;
        boolean inSession = false;

        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 4; j++) {
                String slot = "";
                String xm;
                if (i < 12) { // am
                    xm = "am";
                    Integer hour = i;
                    if (hour.equals(0)) {
                        hour = 12;
                    }
                    slot += hour.toString();
                }
                else { // pm
                    xm = "pm";
                    Integer hour = i - 12;
                    if (hour.equals(0)) {
                        hour = 12;
                    }
                    slot += hour.toString();
                }
                Integer minutes = j * 15;
                String minutesString = minutes.toString();
                if (minutesString.length() == 1) {
                    minutesString = "0" + minutesString;
                }
                slot += ":" + minutesString;
                slot += xm;

                if (slot.equals(time1)) {
                    inSession = true;
                }
                if (inSession) {
                    if (timeSlots.get(itr).equals("-")) {
                        return false;
                    }
                }
                if (slot.equals(time2)) {
                    inSession = false;
                    return true;
                }
                itr++;
            }
        }

        return false;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Appends minutes to a date object and returns the new date as a date object.
     *
     * @param date
     * @param minutes
     * @return date + minutes
     */
    public static Date appendMinutesToDate(Date date, int minutes) {
        long millis = date.getTime();
        millis = (millis) + ((long) (minutes * 60000));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);

        return calendar.getTime();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a string like 01:23:45, convert it to minutes (84 minutes with rounding).
     *
     * @param time
     * @return minutes
     */
    public static int getMinutesFromTimeString(String time) {
        int minutes = 0;

        try {
            String[] components = time.split(":");
            int hours = Integer.parseInt(components[0].trim());
            int min = Integer.parseInt(components[1].trim());
            int sec = Integer.parseInt(components[2].trim());

            // Round up to the next minute
            if (sec >= 30) {
                min += 1;
            }
            minutes = (hours * 60) + min;
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
            minutes = 0;
        }

        return minutes;
    }
    //---------------------------------------------------------------------------------------------
}
