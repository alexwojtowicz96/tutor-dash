package com.example.tutordash.core;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.tutordash.core.vendor.codec.binary.Base64;
import com.example.tutordash.model.Session;

import java.util.ArrayList;
import java.util.List;
import com.example.tutordash.core.vendor.codec.digest.HmacUtils;
import com.example.tutordash.model.VidyoIoDataModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Decompiled from generateToken.jar found here:
 * https://vidyo.io/blog/how-to/using-token-authentication-embedded-video-conference-sessions/
 *
 * Adapted for our purposes. This will generate a token to be used as the key for
 * new Vidyo.io video sessions.
 */
public class VidyoTokenGenerator {

    public static final String TAG = "VidyoTokenGenerator";
    private static final String DELIM = "\u0000";

    private VidyoIoDataModel dataModel;
    private Context context;
    //---------------------------------------------------------------------------------------------

    public VidyoTokenGenerator(Context context) {
        this.context = context;
        this.dataModel = null;
        getAttributesFromFirestore();
    }
    //---------------------------------------------------------------------------------------------

    public Context getContext() {
        return this.context;
    }

    public VidyoIoDataModel getDataModel() {
        return this.dataModel;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setDataModel(VidyoIoDataModel dataModel) {
        this.dataModel = dataModel;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the developer key, application id, application name, and any other important
     * information regarding the vidyo.io membership stored in Firestore.
     */
    public void getAttributesFromFirestore() {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        DocumentReference ref = database.collection("supportCreds").document("vidyo.io");
        final String errMsg = "There was a problem setting up your web conference. Please try again later.";

        ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    try {
                        VidyoIoDataModel model = task.getResult().toObject(VidyoIoDataModel.class);
                        if (model != null && model.findIfModelIsValid()) {
                            dataModel = model;
                            if (!dataModel.findIfMembershipHasExpired()) {
                                Toast.makeText(context, "Ready to connect", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(context, "Vidyo.io membership has expired...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            throw new Exception("Vidyo.io data was found, but local model was invalid!");
                        }
                    }
                    catch (Exception ex) {
                        Log.d(TAG, ex.getLocalizedMessage());
                        Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    String ex = "";
                    if (task.getException() != null) {
                        ex = task.getException().getLocalizedMessage();
                    }
                    Log.d(TAG, "Could not get Vidyo.io data from Firestore =>\n" + ex);
                    Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * If the generator has the valid data model, then it is valid.
     * @return TRUE if valid, FALSE otherwise
     */
    public boolean isValid() {
        return (this.dataModel != null && this.dataModel.findIfModelIsValid());
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generates a provision token.
     *
     * @param key
     * @param jid
     * @param expires
     * @param vcard
     * @return provision token
     */
    public String generateProvisionToken(
            String key,
            String jid,
            String expires,
            String vcard
    ) {
        try {
            List<String> stringsToJoin = new ArrayList<String>();
            stringsToJoin.add(this.dataModel.getProvisionToken());
            stringsToJoin.add(jid);
            stringsToJoin.add(calculateExpiry(expires));
            stringsToJoin.add(vcard);

            final String payload = joinStrings(DELIM, stringsToJoin);

            stringsToJoin = new ArrayList<String>();
            stringsToJoin.add(payload);
            stringsToJoin.add(HmacUtils.hmacSha384Hex(key, payload));

            return new String(Base64.encodeBase64(joinStrings(DELIM, stringsToJoin).getBytes()));
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());

            return null;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Joins strings together for the purpose of generating a key to hash.
     *
     * @param betweenChars
     * @param stringsToJoin
     * @return joined strings
     */
    public String joinStrings(String betweenChars, List<String> stringsToJoin) {
        StringBuilder b = new StringBuilder();
        List<String> prunedStringsToJoin = new ArrayList<String>();

        if (stringsToJoin != null && stringsToJoin.size() > 0) {
            prunedStringsToJoin = stringsToJoin;
            for (int i = 0; i < prunedStringsToJoin.size(); i++) {
                final String curr = stringsToJoin.get(i);
                b.append(curr);
                if (i != prunedStringsToJoin.size() - 1) {
                    b.append(betweenChars);
                }
            }
        }

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Calculates expiry.
     *
     * @param expires
     * @return expiry as a string
     */
    public String calculateExpiry(String expires) {
        try {
            long expiresLong = 0L;
            long currentUnixTimestamp = System.currentTimeMillis() / 1000L;

            String serialNum = "";
            char[] expArr = expires.toCharArray();
            for (int i = 0; i < expArr.length; i++) {
                final char c = expArr[i];
                if (c == '.') {
                    break;
                }
                serialNum += c;
            }
            expiresLong = Long.parseLong(serialNum);

            return "" + (62167219200L + currentUnixTimestamp + expiresLong);
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
            return null;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Actually generates the token.
     *
     * @param userName
     * @return token
     */
    public String generateToken(String userName) {
        String key = this.dataModel.getDeveloperKey();
        String appID = this.dataModel.getApplicationID();
        String expiresInSeconds = new Float((Session.WAITING_WINDOW / 2) * 60).toString();
        String token = null;

        try {
            token = generateProvisionToken(key, userName + "@" + appID, expiresInSeconds, "");
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }

        return token;
    }
    //---------------------------------------------------------------------------------------------
}