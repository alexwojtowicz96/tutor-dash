package com.example.tutordash.core;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Container for all permission-related interactions.
 */
public class PermissionUtils {

    public static final String TAG = "PermissionUtils";

    public static final int REQUEST_CODE_FOR_TRANSCRIPT_UPLOAD = 28;
    public static final int REQUEST_CODE_FOR_FILE_SELECT = 52;
    public static final int REQUEST_CODE_FOR_AVATAR_UPLOAD = 33;
    public static final int REQUEST_CODE_FOR_LOCATION = 209;
    public static final int REQUEST_CODE_FOR_MICROPHONE = 41;
    public static final int REQUEST_CODE_FOR_CAMERA = 107;
    public static final int REQUEST_CODE_FOR_PAYMENT_SUBMIT = 847;
    //---------------------------------------------------------------------------------------------

    public PermissionUtils() {}

    /**
     * Checks if permission is granted to the app to view local device's file system
     *
     * @param context
     * @return TRUE if permission is granted, FALSE otherwise
     */
    public static boolean hasReadPermission(Context context) {
        String permissionString = Manifest.permission.READ_EXTERNAL_STORAGE;
        int permissionInt = PackageManager.PERMISSION_GRANTED;

        return ContextCompat.checkSelfPermission(context, permissionString) == permissionInt;
    }
    //---------------------------------------------------------------------------------------------

    public static int getAudioPermissionValue(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
    }
    //---------------------------------------------------------------------------------------------

    public static int getCameraPermissionValue(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
    }
    //---------------------------------------------------------------------------------------------

    public static int getFineLocationPermissionValue(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
    }
    //---------------------------------------------------------------------------------------------

    public static int getCourseLocationPermissionValue(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
    }
    //---------------------------------------------------------------------------------------------

    public static int getReadStoragePermissionValue(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
    }
    //---------------------------------------------------------------------------------------------
}
