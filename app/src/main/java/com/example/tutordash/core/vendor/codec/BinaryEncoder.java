// 
// Decompiled by Procyon v0.5.36
// 

package com.example.tutordash.core.vendor.codec;

public interface BinaryEncoder extends Encoder
{
    byte[] encode(final byte[] p0) throws EncoderException;
}
