package com.example.tutordash.core;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.tutordash.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Use this class to cache data on the user's device in the Tutor Dash asset folder.
 */
public class CachingService {

    public static final String TAG = "CachingService";

    private Context context;

    public CachingService(Context context) {
        this.context = context;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Saves the current user to internal storage.
     */
    public void saveCurrentUser() {
        if (ActivityUtils.CURRENT_USER_OBJECT != null && this.context != null) {
            try {
                File file = new File (this.context.getFilesDir().getCanonicalPath() + "/" + this.getCurrUserFileName());
                if (file.isFile() && file.exists()) {
                    file.delete();
                }
            }
            catch (IOException ex) {
                Log.d(TAG, ex.toString());
            }

            Gson gson = new Gson();
            String currUserJson = gson.toJson(ActivityUtils.CURRENT_USER_OBJECT);

            FileOutputStream fout = null;
            String fileName = this.getCurrUserFileName();
            try {
                fout = this.context.openFileOutput(fileName, Context.MODE_PRIVATE);
                fout.write(currUserJson.getBytes());
            }
            catch (FileNotFoundException ex) {
                Log.d(TAG, "In saveCurrentUser() => Could not find file : " + fileName + "\n" + ex);
            }
            catch (IOException ex) {
                Log.d(TAG, "In saveCurrentUser() => Caught IOException : \n" + ex);
            }
            finally {
                if (fout != null) {
                    try {
                        fout.close();
                    }
                    catch (IOException ex) {
                        Log.d(TAG, ex.toString());
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Loads the current user from memory. Returns true if the user was loaded, false otherwise.
     *
     * @return TRUE if successful load, FALSE otherwise
     */
    public boolean loadCurrentUserFromCache() {
        boolean wasLoaded = false;
        FileInputStream fin = null;

        String fileName = this.getCurrUserFileName();
        try {
            fin = this.context.openFileInput(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(fin);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder b = new StringBuilder();

            String line;
            int lineCounter = 0;
            while ((line = bufferedReader.readLine()) != null) {
                b.append(line + "\n");
                lineCounter += 1;
                if (lineCounter > 1) {
                    throw new Exception("Expected 1 line in file : " + fileName);
                }
            }

            Gson gson = new Gson();
            User currUser = new User();
            Type type = new TypeToken<User>(){}.getType();
            currUser = gson.fromJson(b.toString(), type);

            if (currUser != null && !currUser.equals(new User()) && currUser.isValidUser()) {
                ActivityUtils.CURRENT_USER_OBJECT = currUser;
                ActivityUtils.CURRENT_USER_SCHOOL = currUser.getSchool();
                ActivityUtils.CURRENT_USER_EMAIL = currUser.getEmail();
                ActivityUtils.CURRENT_USERNAME = currUser.getuName();

                if (ActivityUtils.CURRENT_USER_OBJECT != null &&
                        ActivityUtils.CURRENT_USER_OBJECT.isValidUser() &&
                        ActivityUtils.CURRENT_USERNAME != null &&
                        ActivityUtils.CURRENT_USER_EMAIL != null &&
                        ActivityUtils.CURRENT_USER_SCHOOL != null) {
                    wasLoaded = true;
                }
                else {
                    throw new Exception("User was found to be valid in cache, but for some reason, " +
                            "the values in ActivityUtils were not initialized correctly.");
                }
            }
            else {
                throw new Exception("Found file and parsed text, but could not load user from json");
            }
        }
        catch (FileNotFoundException ex) {
            Log.d(TAG, "While loading current users from cache, " +
                    "Could not find a file named : " + fileName + "\n" + ex);
        }
        catch (IOException ex) {
            Log.d(TAG, "While loading current user from cache, caught IOException : \n" + ex);
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
        finally {
            if (fin != null) {
                try {
                    fin.close();
                }
                catch (IOException ex) {
                    Log.d(TAG, ex.toString());
                }
            }
        }

        return wasLoaded;
    }
    //---------------------------------------------------------------------------------------------

    /** TODO
     * Saves an arbitrary string to a file.
     *
     * @param string
     * @param fileName
     *
     * @return TRUE if saved successfully, FALSE otherwise
     */
    public boolean saveString(String string, String fileName) {
        return true;
    }
    //---------------------------------------------------------------------------------------------

    /** TODO
     * Gets an arbitrary string from file.
     *
     * @param fileName
     * @return The entire contents of a file as a string
     */
    public String getString(String fileName) {
        return "";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Saves a list of users to internal storage.
     *
     * @param users
     * @param fileName
     */
    public void saveUserList(List<User> users, String fileName) {
        if (users != null && users.size() > 0 && fileName != null) {
            if (!fileName.equals(this.getCurrUserFileName())) {
                try {
                    File file = new File (this.context.getFilesDir().getCanonicalPath() + "/" + fileName);
                    if (file.isFile() && file.exists()) {
                        file.delete();
                    }
                }
                catch (IOException ex) {
                    Log.d(TAG, ex.toString());
                }

                FileOutputStream fout = null;
                try {
                    fout = this.context.openFileOutput(fileName, Context.MODE_PRIVATE);
                    Gson gson = new Gson();
                    StringBuilder b = new StringBuilder();
                    for (int i = 0; i < users.size(); i++) {
                        User u = users.get(i);
                        String userJson = gson.toJson(u);
                        b.append(userJson);
                        if (i != users.size() - 1) {
                            b.append("\n");
                        }
                    }
                    fout.write(b.toString().getBytes());
                }
                catch (FileNotFoundException ex) {
                    Log.d(TAG, "In saveUserList() => Could not find file : " + fileName + "\n" + ex);
                }
                catch (IOException ex) {
                    Log.d(TAG, "In saveUserList() => Caught IOException : \n" + ex);
                }
                finally {
                    if (fout != null) {
                        try {
                            fout.close();
                        }
                        catch (IOException ex) {
                            Log.d(TAG, ex.toString());
                        }
                    }
                }
            }
            else {
                Log.d(TAG, "This filename is taken. You cannot use it! : " + fileName);
            }
        }
        else {
            Log.d(TAG, "Cannot cache a list of users that doesn't exist!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Loads the user list stored in the supplied file name
     *
     * @param fileName
     *
     * @return NULL if no file is found or cannot be read. Otherwise, it returns the list
     * of users found in the file.
     */
    public ArrayList<User> loadUserListFromCache(String fileName) {
        boolean hasError = false;
        ArrayList<User> users = new ArrayList<User>();
        FileInputStream fin = null;
        try {
            fin = this.context.openFileInput(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(fin);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String line;
            Gson gson = new Gson();
            while ((line = bufferedReader.readLine()) != null) {
                if (!line.equals("")) {
                    User currUser = new User();
                    Type type = new TypeToken<User>(){}.getType();
                    currUser = gson.fromJson(line, type);
                    if (currUser != null && currUser.isValidUser()) {
                        users.add(currUser.initNullValues());
                    }
                    else {
                        hasError = true;
                        Log.d(TAG, "User JSON could not be parsed into user object.");
                        break;
                    }
                }
            }
        }
        catch (FileNotFoundException ex) {
            Log.d(TAG, "While loading current users from cache, " +
                    "Could not find a file named : " + fileName + "\n" + ex);
        }
        catch (IOException ex) {
            Log.d(TAG, "While loading current user from cache, caught IOException : \n" + ex);
        }
        finally {
            if (fin != null) {
                try {
                    fin.close();
                }
                catch (IOException ex) {
                    Log.d(TAG, ex.toString());
                }
            }
        }

        if (users.equals(new ArrayList<User>()) || hasError) {
            Log.d(TAG, "There was an error loading the list from memory. Returned null value.");
            return null;
        }
        else {
            return users;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a file name, delete it if it exists. This file must exist in the directory for our app.
     * You can get this by calling this.context.getFilesDir().getCanonicalFile().toString().
     *
     * @param fileName
     */
    public void invalidateCacheByFilename(String fileName) {
        try {
            File file = new File(this.context.getFilesDir().getCanonicalFile().toString() + "/" + fileName);
            if (file.isFile() && file.exists()) {
                if (!file.delete()) {
                    Log.d(TAG, "Could not delete file : " + fileName);
                }
            }
            else {
                Log.d(TAG, "File could not be found. Are you sure it exists in " +
                        "this.context.getFilesDir().getCanonicalFile().toString()? : " + fileName);
            }
        }
        catch (IOException ex) {
            Log.d(TAG, "Error trying to get files directory for app : \n" + ex);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Deletes the file that is being used to cache the current user's data.
     */
    public void invalidateCurrentUserCache() {
        String fileName = this.getCurrUserFileName();
        this.invalidateCacheByFilename(fileName);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the filename for the current user to be stored in.
     *
     * @return fileName
     */
    @NonNull
    public String getCurrUserFileName() {
        return "tutor_dash_current_user.dat";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given the name of a school, this will get the name of the file to store something in...
     * Use this in tandem with loadUserList and saveUserList in order to ensure that if another
     * user from another school signs in, we are not pulling the wrong list of users.
     *
     * @param schoolName
     *
     * @return fileName
     */
    @NonNull
    public String getFileForCachedUsersBySchool(String schoolName) {
        return "tutor_dash_users_at_" + schoolName.toLowerCase().trim().replace(" ", "_") + ".dat";
    }
    //---------------------------------------------------------------------------------------------
}
