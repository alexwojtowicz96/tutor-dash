// 
// Decompiled by Procyon v0.5.36
// 

package com.example.tutordash.core.vendor.codec;

public interface StringEncoder extends Encoder
{
    String encode(final String p0) throws EncoderException;
}
