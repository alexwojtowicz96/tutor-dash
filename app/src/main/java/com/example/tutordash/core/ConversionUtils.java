package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.Course;
import com.example.tutordash.model.RenderCourseModel;
import com.example.tutordash.model.Session;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is a workaround. Some objects don't serialize properly when pulling them from
 * Firebase, so some models need to be manually checked before attempting to assign their
 * data values to their respective local models.
 */
public class ConversionUtils {

    public static final String TAG = "ConversionUtils";

    /**
     * Converts the courses collection to the map of courses that should be stored in the
     * User model.
     *
     * @param querySnapshot
     * @return courses
     */
    public static Map<String, Course> getCoursesFromQuerySnapshot(QuerySnapshot querySnapshot) {
        Map<String, Course> courses = new HashMap<>();
        if (querySnapshot != null && querySnapshot.size() > 0) {
            for (DocumentSnapshot courseDoc : querySnapshot) {
                if (courseDoc != null) {
                    boolean isOffered = false;
                    boolean isEligible = false;
                    Number hours = 0;
                    Number payRate = 0;
                    Number rating = 0;

                    Map<String, Object> dataMap = courseDoc.getData();
                    if (dataMap != null && dataMap.size() > 0) {
                        try {
                            if (dataMap.containsKey("offered")) {
                                isOffered = Boolean.parseBoolean(dataMap.get("offered").toString());
                            }
                            if (dataMap.containsKey("eligible")) {
                                isEligible = Boolean.parseBoolean(dataMap.get("eligible").toString());
                            }
                            if (dataMap.containsKey("hours")) {
                                hours = Double.parseDouble(dataMap.get("hours").toString());
                            }
                            if (dataMap.containsKey("payRate")) {
                                payRate = Double.parseDouble(dataMap.get("payRate").toString());
                            }
                            if (dataMap.containsKey("rating")) {
                                rating = Double.parseDouble(dataMap.get("rating").toString());
                            }
                        }
                        catch (Exception ex) {
                            Log.d(TAG, "Error parsing course object: " + ex.getMessage());
                        }

                        Course course = new Course(hours, isEligible, isOffered, payRate, rating);
                        courses.put(courseDoc.getId(), course);
                    }
                }
            }
        }

        return courses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts a document snapshot into a Course object. Returns NULL if something goes
     * horribly wrong.
     *
     * @param result
     * @return course
     */
    public static Course getCourseFromDocumentSnapshot(DocumentSnapshot result) {
        if (result == null) {
            return null;
        }
        else {
            Course course = new Course();

            try {
                Map<String, Object> dataMap = result.getData();
                boolean isOffered = false;
                boolean isEligible = false;
                Number hours = 0;
                Number payRate = 0;
                Number rating = 0;

                if (dataMap != null && dataMap.size() > 0) {
                    if (dataMap.containsKey("offered")) {
                        isOffered = Boolean.parseBoolean(dataMap.get("offered").toString());
                    }
                    if (dataMap.containsKey("eligible")) {
                        isEligible = Boolean.parseBoolean(dataMap.get("eligible").toString());
                    }
                    if (dataMap.containsKey("hours")) {
                        hours = Double.parseDouble(dataMap.get("hours").toString());
                    }
                    if (dataMap.containsKey("payRate")) {
                        payRate = Double.parseDouble(dataMap.get("payRate").toString());
                    }
                    if (dataMap.containsKey("rating")) {
                        rating = Double.parseDouble(dataMap.get("rating").toString());
                    }
                }
                course = new Course(hours, isEligible, isOffered, payRate, rating);
            }
            catch (Exception ex) {
                Log.d(TAG, ex.getMessage());
                course = null;
            }

            return course;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts the sessions collection for a user into a map of strings to sessions.
     *
     * @param result
     * @return sessions map
     */
    public static Map<String, Session> getSessionsFromQuerySnapshot(QuerySnapshot result) {
        Map<String, Session> sessions = new HashMap<String, Session>();
        if (result != null && result.size() > 0) {
            for (DocumentSnapshot sessionSnap : result) {
                if (sessionSnap != null && !sessionSnap.getId().equals("BLANK")) {
                    Session session = new Session(sessionSnap.getId());
                    try {
                        if (sessionSnap.get("cancellerUID") != null) {
                            session.setCancellerUID(sessionSnap.get("cancellerUID").toString());
                        }
                        if (sessionSnap.get("end") != null) {
                            session.setEnd((Timestamp) sessionSnap.get("end"));
                        }
                        if (sessionSnap.get("expectedAmountOwed") != null) {
                            session.setExpectedAmountOwed(Float.parseFloat(sessionSnap.get("expectedAmountOwed").toString()));
                        }
                        if (sessionSnap.get("expectedMinutes") != null) {
                            session.setExpectedMinutes(Integer.parseInt(sessionSnap.get("expectedMinutes").toString()));
                        }
                        if (sessionSnap.get("hasHappened") != null) {
                            session.setHasHappened((boolean) sessionSnap.get("hasHappened"));
                        }
                        if (sessionSnap.get("hourlyPayRate") != null) {
                            session.setHourlyPayRate(Float.parseFloat(sessionSnap.get("hourlyPayRate").toString()));
                        }
                        if (sessionSnap.get("isPending") != null) {
                            session.setIsPending((boolean) sessionSnap.get("isPending"));
                        }
                        if (sessionSnap.get("online") != null) {
                            session.setOnline((boolean) sessionSnap.get("online"));
                        }
                        if (sessionSnap.get("sessionHasConcluded") != null) {
                            session.setSessionHasConcluded((boolean) sessionSnap.get("sessionHasConcluded"));
                        }
                        if (sessionSnap.get("start") != null) {
                            session.setStart((Timestamp) sessionSnap.get("start"));
                        }
                        if (sessionSnap.get("tuteeHasConcluded") != null) {
                            session.setTuteeHasConcluded((boolean) sessionSnap.get("tuteeHasConcluded"));
                        }
                        if (sessionSnap.get("tuteePresent") != null) {
                            session.setTuteePresent((boolean) sessionSnap.get("tuteePresent"));
                        }
                        if (sessionSnap.get("tuteeUID") != null) {
                            session.setTuteeUID(sessionSnap.get("tuteeUID").toString());
                        }
                        if (sessionSnap.get("tutorHasConcluded") != null) {
                            session.setTutorHasConcluded((boolean) sessionSnap.get("tutorHasConcluded"));
                        }
                        if (sessionSnap.get("tutorPresent") != null) {
                            session.setTutorPresent((boolean) sessionSnap.get("tutorPresent"));
                        }
                        if (sessionSnap.get("tutorUID") != null) {
                            session.setTutorUID(sessionSnap.get("tutorUID").toString());
                        }
                        if (sessionSnap.get("wasCancelled") != null) {
                            session.setWasCancelled((boolean) sessionSnap.get("wasCancelled"));
                        }
                        if (sessionSnap.get("courseID") != null) {
                            session.setCourseID(sessionSnap.get("courseID").toString());
                        }
                        if (sessionSnap.get("wasAccepted") != null) {
                            session.setWasAccepted((boolean) sessionSnap.get("wasAccepted"));
                        }
                        if (sessionSnap.get("tutorPicUrl") != null) {
                            session.setTutorPicUrl(sessionSnap.get("tutorPicUrl").toString());
                        }
                        if (sessionSnap.get("tuteePicUrl") != null) {
                            session.setTuteePicUrl(sessionSnap.get("tuteePicUrl").toString());
                        }
                        if (sessionSnap.get("involvedUsers") != null) {
                            session.setInvolvedUsers((List<String>) sessionSnap.get("involvedUsers"));
                        }
                        if (sessionSnap.get("alarmCode") != null) {
                            session.setAlarmCode((int) sessionSnap.get("alarmCode"));
                        }
                    }
                    catch (Exception ex) {
                        Log.d(TAG, "An error occurred while parsing sessions : " + ex);
                    }
                    finally {
                        sessions.put(session.getSessionID(), session);
                    }
                }
            }
        }

        return sessions;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts a list of RenderCourseModel to a list of strings where each string is
     * the courseId of the model.
     *
     * @param list
     * @return strings
     */
    public static ArrayList<String> renderCourseListToStringArrayList(List<RenderCourseModel> list) {
        ArrayList<String> strings = new ArrayList<String>();
        if (list != null) {
            for (RenderCourseModel model : list) {
                strings.add(model.getCourseId());
            }
        }

        return strings;
    }
    //---------------------------------------------------------------------------------------------
}
