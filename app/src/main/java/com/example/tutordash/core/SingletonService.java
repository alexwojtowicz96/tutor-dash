package com.example.tutordash.core;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Used in messaging.
 *
 * REFERENCES:
 * https://blog.usejournal.com/send-device-to-device-push-notifications-without-server-side-code-238611c143
 */
public class SingletonService {

    private static SingletonService instance;
    private RequestQueue requestQueue;
    private Context context;

    private SingletonService(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
    }
    //---------------------------------------------------------------------------------------------

    public static synchronized SingletonService getInstance(Context context) {
        if (instance == null) {
            instance = new SingletonService(context);
        }

        return instance;
    }
    //---------------------------------------------------------------------------------------------

    public RequestQueue getRequestQueue() {
        if (this.requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }
    //---------------------------------------------------------------------------------------------

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
    //---------------------------------------------------------------------------------------------
}
