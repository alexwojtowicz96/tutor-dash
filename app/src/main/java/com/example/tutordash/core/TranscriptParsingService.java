package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.CourseEntry;
import com.tom_roush.pdfbox.cos.COSDocument;
import com.tom_roush.pdfbox.pdfparser.PDFParser;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.text.PDFTextStripper;
import com.tom_roush.pdfbox.io.RandomAccessFile;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Collections;

public class TranscriptParsingService {

    public static final String TAG = "TranscriptService";

    public TranscriptParsingService() {}

    /**
     * Determines if a file is a pdf based on file extension.
     *
     * @param file
     * @return TRUE if PDF, FALSE otherwise
     * @throws IOException
     */
    public static boolean isPdf(File file) throws IOException {
        char[] fileNameArr = file.getCanonicalPath().toCharArray();
        String ext = new String();
        StringBuilder b = new StringBuilder();
        for (int i = fileNameArr.length - 1; i > -1; i--) {
            char curr = fileNameArr[i];
            if (curr == '.') {
                break;
            }
            else {
                b.append(curr);
            }
        }
        ext = b.reverse().toString().toLowerCase();

        return ext.equals("pdf");
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Converts a PDF to string.
     *
     * @param file
     * @return string with all text from pdf
     */
    public static String pdfToString(File file) {
        String text = new String();
        try {
            if (TranscriptParsingService.isPdf(file)) {
                PDFParser parser = new PDFParser(new RandomAccessFile(file, "r"));
                parser.parse();
                COSDocument cosDoc = parser.getDocument();
                PDFTextStripper stripper = new PDFTextStripper();
                PDDocument document = new PDDocument(cosDoc);
                text = stripper.getText(document);
                cosDoc.close();
                document.close();
            }
            else {
                throw new Exception("Not a pdf => " + file.getCanonicalPath());
            }
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
        }

        return text;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Performs checks to determine if a document is indeed a valid and official parsable
     * PDF academic transcript.
     *
     * @param transcript
     * @return TRUE if valid transcript, FALSE otherwise
     * @throws IOException
     */
    public static boolean isValidTranscript(File transcript) throws IOException {
        String fullText = "";

        try {
            fullText = TranscriptParsingService.pdfToString(transcript);
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
            return false;
        }

        return TranscriptParsingService.isPdf(transcript) && TranscriptParsingService.isOfficial(fullText);
    }
    //---------------------------------------------------------------------------------------------

    /** TODO : This will only work for ODU
     * Checks to make sure that the document supplied is in fact an official transcript
     * granted by the university.
     *
     * @param fullText
     * @return TRUE if official, FALSE otherwise
     */
    public static boolean isOfficial(String fullText) {
        String testString = "OLD DOMINION UNIVERSITY • OLD DOMINION UNIVERSITY • OLD DOMINION UNIVERSITY";

        if (!fullText.contains(testString)) {
            return false;
        }
        else {
            return true;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Reorders courses in ascending alphabetical order (by course ID)
     *
     * @param courses
     * @return all courses in ascending alphabetical order
     */
    public static List<CourseEntry> alphabetizeDescending(List<CourseEntry> courses) {
        List<CourseEntry> orderedCourses = new LinkedList<CourseEntry>();
        courses = TranscriptParsingService.alphabetizeAscending(courses);
        for (int i = courses.size() - 1; i > -1; i--) {
            orderedCourses.add(courses.get(i));
        }

        return orderedCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Reorders courses in descending alphabetical order (by course ID). NOTE: this is only
     * guaranteed to work as long as all of the course ids are unique.
     *
     * @param courses
     * @return all courses in descending alphabetical order
     */
    public static List<CourseEntry> alphabetizeAscending(List<CourseEntry> courses) {
        List<CourseEntry> orderedCourses = new LinkedList<CourseEntry>();
        List<String> courseIds = new LinkedList<String>();
        for (CourseEntry course : courses) {
            courseIds.add(course.getCourseId());
        }
        Collections.sort(courseIds);
        for (int i = 0; i < courseIds.size(); i++) {
            String courseId = courseIds.get(i);
            for (CourseEntry course : courses) {
                if (course.getCourseId().equals(courseId)) {
                    orderedCourses.add(course);
                    break;
                }
            }
        }

        return orderedCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets rid of courses that a user may have taken twice. The highest grade is what
     * determines which course is selected to stay if two courses have the same ID in the
     * list of all courses that this user has taken.
     *
     * @param courses
     * @return prunedCourses
     */
    public static List<CourseEntry> pruneDuplicateCourses(List<CourseEntry> courses) {
        List<CourseEntry> prunedCourses = new LinkedList<CourseEntry>();
        List<Integer> indexesToPrune = new LinkedList<Integer>();
        try {
            for (int i = 0; i < courses.size(); i++) {
                CourseEntry toCompare = courses.get(i);
                for (int j = 0; j < courses.size(); j++) {
                    CourseEntry currCourse = courses.get(j);
                    if (j != i) {
                        String courseIdToCompare = toCompare.getCourseId();
                        String courseGradeToCompare = toCompare.getGrade();
                        String currCourseId = currCourse.getCourseId();
                        String currCourseGrade = currCourse.getGrade();
                        if (courseIdToCompare.equals(currCourseId)) {
                            if (GradeUtils.gradeIsBetterThan(courseGradeToCompare, currCourseGrade)) {
                                indexesToPrune.add(j);
                            }
                            else {
                                indexesToPrune.add(i);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
        }

        if (!indexesToPrune.isEmpty()) {
            for (int i = 0; i < courses.size(); i++) {
                if (!indexesToPrune.contains(i)) {
                    prunedCourses.add(courses.get(i));
                }
            }
        }
        else { // Nothing was pruned (every course was unique)
            prunedCourses = courses;
        }

        return prunedCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO : This only works for ODU
     * Parses a confirmed pdf academic transcript.
     *
     * @param transcript
     * @return eligibleCourses a list of all courses (CourseEntry objects) found in the transcript
     */
    public static List<CourseEntry> parseTranscript(File transcript) {
        List<CourseEntry> eligibleCourses = new LinkedList<CourseEntry>();
        String courseId = new String();
        String courseName = new String();
        String courseGrade = new String();
        String courseString = new String();
        String fullText = TranscriptParsingService.pdfToString(transcript);

        // array_lines is the fullText string split by new lines.
        String[] array_lines = fullText.split("\\r?\\n");

        try {
            // This loop is cycling through the lines and splitting them into smaller words. This helps test against the regex.
            for (int i = 0; i < array_lines.length; i++) {
                // This gets rid of double spacing.
                array_lines[i] = array_lines[i].trim().replaceAll("\\s+", " ");
                // Splits by spaces.
                String[] parts = array_lines[i].split("\\s");
                // This loop checks specifically for an out of bounds index, so avoid being sent to catch.
                // As well as checks for the CourseId pattern. Ex. CS110 is regex: ([A-Z]+)([0-9]{3}[A-Z]?)
                for (int j = 0; j < parts.length; j++) {
                    try {
                        String temptest = parts[j + 1];
                    }
                    catch (IndexOutOfBoundsException ex) {
                        continue;
                    }
                    if (parts[j].matches("[A-Z]+") && parts[j + 1].matches("[0-9]{3}[A-Z]?")) {
                        // Once the CourseId is found, this loop checks for a grade. This is vague because Course Names can be between 1-7 words.
                        // So the bound is until the end of the line, represented as parts.length
                        for (int grade_check = j; grade_check < parts.length; grade_check++) {
                            if (parts[grade_check].matches("[0-9]{1,2}[\\.][0-9]{2}")) {
                                String grade = parts[grade_check + 1];
                                // This is a test for a real grade, as well as a passing grade
                                if (GradeUtils.isGrade(grade)) {
                                    if (GradeUtils.checkIfPassingGrade(grade)) {
                                        courseId = parts[j] + parts[j + 1];
                                        courseName = "";
                                        // Gets full course name by bounding the String[] from the first word to the last.
                                        for (int name_it = j + 2; name_it < parts.length; name_it++) {
                                            if (parts[name_it].matches("[0-9]{1,2}[\\.][0-9]{2}")) {
                                                name_it = parts.length;
                                            }
                                            else {
                                                courseName = courseName + " " + parts[name_it];
                                            }
                                        }
                                        courseGrade = grade;
                                        courseString = courseId + "," + courseName.trim() + "," + courseGrade
                                                + "," + "Old Dominion University";
                                        CourseEntry CourseEntry = new CourseEntry(courseString);
                                        eligibleCourses.add(CourseEntry);
                                    }
                                }
                                grade_check = parts.length;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Log.d(TAG, ex + "");
        }

        return TranscriptParsingService.alphabetizeAscending(
                TranscriptParsingService.pruneDuplicateCourses(eligibleCourses));
    }
    //---------------------------------------------------------------------------------------------
}
