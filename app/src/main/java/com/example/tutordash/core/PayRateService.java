package com.example.tutordash.core;

import androidx.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import com.example.tutordash.model.User;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.CourseAnalytics;
import com.example.tutordash.model.SchoolAnalytics;

import com.google.firebase.firestore.DocumentReference;

/**
 * Used to do pay rate calculations after the tutee has supplied a rating for their tutor.
 * This entire class is TODO : doesn't work...
 */
public class PayRateService {

    public static final String TAG = "PayRateService";

    public FirebaseFirestore database;
    public Float adjustmentValue;
    public Float newCourseRating;
    public User tutor;
    public Course course;
    public String courseID;
    public Float maximum;
    public Float minimum;
    public SchoolAnalytics schoolInfo;
    public CourseAnalytics courseInfo;
    //---------------------------------------------------------------------------------------------

    public PayRateService() {
        database = FirebaseFirestore.getInstance();
        adjustmentValue = .01f;
        maximum = 20f;
        minimum = 7f;
    }
    //---------------------------------------------------------------------------------------------

    public PayRateService(Float newCourseRating, Course course, String courseID) {
        this.database = FirebaseFirestore.getInstance();
        this.adjustmentValue = Float.valueOf(".01");
        this.newCourseRating = newCourseRating;
        this.tutor = null;
        this.course = course;
        this.courseID = courseID;
        this.maximum = Float.valueOf("20");
        this.minimum = Float.valueOf("1");
        this.schoolInfo = null;
        this.courseInfo = null;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Calculates new payrate for a given course.
     * @return updated course object
     */
    public Course calculateNewPayrate() {
        String school = "Old Dominion University"; // TODO this should not be hardcoded
        DocumentReference sRef = database.collection("schoolAnalytics").document(school);

        sRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful() && task.getResult()!= null){
                    schoolInfo = task.getResult().toObject(SchoolAnalytics.class);

                    DocumentReference cRef = sRef.collection("courseAnalytics").document(courseID);
                    cRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful()&&task.getResult() != null){
                                courseInfo = task.getResult().toObject(CourseAnalytics.class);

                                // Calculate demand to determine whether or not User value goes up.
                                Float newSchoolTotalSession = schoolInfo.getTotalSessions() + 1;
                                Float newCourseTotalSession = courseInfo.getTotalSessions() + 1;

                                Float tempPayrate = adjustDemandPayrate(
                                        schoolInfo,
                                        courseInfo,
                                        adjustmentValue,
                                        newSchoolTotalSession,
                                        newCourseTotalSession
                                );

                                // Calculate whether or not new rating effects value
                                tempPayrate = adjustRatingPayrate(
                                        courseInfo,
                                        newCourseRating,
                                        adjustmentValue,
                                        tempPayrate
                                );

                                if (tempPayrate > maximum) {
                                    tempPayrate = maximum;
                                }
                                else if (tempPayrate < minimum) {
                                    tempPayrate = minimum;
                                }
                                course.setPayRate(tempPayrate);

                                // Write new values back to database
                                courseInfo = pushCourseInfo(
                                        courseInfo,
                                        newCourseTotalSession,
                                        tempPayrate,
                                        cRef
                                );
                                schoolInfo = pushSchoolInfo(
                                        schoolInfo,
                                        newSchoolTotalSession,
                                        tempPayrate,
                                        sRef,
                                        school
                                );
                            }
                        }
                    });
                }
            }
        });

        return course;
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     * @param schoolInfo
     * @param courseInfo
     * @param adjustmentValue
     * @param newSchoolTotalSession
     * @param newCourseTotalSession
     * @return
     */
    public Float adjustDemandPayrate(
            SchoolAnalytics schoolInfo,
            CourseAnalytics courseInfo,
            Float adjustmentValue,
            Float newSchoolTotalSession,
            Float newCourseTotalSession
    ) {
        Float courseOverTotalCourse = 1 / (schoolInfo.getTotalCourses());
        Float courseDemand = newCourseTotalSession / newSchoolTotalSession;

        if (courseDemand < courseOverTotalCourse) {
            adjustmentValue = adjustmentValue * (-1);
        }
        else if (courseDemand.equals(courseOverTotalCourse)) {
            adjustmentValue = Float.valueOf("0");
        }

        return course.getPayRate().floatValue() + adjustmentValue;
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     * @param courseInfo
     * @param newCourseRating
     * @param adjustmentValue
     * @param tempPayrate
     * @return
     */
    public Float adjustRatingPayrate(
            CourseAnalytics courseInfo,
            Float newCourseRating,
            Float adjustmentValue,
            Float tempPayrate
    ) {
        Float currAvgPayRate = courseInfo.getAvgPayRate();

        if (newCourseRating > currAvgPayRate) {
            adjustmentValue = Float.valueOf(".01");
        }
        else if (newCourseRating < currAvgPayRate) {
            adjustmentValue = Float.valueOf("-.01");
        }
        else {
            adjustmentValue = Float.valueOf("0");
        }

        return tempPayrate + adjustmentValue;
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     * @param courseInfo
     * @param newCourseTotalSession
     * @param tempPayrate
     * @param cRef
     * @return
     */
    public CourseAnalytics pushCourseInfo(
            CourseAnalytics courseInfo,
            Float newCourseTotalSession,
            Float tempPayrate,
            DocumentReference cRef
    ) {
        courseInfo.setTotalSessions(newCourseTotalSession);
        courseInfo.setAvgPayRate(
                ((courseInfo.getAvgTutorRating() * (newCourseTotalSession - 1))
                        + tempPayrate) / (newCourseTotalSession)
        );

        cRef.set(courseInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success!");
                }
                else {
                    Log.d(TAG,"Failed to update new course values: " + courseID);
                }
            }
        });

        return courseInfo;
    }
    //---------------------------------------------------------------------------------------------

    /**
     *
     * @param schoolInfo
     * @param newSchoolTotalSession
     * @param tempPayrate
     * @param sRef
     * @param school
     * @return
     */
    public SchoolAnalytics pushSchoolInfo(
            SchoolAnalytics schoolInfo,
            Float newSchoolTotalSession,
            Float tempPayrate,
            DocumentReference sRef,
            String school
    ) {
        schoolInfo.setTotalSessions(newSchoolTotalSession);
        schoolInfo.setAvgCoursePay(
                ((schoolInfo.getAvgCoursePay()*(newSchoolTotalSession - 1))
                        + tempPayrate) / (newSchoolTotalSession));

        sRef.set(schoolInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "Success!");
                }
                else {
                    Log.d(TAG,"Failed to update new school values: " + school);
                }
            }
        });

        return schoolInfo;
    }
    //---------------------------------------------------------------------------------------------
}
