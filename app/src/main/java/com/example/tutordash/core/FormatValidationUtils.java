package com.example.tutordash.core;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Use this class to validate input formats for account creation user inputs.
 */
public class FormatValidationUtils {

    private static final String TAG = "FormatValidationUtils";

    public FormatValidationUtils() {}
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a password is allowed to be set as a user's password.
     * This will be determined by preset rules like password must be > 8 characters
     * or password must contain a number and symbol.
     *
     * Password must:
     * - Be 8 - 20 characters long
     * - Contain at least 1 capital letter
     * - Contain at least 1 lowercase letter
     * - Contain a numeric digit
     * - Cannot contain username or email
     *
     * @param password
     * @return TRUE if valid format, FALSE otherwise
     */
    public static boolean passwordIsValidFormat(String password, String username, String email) {
        boolean isValid = true;
        if (password.length() < 8 || password.length() > 20) {
            isValid = false;
        }
        else if (!hasCapitalLetter(password)) {
            isValid = false;
        }
        else if (!hasLowercaseLetter(password)) {
            isValid = false;
        }
        else if (!hasNumber(password)) {
            isValid = false;
        }
        else if (password.toLowerCase().contains(username.toLowerCase()) ||
                password.toLowerCase().contains(email.toLowerCase())) {
            isValid = false;
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a username is allowed to be set as a user's username.
     * For example, the username should not contain certain special characters.
     * This only checks the format, not if it is available (no uniqueness checks).
     *
     * Username must:
     * - Be 6 - 16 characters long
     * - Contain only letters or numbers
     *
     * @param username
     * @return TRUE if valid format, FALSE otherwise
     */
    public static boolean usernameIsValidFormat(String username) {
        boolean isValid = true;
        if (username.length() < 6 || username.length() > 16) {
            isValid = false;
        }
        else {
            ArrayList<String> capitalLetters = getLetters(true);
            ArrayList<String> lowercaseLetters = getLetters(false);
            ArrayList<String> numbers = getNumbers();
            char[] stringChars = username.toCharArray();
            for (Character c : stringChars) {
                if (capitalLetters.contains(c.toString()) ||
                    lowercaseLetters.contains(c.toString()) ||
                    numbers.contains(c.toString())) {
                    // do nothing
                }
                else {
                    isValid = false;
                    break;
                }
            }
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if an email is allowed to be set as a user's email. This only checks
     * the format of the email (no uniqueness checks dealing with the database).
     *
     * @param email
     * @return TRUE if valid format, FALSE otherwise
     */
    public static boolean emailIsValidFormat(String email) {
        final String pattern = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        final Pattern emailRegex = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailRegex.matcher(email);

        return matcher.find();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a string has a capital letter.
     *
     * @param string
     * @return TRUE if has capital letter, FALSE otherwise
     */
    public static boolean hasCapitalLetter(String string) {
        boolean hasCapitalLetter = false;
        ArrayList<String> letters = getLetters(true);
        char[] stringChars = string.toCharArray();
        for (Character c : stringChars) {
            if (letters.contains(c.toString())) {
                hasCapitalLetter = true;
                break;
            }
        }

        return hasCapitalLetter;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a string has a lowercase letter.
     *
     * @param string
     * @return TRUE if has lowercase letter, FALSE otherwise
     */
    public static boolean hasLowercaseLetter(String string) {
        boolean hasLowercaseLetter = false;
        ArrayList<String> letters = getLetters(false);
        char[] stringChars = string.toCharArray();
        for (Character c : stringChars) {
            if (letters.contains(c.toString())) {
                hasLowercaseLetter = true;
                break;
            }
        }

        return hasLowercaseLetter;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a string contains a numeric digit (0 - 9).
     *
     * @param string
     * @return TRUE if has number, FALSE otherwise
     */
    public static boolean hasNumber(String string) {
        boolean hasNumber = false;
        ArrayList<String> numbers = getNumbers();
        char[] stringChars = string.toCharArray();
        for (Character c : stringChars) {
            if (numbers.contains(c.toString())) {
                hasNumber = true;
                break;
            }
        }

        return hasNumber;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns a list of all English letters.
     *
     * @param uppercase - if TRUE, returns uppercase list, otherwise returns lowercase list
     * @return list of English letters
     */
    private static ArrayList<String> getLetters(boolean uppercase) {
        ArrayList<String> letters = new ArrayList<String>();
        letters.add("a"); letters.add("b"); letters.add("c"); letters.add("d");
        letters.add("e"); letters.add("f"); letters.add("g"); letters.add("h");
        letters.add("i"); letters.add("j"); letters.add("k"); letters.add("l");
        letters.add("m"); letters.add("n"); letters.add("o"); letters.add("p");
        letters.add("q"); letters.add("r"); letters.add("s"); letters.add("t");
        letters.add("u"); letters.add("v"); letters.add("w"); letters.add("x");
        letters.add("y"); letters.add("z");

        if (uppercase) {
            ArrayList<String> temp = new ArrayList<String>();
            for (String letter : letters) {
                temp.add(letter.toUpperCase());
            }
            letters = temp;
        }

        return letters;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns a list of all numeric digits (0 - 9).
     *
     * @return list of numbers as strings
     */
    private static ArrayList<String> getNumbers() {
        ArrayList<String> numbers = new ArrayList<String>();
        numbers.add("0"); numbers.add("1"); numbers.add("2"); numbers.add("3");
        numbers.add("4"); numbers.add("5"); numbers.add("6"); numbers.add("7");
        numbers.add("8"); numbers.add("9");

        return numbers;
    }
    //---------------------------------------------------------------------------------------------
}
