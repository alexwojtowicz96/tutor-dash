// 
// Decompiled by Procyon v0.5.36
// 

package com.example.tutordash.core.vendor.codec;

public interface Decoder
{
    Object decode(final Object p0) throws DecoderException;
}
