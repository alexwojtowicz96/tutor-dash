// 
// Decompiled by Procyon v0.5.36
// 

package com.example.tutordash.core.vendor.codec;

public interface BinaryDecoder extends Decoder
{
    byte[] decode(final byte[] p0) throws DecoderException;
}
