package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.User;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LocationService {

    public static final String TAG = "LocationService";
    public static final int MIN_UPDATE_TIME_SECONDS = 10;
    public static final int MIN_UPDATE_DISTANCE_METERS = 3;
    public static final Float MAX_MARGINAL_MILES = Float.parseFloat("0.005");
    public static final Float MAX_VISIBLE_MILES_AWAY = Float.valueOf(30);

    public LocationService() {}

    /**
     * Get the distance between two GeoPoints in miles. IMPORTANT! This returns -1.0
     * as a result if either or both of the points are equal to new GeoPoint(0, 0).
     * If either of the points are (0, 0), this is considered as NULL, and therefore
     * distance cannot be computed.
     *
     * @param pointA
     * @param pointB
     * @return distance between pointA and pointB in miles
     */
    public static Float getMilesAway(GeoPoint pointA, GeoPoint pointB) {
        Float result = Float.valueOf(-1);
        double pointA_lat = pointA.getLatitude();
        double pointA_lon = pointA.getLongitude();
        double pointB_lat = pointB.getLatitude();
        double pointB_lon = pointB.getLongitude();

        if ((pointA == null || pointA.equals(new GeoPoint(0, 0))) ||
                (pointB == null || pointB.equals(new GeoPoint(0, 0)))) {
            return result;
        }
        else if ((pointA_lat == 0) && (pointA_lon == 0)) {
            return result;
        }
        else if ((pointB_lat == 0) && (pointB_lon == 0)) {
            return result;
        }
        if ((pointA_lat == pointB_lat) && (pointA_lon == pointB_lon)) {
            result = Float.valueOf(0);
        }
        else {
            double theta = pointA_lon - pointB_lon;
            double part1 = Math.sin(Math.toRadians(pointA_lat));
            double part2 = Math.sin(Math.toRadians(pointB_lat));
            double part3 = Math.cos(Math.toRadians(pointA_lat));
            double part4 = Math.cos(Math.toRadians(pointB_lat));
            double part5 = Math.cos(Math.toRadians(theta));
            double dist = part1 * part2 + part3 * part4 * part5;
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            result = (float) dist;
        }

        return result;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a list of users, this will return a map of user.uName mapped to distance away
     * from the currently logged-in user. If either of the users have null location values,
     * then this entry will exist in the map as -1.
     *
     * @param users
     * @return map
     */
    public static Map<String, Float> getUserLocationMap(ArrayList<User> users) {
        Map<String, Float> map = new HashMap<String, Float>();
        GeoPoint currUserLoc = new GeoPoint(0, 0);
        if (ActivityUtils.CURRENT_USER_OBJECT.getLocation() != null) {
            currUserLoc = ActivityUtils.CURRENT_USER_OBJECT.getLocation();
        }
        for (User u : users) {
            GeoPoint otherUserLoc = new GeoPoint(0, 0);
            if (u.getLocation() != null) {
                otherUserLoc = u.getLocation();
            }
            Float milesAway = LocationService.getMilesAway(currUserLoc, otherUserLoc);
            map.put(u.getuName(), milesAway);
        }

        return map;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Removes entries in the map whose value is greater than maxDistance.
     *
     * @param map
     * @param maxDistance
     * @return pruned map
     */
    public static Map<String, Float> pruneLocationMap(Map<String, Float> map, Float maxDistance) {
        Map<String, Float> prunedMap = new HashMap<String, Float>();
        Set<String> users = map.keySet();
        for (String u : users) {
            if (map.containsKey(u)) {
                if (map.get(u) <= maxDistance) {
                    prunedMap.put(u, map.get(u));
                }
            }
            else {
                Log.d(TAG, "Map does not contain key : " + u);
            }
        }

        return prunedMap;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given two geopoints, this method will determine if the distance between the two
     * points is marginal or not. Marginal distances refer to distances so small that we can
     * simply ignore them.
     *
     * @param pointA
     * @param pointB
     * @return TRUE if distance is marginal, FALSE otherwise.
     */
    public static boolean isMarginalDistance(GeoPoint pointA, GeoPoint pointB) {
        float milesBetween = getMilesAway(pointA, pointB);
        if (milesBetween > MAX_MARGINAL_MILES) {
            return false;
        }
        else {
            return true;
        }
    }
    //---------------------------------------------------------------------------------------------
}
