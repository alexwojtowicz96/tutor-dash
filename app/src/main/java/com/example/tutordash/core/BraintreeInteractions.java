package com.example.tutordash.core;

/**
 * Contains the Braintree interactions that the faux "server" will use.
 */
public class BraintreeInteractions {

    public static final String TAG = "BraintreeInteractions";

    public static final String INIT_PAYMENT = "INIT_PAYMENT";
    public static final String TRANSACTION = "TRANSACTION";
    public static final String CREATE_CUSTOMER = "CREATE_CUSTOMER";
    public static final String FIND_CUSTOMER = "FIND_CUSTOMER";
    public static final String VOID_TRANSACTION = "VOID_TRANSACTION";
    public static final String FIND_TRANSACTION = "FIND_TRANSACTION";
    public static final String CREATE_PAYMENT_NONCE = "CREATE_PAYMENT_NONCE";

    public BraintreeInteractions() {}
    //---------------------------------------------------------------------------------------------

    /**
     * Add any new interactions to this array.
     * @return All available tasks.
     */
    public static String[] getAllInteractions() {
        return new String[] {
                INIT_PAYMENT,
                TRANSACTION,
                CREATE_CUSTOMER,
                FIND_CUSTOMER,
                VOID_TRANSACTION,
                FIND_TRANSACTION,
                CREATE_PAYMENT_NONCE
        };
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the interaction exists.
     *
     * @param interaction
     * @return TRUE if interaction exists, FALSE otherwise
     */
    public static boolean hasInteraction(String interaction) {
        String[] interactions = getAllInteractions();
        boolean hasInteraction = false;

        for (String i : interactions) {
            if (interaction.equals(i)) {
                hasInteraction = true;
                break;
            }
        }

        return hasInteraction;
    }
    //---------------------------------------------------------------------------------------------
}