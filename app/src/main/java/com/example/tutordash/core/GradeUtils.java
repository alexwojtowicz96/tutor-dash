package com.example.tutordash.core;

import java.util.LinkedList;
import java.util.List;

/**
 * Use this for performing operations on grades.
 */
public class GradeUtils {

    /**
     * Determines if a grade is better than another grade.
     *
     * @param a the first grade
     * @param b the second grade
     * @return TRUE if 'a' is better than 'b', FALSE otherwise
     */
    public static boolean gradeIsBetterThan(String a, String b) throws Exception {
        boolean isBetter = true;
        if (GradeUtils.isGrade(a) && GradeUtils.isGrade(b)) {
            List<String> letterGrades = new LinkedList<String>();
            letterGrades.add("A");
            letterGrades.add("A-");
            letterGrades.add("B+");
            letterGrades.add("B");
            letterGrades.add("B-");
            letterGrades.add("C+");
            letterGrades.add("C");
            letterGrades.add("C-");
            letterGrades.add("D");
            letterGrades.add("D-");
            letterGrades.add("F");

            if (letterGrades.contains(a) && letterGrades.contains(b)) {
                int aIndex = letterGrades.indexOf(a);
                int bIndex = letterGrades.indexOf(b);
                if (bIndex >= aIndex) {
                    isBetter = false;
                }
            }
            else if (letterGrades.contains(a) && !letterGrades.contains(b)) {
                // a is a letter? then a is better, so do nothing...
            }
            else if (!letterGrades.contains(a) && letterGrades.contains(b)) {
                isBetter = false;
            }
            else if (!letterGrades.contains(a) && !letterGrades.contains(b)) {
                if (!a.contains("P")) {
                    isBetter = false; // if a isn't a passing grade, then b defaults to better
                }
            }
        }
        else {
            String msg = "";
            if (!GradeUtils.isGrade(a)) {
                msg += "'" + a + "' " + "is not a grade!";
            }
            if (!GradeUtils.isGrade(b)) {
                if (!msg.equals("")) {
                    msg += "\n";
                }
                msg += "'" + b + "' " + "is not a grade!";
            }
            throw new Exception(msg);
        }

        return isBetter;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if a grade is valid based on ODU grading symbol conventions.
     *
     * @param grade
     * @return TRUE if valid grade option, FALSE otherwise
     */
    public static boolean isGrade(String grade) {
        List<String> validGrades = new LinkedList<String>();
        validGrades.add("A");
        validGrades.add("B");
        validGrades.add("C");
        validGrades.add("D");
        validGrades.add("F");
        validGrades.add("Z");
        validGrades.add("P");
        validGrades.add("W");
        validGrades.add("WF");
        validGrades.add("I");
        validGrades.add("II");
        validGrades.add("Q");
        validGrades.add("O");
        validGrades.add("IB");
        validGrades.add("AP");
        validGrades.add("CP");
        validGrades.add("DP");
        validGrades.add("DN");
        validGrades.add("MP");
        validGrades.add("XP");
        validGrades.add("U");
        validGrades.add("XL");
        validGrades.add("T");
        validGrades.add("R");

        List<String> validSymbols = new LinkedList<String>();
        validSymbols.add("+");
        validSymbols.add("-");
        validSymbols.add("*");
        validSymbols.add("/");
        validSymbols.add("&");
        validSymbols.add("#");
        validSymbols.add(">");

        final int MAX_GRADE_LENGTH = 3; // max length a grade can be (at ODU at least)
        boolean isValid = true;
        if (grade.length() > MAX_GRADE_LENGTH || grade.length() == 0) {
            isValid = false;
        }
        if (!validGrades.contains(grade)) {
            char[] gradeChars = grade.toCharArray();
            if (gradeChars[0] == '*') {
                if (gradeChars[1] != 'F') {
                    isValid = false;
                }
            }
            else {
                for (int i = 0; i < gradeChars.length; i++) {
                    char curr = gradeChars[i];
                    if (!validGrades.contains(new Character(curr).toString())) {
                        if (!validSymbols.contains(new Character(curr).toString())) {
                            isValid = false;
                        }
                    }
                }
            }
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    public static boolean checkIfPassingGrade(String grade) {
        List<String> passingGrades = new LinkedList<String>();
        passingGrades.add("A+");
        passingGrades.add("A-");
        passingGrades.add("A");
        passingGrades.add("B");
        passingGrades.add("B+");
        passingGrades.add("P");
        Boolean valid = false;
        if (passingGrades.contains(grade)) {
            valid = true;
        }

        return valid;
    }
    //---------------------------------------------------------------------------------------------
}
