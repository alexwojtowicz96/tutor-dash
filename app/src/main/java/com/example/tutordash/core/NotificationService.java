package com.example.tutordash.core;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tutordash.model.FirebaseNotificationDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Use this class to create notifications. They should model this JSON structure:
 *
 * {
 *   "to": "/topics/notification_userId",
 *   "data":
 *   {
 *     "title"   : "Notification title",
 *     "message" : "Notification message",
 *     "key1"    : "value1",
 *     "key2"    : "value2" //additional data you want to pass
 *   }
 * }
 *
 * REFERENCES:
 * https://blog.usejournal.com/send-device-to-device-push-notifications-without-server-side-code-238611c143
 */
public class NotificationService {

    public static final String TAG = "NotificationService";

    public static final String FCM_API = "https://fcm.googleapis.com/fcm/send";
    public static final String CONTENT_TYPE = "application/json";

    private FirebaseNotificationDataModel dataModel;
    private String notificationTitle;
    private String notificationMessage;
    private String notificationTopic;
    private boolean isHighPriority;
    private Context context;
    //---------------------------------------------------------------------------------------------

    public NotificationService(Context context) {
        this.dataModel = new FirebaseNotificationDataModel();
        this.context = context;
        this.notificationTitle = "TEST TITLE";
        this.notificationMessage = "TEST MESSAGE";
        this.notificationTopic = "/topics/" + MessageService.DM_TOPIC;
        this.isHighPriority = true;
    }

    public NotificationService(Context context, String title, String msg) {
        this.dataModel = new FirebaseNotificationDataModel();
        this.context = context;
        this.notificationTitle = title;
        this.notificationMessage = msg;
        this.notificationTopic = "/topics/" + MessageService.DM_TOPIC;
        this.isHighPriority = true;
    }

    public NotificationService(Context context, String title, String msg, String topic, boolean isHighPriority) {
        this.dataModel = new FirebaseNotificationDataModel();
        this.context = context;
        this.notificationTitle = title;
        this.notificationMessage = msg;
        this.notificationTopic = topic;
        this.isHighPriority = isHighPriority;
    }
    //---------------------------------------------------------------------------------------------

    public String getTitle() {
        return this.notificationTitle;
    }

    public String getTopic() {
        return this.notificationTopic;
    }

    public String getMessage() {
        return this.notificationMessage;
    }

    public Context getContext() {
        return this.context;
    }

    public boolean isHighPriority() {
        return this.isHighPriority;
    }

    public FirebaseNotificationDataModel getDataModel() {
        return this.dataModel;
    }

    public void setTitle(String title) {
        this.notificationTitle = title;
    }

    public void setMessage(String msg) {
        this.notificationMessage = msg;
    }

    public void setTopic(String topic) {
        this.notificationTopic = topic;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setHighPriority(boolean isHighPriority) {
        this.isHighPriority = isHighPriority;
    }

    public void setDataModel(FirebaseNotificationDataModel dataModel) {
        this.dataModel = dataModel;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Puts the notification data into the JObject so it is ready to be sent.
     * @return serialized notification object
     */
    private JSONObject serialize(List<Pair<String, String>> additionalData) {
        JSONObject notification = new JSONObject();
        JSONObject notificationBody = new JSONObject();

        try {
            notificationBody.put("title", concatPriorityWithTitle(
                    this.isHighPriority,
                    this.notificationTitle)
            );
            notificationBody.put("message", this.notificationMessage);
            if (additionalData != null && additionalData.size() > 0) {
                for (Pair<String, String> item : additionalData) {
                    notificationBody.put(item.first, item.second);
                }
            }

            notification.put("to", this.notificationTopic);
            notification.put("data", notificationBody);
        }
        catch (JSONException ex) {
            Log.e(TAG, "Failed to serialize notification : " + ex.getMessage());
        }

        return notification;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO : this new implementation needs testing.
     * Actually sends the notification (hopefully).
     *
     * @param additionalData - Any other data that needs to be passed.
     */
    public void sendNotification(List<Pair<String, String>> additionalData) {
        boolean canProceed = false;
        while (!this.dataModel.findIsValidModel()) {
            if (this.dataModel.hasAttemptedInit) {
                break;
            }
        }
        if (this.dataModel.findIsValidModel()) {
            canProceed = true;
        }

        // If we can proceed, then our data model has been initialized.
        if (canProceed) {
            JSONObject notification = serialize(additionalData);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    FCM_API,
                    notification,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "onResponse : " + response.toString());
                            //Toast.makeText(context, "Notification Sent!", Toast.LENGTH_SHORT).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Request Error", Toast.LENGTH_LONG).show();
                            Log.d(TAG, "onErrorResponse : Didn't work");
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "key=" + dataModel.getServerKey());
                    params.put("Content-Type", CONTENT_TYPE);

                    return params;
                }
            };

            SingletonService.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
        }
        else {
            Log.d(TAG, "Cannot proceed : Data model was not initialized!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Concatenates priority of notification with the title. This is done so we can separate
     * the title and priority when the message is received in order to determine which channel
     * to push it to.
     *
     * @param isHighPriority
     * @param title
     * @return concatenated string with title and isHighPriority
     */
    public static String concatPriorityWithTitle(boolean isHighPriority, String title) {
        final String sep = getStringSeparator();

        String priorityString = "LOW";
        if (isHighPriority) {
            priorityString = "HIGH";
        }

        StringBuilder b = new StringBuilder();
        b.append(priorityString);
        b.append(sep);
        b.append(title);

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns a string array of size 2 where [0] is the priority and [1] is the title. If
     * for whatever reason it can't figure out the priority, this defaults to high priority and
     * the title is just the jointMsg.
     *
     * @param jointMsg
     * @return A string array of the isHighPriority (as a string) and title values
     */
    public static String[] separatePriorityFromTitle(String jointMsg) {
        String[] values = new String[2];
        final String sep = getStringSeparator();

        if (jointMsg != null && !jointMsg.trim().isEmpty()) {
            String[] sepVals = jointMsg.split(sep);
            if (sepVals.length != 2) {
                values[0] = "HIGH";
                values[1] = jointMsg;
            }
            else {
                values[0] = sepVals[0];
                values[1] = sepVals[1];
            }
        }
        else {
            values[0] = "HIGH";
            values[1] = "NULL MSG";
        }

        return values;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Statically random string used in concatenation for this class.
     * @return string separator
     */
    private static String getStringSeparator() {
        return "0kkdduebauwQuUION0";
    }
    //---------------------------------------------------------------------------------------------
}
