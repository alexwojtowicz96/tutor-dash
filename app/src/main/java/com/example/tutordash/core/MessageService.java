package com.example.tutordash.core;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.tutordash.AcceptRequestActivity;
import com.example.tutordash.LoginActivity;
import com.example.tutordash.R;

import com.example.tutordash.model.Session;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * This class describes messaging functions and utilities.
 */
public class MessageService extends FirebaseMessagingService {

    public static final String TAG = "MessageService";

    // This is the base of the topic name for notifications relating to direct messaging.
    // Append this with the uName of the current user to use it.
    // By doing this, the current user will always get notifications on the feed.
    public static final String DM_TOPIC = "DM_Firebase_Chat_";

    private final String HIGH_PRIORITY_CHANNEL_ALIAS = "High Priority Channel";
    private final String LOW_PRIORITY_CHANNEL_ALIAS = "Low Priority Channel";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * When a message is received, this code will run.
     *
     * Apps targeting SDK 26 or above (Android O) must implement notification channels
     * and add its notifications to at least one of them. Therefore, confirm if version
     * is Oreo or higher, then setup notification channel.
     *
     * @param remoteMessage
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage != null) {
            Session sessionRequest = null;    // Session that gets sent from tutee for tutor response
            Session sessionResponse = null;   // Session that tutee receives from "ACCEPT" tutor response
            Session sessionCancelled = null;  // Session that other user cancels (sends back in this notification)
            Gson gson = new Gson();
            if (remoteMessage.getData().containsKey("sessionRequest")) {
                sessionRequest = gson.fromJson(remoteMessage.getData().get("sessionRequest"), Session.class);
            }
            else if (remoteMessage.getData().containsKey("sessionResponse")) {
                sessionResponse = gson.fromJson(remoteMessage.getData().get("sessionResponse"), Session.class);
            }
            else if (remoteMessage.getData().containsKey("sessionCancelled")) {
                sessionCancelled = gson.fromJson(remoteMessage.getData().get("sessionCancelled"), Session.class);
            }

            // Keep current user up-to-date if they are currently using the app.
            if (sessionRequest != null) {
                sessionRequest.makeNullValues();
                ActivityUtils.SESSION_REQUESTED = sessionRequest;
                if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                    ActivityUtils.CURRENT_USER_OBJECT.putNewRequestId(sessionRequest.getSessionID());
                }

                // Update the last request timestamp for this tutor
                final String tutorUID = sessionRequest.getTutorUID();
                FirebaseFirestore.getInstance().collection("users").document(tutorUID)
                        .update("timeOfLastRequest", new Timestamp(new Date())).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (!task.isSuccessful()) {
                                    Log.d(TAG, "Failed to update timeOfLastRequest for user : " + tutorUID);
                                }
                            }
                        });
            }
            if (sessionResponse != null) {
                sessionResponse.makeNullValues();

                // Set alarm for tutee since a response means it was accepted.
                AlarmService.setSessionAlarms(getApplicationContext(), sessionResponse);

                // I don't even know if I'm using user.sessions....
                if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                    if (ActivityUtils.CURRENT_USER_OBJECT.getSessions() == null) {
                        ActivityUtils.CURRENT_USER_OBJECT.setSessions(new HashMap<String, Session>());
                    }
                    ActivityUtils.CURRENT_USER_OBJECT.getSessions().put(sessionResponse.getSessionID(), sessionResponse);
                    ActivityUtils.CURRENT_USER_OBJECT.putNewPendingSessionId(sessionResponse.getSessionID());
                }
            }
            if (sessionCancelled != null) {

                // Cancel the alarm for the other user
                AlarmService.cancelSessionAlarms(getApplicationContext(), sessionCancelled);

                if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                    ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds().remove(sessionCancelled.getSessionID());
                    if (!ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds().contains(sessionCancelled.getSessionID())) {
                        ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds().add(sessionCancelled.getSessionID());
                    }
                }
            }

            final Intent intent;
            if (sessionRequest != null && sessionRequest.findIsValidSession()) {
                intent = new Intent(this, AcceptRequestActivity.class);
            }
            else {
                intent = new Intent(this, LoginActivity.class);
                if (remoteMessage.getData().containsKey("chatMessage") &&
                        remoteMessage.getData().get("chatMessage").equals("true"))
                {
                    intent.putExtra("GO_TO_MESSAGES", "true");
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int notificationID = new Random().nextInt(3000);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                this.setupChannels(notificationManager);
            }

            if (remoteMessage.getData().containsKey("sender") &&
                    ActivityUtils.IN_CHAT_WITH != null &&
                    ActivityUtils.IN_CHAT_WITH.equals(remoteMessage.getData().get("sender")))
            {
                // The current user is in the chat with the user who sent this message.
                // Ignore the notification.
                Log.d(TAG, "Ignoring notification!");
            }
            else {
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );

                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.availability_beacon);
                Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder builder;
                String jointMsg = remoteMessage.getData().get("title");
                String[] values = NotificationService.separatePriorityFromTitle(jointMsg);
                if (values[0].trim().toUpperCase().equals("HIGH")) {
                    builder = new NotificationCompat.Builder(this, HIGH_PRIORITY_CHANNEL_ALIAS)
                            .setSmallIcon(R.drawable.availability_beacon)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(values[1])
                            .setContentText(remoteMessage.getData().get("message"))
                            .setAutoCancel(true)
                            .setSound(notificationSoundUri)
                            .setContentIntent(pendingIntent);
                }
                else {
                    builder = new NotificationCompat.Builder(this, LOW_PRIORITY_CHANNEL_ALIAS)
                            .setSmallIcon(R.drawable.availability_beacon)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(values[1])
                            .setContentText(remoteMessage.getData().get("message"))
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent);
                }

                // Set notification color to match Tutor Dash template theme
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder.setColor(getResources().getColor(R.color.colorPrimaryDark));
                }
                notificationManager.notify(notificationID, builder.build());
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Required to set up channels on Android API >= 26
     * @param notificationManager
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(NotificationManager notificationManager) {
        final CharSequence highPriorityChannelName = "New High Priority Notification";
        final CharSequence lowPriorityChannelName = "New Low Priority Notification";
        final String description = "Device-to-Device Notification";

        // Define the high priority channel
        NotificationChannel highPriorityChannel;
        highPriorityChannel = new NotificationChannel(
                HIGH_PRIORITY_CHANNEL_ALIAS,
                highPriorityChannelName,
                NotificationManager.IMPORTANCE_HIGH
        );
        highPriorityChannel.setDescription(description);
        highPriorityChannel.enableLights(true);
        highPriorityChannel.setLightColor(Color.GREEN);
        highPriorityChannel.enableVibration(true);

        // Define the low priority channel
        NotificationChannel lowPriorityChannel;
        lowPriorityChannel = new NotificationChannel(
                LOW_PRIORITY_CHANNEL_ALIAS,
                lowPriorityChannelName,
                NotificationManager.IMPORTANCE_LOW
        );
        lowPriorityChannel.setDescription(description);
        lowPriorityChannel.enableLights(true);
        lowPriorityChannel.setLightColor(Color.BLUE);
        lowPriorityChannel.enableVibration(false);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(highPriorityChannel);
            notificationManager.createNotificationChannel(lowPriorityChannel);
        }
        else {
            Log.d(TAG, "Could not setup channels : Notification Manager was null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the topic (a string) for a given course ID and tutee option. To use this properly,
     * specify whether the user is a tutee or tutor (true or false).
     *
     * It might look like the logic is backwards, but if I am a tutee, and I have CS150 in my
     * coursesSeekingAssistance collection, then I want to receive notifications from
     * the OFFERING_CS150_OLD_DOMINION_UNIVERSITY topic. When tutors send out an alert for
     * CS150, they will send a notification with that EXACT topic name, so I will receive
     * the message since I am subscribed to that topic.
     *
     * @param courseId
     * @param schoolName
     * @param isTutee
     *
     * @return topic
     */
    public static String getTopic(String courseId, String schoolName, boolean isTutee) {
        String prefix = "SEEKING";
        if (isTutee) {
            prefix = "OFFERING";
        }

        return (prefix + "_" +
                courseId.trim().toUpperCase().replaceAll(" ", "_") + "_" +
                schoolName.trim().toUpperCase().replaceAll(" ", "_")
        );
    }
    //---------------------------------------------------------------------------------------------
}
