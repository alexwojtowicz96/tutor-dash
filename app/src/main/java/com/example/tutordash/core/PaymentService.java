package com.example.tutordash.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.PayPalAccount;
import com.braintreegateway.PaymentMethod;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class PaymentService {

    public static final String TAG = "PaymentService";

    private Activity activity;

    private PaymentService() {}

    public PaymentService(Activity activity) {
        this.activity = activity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the total amount of based on the length and pay-rate. This will return a serial
     * float in the format of $XX.XX (rounded to nearest cent).
     *
     * @param length
     * @param rate
     * @return total amount owed based on length
     */
    public static Float getTotal(int length, Float rate) {
        return Float.parseFloat(getTotalAsString(length, rate));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This is the same as PaymentService.getTotal(), only this returns price as a formatted
     * string for money.
     *
     * @param length
     * @param rate
     * @return total as a string
     */
    public static String getTotalAsString(int length, Float rate) {
        Float amount = (Float.valueOf(length) / Float.valueOf(60)) * rate;

        return ActivityUtils.getFormattedFloat(amount);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Executes a transaction in Braintree. Updates Firestore onComplete. This can't be static,
     * so to use it, you need to create a new PaymentService and call .executeTransaction()
     *
     * @param transaction
     * @param context
     */
    public void executeTransaction(
            BraintreeTransaction transaction,
            Context context,
            String successMsg,
            String failureMsg
    ) {
        try {
            // Coming in, the transaction will not be valid since there is no ID yet.
            // However, it should NOT be null!
            if (transaction != null) {
                if (transaction.getAmount() <= 0) {
                    transaction.setAmount(Float.parseFloat(new Double(0.01).toString()));
                }
                if (transaction.getAmount() > 0) {
                    TransactionModule module = new TransactionModule(
                            transaction,
                            context,
                            successMsg,
                            failureMsg
                    );
                    module.execute();
                }
                else {
                    throw new Exception("Transaction must have an amount greater than $0.00!");
                }
            }
            else {
                throw new Exception("Transaction was null!");
            }
        }
        catch (Exception ex) {
            Log.d(TAG, "Failed to execute the transaction in PaymentService :\n" + ex.getMessage());
            ActivityUtils.toastOnUiThread(failureMsg, this.activity, context);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This will be used in PaymentService.executeTransaction(). When we create a module, it will
     * just execute the transaction when it's ready.
     */
    public class TransactionModule implements BraintreeActivity {

        public final String TAG = "TransactionModule_PS";

        private BraintreeTransaction transaction;
        private Context context;
        private String successMsg;
        private String failureMsg;
        private BraintreeFauxServer server;
        private Customer customer;
        //-----------------------------------------------------------------------------------------

        private TransactionModule() {}

        public TransactionModule(
                BraintreeTransaction transaction,
                Context context,
                String successMsg,
                String failureMsg
        ) {
            this.context = context;
            this.successMsg = successMsg;
            this.failureMsg = failureMsg;
            this.transaction = transaction;
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Initializes the server. This will automatically do everything involved with
         * submitting the transaction.
         */
        public void execute() {
            this.server = new BraintreeFauxServer(this, context);
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when the faux "server" object is initialized.
         *
         * @param isReady
         */
        public void onServerReady(boolean isReady) {
            if (isReady && this.server.isReady()) {
                this.server.findCustomer(transaction.getInitiatorUID());
            }
            else {
                Log.d(this.TAG, "Server was not ready when it should have been ready!");
                ActivityUtils.toastOnUiThread(failureMsg, activity, context);
            }
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called in the activity that implements this class. It is intended that the
         * server will do something, and once it is finished, it will call this method in the
         * activity that called the server method.
         *
         * @param amount - Amount to submit
         * @param clientToken - Token generated on the faux "server" that configures the transaction
         */
        public void submitPayment(Float amount, String clientToken) {
            if (clientToken != null && amount != null && amount > 0) {
                try {
                    // TODO, this isn't perfect. It looks for the thing closes to the payment
                    //  name in the tags...
                    if (this.customer != null)  {
                        boolean hasPayment = false;
                        PaymentMethod method = null;

                        if (findMethodFromTransactionTags(this.transaction) != null) {
                            method = findMethodFromTransactionTags(this.transaction);
                            hasPayment = true;
                        }
                        else if (findFirstMethod() != null) {
                            method = findFirstMethod();
                            hasPayment = true;
                        }
                        if (method != null && hasPayment) {
                            final String paymentToken = method.getToken();
                            this.server.createPaymentNonce(paymentToken);
                        }
                        else {
                            throw new Exception("Somehow this customer does not have payments set up? : "
                                    + transaction.getInitiatorUID());
                        }
                    }
                    else {
                        Log.d(TAG, "The customer object was null!");
                        ActivityUtils.toastOnUiThread(failureMsg, activity, context);
                    }
                }
                catch (Exception ex) {
                    Log.d(TAG, "Failed to submit payment :\n" + ex.getMessage());
                    ActivityUtils.toastOnUiThread(failureMsg, activity, context);
                }
            }
            else {
                Log.d(this.TAG, "Failed to submit payment. Null clientToken or amount!");
                ActivityUtils.toastOnUiThread(failureMsg, activity, context);
            }
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Finds the payment method in this.customer's collection of payment methods that
         * best matches the tags attached to a BraintreeTransaction object.
         *
         * @param transaction
         * @return payment method or NULL
         */
        public PaymentMethod findMethodFromTransactionTags(BraintreeTransaction transaction) {
            List<CreditCard> cards = this.customer.getCreditCards();
            List<PayPalAccount> paypals = this.customer.getPayPalAccounts();
            PaymentMethod method = null;

            if (cards != null && cards.size() > 0) {
                for (CreditCard card : cards) {
                    if (transaction.findIfHasWordInTags(card.getCardType())) {
                        method = card;
                        break;
                    }
                }
            }
            if (method == null) {
                if (paypals != null && paypals.size() > 0) {
                    for (PayPalAccount paypal : paypals) {
                        if (transaction.findIfHasWordInTags("PayPal")) {
                            method = paypal;
                            break;
                        }
                    }
                }
            }

            return method;
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Finds the first payment method in this.customer's collection of payment methods.
         *
         * @return First payment method or NULL
         */
        public PaymentMethod findFirstMethod() {
            try {
                List<CreditCard> cards = this.customer.getCreditCards();
                List<PayPalAccount> paypals = this.customer.getPayPalAccounts();
                PaymentMethod method = null;

                if (cards != null && cards.size() > 0) {
                    method = cards.get(0);
                }
                else if (paypals != null && paypals.size() > 0) {
                    method = paypals.get(0);
                }
                return method;
            }
            catch (Exception ex) {
                return null;
            }
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when the customer object is found.
         *
         * @param customer - The customer object retrieved from Braintree
         */
        public void onCustomerFound(Customer customer, boolean wasFound) {
            if (customer != null && wasFound) {
                this.customer = customer;
                this.server.initPayment(transaction.getInitiatorUID(), transaction.getAmount());
            }
            else {
                Log.d(this.TAG, "Failed to find the customer : " + transaction.getInitiatorUID());
                ActivityUtils.toastOnUiThread(failureMsg, activity, context);
            }
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when a customer is created in Braintree.
         *
         * @param customer
         * @param wasCreated
         */
        public void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated) {
            // Not in use
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when a submitted transaction is completed.
         *
         * @param transaction
         * @param wasSuccessful
         */
        public void onTransactionComplete(BraintreeTransaction transaction, boolean wasSuccessful) {
            if (wasSuccessful && transaction != null && transaction.findIfIsValidModel()) {
                this.transaction.setTransactionID(transaction.getTransactionID());
                FirebaseFirestore database = FirebaseFirestore.getInstance();
                database.collection("transactions").document(this.transaction.getTransactionID())
                        .set(this.transaction).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "TRANSACTION SUCCESSFUL!");
                            ActivityUtils.toastOnUiThread(successMsg, activity, context);
                        }
                        else {
                            Log.d(TAG, "TRANSACTION FAILED : Could not complete Firestore task");
                            String msg = "Task completed, but could not update in Firestore";
                            ActivityUtils.toastOnUiThread(msg, activity, context);
                        }
                    }
                });
            }
            else {
                Log.d(this.TAG, "Failed to complete transaction!");
                ActivityUtils.toastOnUiThread(failureMsg, activity, context);
            }
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when a submitted void for a transaction is completed.
         *
         * @param transactionID
         * @param wasVoided
         */
        public void onTransactionVoided(String transactionID, boolean wasVoided) {
            // Not in use
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when a transaction is found (from its ID).
         *
         * @param transaction
         * @param wasFound
         */
        public void onTransactionFound(Transaction transaction, boolean wasFound) {
            // Not in use
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Gets called when a payment method nonce is created.
         *
         * @param nonce
         * @param wasCreated
         */
        public void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated) {
            if (nonce != null && nonce.getNonce() != null && wasCreated) {
                this.server.executeTransaction(nonce.getNonce(), transaction.getAmount());
            }
            else {
                Log.d(this.TAG, "Failed to create payment method nonce.");
                ActivityUtils.toastOnUiThread(failureMsg, activity, context);
            }
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}
