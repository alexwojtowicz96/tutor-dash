package com.example.tutordash.core;

import android.content.Context;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.StorageReference;

import java.io.InputStream;

/**
 * This class is necessary for the Glide library to work in this app.
 */
@GlideModule
public class MyAppGlideModule extends AppGlideModule {

    public static final String TAG = "MyAppGlideModule";

    /**
     * Registers FirebaseImageLoader to handle StorageReference
     *
     * @param context
     * @param glide
     * @param registry
     */
    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        try {
            if (context == null || glide == null || registry == null) {
                String ex = "";
                if (context == null) {
                    ex += "The context was null!\n";
                }
                if (glide == null) {
                    ex += "The glide object was null!\n";
                }
                if (registry == null) {
                    ex += "The registry was null!";
                }
                throw new Exception(ex);
            }
            else {
                registry.append(
                        StorageReference.class,
                        InputStream.class,
                        new FirebaseImageLoader.Factory()
                );
            }
        }
        catch (Exception ex) {
            Log.d(TAG, "Something went wrong with Glide Registration :\n" + ex.getLocalizedMessage());
        }
    }
    //---------------------------------------------------------------------------------------------
}
