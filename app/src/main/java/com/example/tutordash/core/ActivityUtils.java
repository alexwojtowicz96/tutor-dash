package com.example.tutordash.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.util.Pair;

import com.example.tutordash.model.Course;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.Token;
import com.example.tutordash.model.User;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Use this class to host important bits of important information that need to be
 * reused between activities and fragments. Be careful though...
 */
public class ActivityUtils {

    public static ArrayList<String> ELIGIBLE_COURSES;
    public static String CURRENT_USERNAME;
    public static String CURRENT_USER_EMAIL;
    public static String CURRENT_USER_SCHOOL;
    public static User CURRENT_USER_OBJECT;
    public static User TMP_USR;
    public static Context TMP_CONTEXT;
    public static Token PASSWORD_RESET_TOKEN;
    public static boolean IS_TUTEE_MODE = true;
    public static boolean HAS_SENT_CONFIRMATION_EMAIL = false;
    public static boolean IS_TRANSCRIPT_MODE = false;
    public static Pair<String, Course> COURSE_OF_INTEREST;
    public static Session SESSION_REQUESTED;
    public static int SESSIONS_VIEW_MODE = 0;
    public static String IN_CHAT_WITH = null;

    public static final int PENDING_SESSIONS_ID = 38867;
    public static final int PREVIOUS_SESSIONS_ID = 23115;
    public static final int CANCELLED_SESSIONS_ID = 50989;
    //---------------------------------------------------------------------------------------------

    /**
     * Use this to make an intent... Probably unnecessary.
     *
     * @param context
     * @param activityClass
     * @return newIntent
     */
    public static Intent makeIntent(Context context, Class activityClass) {
        return new Intent(context, activityClass);
    }
    //---------------------------------------------------------------------------------------------

    public static String getPackageNameWithClass(Context context) {
        String name = new String();
        if (context != null) {
            try {
                name = context.getClass().getCanonicalName();
                if (name.length() > 0 && name.contains("/")) {
                    name = name.replaceAll("/", ".");
                }
            }
            catch (NullPointerException ex) {
                System.err.println("Error getting package name for class: " + context.toString());
                ex.printStackTrace();
            }
        }

        return name;
    }
    //---------------------------------------------------------------------------------------------

    public static String getPackageNameWithoutClass(Context context) {
        String pkg = ActivityUtils.getPackageNameWithClass(context);
        char[] pkgArr = pkg.toCharArray();
        StringBuilder b = new StringBuilder();
        boolean insideClassName = true;

        for (int i = pkgArr.length - 1; i > -1; i--) {
            char curr = pkgArr[i];
            if (!insideClassName) {
                b.append(curr);
            }
            if (curr == '.') {
                insideClassName = false;
            }
        }

        return b.reverse().toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Returns a formatted float value (2 places after the decimal point). You can use this for
     * money (price), distance, or rating just to name a few applications. This returns a string.
     * This could be written better, but I think it will do.
     *
     * @param number
     * @return formatted number in a string.
     */
    public static String getFormattedFloat(Float number) {
        DecimalFormat decimalFormat1 = new DecimalFormat("#.##");
        DecimalFormat decimalFormat2 = new DecimalFormat("##.##");
        DecimalFormat decimalFormat3 = new DecimalFormat("###.##");
        String num;

        if (number < 10) {
            num = decimalFormat1.format(number);
        }
        else if (number < 100) {
            num = decimalFormat2.format(number);
        }
        else if (number < 1000) {
            num = decimalFormat3.format(number);
        }
        else {
            num = number.toString();
        }

        char[] numArr = num.toCharArray();
        if (numArr.length == 1) {
            num += ".00";
        }
        else if (numArr.length == 2) {
            if (!num.contains(".")) {
                num += ".00";
            }
        }
        else if (numArr.length > 2) {
            char[] end = new char[2];
            int x = 0;
            for (int i = numArr.length - 2; i < numArr.length; i++) {
                end[x] = numArr[i];
                x += 1;
            }
            if (new Character(end[0]).equals('.')) {
                num += "0";
            }
            else if (new Character(end[1]).equals('.')) {
                // Impossible? it can't end with '.'
            }
        }

        return num;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generate random int between two numbers.
     *
     * @param min
     * @param max
     * @return random int
     */
    public static int generateRandomInt(int min, int max) {
        Random random = new Random();

        return random.nextInt(max - min) + min;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the main thread.
     */
    public static void toastOnUiThread(String msg, Activity activity, Context context) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

}
