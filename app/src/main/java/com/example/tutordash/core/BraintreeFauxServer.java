package com.example.tutordash.core;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.ClientTokenRequest;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.ResourceCollection;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.braintreegateway.TransactionSearchRequest;
import com.braintreegateway.exceptions.NotFoundException;
import com.example.tutordash._TestBraintreeActivity;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeDataModel;
import com.example.tutordash.model.BraintreeTransaction;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

/**
 * This class ideally would be on the server... but we don't have a server, so this class
 * is a faux "server" that will live on the client. This is bad practice FYI, but I think it
 * will get the job done for the prototype.
 *
 * REFERENCES:
 * https://developers.braintreepayments.com/reference/request/client-token/generate/java
 * https://developers.braintreepayments.com/guides/customers/java
 * https://developers.braintreepayments.com/guides/client-sdk/setup/android/v2
 * https://developers.braintreepayments.com/start/hello-client/android/v2
 * https://developers.braintreepayments.com/start/hello-server/java
 *
 * LOGIN:
 * https://sandbox.braintreegateway.com/login
 */
public class BraintreeFauxServer {

    public static final String TAG = "BraintreeFauxServer";

    private BraintreeDataModel dataModel;
    private BraintreeGateway gateway;
    private Context context;
    private BraintreeActivity activity;
    //---------------------------------------------------------------------------------------------

    public BraintreeFauxServer(BraintreeActivity activity) {
        this.context = ((Activity) activity).getApplicationContext();
        this.activity = activity;
        initDataModel();
    }
    //---------------------------------------------------------------------------------------------

    public BraintreeFauxServer(BraintreeActivity activity, Context context) {
        this.context = context;
        this.activity = activity;
        initDataModel();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Pulls the Braintree data from Firebase.
     */
    private void initDataModel() {
        FirebaseFirestore.getInstance().collection("supportCreds").document("braintree")
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    try {
                        BraintreeDataModel model = task.getResult().toObject(BraintreeDataModel.class);
                        if (model != null && model.findIfIsValidModel()) {
                            dataModel = model;
                            initGateway();
                            onServerReady();
                        }
                        else {
                            throw new Exception("The data model was not initialized properly!");
                        }
                    }
                    catch (Exception ex) {
                        Log.d(TAG, ex.getMessage());
                    }
                }
                else {
                    Log.d(TAG, "Failed to query braintree data from Firestore");
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the server is ready to use.
     */
    public void onServerReady() {
        if (this.isReady()) {
            this.activity.onServerReady(true);
        }
        else {
            Log.d(TAG, "The server was supposed to be ready but didn't pass the isReady check!");
            this.activity.onServerReady(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the Braintree gateway.
     */
    private void initGateway() {
        if (dataModel.findIfIsSandbox()) {
            this.gateway = new BraintreeGateway(
                    Environment.SANDBOX,
                    dataModel.getMerchantID(),
                    dataModel.getApiPublicKey(),
                    dataModel.getApiPrivateKey());
        }
        else if (dataModel.findIfIsDevelopment()) {
            this.gateway = new BraintreeGateway(
                    Environment.DEVELOPMENT,
                    dataModel.getMerchantID(),
                    dataModel.getApiPublicKey(),
                    dataModel.getApiPrivateKey());
        }
        else if (dataModel.findIfIsProduction()) {
            this.gateway = new BraintreeGateway(
                    Environment.PRODUCTION,
                    dataModel.getMerchantID(),
                    dataModel.getApiPublicKey(),
                    dataModel.getApiPrivateKey());
        }
        else {
            final String err = "This environment is not supported, gateway uninitialized : ";
            Log.e(TAG, err + dataModel.getEnvironment());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Determines if the faux "server" is ready to be used.
     * @return TRUE if ready, FALSE otherwise.
     */
    public boolean isReady() {
        boolean isReady = true;
        if (this.gateway == null || this.dataModel == null || !this.dataModel.findIfIsValidModel()) {
            isReady = false;
        }

        return isReady;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the payment. Ideally, what will happen is this will generate a token and then
     * call a method in the associated activity that brings up a Braintree Fragment with the
     * credit card and paypal options...
     *
     * @param customerID
     * @param amount
     */
    public void initPayment(String customerID, Float amount) {
        try {
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.INIT_PAYMENT,
                    new String[] { customerID, amount.toString() }
            );
            task.execute();
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the task that will execute a transaction.
     *
     * @param nonceFromClient
     * @param amount
     */
    public void executeTransaction(String nonceFromClient, Float amount) {
        try {
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.TRANSACTION,
                    new String[] { amount.toString(), nonceFromClient }
            );
            task.execute();
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when executeTransaction has finished executing.
     * @param result
     */
    public void onExecuteTransactionResult(Result<Transaction> result) {
        if (result != null && result.isSuccess()) {
            Log.d(TAG, "Transaction Successful!" + result);

            BraintreeTransaction transaction = createTransaction(result);
            activity.onTransactionComplete(transaction, true);
        }
        else {
            String ex = "";
            if (result != null && result.getMessage() != null) {
                ex = result.getMessage();
            }
            Log.d(TAG, "There was a problem. The result was not successful :\n" + ex);
            activity.onTransactionComplete(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the task that creates a new customer in Braintree.
     * @param braintreeCustomer
     */
    public void createCustomer(BraintreeCustomer braintreeCustomer) {
        if (braintreeCustomer.isValid()) {
            Gson gson = new Gson();
            String customerString = gson.toJson(braintreeCustomer);
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.CREATE_CUSTOMER,
                    new String[] { customerString }
            );
            task.execute();
        }
        else {
            String msg = "This customer is not valid";
            Log.d(TAG, "Not a valid customer! : " + braintreeCustomer);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when createCustomer() has finished executing
     * @param result
     */
    public void onCreateCustomerResult(Result<Customer> result, BraintreeCustomer customer) {
        if (result.isSuccess() && result.getTarget() != null) {
            activity.onCustomerCreated(customer, true);
        }
        else {
            activity.onCustomerCreated(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the task that will find if a customer exists by ID.
     * @param customerId
     */
    public void findCustomer(String customerId) {
        if (customerId != null) {
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.FIND_CUSTOMER,
                    new String[] { customerId }
            );
            task.execute();
        }
        else {
            Log.d(TAG, "Could not find customer with null customer ID");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when findCustomer() has finished executing.
     * @param customer
     */
    public void onFindCustomerResult(Customer customer) {
        if (customer != null) {
            activity.onCustomerFound(customer, true);
        }
        else {
            activity.onCustomerFound(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the task that will attempt to void a transaction with a valid model for that
     * transaction.
     *
     * @param transactionModel
     */
    public void voidTransaction(BraintreeTransaction transactionModel) {
        if (transactionModel != null && transactionModel.findIfIsValidModel()) {
            Gson gson = new Gson();
            try {
                String transactionData = gson.toJson(transactionModel);
                FauxServerTask task = new FauxServerTask(
                        activity,
                        BraintreeInteractions.VOID_TRANSACTION,
                        new String[] { transactionData }
                );
                task.execute();
            }
            catch (Exception ex) {
                Log.d(TAG, "Failed to serialize transaction model to string :\n" + ex.getLocalizedMessage());
            }
        }
        else {
            Log.d(TAG, "This transaction model is not valid. Cannot void.");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the transaction void result is returned.
     * @param result
     */
    public void onVoidTransactionResult(Result<Transaction> result) {
        if (result != null && result.isSuccess()) {
            activity.onTransactionVoided(result.getTarget().getId(), true);
        }
        else {
            activity.onTransactionVoided(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the task that will find a transaction by its ID.
     * @param transactionID
     */
    public void findTransaction(String transactionID) {
        try {
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.FIND_TRANSACTION,
                    new String[] { transactionID }
            );
            task.execute();
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when server.findTransaction(ID) has finished executing.
     * @param result
     */
    public void onFindTransactionResult(Transaction result) {
        if (result != null) {
            activity.onTransactionFound(result, true);
        }
        else {
            activity.onTransactionFound(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the call to the faux "server" that will create a payment method nonce.
     * @param paymentToken
     */
    public void createPaymentNonce(String paymentToken) {
        try {
            FauxServerTask task = new FauxServerTask(
                    activity,
                    BraintreeInteractions.CREATE_PAYMENT_NONCE,
                    new String[] { paymentToken }
            );
            task.execute();
        }
        catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the payment method nonce is ready (or null).
     * @param result
     */
    public void onPaymentNonceCreated(Result<PaymentMethodNonce> result) {
        if (result != null && result.getTarget() != null) {
            activity.onPaymentNonceCreated(result.getTarget(), true);
        }
        else {
            activity.onPaymentNonceCreated(null, false);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This inner class will handle the "server" interactions that should take place on the
     * backend. It's a container for all async tasks. We can't run network operations on the
     * main thread since it is a security risk... (we don't have server, so this is why
     * we have this issue in the first place).
     *
     * Each of these tasks will call a method in the BraintreeFauxServer class. The idea is that
     * classes that use this faux server will implement an interface that this class calls methods
     * in, and essentially this will function as a callback method interface.
     */
    private class FauxServerTask extends AsyncTask<Void, Void, Void> {

        private final String TAG = "FauxServerTask";

        private BraintreeActivity activity;
        private String task;
        private String[] otherParams;
        //-----------------------------------------------------------------------------------------

        public FauxServerTask(BraintreeActivity activity, String task, String[] otherParams) {
            this.activity = activity;
            this.task = task;
            this.otherParams = otherParams;
        }
        //-----------------------------------------------------------------------------------------

        /**
         * All tasks are done in the background. Depending on what we passed in when we
         * created the task will determine what gets done.
         *
         * @param params
         * @return null
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (task.equals(BraintreeInteractions.INIT_PAYMENT)) {
                    String customerID = otherParams[0];
                    Float amount = Float.parseFloat(otherParams[1]);
                    task_initPayment(customerID, amount);
                }
                else if (task.equals(BraintreeInteractions.TRANSACTION)) {
                    String amount = otherParams[0];
                    String nonceFromClient = otherParams[1];
                    task_executeTransaction(amount, nonceFromClient);
                }
                else if (task.equals(BraintreeInteractions.FIND_CUSTOMER)) {
                    String customerID = otherParams[0];
                    task_findCustomer(customerID);
                }
                else if (task.equals(BraintreeInteractions.CREATE_CUSTOMER)) {
                    String customerDataString = otherParams[0];
                    task_createCustomer(customerDataString);
                }
                else if (task.equals(BraintreeInteractions.VOID_TRANSACTION)) {
                    String transactionData = otherParams[0];
                    task_voidTransaction(transactionData);
                }
                else if (task.equals(BraintreeInteractions.FIND_TRANSACTION)) {
                    String transactionID = otherParams[0];
                    task_findTransaction(transactionID);
                }
                else if (task.equals(BraintreeInteractions.CREATE_PAYMENT_NONCE)) {
                    String paymentToken = otherParams[0];
                    task_createPaymentNonce(paymentToken);
                }
                else {
                    String msg = "The server could not run task : " + task;
                    Log.d(this.TAG, "Task not found : " + task);
                }
            }
            catch (Exception ex) {
                Log.d(this.TAG, ex.getMessage());
            }

            return null;
        }
        //-----------------------------------------------------------------------------------------

        /**
         * ========================================================================================
         * TASKS (FauxServerTask)
         * ========================================================================================
         */
        private void task_initPayment(String customerID, Float amount) {
            String clientToken = "";
            ClientTokenRequest clientTokenRequest = new ClientTokenRequest().customerId(customerID);
            clientToken = gateway.clientToken().generate(clientTokenRequest);

            activity.submitPayment(amount, clientToken);
        }
        //-----------------------------------------------------------------------------------------

        private void task_createCustomer(String customerDataString) {
            Gson gson = new Gson();
            BraintreeCustomer braintreeCustomer = gson.fromJson(customerDataString, BraintreeCustomer.class);

            if (braintreeCustomer != null && braintreeCustomer.isValid()) {
                CustomerRequest customerRequest = new CustomerRequest()
                        .customerId(braintreeCustomer.getCustomerId())
                        .id(braintreeCustomer.getCustomerId())
                        .firstName(braintreeCustomer.getFirstName())
                        .lastName(braintreeCustomer.getLastName())
                        .company(braintreeCustomer.getCompany())
                        .phone(braintreeCustomer.getPhone())
                        .fax(braintreeCustomer.getFax())
                        .website(braintreeCustomer.getWebsite())
                        .email(braintreeCustomer.getEmail());

                Result<Customer> result = gateway.customer().create(customerRequest);
                onCreateCustomerResult(result, braintreeCustomer);
            }
            else {
                String msg = "Failed to create customer. Could not serialize customer object";
                Log.d(this.TAG, msg);
                onCreateCustomerResult(null, null);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void task_findCustomer(String customerID) {
            Customer customer = null;
            try {
                // This here is EXTREMELY bad practice, but the documented way isn't working.
                //customer = gateway.customer().find(customerID);  // Ideally, this would work, but it doesn't...
                ResourceCollection<Customer> allCustomers = gateway.customer().all();
                for (Customer c : allCustomers) {
                    if (c.getId().equals(customerID)) {
                        customer = c;
                        break;
                    }
                }
                onFindCustomerResult(customer);
            }
            catch (Exception ex) {
                Log.d(this.TAG, ex.getMessage());
                onFindCustomerResult(null);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void task_executeTransaction(String amount, String nonceFromClient) {
            try {
                TransactionRequest request = new TransactionRequest()
                        .amount(new BigDecimal(amount))
                        .paymentMethodNonce(nonceFromClient)
                        .options()
                        .submitForSettlement(true)
                        .done();

                Result<Transaction> result = gateway.transaction().sale(request);
                onExecuteTransactionResult(result);
            }
            catch (Exception ex) {
                Log.d(this.TAG, ex.getMessage());
                onExecuteTransactionResult(null);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void task_voidTransaction(String transactionData) {
            Gson gson = new Gson();

            try {
                BraintreeTransaction transactionModel = gson.fromJson(transactionData, BraintreeTransaction.class);
                if (transactionModel != null && transactionModel.findIfIsValidModel()) {
                    Result<Transaction> result = gateway.transaction().voidTransaction(transactionModel.getTransactionID());
                    onVoidTransactionResult(result);
                }
                else {
                    throw new Exception("Not a valid transaction model");
                }
            }
            catch (Exception ex) {
                Log.d(this.TAG, ex.getMessage());
                onVoidTransactionResult(null);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void task_findTransaction(String transactionID) {
            try {
                // IDK wtf is going on but this won't work either.
                //Transaction result = gateway.transaction().find(transactionID);
                TransactionSearchRequest request = new TransactionSearchRequest();
                request = request.amount().greaterThanOrEqualTo(0);
                ResourceCollection<Transaction> collection = gateway.transaction().search(request);

                Transaction result = null;
                for (Transaction transaction : collection) {
                    if (transaction.getId().equals(transactionID)) {
                        result = transaction;
                        break;
                    }
                }
                onFindTransactionResult(result);
            }
            catch (NotFoundException ex) {
                Log.d(this.TAG, "Could not find this transaction : " + transactionID + "\n" + ex.getMessage());
                onFindTransactionResult(null);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void task_createPaymentNonce(String paymentToken) {
            try {
                Result<PaymentMethodNonce> result = gateway.paymentMethodNonce().create(paymentToken);
                onPaymentNonceCreated(result);
            }
            catch (Exception ex) {
                Log.d(this.TAG, "Could not create payment method nonce :\n" + ex.getMessage());
                onPaymentNonceCreated(null);
            }
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * GETS (BraintreeFauxServer)
     * ============================================================================================
     */
    public BraintreeDataModel getDataModel() {
        return this.dataModel;
    }

    public BraintreeActivity getActivity() {
        return this.activity;
    }

    public Context getContext() {
        return this.context;
    }

    public BraintreeGateway getGateway() {
        return this.gateway;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * SETS (BraintreeFauxServer)
     * ============================================================================================
     */
    public void setDataModel(BraintreeDataModel model) {
        if (model.findIfIsValidModel()) {
            this.dataModel = model;
        }
        else {
            Log.d(TAG, "Refused to override current data model with an invalid model");
        }
    }

    public void setGateway(BraintreeGateway gateway) {
        this.gateway = gateway;
    }

    public void setActivity(_TestBraintreeActivity activity) {
        this.activity = activity;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generic method that may need to change later on. It creates a local model for a
     * transaction with the current user as the initiator.
     *
     * @param result
     * @return braintreeTransaction
     */
    public BraintreeTransaction createTransaction(Result<Transaction> result) {
        final Timestamp now = new Timestamp(new Date());
        BraintreeTransaction transaction = new BraintreeTransaction();

        transaction.setTransactionID(result.getTarget().getId());
        transaction.setInitiatorUID(ActivityUtils.CURRENT_USERNAME);
        transaction.setReceiverUID(BraintreeTransaction.DEFAULT_RECEIVER); // TODO?
        transaction.setPaymentMethod(result.getTarget().getPaymentInstrumentType().toUpperCase());
        transaction.setDateSubmitted(now);
        transaction.setDateUpdated(now);
        transaction.setAmount(Float.parseFloat(ActivityUtils.getFormattedFloat(result.getTarget().getAmount().floatValue())));
        transaction.setIsVoid(false);
        transaction.setInvolved(new ArrayList<String>() {{ add(transaction.getInitiatorUID()); add(transaction.getReceiverUID()); }});

        return transaction;
    }
    //---------------------------------------------------------------------------------------------
}
