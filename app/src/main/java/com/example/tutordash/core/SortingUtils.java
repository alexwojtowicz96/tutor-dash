package com.example.tutordash.core;

import android.util.Log;

import com.example.tutordash.model.User;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * Use this class to sort results (collection of users) Keep in mind, none of these methods
 * are pruning the results at all. They simply rearrange the order of them.
 */
public class SortingUtils {
    public static final String TAG = "SortingUtils";

    public static final int SORT_PRICE_HIGH_TO_LOW = 1000;
    public static final int SORT_PRICE_LOW_TO_HIGH = 1001;
    public static final int SORT_RATING_LOW_TO_HIGH = 2000;
    public static final int SORT_RATING_HIGH_TO_LOW = 2001;
    public static final int SORT_DISTANCE_CLOSE_TO_FAR = 3000;
    public static final int SORT_DISTANCE_FAR_TO_CLOSE = 3001;

    public static final String SORT1_TEXT = "Price (Lowest to Highest)";
    public static final String SORT2_TEXT = "Price (Highest to Lowest)";
    public static final String SORT3_TEXT = "Rating (Lowest to Highest)";
    public static final String SORT4_TEXT = "Rating (Highest to Lowest)";
    public static final String SORT5_TEXT = "Distance (Closest to Farthest)";
    public static final String SORT6_TEXT = "Distance (Farthest to Closest)";
    //---------------------------------------------------------------------------------------------

    public SortingUtils() {}

    /**
     * Sorts list of users based on price descending.
     *
     * @param userList
     * @param targetCourse
     * @return sorted list of users
     */
    public static ArrayList<User> priceHighToLow(ArrayList<User> userList, String targetCourse) {
        final int initialSize = userList.size();
        ArrayList<User> sortedUsers = new ArrayList<User>();
        ArrayList<String> notFound = new ArrayList<String>();
        for (User u : userList) {
            if (!u.getCourses().containsKey(targetCourse)) {
                notFound.add(u.getuName());
            }
        }
        if (notFound.size() > 0) {
            String error = "Error when sorting by price : \n" +
                    "There were users who didn't have a course '" + targetCourse + "'\n";
            for (String u : notFound) {
                error += ("\t => " + u + "\n");
            }
            Log.d(TAG, error);
            sortedUsers = userList;
        }
        else {
            ArrayList<Integer> indexesToSkip = new ArrayList<Integer>();
            for (User user : userList) {
                Float max = Float.valueOf(0);
                Integer indexWithMax = -1;
                for (int i = 0; i < userList.size(); i++) {
                    User u = userList.get(i);
                    Float coursePrice = u.getCourses().get(targetCourse).getPayRate().floatValue();
                    if (!indexesToSkip.contains(i) && coursePrice > max) {
                        max = coursePrice;
                        indexWithMax = i;
                    }
                }
                if (indexWithMax.equals(-1)) { // The rest of the list has a price of $0.00 for this course
                    int x = 0;
                    for (User u : userList) {
                        if (!indexesToSkip.contains(x)) {
                            sortedUsers.add(u);
                        }
                        x += 1;
                    }
                    break;
                }
                else {
                    indexesToSkip.add(indexWithMax);
                    sortedUsers.add(userList.get(indexWithMax));
                }
            }
        }

        if (sortedUsers.size() != initialSize) {
            Log.d(TAG, "An error occurred while soring by price. Initial size of list = "
                    + initialSize + ", final size of list = " + sortedUsers.size());
            return userList;
        }
        else {
            return sortedUsers;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts list of users based on price ascending.
     *
     * @param userList
     * @param targetCourse
     * @return sorted list of users
     */
    public static ArrayList<User> priceLowToHigh(ArrayList<User> userList, String targetCourse) {
        ArrayList<User> sortedUsers = new ArrayList<User>();
        ArrayList<User> tmp = SortingUtils.priceHighToLow(userList, targetCourse);
        for (int i = tmp.size() - 1; i > -1; i--) {
            sortedUsers.add(tmp.get(i));
        }

        return sortedUsers;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts list of users based on rating descending.
     *
     * @param userList
     * @param targetCourse - pass in "" if you are sorting based on overall rating
     * @return sorted list of users
     */
    public static ArrayList<User> ratingHighToLow(ArrayList<User> userList, String targetCourse, boolean isTuteeMode) {
        final int initialSize = userList.size();
        ArrayList<User> sortedUsers = new ArrayList<User>();
        ArrayList<String> notFound = new ArrayList<String>();
        if (!targetCourse.equals("")) {
            for (User u : userList) {
                if (!u.getCourses().containsKey(targetCourse)) {
                    notFound.add(u.getuName());
                }
            }
        }
        if (notFound.size() > 0) {
            String error = "Error when sorting by rating : \n" +
                    "There were users who didn't have a course '" + targetCourse + "'\n";
            for (String u : notFound) {
                error += ("\t => " + u + "\n");
            }
            Log.d(TAG, error);
            sortedUsers = userList;
        }
        else {
            ArrayList<Integer> indexesToSkip = new ArrayList<Integer>();
            for (User user : userList) {
                Float max = Float.valueOf(0);
                Integer indexWithMax = -1;
                for (int i = 0; i < userList.size(); i++) {
                    User u = userList.get(i);
                    Float rating;
                    if (isTuteeMode) {
                        if (!targetCourse.equals("")) {
                            rating = u.getCourses().get(targetCourse).getRating().floatValue();
                        }
                        else {
                            rating = u.getTutorRating().floatValue();
                        }
                    }
                    else {
                        rating = u.getTuteeRating().floatValue();
                    }
                    if (!indexesToSkip.contains(i) && rating > max) {
                        max = rating;
                        indexWithMax = i;
                    }
                }
                if (indexWithMax.equals(-1)) { // The rest of the ratings are 0 (either overall or course ratings)
                    int x = 0;
                    for (User u : userList) {
                        if (!indexesToSkip.contains(x)) {
                            sortedUsers.add(u);
                        }
                        x += 1;
                    }
                    break;
                }
                else {
                    indexesToSkip.add(indexWithMax);
                    sortedUsers.add(userList.get(indexWithMax));
                }
            }
        }

        if (sortedUsers.size() != initialSize) {
            Log.d(TAG, "An error occurred while soring by rating. Initial size of list = "
                    + initialSize + ", final size of list = " + sortedUsers.size());
            return userList;
        }
        else {
            return sortedUsers;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts list of users based on rating ascending.
     *
     * @param userList
     * @param targetCourse - pass in "" if you are sorting based on overall rating
     * @return sorted list of users
     */
    public static ArrayList<User> ratingLowToHigh(ArrayList<User> userList, String targetCourse, boolean isTuteeMode) {
        ArrayList<User> sortedUsers = new ArrayList<User>();
        ArrayList<User> tmp = SortingUtils.ratingHighToLow(userList, targetCourse, isTuteeMode);
        for (int i = tmp.size() - 1; i > -1; i--) {
            sortedUsers.add(tmp.get(i));
        }

        return sortedUsers;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts list of users based distance away from current user (descending).
     * If the current user is null, then this returns the same list, but the current
     * user really should never be null anyway...
     *
     * @param userList
     * @return sorted list of users
     */
    public static ArrayList<User> distanceFarToClose(ArrayList<User> userList) {
        ArrayList<User> sortedUsers = new ArrayList<User>();
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            sortedUsers = userList;
        }
        else {
            final Map<String, Float> userLocMap = LocationService.getUserLocationMap(userList);
            final Set<String> keys = userLocMap.keySet();
            if (keys.size() != userList.size()) {
                Log.d(TAG, "For some reason, the size of the map does not equal size of userList");
                sortedUsers = userList;
            }
            else {
                ArrayList<Integer> indexesToSkip = new ArrayList<Integer>();
                for (int i = 0; i < userList.size(); i++) {
                    Float max = Float.valueOf(-1);
                    Integer indexWithMax = -1;
                    for (int j = 0; j < userList.size(); j++) {
                        User u = userList.get(j);
                        if (!keys.contains(u.getuName())) {
                            Log.d(TAG, "Critical Error : Could not find '" + u.getuName() + "' in keyset!");
                            sortedUsers = userList;
                            break;
                        }
                        else {
                            Float cUserDistance = userLocMap.get(u.getuName());
                            if (!indexesToSkip.contains(j) && cUserDistance > max) {
                                indexWithMax = j;
                            }
                        }
                    }
                    if (!indexWithMax.equals(-1)) {
                        indexesToSkip.add(indexWithMax);
                        sortedUsers.add(userList.get(indexWithMax));
                    }
                    else {
                        // The rest of the users have -1 as their distance away (null location)
                        for (int x = 0; x < userList.size(); x++) {
                            if (!indexesToSkip.contains(x)) {
                                sortedUsers.add(userList.get(x));
                            }
                        }
                        break;
                    }
                }
            }
        }

        if (sortedUsers.size() != userList.size()) {
            Log.d(TAG, "ERROR! sortedUsers.size = " + sortedUsers.size() +
                    "\n\tShould have been : " + userList.size());
            return userList;
        }
        else {
            return sortedUsers;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts list of users based distance away from current user (ascending).
     * If the current user is null, then this returns the same list, but the current
     * user really should never be null anyway...
     *
     * @param userList
     * @return sorted list of users
     */
    public static ArrayList<User> distanceCloseToFar(ArrayList<User> userList) {
        ArrayList<User> sortedUsers = new ArrayList<User>();
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            sortedUsers = userList;
        }
        else {
            ArrayList<User> reverseSorted = SortingUtils.distanceFarToClose(userList);
            for (int i = reverseSorted.size() - 1; i > -1; i--) {
                sortedUsers.add(reverseSorted.get(i));
            }
        }
        return sortedUsers;
    }
    //---------------------------------------------------------------------------------------------
}
