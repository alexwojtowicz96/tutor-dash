package com.example.tutordash.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;

/**
 * This class is setup to ONLY handle alarms dealing with sessions that are ready to be
 * entered.
 */
public class AlarmService extends BroadcastReceiver {

    public static final String TAG = "AlarmService";

    public static final int DEFAULT_FLAG = 0;
    public static final int EXPIRE_FLAG = 9577;
    public static final int ALARM_OFFSET = 1;
    public static final int GENERIC_ALARM_ID = 85721;

    public AlarmService() {}
    //---------------------------------------------------------------------------------------------

    /**
     * The assumption here is that we are only receiving type of alarm. I can't find a way
     * to make this flexible, so I think this is how it has to be.
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final int flag = intent.getIntExtra("flag", -1);

        if (auth.getCurrentUser() != null) {
           final String authEmail = auth.getCurrentUser().getEmail();
           database.collection("users").whereEqualTo("authEmail", authEmail)
                   .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && task.getResult() != null && task.getResult().size() == 1) {
                                for (DocumentSnapshot doc : task.getResult()) {
                                    try {
                                        User user = doc.toObject(User.class);
                                        if (user == null || !user.isValidUser()) {
                                            throw new Exception("User is not valid : " + doc.getId());
                                        }
                                        else {
                                            // TODO send extra data so that it takes the user
                                            //  to the session from clicking on the notification.
                                            //  This may be harder than I think though...

                                            if (flag == DEFAULT_FLAG) {
                                                final String title = "Reminder: You have a session to attend!";
                                                final String msg = user.getfName() + ", your session is about to begin! Are you ready?";
                                                final String topic = "/topics/" + MessageService.DM_TOPIC + user.getuName();
                                                NotificationService notificationService = new NotificationService(context);
                                                notificationService.setTopic(topic);
                                                notificationService.setHighPriority(true);
                                                notificationService.setTitle(title);
                                                notificationService.setMessage(msg);

                                                notificationService.sendNotification(null);
                                            }
                                            else if (flag == EXPIRE_FLAG) {
                                                // TODO somehow this needs to cancel the session
                                                //  it needs to know if the current users is looking
                                                //  at the handshake activity. If they are, finish
                                                //  that activity and toast expiration msg.
                                                //  It also needs to know who prevented the handshake
                                                //  from completing (if applicable).
                                                //  All of this data needs to be updated in Firestore.
                                            }
                                            else {
                                                throw new Exception("This flag was not expected : " + flag);
                                            }
                                        }
                                    }
                                    catch (Exception ex) {
                                        Log.d(TAG, ex.getMessage());

                                        // If something happens, send it to me... Strictly temporary
                                        final String title = "ERROR";
                                        final String msg = ex.getMessage();
                                        final String topic = "/topics/" + MessageService.DM_TOPIC + "awojt001";
                                        NotificationService notificationService = new NotificationService(context);
                                        notificationService.setTopic(topic);
                                        notificationService.setHighPriority(true);
                                        notificationService.setTitle(title);
                                        notificationService.setMessage(msg);

                                        notificationService.sendNotification(null);
                                    }
                                }
                            }
                            else {
                                String ex = "";
                                if (task.getException() != null) {
                                    ex = task.getException().getMessage();
                                }
                                Log.d(TAG, "Failed to retrieve user with authEmail : " + authEmail + "\n" + ex);
                            }
                        }
                    });
        }
        else {
            Log.d(TAG, "Could not fire alarm since current user was not signed in!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets an alarm minutesBefore minutes the date with the given alarmID
     *
     * @param context
     * @param date
     * @param minutesBefore
     * @param alarmID
     */
    public static void setAlarm(Context context, Date date, int minutesBefore, int alarmID, int flag) {
        long dateMillis = date.getTime();
        long beforeMillis = Long.valueOf(minutesBefore) * Long.valueOf(60000);
        long alarmMillis = dateMillis - beforeMillis;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra("flag", flag);
        intent.setAction("com.example.tutordash.core.AlarmService");
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmID, intent, 0);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmMillis, pendingIntent);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Cancels the alarm with the given alarmID
     *
     * @param context
     * @param alarmID
     */
    public static void cancelAlarm(Context context, int alarmID, int flag) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra("flag", flag);
        intent.setAction("com.example.tutordash.core.AlarmService");
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmID, intent, 0);

        alarmManager.cancel(pendingIntent);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the two alarms for a session (alert before session ends and alert when session should
     * expire).
     *
     * @param context
     * @param session
     */
    public static void setSessionAlarms(Context context, Session session) {
        final int timeCutoff = Session.WAITING_WINDOW / 2;

        // This alarm will alert the user when the session is about to start
        setAlarm(
                context,
                session.getStart().toDate(),
                timeCutoff,
                session.getAlarmCode(),
                DEFAULT_FLAG
        );

        // This alarm will trigger an event to occur (basically it is signaling the expiration of a session)
        setAlarm(
                context,
                CalendarService.appendMinutesToDate(session.getStart().toDate(), timeCutoff),
                0,
                session.getAlarmCode() + ALARM_OFFSET,
                EXPIRE_FLAG
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Cancels the two alarms for a session (alert upon start and alert upon expiration).
     *
     * @param context
     * @param session
     */
    public static void cancelSessionAlarms(Context context, Session session) {
        cancelAlarm(context, session.getAlarmCode(), DEFAULT_FLAG);
        cancelAlarm(context, session.getAlarmCode() + ALARM_OFFSET, EXPIRE_FLAG);
    }
    //---------------------------------------------------------------------------------------------
}
