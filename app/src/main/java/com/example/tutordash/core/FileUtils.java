package com.example.tutordash.core;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.io.File;

/**
 * Use this class to handle file I/O operations on Android clients.
 */
public class FileUtils {

    public static final String TAG = "FileUtils";
    //---------------------------------------------------------------------------------------------

    /**
     * Creates a new intent, which allows the user to select PDFs using the file manager
     *
     * @param activity
     */
    public static void selectPdf(Activity activity) {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(intent, PermissionUtils.REQUEST_CODE_FOR_FILE_SELECT);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Creates a new intent, which allows the user to select an image off their phone
     *
     * @param activity
     */
    public static void selectImage(Activity activity) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(intent, PermissionUtils.REQUEST_CODE_FOR_AVATAR_UPLOAD);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the true file extension from a uri.
     *
     * @param uri
     * @param context
     * @return extension
     */
    public static String getExtension(Uri uri, Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generates a random sequence of characters of specified length. You can use this
     * to generate tokens, passwords, etc. Whatever you want!
     *
     * @param length
     * @return token
     */
    public static String generateToken(int length) {
        char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '_', '!',
            '%', '&', '(', ')'
        };

        StringBuilder b = new StringBuilder();
        Random random = new Random();
        final int low = 0;
        final int high = chars.length;

        for (int i = 0; i < length; i++) {
            int num = random.nextInt(high - low) + low;
            if (random.nextInt(2) == 1) {
                b.append(new Character(chars[num]).toString().toUpperCase());
            }
            else {
                b.append(chars[num]);
            }
        }

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generates a token of length within the specified range.
     *
     * @param lowerBound
     * @param upperBound
     * @return token
     */
    public static String generateToken(int lowerBound, int upperBound) {
        Random random = new Random();
        int length = random.nextInt(upperBound - lowerBound) + lowerBound;

        return generateToken(length);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Generates a random email. Use this for the authentication hack dealing with password
     * resets.
     */
    public static String generateRandomEmail() {
        char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'z',
                '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
        };

        StringBuilder b = new StringBuilder();
        Random random = new Random();
        final int low = 0;
        final int high = chars.length;

        // generate username
        int num = random.nextInt(26 - low) + low;
        b.append(new Character(chars[num]).toString());
        num = random.nextInt(16 - 10) + 10;
        for (int i = 0; i < num; i++) {
            int n = random.nextInt(high - low) + low;
            b.append(new Character(chars[n]).toString());
        }
        b.append("@");

        // generate lvl 2 domain
        for (int i = 0; i < 3; i++) {
            int n = random.nextInt(26 - low) + low;
            b.append(new Character(chars[n]).toString());
        }
        b.append(".");

        // generate TLD
        for (int i = 0; i < 3; i++) {
            int n = random.nextInt(26 - low) + low;
            b.append(new Character(chars[n]).toString());
        }

        return b.toString();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Given a directory path, convert it to a list of paths where List[0] is the most
     * general directory, and List[size-1] is the most specific directory. When you use
     * this, make sure not to include a filename.
     *
     * @param path
     * @return List{"path", "to", "file"}
     */
    private static List<String> getDirectoryStructure(String path) {
        List<String> structure = new LinkedList<String>();
        char[] pathArr = path.toCharArray();
        String dir = new String();

        for (int i = 0; i < pathArr.length; i++) {
            char curr = pathArr[i];
            if (curr == '/') {
                structure.add(dir);
                dir = new String();
            }
            else {
                dir += curr;
            }
        }
        if (!dir.equals(new String())) {
            structure.add(dir);
        }
        List<String> tmp = new LinkedList<String>();
        for (String string : structure) {
            if (!string.equals(new String())) {
                tmp.add(string);
            }
        }
        structure = tmp;

        return structure;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to get the file object from a uri.
     *
     * @param context
     * @param uri
     * @return a new file from the Uri (not checked for validity)
     */
    public static File getDownloadedFileFromUri(Context context, Uri uri) throws IOException {
        // Create strings for each applicable directory provided by Environment
        String downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getCanonicalPath();
        String alarmsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS).getCanonicalPath();
        String dcimDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getCanonicalPath();
        String moviesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getCanonicalPath();
        String musicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getCanonicalPath();
        String notifsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS).getCanonicalPath();
        String picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getCanonicalPath();
        String podcastsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS).getCanonicalPath();
        String ringTonesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES).getCanonicalPath();

        String uriString = uri.toString();
        String realPath = new String();

        // Get real path of file, depending on directory
        if (uriString.contains("Download")) {
            String[] splits = uriString.split("Download", 2);
            String pathFromDownloadsDir = splits[1].replace("%2F", "/");
            realPath = downloadsDir + pathFromDownloadsDir;
        }
        else if (uriString.contains("downloads")) {
            // Different case for the downloads folder accessed outside of internal storage(?)
            // See https://imgur.com/a/ZfVsdOT
            realPath = downloadsDir + "/" + DocumentFile.fromSingleUri(context, uri).getName();
        }
        else if (uriString.contains("Alarms")) {
            String[] splits = uriString.split("Alarms", 2);
            String pathFromAlarmsDir = splits[1].replace("%2F", "/");
            realPath = alarmsDir + pathFromAlarmsDir;
        }
        else if (uriString.contains("DCIM")) {
            String[] splits = uriString.split("DCIM", 2);
            String pathFromDCIMDir = splits[1].replace("%2F", "/");
            realPath = dcimDir + pathFromDCIMDir;
        }
        else if (uriString.contains("Movies")) {
            String[] splits = uriString.split("Movies", 2);
            String pathFromMoviesDir = splits[1].replace("%2F", "/");
            realPath = moviesDir + pathFromMoviesDir;
        }
        else if (uriString.contains("Music")) {
            String[] splits = uriString.split("Music", 2);
            String pathFromMusicDir = splits[1].replace("%2F", "/");
            realPath = musicDir + pathFromMusicDir;
        }
        else if (uriString.contains("Notifications")) {
            String[] splits = uriString.split("Notifications", 2);
            String pathFromNotifsDir = splits[1].replace("%2F", "/");
            realPath = notifsDir + pathFromNotifsDir;
        }
        else if (uriString.contains("Pictures")) {
            String[] splits = uriString.split("Pictures", 2);
            String pathFromPicsDir = splits[1].replace("%2F", "/");
            realPath = picturesDir + pathFromPicsDir;
        }
        else if (uriString.contains("Podcasts")) {
            String[] splits = uriString.split("Podcasts", 2);
            String pathFromPodcastsDir = splits[1].replace("%2F", "/");
            realPath = podcastsDir + pathFromPodcastsDir;
        }
        else if (uriString.contains("Ringtones")) {
            String[] splits = uriString.split("Ringtones", 2);
            String pathFromRingtonesDir = splits[1].replace("%2F", "/");
            realPath = ringTonesDir + pathFromRingtonesDir;
        }

        // In the case where the PDF is not in any of the directories listed above,
        // attempt to get proper file path of the selected PDF.
        else {
            if (uriString.contains("primary%3A")){
                // For custom folders inside internal storage
                String [] splits = uriString.split("primary%3A");
                String pathFromCustomDir = splits[1].replace("%2F", "/");
                realPath = Environment.getExternalStorageDirectory().toString() + "/" + pathFromCustomDir;
            }
            else {
                // For files that are just in root
                realPath = Environment.getExternalStorageDirectory().toString() + "/"
                        + DocumentFile.fromSingleUri(context, uri).getName();
            }
        }

        // Prep file and do pre-checks
        File f = new File (realPath.replace("%20", " "));
        if (f.isFile() && f.exists()) {
            Log.d(TAG, "File successfully found.");
        }
        else {
            Log.d(TAG, "File does not exist or the file path is incorrect.");
        }

        // Return file to check if is an official transcript
        return f;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the filename from Uri.
     *
     * @param context
     * @param uri
     * @return filename from Uri as a string
     */
    public static String getFileNameFromUri(Context context, Uri uri) {
        return DocumentFile.fromSingleUri(context, uri).getName();
    }
    //---------------------------------------------------------------------------------------------
}
