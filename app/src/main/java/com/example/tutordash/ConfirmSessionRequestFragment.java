package com.example.tutordash;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.PaymentService;
import com.example.tutordash.dialog.SubmitRequestDialog;

/**
 * Fragment that displays right before the user sends a session request.
 */
public class ConfirmSessionRequestFragment extends Fragment {

    public TextView tutorView;
    public TextView courseView;
    public TextView dateView;
    public TextView timeView;
    public TextView deliveryMethodView;
    public TextView totalView;
    public Button submitBtn;
    //---------------------------------------------------------------------------------------------

    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_confirm_session_request, container, false);

        this.tutorView = view.findViewById(R.id.requestSummary1);
        this.courseView = view.findViewById(R.id.requestSummary2);
        this.dateView = view.findViewById(R.id.requestSummary3);
        this.timeView = view.findViewById(R.id.requestSummary4);
        this.deliveryMethodView = view.findViewById(R.id.requestSummary5);
        this.totalView = view.findViewById(R.id.requestSummary6);
        this.submitBtn = view.findViewById(R.id.actuallySubmitBtn);

        initDataInViews();
        setSubmitListener();

        return view;
    }
    //---------------------------------------------------------------------------------------------

    public void initDataInViews() {
        ScheduleSessionActivity activity = (ScheduleSessionActivity) getActivity();

        this.tutorView.setText(ActivityUtils.TMP_USR.getuName());
        this.courseView.setText(ActivityUtils.COURSE_OF_INTEREST.first);
        this.deliveryMethodView.setText(activity.deliveryMethodText);
        this.dateView.setText(activity.monthSpinnerText + " " + activity.daySpinnerText + ", " + activity.yearSpinnerText);
        this.timeView.setText(activity.startTimeSpinnerText + " - " + activity.endTimeSpinnerText);
        this.totalView.setText("$" + PaymentService.getTotalAsString(
                CalendarService.getTimeInterval(
                        CalendarService.convertTimeToMilitaryTime(activity.startTimeSpinnerText),
                        CalendarService.convertTimeToMilitaryTime(activity.endTimeSpinnerText)
                ),
                ActivityUtils.COURSE_OF_INTEREST.second.getPayRate().floatValue()
        ));
    }
    //---------------------------------------------------------------------------------------------

    public void setSubmitListener() {
        ScheduleSessionActivity activity = (ScheduleSessionActivity) getActivity();

        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitRequestDialog dialog = new SubmitRequestDialog();
                dialog.show(activity.getSupportFragmentManager(), "SubmitRequestDialog");
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
