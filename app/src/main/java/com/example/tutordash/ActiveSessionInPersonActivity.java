package com.example.tutordash;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.PaymentService;
import com.example.tutordash.dialog.FinishSessionDialog;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * This is the original active session activity...
 * Active session activity for IN-PERSON sessions.
 */
public class ActiveSessionInPersonActivity extends AppCompatActivity {

    public static final String TAG = "ActiveSession_In-Person";

    public Chronometer chronometer;
    public Button endSessionButton;
    public TextView classText;
    public TextView tutorNameText;
    public TextView tuteeNameText;
    public CircleImageView tutorAvatar;
    public CircleImageView tuteeAvatar;
    public ConstraintLayout iHaveFinishedLayout;
    public ConstraintLayout otherUserFinishedLayout;
    public TextView iFinishedTextView;
    public TextView otherUserFinishedTextView;
    public Button undoFinishButton;
    public Button confirmFinishButton;

    public boolean hasDisplayedEndPrompt = false;
    public boolean isDone = false;

    public FirebaseFirestore db;
    public CollectionReference cr;
    public DocumentReference sessionRef;
    public StorageReference storageReference;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_session);

        if (hasIntegrity()) {
            this.db = FirebaseFirestore.getInstance();
            this.cr = db.collection("sessions");
            this.sessionRef = cr.document(ActivityUtils.SESSION_REQUESTED.getSessionID());
            this.storageReference = FirebaseStorage.getInstance().getReference();

            this.chronometer = findViewById(R.id.chronometer);
            this.endSessionButton = findViewById(R.id.endSessionButton);
            this.classText = findViewById(R.id.className);
            this.tutorNameText = findViewById(R.id.leftName);
            this.tuteeNameText = findViewById(R.id.rightName);
            this.tutorAvatar = findViewById(R.id.leftAvatar);
            this.tuteeAvatar = findViewById(R.id.rightAvatar);
            this.iHaveFinishedLayout = findViewById(R.id.awaitingFinishLayout);
            this.otherUserFinishedLayout = findViewById(R.id.alertOtherUserHasFinishedLayout);
            this.iFinishedTextView = findViewById(R.id.awaitingFinishView);
            this.otherUserFinishedTextView = findViewById(R.id.alertOtherUserHasFinishedView);
            this.undoFinishButton = findViewById(R.id.undoFinishButton);
            this.confirmFinishButton = findViewById(R.id.confirmFinishButton);

            initView();
            initTimer();
            setButtonListeners(false);
            setSessionDocumentListener();
        }
        else {
            String msg = "Something went wrong. This is a problem!";
            Log.d(TAG, "Data did not have integrity!");
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
    }
    //---------------------------------------------------------------------------------------------

    public void initView() {
        Session currentSession = ActivityUtils.SESSION_REQUESTED;

        this.classText.setText(currentSession.getCourseID());
        this.tutorNameText.setText(currentSession.getTutorUID());
        this.tuteeNameText.setText(currentSession.getTuteeUID());

        if (currentSession.getTutorPicUrl() != null && !currentSession.getTutorPicUrl().isEmpty()) {
            GlideApp.with(getApplicationContext()).load(storageReference
                    .child(currentSession.getTutorPicUrl())).into(this.tutorAvatar);
        }
        if (currentSession.getTuteePicUrl() != null && !currentSession.getTuteePicUrl().isEmpty()) {
            GlideApp.with(getApplicationContext()).load(storageReference
                    .child(currentSession.getTuteePicUrl())).into(this.tuteeAvatar);
        }

        String otherUsername = currentSession.getTuteeUID();
        if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(currentSession.getTuteeUID())) {
            otherUsername = currentSession.getTutorUID();
        }
        this.otherUserFinishedTextView.setText(otherUserFinishedTextView.getText().toString().replace("%USER%", otherUsername));
        this.iFinishedTextView.setText(iFinishedTextView.getText().toString().replace("%USER%", otherUsername));

        // Hide these by default
        this.iHaveFinishedLayout.setVisibility(View.GONE);
        this.otherUserFinishedLayout.setVisibility(View.GONE);
    }
    //---------------------------------------------------------------------------------------------

    public void initTimer() {
        this.chronometer.setBase(SystemClock.elapsedRealtime());
        this.chronometer.setText("00:00:00");
        this.chronometer.start();

        this.chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - (h * 3600000)) / (60000);
                int s = (int) (time - (h * 3600000) - (m * 60000)) / 1000;
                String t = (h < 10 ? "0" + h : h)
                        + ":" + (m < 10 ? "0" + m : m)
                        + ":" + (s < 10 ? "0" + s : s);
                chronometer.setText(t);

                int minutesElapsed = CalendarService.getMinutesFromTimeString(chronometer.getText().toString().trim());
                if (minutesElapsed == ActivityUtils.SESSION_REQUESTED.getExpectedMinutes()) {
                    if (!hasDisplayedEndPrompt) {
                        hasDisplayedEndPrompt = true;
                        // TODO display a dialog that asks the users if they want to end.
                        //  Why? Because the expected length of the session has been reached.
                        //  If possible, output a sound as well.
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void setSessionDocumentListener() {
        sessionRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(
                    @androidx.annotation.Nullable DocumentSnapshot documentSnapshot,
                    @androidx.annotation.Nullable FirebaseFirestoreException e
            ) {
                if (!isDone) {
                    if (e != null) {
                        String msg = "Something went wrong on the database server";
                        Log.d(TAG, e.getMessage());
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                    else if (documentSnapshot != null && documentSnapshot.exists()) {
                        if (documentSnapshot.contains("tutorHasConcluded")) {
                            ActivityUtils.SESSION_REQUESTED.setTutorHasConcluded(
                                    (boolean) documentSnapshot.get("tutorHasConcluded"));
                        }
                        if (documentSnapshot.contains("tuteeHasConcluded")) {
                            ActivityUtils.SESSION_REQUESTED.setTuteeHasConcluded(
                                    (boolean) documentSnapshot.get("tuteeHasConcluded"));
                        }
                        checkConclusionStatus();
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks the status of the current session. If either the tutor or tutee have opted to
     * end the session, action must be taken. If both users have ended, then this activity must
     * end ASAP.
     */
    public void checkConclusionStatus() {
        Session session = ActivityUtils.SESSION_REQUESTED;

        if (session.getTutorHasConcluded() && session.getTuteeHasConcluded()) {
            // Both users have decided to end the session
            clickableEndButton(false);
            this.iHaveFinishedLayout.setVisibility(View.GONE);
            this.otherUserFinishedLayout.setVisibility(View.GONE);
            finishConcludingSession();
        }
        else {
            if (session.getTuteeHasConcluded() || session.getTutorHasConcluded()) {
                // Only one user has decided to end the session
                clickableEndButton(false);
                if (session.getTutorUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                    // I am the tutor
                    if (session.getTutorHasConcluded()) {
                        this.iHaveFinishedLayout.setVisibility(View.VISIBLE);
                        this.otherUserFinishedLayout.setVisibility(View.GONE);
                    }
                    else {
                        this.iHaveFinishedLayout.setVisibility(View.GONE);
                        this.otherUserFinishedLayout.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    // I am the tutee
                    if (session.getTuteeHasConcluded()) {
                        this.iHaveFinishedLayout.setVisibility(View.VISIBLE);
                        this.otherUserFinishedLayout.setVisibility(View.GONE);
                    }
                    else {
                        this.iHaveFinishedLayout.setVisibility(View.GONE);
                        this.otherUserFinishedLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
            else {
                // Neither user has decided to end the session
                clickableEndButton(true);
                this.iHaveFinishedLayout.setVisibility(View.GONE);
                this.otherUserFinishedLayout.setVisibility(View.GONE);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Updates local and remote data with the desireToEnd option. If you want to end the session,
     * pass in TRUE. If you want to undo the session ending, pass in FALSE.
     *
     * @param desireToEnd
     */
    public void endSession(boolean desireToEnd) {
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        Session session = ActivityUtils.SESSION_REQUESTED;

        if (cUser.getuName().equals(session.getTutorUID())) {
            sessionRef.update("tutorHasConcluded", desireToEnd).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        ActivityUtils.SESSION_REQUESTED.setTutorHasConcluded(desireToEnd);
                    }
                    else {
                        String msg = "Failed to conclude session for tutee";
                        Log.d(TAG, msg);
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else if (cUser.getuName().equals(session.getTuteeUID())) {
            sessionRef.update("tuteeHasConcluded", desireToEnd).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        ActivityUtils.SESSION_REQUESTED.setTuteeHasConcluded(desireToEnd);
                    }
                    else {
                        String msg = "Failed to conclude session for tutor";
                        Log.d(TAG, msg);
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else {
            clickableEndButton(true);
            String msg = "Error: Username does not match tutor or tutee";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * If you only want to initialize the end session button, then pass in TRUE.
     *
     * @param onlyEndSessionBtn
     */
    public void setButtonListeners(boolean onlyEndSessionBtn) {
        this.endSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickableEndButton(false);
                FinishSessionDialog dialog = new FinishSessionDialog();
                dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
            }
        });

        if (!onlyEndSessionBtn) {
            this.confirmFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickableEndButton(false);
                    FinishSessionDialog dialog = new FinishSessionDialog();
                    dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
                }
            });

            this.undoFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    endSession(false);
                    clickableEndButton(true);
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    public void clickableEndButton(boolean option) {
        if (option) {
            setButtonListeners(true);
        }
        else {
            this.endSessionButton.setOnClickListener(null);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks to see if the data has integrity.
     *
     * @return TRUE if hasIntegrity, FALSE otherwise
     */
    public boolean hasIntegrity() {
        boolean hasIntegrity = true;
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        Session cSession = ActivityUtils.SESSION_REQUESTED;
        List<String> logMessages = new ArrayList<String>();

        if (cUser == null || !cUser.isValidUser() || cSession == null || !cSession.findIsValidSession()) {
            hasIntegrity = false;
            if (cUser == null) {
                logMessages.add("ActivityUtils.CURRENT_USER_OBJECT was null!");
            }
            else if (!cUser.isValidUser()) {
                logMessages.add("The current user was not in a valid state (.isValidUser() returned FALSE)");
            }
            if (cSession == null) {
                logMessages.add("ActivityUtils.SESSION_REQUESTED was null!");
            }
            else if (!cSession.findIsValidSession()) {
                logMessages.add("The current session was not in a valid state (.findIsValidSession() returned FALSE)");
            }
        }
        if (logMessages.size() > 0) {
            for (String msg : logMessages) {
                Log.d(TAG, msg);
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This is a very important method!
     *
     * Essentially, it will update the session time based on the time upon both users concluding.
     * It will also update the expected amount owed and expected minutes of this session to
     * reflect the actual values. Once this is done, it will actually transfer the money from
     * the tutee's outgoing payment account into the tutor's incoming payment account. If all
     * of this goes well, then it will finish the activity and move onto the rating activity.
     */
    public void finishConcludingSession() {
        this.chronometer.stop();
        String rawTimeString = this.chronometer.getText().toString().trim();
        final int minutesElapsed = CalendarService.getMinutesFromTimeString(rawTimeString);
        final Float amountOwed = PaymentService.getTotal(minutesElapsed, ActivityUtils.SESSION_REQUESTED.getHourlyPayRate());
        final Session session = ActivityUtils.SESSION_REQUESTED;

        ActivityUtils.SESSION_REQUESTED.setExpectedAmountOwed(amountOwed);
        ActivityUtils.SESSION_REQUESTED.setHasHappened(true);
        ActivityUtils.SESSION_REQUESTED.setExpectedMinutes(minutesElapsed);
        db.collection("sessions").document(session.getSessionID())
                .set(ActivityUtils.SESSION_REQUESTED).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    db.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName())
                            .update("pendingSessionRatingID", session.getSessionID())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ActivityUtils.CURRENT_USER_OBJECT.setPendingSessionRatingID(session.getSessionID());
                                        makeSessionIDFinished();
                                    }
                                    else {
                                        String msg = "Something went wrong. This is bad!";
                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                        String ex = "";
                                        if (task.getException() != null) {
                                            ex = task.getException().getMessage();
                                        }
                                        Log.d(TAG, ex);
                                    }
                                    finish();
                                }
                            });
                }
                else {
                    ActivityUtils.SESSION_REQUESTED = session; // revert
                    String msg = "Something went wrong";
                    String ex = "";
                    if (task.getException() != null) {
                        ex = " : " + task.getException().getMessage();
                    }
                    Toast.makeText(getApplicationContext(), msg + ex, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Could not override session document : " + session.getSessionID()
                        + "\n" + ex);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Moves the session ID from PENDING to FINISHED locally and in Firestore.
     */
    public void makeSessionIDFinished() {
        List<String> pendingSessions = ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
        List<String> finishedSessions = ActivityUtils.CURRENT_USER_OBJECT.getSessionsFinishedIds();
        finishedSessions.add(ActivityUtils.SESSION_REQUESTED.getSessionID());
        pendingSessions.remove(ActivityUtils.SESSION_REQUESTED.getSessionID());

        // Update those two fields separately
        DocumentReference uRef = db.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        uRef.update("sessionsPendingIds", pendingSessions).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    uRef.update("sessionsFinishedIds", finishedSessions).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ActivityUtils.CURRENT_USER_OBJECT.setSessionsPendingIds(pendingSessions);
                                ActivityUtils.CURRENT_USER_OBJECT.setSessionsFinishedIds(finishedSessions);
                                Intent intent = new Intent(getApplicationContext(), RatingActivity.class);
                                startActivity(intent);
                                isDone = true;
                            }
                            else {
                                String msg = "Something bad just happened.";
                                String ex = "";
                                if (task.getException() != null) {
                                    ex = task.getException().getMessage();
                                }
                                Toast.makeText(getApplicationContext(), msg + " : " + ex, Toast.LENGTH_LONG).show();
                                Log.d(TAG, "Failed to update sessionsFinishedIds\n" + ex);
                            }
                        }
                    });
                }
                else {
                    String msg = "Something bad just happened.";
                    String ex = "";
                    if (task.getException() != null) {
                        ex = task.getException().getMessage();
                    }
                    Toast.makeText(getApplicationContext(), msg + " : " + ex, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Failed to update sessionsPendingIds\n" + ex);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        clickableEndButton(false);
        FinishSessionDialog dialog = new FinishSessionDialog();
        dialog.show(getSupportFragmentManager(), "FinishSessionDialog");
    }
    //---------------------------------------------------------------------------------------------
}
