package com.example.tutordash;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.documentfile.provider.DocumentFile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.model.CourseEntry;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.core.TranscriptParsingService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

public class UploadTranscriptActivity extends AppCompatActivity {

    public static final String TAG = "UploadTranscriptActivity";

    public LinearLayout uploadLayout;
    public LinearLayout loadingLayout;
    public TextView promptView;
    public TextView statusView;
    public Button uploadBtn;
    public Button selectBtn;
    public TextView loadingTextView;

    public Uri pdfUri;
    public FirebaseStorage storage;
    public FirebaseDatabase database;
    public FirebaseAuth mAuth;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_transcript);

        uploadLayout = findViewById(R.id.uploadTranscriptLayout);
        loadingLayout = findViewById(R.id.loadingLayout);
        promptView = findViewById(R.id.promptUploadView);
        statusView = findViewById(R.id.statusView);
        uploadBtn = findViewById(R.id.uploadBtn);
        selectBtn = findViewById(R.id.selectBtn);
        loadingTextView = findViewById(R.id.loadingTextView);
        storage = FirebaseStorage.getInstance();
        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        PDFBoxResourceLoader.init(getApplicationContext());

        loadingLayout.setVisibility(LinearLayout.GONE);

        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionUtils.hasReadPermission(UploadTranscriptActivity.this)) {
                    FileUtils.selectPdf(UploadTranscriptActivity.this);
                }
                else {
                    ActivityCompat.requestPermissions(
                            UploadTranscriptActivity.this,
                            new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                            PermissionUtils.REQUEST_CODE_FOR_TRANSCRIPT_UPLOAD
                    );
                }
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadBtn.setClickable(false);
                try {
                    if (pdfUri != null) {
                        final File transcript = FileUtils.getDownloadedFileFromUri(getApplicationContext(), pdfUri);
                        if (transcript.isFile() && transcript.exists()) {
                            loadingLayout.setVisibility(LinearLayout.VISIBLE);
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                public void run() {
                                    attemptParseTranscript(transcript);
                                }
                            };
                            handler.postDelayed(runnable, 1500);
                        }
                        else {
                            String msg = "An error occurred. Please try again later";
                            Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_SHORT).show();
                            uploadBtn.setClickable(true);
                        }
                    }
                    else {
                        String msg = "Please select a file to upload";
                        Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_SHORT).show();
                        uploadBtn.setClickable(true);
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    String msg = "An unknown error has occurred. Please restart application.";
                    Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_SHORT).show();
                    uploadBtn.setClickable(true);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to parse a file as a transcript. Updates UI accordingly.
     *
     * @param transcript
     */
    public void attemptParseTranscript(final File transcript) {
        try {
            if (TranscriptParsingService.isValidTranscript(transcript)) {
                selectBtn.setClickable(false);
                loadingLayout.setVisibility(LinearLayout.VISIBLE);
                loadingTextView.setText("Parsing...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<CourseEntry> courses = TranscriptParsingService.parseTranscript(transcript);
                        ActivityUtils.ELIGIBLE_COURSES = new ArrayList<String>();
                        for (int i = 0; i < courses.size(); i++) {
                            ActivityUtils.ELIGIBLE_COURSES.add(courses.get(i).toString());
                        }
                        ActivityUtils.IS_TRANSCRIPT_MODE = true; // disables already offered courses (can only choose off of transcript)
                        startActivity(ActivityUtils.makeIntent(getApplicationContext(), ChooseCoursesActivity.class));
                        finish();
                    }
                }).start();
            }
            else {
                String msg = "Sorry, this file is not recognized as an official university transcript";
                Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_SHORT).show();
                uploadBtn.setClickable(true);
                loadingLayout.setVisibility(LinearLayout.GONE);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loadingLayout.setVisibility(LinearLayout.GONE);
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_TRANSCRIPT_UPLOAD &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            FileUtils.selectPdf(UploadTranscriptActivity.this);
        }
        else {
            String msg = "Please allow permission";
            Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks whether the user has selected a file or not
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_FILE_SELECT &&
                resultCode == RESULT_OK &&
                data != null
        ) {
            pdfUri = data.getData();
            String pdfName = DocumentFile.fromSingleUri(UploadTranscriptActivity.this, pdfUri).getName();
            statusView.setText("File Selected : " + pdfName);
        }
        else {
            String msg = "Please select a file to upload";
            Toast.makeText(UploadTranscriptActivity.this, msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------
}
