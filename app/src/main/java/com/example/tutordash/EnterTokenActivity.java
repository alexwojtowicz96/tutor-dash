package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.EmailService;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.model.Token;
import com.google.firebase.Timestamp;

import java.util.Date;

/**
 * This activity is for when users reset their password and need to enter the token that
 * was emailed to them.
 */
public class EnterTokenActivity extends AppCompatActivity {

    public static final String TAG = "EnterTokenActivity";

    Button submitTokenBtn;
    EditText tokenEditText;
    Button resendTokenBtn;

    Token token;
    final int maxAttempts = 5;
    int attempts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_token);

        init();
    }
    //---------------------------------------------------------------------------------------------

    private void init() {
        submitTokenBtn = findViewById(R.id.submitTokenBtn);
        tokenEditText = findViewById(R.id.tokenEditText);
        resendTokenBtn = findViewById(R.id.resendBtn);
        token = ActivityUtils.PASSWORD_RESET_TOKEN;

        resendTokenBtn.setVisibility(View.GONE);
        resendTokenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResendListener();
            }
        });

        if (token != null) {
            if (token.isValid()) {
                if (token.isActive()) {
                    submitTokenBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setSubmitListener();
                        }
                    });
                }
                else {
                    submitTokenBtn.setClickable(false);
                    resendTokenBtn.setVisibility(View.VISIBLE);
                    String msg = "Your token has expired. Please try again.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Token was not active : timer expired");
                    nullifyToken();
                }
            }
            else {
                submitTokenBtn.setClickable(false);
                resendTokenBtn.setVisibility(View.VISIBLE);
                String msg = "An error occurred. Please try again later";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                Log.d(TAG, "Token was not valid");
                nullifyToken();
            }
        }
        else {
            submitTokenBtn.setClickable(false);
            resendTokenBtn.setVisibility(View.VISIBLE);
            String msg = "An error occurred. Please try again later";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "Token was not initialized");
        }
    }
    //---------------------------------------------------------------------------------------------

    private void setSubmitListener() {
        if (token != null) {
            if (token.isActive()) {
                String tokenText = tokenEditText.getText().toString().trim();
                if (!tokenText.isEmpty()) {
                    if (attempts < maxAttempts - 1) {
                        if (tokenText.equals(token.getKey())) {
                            startActivity(ActivityUtils.makeIntent(getApplicationContext(), ResetPasswordActivity.class));
                            nullifyToken();
                            finish();
                        }
                        else {
                            attempts += 1;
                            int attemptsLeft = maxAttempts - attempts;
                            String s;
                            String x = " attempts";
                            String y = " attempt";
                            if (attemptsLeft == 1) {
                                s = "" + attemptsLeft + y;
                            }
                            else {
                                s = "" + attemptsLeft + x;
                            }
                            String msg = "Incorrect token. " + s + " left.";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        submitTokenBtn.setClickable(false);
                        resendTokenBtn.setVisibility(View.VISIBLE);
                        String msg = "You have no more attempts. Please resend the token.";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        nullifyToken();
                    }
                }
                else {
                    String msg = "Please enter your token";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            }
            else {
                submitTokenBtn.setClickable(false);
                resendTokenBtn.setVisibility(View.VISIBLE);
                String msg = "Your token has expired. Please try again.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                nullifyToken();
            }
        }
        else {
            submitTokenBtn.setClickable(false);
            resendTokenBtn.setVisibility(View.VISIBLE);
            String msg = "Your token is no longer valid. Please try again later.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    private void nullifyToken() {
        this.token = null;
        ActivityUtils.PASSWORD_RESET_TOKEN = null;
    }
    //---------------------------------------------------------------------------------------------

    private void setResendListener() {
        if (attempts >= maxAttempts) {
            String msg = "You still have attempts. You cannot resend!";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
        else {
            if (ActivityUtils.TMP_USR != null && ActivityUtils.TMP_USR.isValidUser()) {
                String msg = "Resending token to " + ActivityUtils.TMP_USR.getEmail();
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

                final String key = FileUtils.generateToken(8);
                final Date now = new Date();
                final int timeout = 5;
                ActivityUtils.PASSWORD_RESET_TOKEN = new Token(key, new Timestamp(now), timeout);
                this.token = ActivityUtils.PASSWORD_RESET_TOKEN;

                String subject = "Tutor Dash Password Reset";
                String message = EmailService.passwordResetTemplate(token, ActivityUtils.TMP_USR);
                String school = ActivityUtils.TMP_USR.getSchool();

                EmailService emailService = new EmailService(getApplicationContext(), school, subject, message);
                emailService.sendEmail(ActivityUtils.TMP_USR.getEmail(), true);

                this.attempts = 0;
                resendTokenBtn.setVisibility(View.GONE);
                finish();
            }
            else {
                String msg = "An error occurred. Please try again later";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------
}
