package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.EmailService;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.model.Token;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

/**
 * This activity is for when users need to enter a token emailed to them because their
 * account has not been verified, i.e., "verified" in Firestore == FALSE
 */
public class VerifyEmailWithTokenActivity extends AppCompatActivity {

    public static final String TAG = "VerifyEmailActivity";

    FirebaseFirestore database;
    FirebaseAuth auth;

    Button submit;
    TextView prompt;
    EditText tokenForm;

    final int maxAttempts = 5;
    int attempts = maxAttempts;
    boolean hasError = false;
    Token token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email_with_token);

        database = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        submit = findViewById(R.id.submitConfirmTokenBtn);
        prompt = findViewById(R.id.topMessage);
        tokenForm = findViewById(R.id.tokenConfirmEditText);
        if (!ActivityUtils.HAS_SENT_CONFIRMATION_EMAIL) {
            sendConfirmationEmail();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
        setup();
    }
    //---------------------------------------------------------------------------------------------

    private void sendConfirmationEmail() {
        if (ActivityUtils.CURRENT_USER_OBJECT != null) {
            String msg = "Sending confirmation email to " + ActivityUtils.CURRENT_USER_OBJECT.getEmail();
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

            final String key = FileUtils.generateToken(6);
            final Date now = new Date();
            final int timeout = 5;
            this.token = new Token(key, new Timestamp(now), timeout);

            String school = ActivityUtils.CURRENT_USER_OBJECT.getSchool();
            String subject = "Tutor Dash Email Confirmation";
            String message = EmailService.emailConfirmationTemplate(token, ActivityUtils.CURRENT_USER_OBJECT);

            EmailService emailService = new EmailService(getApplicationContext(), school, subject, message);
            emailService.sendEmail(ActivityUtils.CURRENT_USER_OBJECT.getEmail(), false);
            if (!this.token.isValid()) {
                this.hasError = true;
            }
        }
        else {
            String msg = "An error occurred. Please try again later.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            this.hasError = true;
        }
    }
    //---------------------------------------------------------------------------------------------

    private void setup() {
        if (!hasError) {
            User user = ActivityUtils.CURRENT_USER_OBJECT;
            prompt.setText("Hi " + user.getFullName() + "! " + "Please confirm your email.");

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (attempts > 1) {
                        String tokenText = tokenForm.getText().toString().trim();
                        if (tokenText.isEmpty()) {
                            String msg = "Please enter your token";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if (token.isValid() && token.isActive()) {
                                if (token.getKey().equals(tokenText)) {
                                    DocumentReference doc = database.collection("users").document(user.getuName());
                                    doc.update("verified", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                ActivityUtils.CURRENT_USER_OBJECT.setVerified(true);
                                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                finish();
                                            }
                                            else {
                                                String msg = "Something went wrong on our server. Please try again later";
                                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                if (auth.getCurrentUser() != null) {
                                                    auth.signOut();
                                                }
                                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                finish();
                                            }
                                        }
                                    });
                                }
                                else {
                                    attempts -= 1;
                                    String msg = "Incorrect token. " + attempts + " attempts left.";
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                String msg = "Your token has expired. Please restart the application and try again.";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    else {
                        submit.setClickable(false);
                        String msg = "You have no more attempts. Please try again to resend the token.";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        if (auth.getCurrentUser() != null) {
                            auth.signOut();
                        }
                        finish();
                    }
                }
            });
        }
        else {
            String msg = "An error occurred. Please try again later. Sorry for the inconvenience";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "There was an error : current user object may be null. Token may be null.");
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            if (auth.getCurrentUser() != null) {
                auth.signOut();
            }
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        if (auth.getCurrentUser() != null) {
            auth.signOut();
        }
        finish();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }
    //---------------------------------------------------------------------------------------------
}
