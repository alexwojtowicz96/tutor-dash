package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.braintreegateway.Customer;
import com.braintreegateway.PaymentMethodNonce;
import com.braintreegateway.Transaction;
import com.example.tutordash.adapter.TransactionsAdapter;
import com.example.tutordash.base.BraintreeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.BraintreeFauxServer;
import com.example.tutordash.model.BraintreeCustomer;
import com.example.tutordash.model.BraintreeTransaction;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * When the user wants to see their previous transactions, they end up here. This activity
 * simply renders the necessary information for each transaction that exists in this
 * user's account (Data is from both Braintree AND Firestore).
 */
public class FinanceTransactionsActivity extends AppCompatActivity implements BraintreeActivity {

    public static final String TAG = "FinanceTransactions_";

    public final int ALL_TRANSACTIONS_MODE = 1;
    public final int INCOMING_TRANSACTIONS_MODE = 2;
    public final int OUTGOING_TRANSACTIONS_MODE = 3;

    public Customer uCustomer;
    public User user;
    public Context context;
    public FirebaseFirestore database;
    public BraintreeFauxServer server;
    public List<BraintreeTransaction> incomingTransactions;
    public List<BraintreeTransaction> outgoingTransactions;

    public Toolbar toolbar;
    public RecyclerView transactionsRecyclerView;
    public LinearLayout notFoundView;
    //---------------------------------------------------------------------------------------------

    /**
     * onCreate, we need to make sure the current user object is not null. Also, we are
     * passing a JSON object that contains the customer from the FinanceActivity. If this
     * string extra does not exist, then we can assume that the customer was null in the
     * previous activity, therefore, there are no transactions to render.
     *
     * Because we are passing the customer object as an extra, we don't actually need the
     * Braintree faux server since we are only viewing transactions on this activity.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance_transactions);
        initializeView();

        if (hasDataIntegrity()) {
            this.context = this.getApplicationContext();
            this.server = new BraintreeFauxServer(this);
            this.database = FirebaseFirestore.getInstance();
            this.user = ActivityUtils.CURRENT_USER_OBJECT;
            this.incomingTransactions = new ArrayList<BraintreeTransaction>();
            this.outgoingTransactions = new ArrayList<BraintreeTransaction>();
        }
        else {
            String msg = "Something went wrong. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Queries Firestore for this current user's transactions.
     */
    public void proceedWithDataQuery() {
        database.collection("transactions").whereArrayContains("involved", user.getuName())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    QuerySnapshot result = task.getResult();
                    for (DocumentSnapshot doc : result) {
                        try {
                            BraintreeTransaction t = doc.toObject(BraintreeTransaction.class);
                            if (t != null && t.findIfIsValidModel()) {
                                if (t.getInitiatorUID().equals(user.getuName())) {
                                    outgoingTransactions.add(t);  // I sent the payment
                                }
                                else {
                                    incomingTransactions.add(t);  // Someone paid me
                                }
                            }
                            else {
                                throw new Exception("Not a valid BraintreeTransaction!");
                            }
                        }
                        catch (Exception ex) {
                            Log.d(TAG, ex.getMessage());
                        }
                    }
                    initUserCustomer();
                    getTransactionStatuses();
                }
                else {
                    String msg = "Something went wrong. Could not retrieve transactions.";
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Failed to retrieve transactions for user : " + user.getuName());
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Ensures that the data has integrity when entering this activity.
     * @return TRUE if data has integrity, FALSE otherwise.
     */
    public boolean hasDataIntegrity() {
        boolean hasIntegrity = true;
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            hasIntegrity = false;
            Log.d(TAG, "Firebase Auth : current user is null!");
        }
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            hasIntegrity = false;
            Log.d(TAG, "Current user is null!");
        }
        else {
            if (!ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
                hasIntegrity = false;
                Log.d(TAG, "Current user is not valid!");
            }
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the view ready and hides the recycler view by default. (No transactions visible
     * until data is available).
     */
    public void initializeView() {
        this.toolbar = findViewById(R.id.transactions_toolbar);
        this.transactionsRecyclerView = findViewById(R.id.transactions_recyclerView);
        this.notFoundView = findViewById(R.id.transactions_noTransactionsFound);

        this.toolbar.setTitle("All Transactions");
        this.toolbar.setNavigationIcon(R.drawable.back_arrow);
        this.setSupportActionBar(this.toolbar);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.transactionsRecyclerView.setVisibility(View.GONE);
        this.notFoundView.setVisibility(View.VISIBLE);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.transactions_menu, menu);

        return true;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * For sorting the transactions.
     * @param item
     * @return TRUE
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.transactions_menu_all:
                if (this.toolbar.getTitle().toString().equals("All Transactions")) {
                    Toast.makeText(context, "Already selected", Toast.LENGTH_SHORT).show();
                }
                else {
                    this.toolbar.setTitle("All Transactions");
                    initDataInView(ALL_TRANSACTIONS_MODE);
                }
                break;

            case R.id.transactions_menu_incoming:
                if (this.toolbar.getTitle().toString().equals("Incoming Transactions")) {
                    Toast.makeText(context, "Already selected", Toast.LENGTH_SHORT).show();
                }
                else {
                    this.toolbar.setTitle("Incoming Transactions");
                    initDataInView(INCOMING_TRANSACTIONS_MODE);
                }
                break;

            case R.id.transactions_menu_outgoing:
                if (this.toolbar.getTitle().toString().equals("Outgoing Transactions")) {
                    Toast.makeText(context, "Already selected", Toast.LENGTH_SHORT).show();
                }
                else {
                    this.toolbar.setTitle("Outgoing Transactions");
                    initDataInView(OUTGOING_TRANSACTIONS_MODE);
                }
                break;
        }

        return true;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the statuses of the transactions queried from Firebase. The statuses are stored in
     * Braintree, so it is somewhat complicated to do this.
     */
    public void getTransactionStatuses() {
        if (this.incomingTransactions.size() > 0 || this.outgoingTransactions.size() > 0) {
            for (BraintreeTransaction t : this.incomingTransactions) {
                this.server.findTransaction(t.getTransactionID());
            }
            for (BraintreeTransaction t : this.outgoingTransactions) {
                this.server.findTransaction(t.getTransactionID());
            }
        }
        else {
            Toast.makeText(context, "No transactions found", Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Puts all the data in the view. Assumes everything is ready to go by now...
     */
    public void initDataInView(int mode) {
        List<BraintreeTransaction> transactions = new ArrayList<BraintreeTransaction>();

        if (mode == ALL_TRANSACTIONS_MODE) {
            List<BraintreeTransaction> allTransactions = new ArrayList<BraintreeTransaction>();
            for (BraintreeTransaction t : this.incomingTransactions) {
                allTransactions.add(t);
            }
            for (BraintreeTransaction t : this.outgoingTransactions) {
                allTransactions.add(t);
            }
            transactions = allTransactions;
        }
        else if (mode == OUTGOING_TRANSACTIONS_MODE) {
            transactions = this.outgoingTransactions;
        }
        else if (mode == INCOMING_TRANSACTIONS_MODE) {
            transactions = this.incomingTransactions;
        }
        else {
            Log.d(TAG, "This mode is not supported : " + mode);
        }

        // Set the adapter as long as there are transactions to render
        if (transactions.size() > 0) {
            LinearLayoutManager manager = new LinearLayoutManager(this.context);
            TransactionsAdapter adapter = new TransactionsAdapter(transactions, this.context);
            this.transactionsRecyclerView.setAdapter(adapter);
            this.transactionsRecyclerView.setLayoutManager(manager);
            showRecyclerView(true);
        }
        else {
            toastOnUiThread("No transactions found");
            showRecyclerView(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    public void showRecyclerView(boolean show) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (show) {
                            transactionsRecyclerView.setVisibility(View.VISIBLE);
                            notFoundView.setVisibility(View.GONE);
                        }
                        else {
                            transactionsRecyclerView.setVisibility(View.GONE);
                            notFoundView.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Finds this.uCustomer if it exists by reading the extras passed from the last activity.
     */
    public void initUserCustomer() {
        Bundle extras = getIntent().getExtras();
        final String key = "U_CUSTOMER";

        if (extras != null && extras.containsKey(key)) {
            Gson gson = new Gson();
            try {
                Customer c = gson.fromJson(extras.getString(key), Customer.class);
                if (c != null) {
                    this.uCustomer = c;
                }
                else {
                    throw new Exception("Failed to deserialize Customer : was null...");
                }
            }
            catch (Exception ex) {
                Log.d(TAG, "Could not deserialize the uCustomer object!\n" + ex.getLocalizedMessage());
                this.uCustomer = null;
            }
        }
        else {
            this.uCustomer = null;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the main thread.
     * @param msg
     */
    public void toastOnUiThread(String msg) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * ANDROID LIFECYCLE METHODS
     * ============================================================================================
     */
    @Override
    public void onStart() {
        super.onStart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRestart() {
        super.onRestart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onPause() {
        super.onPause();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * BRAINTREE ACTIVITY CALLBACK METHODS
     * ============================================================================================
     */
    @Override
    public void onServerReady(boolean isReady) {
        if (isReady && this.server.isReady()) {
            proceedWithDataQuery();
        }
        else {
            toastOnUiThread("Failed to connect to the Braintree server");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called in the activity that implements this class. It is intended that the
     * server will do something, and once it is finished, it will call this method in the
     * activity that called the server method.
     *
     * @param amount - Amount to submit
     * @param clientToken - Token generated on the faux "server" that configures the transaction
     */
    @Override
    public void submitPayment(Float amount, String clientToken) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the customer object is found.
     *
     * @param customer - The customer object retrieved from Braintree
     */
    @Override
    public void onCustomerFound(Customer customer, boolean wasFound) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a customer is created in Braintree.
     *
     * @param customer
     * @param wasCreated
     */
    @Override
    public void onCustomerCreated(BraintreeCustomer customer, boolean wasCreated) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a submitted transaction is completed.
     *
     * @param transaction
     * @param wasSuccessful
     */
    @Override
    public void onTransactionComplete(BraintreeTransaction transaction, boolean wasSuccessful) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a submitted void for a transaction is completed.
     *
     * @param transactionID
     * @param wasVoided
     */
    @Override
    public void onTransactionVoided(String transactionID, boolean wasVoided) {

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a transaction is found (from its ID).
     *
     * @param transaction
     * @param wasFound
     */
    @Override
    public void onTransactionFound(Transaction transaction, boolean wasFound) {
        if (wasFound && transaction != null) {
            final String id = transaction.getId();
            boolean foundIt = false;

            for (BraintreeTransaction t : this.outgoingTransactions) {
                if (t.getTransactionID().equals(id)) {
                    t.setTags(new ArrayList<String>() {{ add(transaction.getStatus().toString().toUpperCase().trim()); }});
                    foundIt = true;
                    break;
                }
            }
            if (!foundIt) {
                for (BraintreeTransaction t : this.incomingTransactions) {
                    if (t.getTransactionID().equals(id)) {
                        t.setTags(new ArrayList<String>() {{ add(transaction.getStatus().toString().toUpperCase().trim()); }});
                        foundIt = true;
                        break;
                    }
                }
                if (!foundIt) {
                    Log.d(TAG, "There was an issue... Could not find transaction : " + transaction.getId());
                }
            }

            // Now that we have the status, update it by rewriting the adapter. The only thing
            // that should change is the status.
            Thread thread = new Thread() {
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            initDataInView(ALL_TRANSACTIONS_MODE);
                        }
                    });
                }
            };
            thread.start();
        }
        else {
            Log.d(TAG, "Failed to return transaction successfully");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when a payment method nonce is created.
     *
     * @param nonce
     * @param wasCreated
     */
    @Override
    public void onPaymentNonceCreated(PaymentMethodNonce nonce, boolean wasCreated) {

    }
    //---------------------------------------------------------------------------------------------
}
