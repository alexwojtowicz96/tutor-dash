package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.adapter.SeekingCoursesAdapter;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.dialog.DataDisappearDialog;
import com.example.tutordash.dialog.SubmitCoursesSeekingDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChooseCoursesSeekingActivity extends AppCompatActivity {

    public static final String TAG = "ChooseCSeekActivity";

    RecyclerView recyclerView;
    Button submitBtn;
    TextView promptView;

    FirebaseFirestore database;

    SeekingCoursesAdapter viewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_courses_seeking);

        database = FirebaseFirestore.getInstance();

        recyclerView = findViewById(R.id.coursesSeekingRecyclerView);
        submitBtn = findViewById(R.id.submitCoursesSeekingBtn);
        promptView = findViewById(R.id.selectCoursesForAssistanceView);

        String promptText = promptView.getText().toString().replace("UNIVERSITY", ActivityUtils.CURRENT_USER_SCHOOL);
        promptView.setText(promptText);

        renderCourses();
        setSubmitListener();
    }
    //---------------------------------------------------------------------------------------------

    public void renderCourses() {
        Query query = database.collection("schoolList").whereEqualTo("name", ActivityUtils.CURRENT_USER_SCHOOL);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful() && task.getResult() != null && task.getResult().size() == 1) {
                    for (DocumentSnapshot doc : task.getResult()) {
                        try {
                            ArrayList<String> coursesAtSchool = (ArrayList<String>) doc.get("courses");
                            ArrayList<String> coursesSeeking = new ArrayList<String>();
                            if (ActivityUtils.CURRENT_USER_OBJECT.getCoursesSeekingAssistance() != null) {
                                coursesSeeking = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getCoursesSeekingAssistance();
                            }
                            if (coursesAtSchool != null && coursesAtSchool.size() > 0) {
                                Collections.sort(coursesAtSchool);
                                viewAdapter = new SeekingCoursesAdapter(
                                        getApplicationContext(),
                                        coursesAtSchool,
                                        coursesSeeking
                                );
                                LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());

                                recyclerView.setAdapter(viewAdapter);
                                recyclerView.setLayoutManager(manager);
                            }
                            else {
                                String msg = "Unfortunately, your university does not have any tutors offering courses.";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception ex) {
                            Log.d(TAG, "Could not cast courses as ArrayList");
                            String msg = "An error occurred. Please restart the application.";
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    Log.d(TAG, "Could not complete task successfully");
                    String msg = "An error occurred. Please try again later.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void setSubmitListener() {
        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewAdapter == null) {
                    String msg = "No courses are available to submit";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    SubmitCoursesSeekingDialog dialog = new SubmitCoursesSeekingDialog();
                    dialog.show(getSupportFragmentManager(), "SubmitCoursesSeekingDialog");
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Attempts to update the list of courses in Firebase while keeping the local user
     * object synced as well.
     */
    public void submitCourses() {
        submitBtn.setClickable(false);
        ArrayList<String> selections = viewAdapter.getSelectedCourseIds();

        DocumentReference ref = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        ref.update("coursesSeekingAssistance", selections).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    ActivityUtils.CURRENT_USER_OBJECT.setCoursesSeekingAssistance(selections);
                    ActivityUtils.CURRENT_USER_OBJECT.subscribe();
                    String msg = "Success! The courses you need assistance in have been updated!";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    submitBtn.setClickable(true);
                    Log.d(TAG, "Could not update coursesSeekingAssistance for user : "
                            + ActivityUtils.CURRENT_USER_OBJECT.getuName());
                    String msg = "Failed to update courses. Please try again later.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        DataDisappearDialog dialog = new DataDisappearDialog();
        dialog.show(getSupportFragmentManager(), "DataDisappearDialog");
    }
    //---------------------------------------------------------------------------------------------
}
