package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.core.VidyoTokenGenerator;
import com.example.tutordash.model.User;
import com.example.tutordash.model.VidyoIoDataModel;
import com.vidyo.VidyoClient.Connector.Connector;
import com.vidyo.VidyoClient.Connector.ConnectorPkg;

/**
 * This activity is NOT intended to be a part of the app. Use it for testing the video
 * chat functionality.
 *
 * REFERENCES:
 * https://www.youtube.com/watch?v=tbT8EEw9wNw
 * https://github.com/Vidyo/vidyo.io-helloworld-android/blob/master/VidyoIODemo/app/src/main/java/com/example/pfuternik/vidyoiodemo/MainActivity.java
 */
public class _TestVideoChatActivity extends AppCompatActivity implements Connector.IConnect {

    public static final String TAG = "_TestVideoChatActivity";
    public static final int MAX_PARTICIPANTS = 2;

    public Button startBtn;
    public Button connectBtn;
    public Button disconnectBtn;
    public FrameLayout videoLayout;

    public Context context;
    public Connector connector;
    public Connector.IConnect iConnect;
    public VidyoTokenGenerator tokenGenerator;
    public boolean isConnected = false;
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called onCreate(). Not checking the integrity of the current user since this is a
     * demo. We need the permissions enabled before initializing the vidyo.io components, so
     * this will loop infinitely through the permission requests until the user accepts them all!
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity___test_video_chat);

        this.startBtn = findViewById(R.id.test_video_chat_start);
        this.connectBtn = findViewById(R.id.test_video_chat_connect);
        this.disconnectBtn = findViewById(R.id.test_video_chat_disconnect);
        this.videoLayout = findViewById(R.id.test_video_chat_frameLayout);

        this.context = getApplicationContext();
        if (hasRequiredPermissions()) {
            initConference();
        }
        else {
            promptForPermission();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Checks if the required camera and microphone permissions are enabled.
     * @return TRUE if enabled, FALSE otherwise
     */
    public boolean hasRequiredPermissions() {
        final int permissionGranted = PackageManager.PERMISSION_GRANTED;

        return (PermissionUtils.getAudioPermissionValue(context) == permissionGranted
                && PermissionUtils.getCameraPermissionValue(context) == permissionGranted);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets the vidyo.io components ready.
     */
    public void initConference() {
        this.connector = null;
        this.iConnect = this;
        this.tokenGenerator = new VidyoTokenGenerator(context);
        ConnectorPkg.setApplicationUIContext(this);
        ConnectorPkg.initialize();

        attachOnStartVideoChat();
        attachOnConnectVideoChat();
        attachOnDisconnectVideoChat();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * The user needs camera and microphone permissions for this activity. This method wraps
     * the prompting of those permissions. They are absolutely necessary, so the idea is that
     * we keep prompting the user to enable permissions if they decline.
     */
    public void promptForPermission() {
        if (PermissionUtils.getAudioPermissionValue(context) != PackageManager.PERMISSION_GRANTED) {
            requestMicrophonePermission();
        }
        else if (PermissionUtils.getCameraPermissionValue(context) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Renders camera permission request.
     */
    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.CAMERA },
                PermissionUtils.REQUEST_CODE_FOR_CAMERA
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Renders microphone permission request.
     */
    public void requestMicrophonePermission() {
        ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.RECORD_AUDIO },
                PermissionUtils.REQUEST_CODE_FOR_MICROPHONE
        );
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets called when the users selects "ACCEPT" or "DENY" on the popup window asking them
     * to allow permission.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        final int granted = PackageManager.PERMISSION_GRANTED;

        if (grantResults.length > 0) {
            if (requestCode == PermissionUtils.REQUEST_CODE_FOR_CAMERA && grantResults[0] == granted) {
                // Camera permission has been granted
            }
            else if (requestCode == PermissionUtils.REQUEST_CODE_FOR_MICROPHONE && grantResults[0] == granted) {
                // Microphone permission has been granted
            }

            // Make sure BOTH permissions have been granted on request result.
            if (hasRequiredPermissions()) {
                initConference();
            }
            else {
                promptForPermission();
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void attachOnStartVideoChat() {
        this.startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connector = new Connector(
                        videoLayout,
                        Connector.ConnectorViewStyle.VIDYO_CONNECTORVIEWSTYLE_Default,
                        MAX_PARTICIPANTS,
                        "",
                        "",
                        0
                );
                connector.showViewAt(
                        videoLayout,
                        0,
                        0,
                        videoLayout.getWidth(),
                        videoLayout.getHeight()
                );
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void attachOnConnectVideoChat() {
        this.connectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void attachOnDisconnectVideoChat() {
        this.disconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connector.disconnect();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void onSuccess() {
        toastOnUiThread("You are connected!");
        this.isConnected = true;
    }
    //---------------------------------------------------------------------------------------------

    public void onFailure(Connector.ConnectorFailReason reason) {
        toastOnUiThread("Failed to connect : " + reason.toString());
        Log.d(TAG, "Failed to connect with vidyo.io : " + reason.toString());
        this.isConnected = false;
}
    //---------------------------------------------------------------------------------------------

    public void onDisconnected(Connector.ConnectorDisconnectReason reason) {
        toastOnUiThread("You have disconnected : " + reason.toString());
        Log.d(TAG, "vidyo.io disconnected : " + reason.toString());
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Toasts a message on the UI thread. By default, the IConnect methods run in the
     * background, so we can't do a toast on them since they are UI components.
     *
     * @param msg
     */
    public void toastOnUiThread(String msg) {
        Thread thread = new Thread() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        thread.start();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initiates the connection to the vidyo.io room.
     */
    public void connect() {
        if (tokenGenerator.isValid()) {
            String msg = "Connecting...";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();

            final User user = ActivityUtils.CURRENT_USER_OBJECT;
            final String uName = user.getuName();
            String token = tokenGenerator.generateToken(uName);
            connector.connect(
                    VidyoIoDataModel.HOST,
                    token,
                    uName,
                    VidyoIoDataModel.DEFAULT_ROOM,
                    iConnect
            );
        }
        else {
            String msg = "Your web conference is preparing... Please try again in a few seconds.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Disconnects from the conference. You need to specify what you want to do with this.isConnected.
     * passing true will do nothing to isConnected, but if you pass in false, then it will set
     * isConnected to false.
     *
     * @param isConnectedOption
     */
    public void disconnect(boolean isConnectedOption) {
        connector.disconnect();
        if (!isConnectedOption) {
            this.isConnected = false;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * ============================================================================================
     * Additional Lifecycle Methods
     * ============================================================================================
     */
    @Override
    public void onPause() {
        super.onPause();
        disconnect(true);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
        disconnect(true);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onResume() {
        super.onResume();
        if (isConnected) {
            connect();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
        disconnect(false);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        if (isConnected) {
            connect();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRestart() {
        super.onRestart();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        disconnect(false);
    }
    //---------------------------------------------------------------------------------------------
}
