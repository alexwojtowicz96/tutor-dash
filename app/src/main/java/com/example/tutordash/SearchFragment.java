package com.example.tutordash;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.SearchModel;
import com.example.tutordash.model.User;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class SearchFragment extends Fragment {

    final public static String NAME = "Search";
    final public String TAG = "SearchFragment";
    final private String SPINNER_SELECT_PROMPT = " ";
    public String spinnerText = SPINNER_SELECT_PROMPT;

    FirebaseFirestore database;
    Button searchBtn;
    EditText usernameEdit;
    EditText milesAwayEdit;
    Switch onlyAvailUsersSwitch;
    Spinner coursesSpinner;
    TextView searchPromptView;
    TextView courseIdLabel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        database = FirebaseFirestore.getInstance();

        searchBtn = view.findViewById(R.id.searchButton);
        usernameEdit = view.findViewById(R.id.usernameSearchEdit);
        milesAwayEdit = view.findViewById(R.id.milesAwayEdit);
        onlyAvailUsersSwitch = view.findViewById(R.id.onlyAvailableSwitch);
        coursesSpinner = view.findViewById(R.id.courseSelectSpinner);
        searchPromptView = view.findViewById(R.id.searchPromptMsg);
        courseIdLabel = view.findViewById(R.id.courseIdLabel);

        initPrompt();
        initSpinner(coursesSpinner);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSearchSubmit();
            }
        });

        return view;
    }
    //---------------------------------------------------------------------------------------------

    public void handleSearchSubmit() {
        ArrayList<User> allUsers = ((MainDiscoveryActivity) getActivity()).getAllUsersList();
        ArrayList<User> queryResults = new ArrayList<User>();
        if (allUsers != null) {
            boolean canInitSearch = true;

            String username = usernameEdit.getText().toString().trim();
            String maxMilesAway = milesAwayEdit.getText().toString().trim();
            boolean onlyshowAvailUsers = onlyAvailUsersSwitch.isChecked();
            String courseId = coursesSpinner.getSelectedItem().toString().trim();

            SearchModel model = new SearchModel();
            if (username.equals("")) { // search based on other params
                Float maxMilesFloat = Float.valueOf(-1);
                if (!maxMilesAway.equals("")) {
                    maxMilesFloat = Float.parseFloat(maxMilesAway);
                }
                model.setCourseId(courseId);
                model.setMilesAway(maxMilesFloat);
                model.setOnlyIncludeAvailableUsers(onlyshowAvailUsers);
                model.setTuteeMode(((MainDiscoveryActivity) getActivity()).isTuteeMode);
                if (model.isDefault()) {
                    canInitSearch = false;
                    String msg = "Please enter your search criteria";
                    Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
            else { // search based on username
                model.setUsernameQuery(username);
                model.setTuteeMode(((MainDiscoveryActivity) getActivity()).isTuteeMode);
                initSpinner(this.coursesSpinner);
                this.onlyAvailUsersSwitch.setChecked(false);
                this.milesAwayEdit.setText("");
            }

            if (canInitSearch) {
                ((MainDiscoveryActivity) getActivity()).initiateSearch(model);
            }
        }
        else {
            Log.d(TAG, "When trying to initiate a search : " +
                    "MainDiscoveryActivity.allUsers was found to have a null value");
            String msg = "An error occurred. Please try again later";
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initPrompt() {
        if (ActivityUtils.CURRENT_USER_OBJECT != null) {
            StringBuilder b = new StringBuilder();
            b.append("Search for ");
            if (((MainDiscoveryActivity) getActivity()).isTuteeMode) {
                b.append("tutors at ");
            }
            else {
                b.append("tutees at ");
            }
            b.append(ActivityUtils.CURRENT_USER_SCHOOL);
            b.append("!");
            searchPromptView.setText(b.toString());
        }
        else {
            Log.d(TAG, "ActivityUtils.CURRENT_USER_OBJECT is null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initSpinner(Spinner spinner) {
        ArrayList<String> courseIds = ((MainDiscoveryActivity) getActivity()).courseIds;
        if (courseIds != null) {
            ArrayList<String> spinnerOptions = new ArrayList<String>(courseIds);
            spinnerOptions.add(0, SPINNER_SELECT_PROMPT);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    getContext(),
                    android.R.layout.simple_spinner_item,
                    spinnerOptions
            );
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    spinnerText = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // ?
                }
            });
        }
        else {
            String msg = "There was a problem retrieving the courses that tutors at " +
                    ActivityUtils.CURRENT_USER_SCHOOL + " offer. Please try again later.";
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        }
    }
    //---------------------------------------------------------------------------------------------
}
