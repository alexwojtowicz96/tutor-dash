package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.AlarmService;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * When a tutor clicks on a session request notification from a tutee, then end up here. They
 * can either get here from navigating to their session requests within the app, OR they
 * can get here by clicking on the notification (outside of the app).
 */
public class AcceptRequestActivity extends AppCompatActivity {

    public static final String TAG = "AcceptRequestActivity";

    FirebaseAuth firebaseAuth;
    FirebaseFirestore database;
    StorageReference storageRef;
    User cUser;

    public TextView headingView;
    public CircleImageView tuteeAvatar;
    public RatingBar tuteeRatingBar;
    public TextView courseView;
    public TextView dateView;
    public TextView timeView;
    public TextView deliveryView;
    public TextView totalView;
    public Button acceptBtn;
    public Button denyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_request);

        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        if (firebaseAuth.getCurrentUser() != null) {
            Session session = ActivityUtils.SESSION_REQUESTED;
            if (session != null && session.findIsValidSession()) {
                // Make sure the user is actually who we think they are.
                database.collection("users").whereEqualTo("authEmail", firebaseAuth.getCurrentUser().getEmail())
                        .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful() && task.getResult() != null && task.getResult().size() == 1) {
                                    for (DocumentSnapshot u : task.getResult()) {
                                        if (u.getId().equals(session.getTutorUID())) {
                                            // Authentication Successful!
                                            cUser = u.toObject(User.class);
                                            // Now find the tutee. We need their rating.
                                            database.collection("users").document(session.getTuteeUID())
                                                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                            if (task.isSuccessful() && task.getResult() != null) {
                                                                User tuteeUser = task.getResult().toObject(User.class);
                                                                if (tuteeUser != null && tuteeUser.isValidUser()) {
                                                                    bindView(tuteeUser);
                                                                }
                                                                else {
                                                                    String msg1 = "Something went wrong. Killing application";
                                                                    String msg2 = "Could not convert tutee user document to object";
                                                                    exitWithError(msg1, msg2);
                                                                }
                                                            }
                                                            else {
                                                                String ex = "";
                                                                if (task.getException() != null) {
                                                                    ex = task.getException().getMessage();
                                                                }
                                                                String msg1 = "An internal server error occurred. Sorry about this.";
                                                                String msg2 = "Task failed : " + task.getResult() + "\n" + ex;
                                                                exitWithError(msg1, msg2);
                                                            }
                                                        }
                                                    });

                                        }
                                        else {
                                            String msg1 = "Something went wrong for some reason. Sorry!";
                                            String msg2 = "The tutor IDs did not match? :\n" +
                                                    "\tsession.getTutorUID() ... " + session.getTutorUID() + "\n" +
                                                    "\tu.getId() ............... " + u.getId() + "\n";
                                            exitWithError(msg1, msg2);
                                        }
                                    }
                                }
                                else {
                                    String ex = "";
                                    if (task.getException() != null) {
                                        ex = task.getException().getMessage();
                                    }
                                    String msg1 = "We could not authenticate you. This session cannot be viewed";
                                    String msg2 = "The task didn't finish properly : " + task.getResult() + "\n" + ex;
                                    exitWithError(msg1, msg2);
                                }
                            }
                        });
            }
            else {
                String msg1 = "Something went horribly wrong. Please log in to view this session.";
                String msg2 = "The session was not a valid session. It may have been null : " + session;
                exitWithError(msg1, msg2);
            }
        }
        else {
            String msg1 = "You must be signed in! Sorry, but our team has failed you!";
            String msg2 = "How did this happen!? The user got notified and they weren't signed in? " +
                    "It is possible that they haven't signed in for a while. Oh well... " +
                    "the request is gone now...";
            exitWithError(msg1, msg2);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Call this if something goes horribly wrong.
     *
     * @param userErrorMsg
     * @param logErrorMsg
     */
    public void exitWithError(String userErrorMsg, String logErrorMsg) {
        Log.d(TAG, logErrorMsg);
        Toast.makeText(getApplicationContext(), userErrorMsg, Toast.LENGTH_LONG).show();
        ActivityUtils.SESSION_REQUESTED = null;
        finish();
        System.exit(0);
    }
    //---------------------------------------------------------------------------------------------

    public void initView() {
        this.headingView = findViewById(R.id.sessionRequestHeader);
        this.tuteeAvatar = findViewById(R.id.tuteeAvatar);
        this.tuteeRatingBar = findViewById(R.id.tuteeRatingBar);
        this.courseView = findViewById(R.id.requestSummaryx2);
        this.dateView = findViewById(R.id.requestSummaryx3);
        this.timeView = findViewById(R.id.requestSummaryx4);
        this.deliveryView = findViewById(R.id.requestSummaryx5);
        this.totalView = findViewById(R.id.requestSummaryx6);
        this.acceptBtn = findViewById(R.id.acceptButton);
        this.denyBtn = findViewById(R.id.denyButton);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This binds all of the data to the view. Keep in mind that the tuteeUser object is only
     * a shallow object. This user will not have the courses or sessions maps.
     *
     * @param tuteeUser
     */
    public void bindView(User tuteeUser) {
        initView();
        tuteeUser = tuteeUser.initNullValues();
        Session session = ActivityUtils.SESSION_REQUESTED;

        try {
            headingView.setText(headingView.getText().toString().replaceAll("%TUTEE%", tuteeUser.getuName()));
            if (tuteeUser.getPicURL() != null && !tuteeUser.getPicURL().isEmpty()) {
                GlideApp.with(this).load(storageRef.child(tuteeUser.getPicURL())).into(tuteeAvatar);
            }

            tuteeRatingBar.setNumStars(5);
            tuteeRatingBar.setMax(5);
            tuteeRatingBar.setIsIndicator(true);
            tuteeRatingBar.setStepSize(new Float(0.1));
            int starColor = getApplicationContext().getResources().getColor(R.color.pencil);
            int colorBlack = getApplicationContext().getResources().getColor(R.color.black);
            LayerDrawable stars = (LayerDrawable) tuteeRatingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            tuteeRatingBar.setRating(tuteeUser.getTuteeRating());

            courseView.setText(session.getCourseID());
            totalView.setText("$" + ActivityUtils.getFormattedFloat(session.getExpectedAmountOwed()));
            if (session.isOnline()) {
                deliveryView.setText("Online");
            }
            else {
                deliveryView.setText("In-Person");
            }

            String date = CalendarService.getFormattedDateFromDate(session.getStart().toDate());
            String startTime = CalendarService.getFormattedTimeFromDate(session.getStart().toDate());
            String endTime = CalendarService.getFormattedTimeFromDate(session.getEnd().toDate());
            dateView.setText(date);
            timeView.setText(startTime + " - " + endTime);

            setAcceptListener(tuteeUser);
            setDenyListener();
        }
        catch (Exception ex) {
            String msg1 = "A fatal error occurred. Could not read session data";
            String msg2 = ex.getMessage();
            exitWithError(msg1, msg2);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Accepting the request means setting wasAccepted to TRUE in Firestore, moving that request
     * ID from requestIds to sessionsPendingIds (and adding a new ID to sessionsPendingIds for the
     * tutee), and sending a notification back to the tutee user.
     *
     * An alarm will be set for the tutor right before the notification gets sent. The tutee
     * will have their alarm set once they receive the notification.
     *
     * @param tuteeUser - shallow object
     */
    public void setAcceptListener(User tuteeUser) {
        this.acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Session session = ActivityUtils.SESSION_REQUESTED;
                session.setWasAccepted(true);
                DocumentReference tutorRef = database.collection("users").document(session.getTutorUID());
                DocumentReference tuteeRef = database.collection("users").document(session.getTuteeUID());

                database.collection("sessions").document(session.getSessionID())
                        .update("wasAccepted", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    // Move this session ID from requestedIds to sessionsPendingIds
                                    ArrayList<String> updatedRequestIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getRequestIds();
                                    updatedRequestIds.remove(session.getSessionID());
                                    tutorRef.update("requestIds", updatedRequestIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                ActivityUtils.CURRENT_USER_OBJECT.setRequestIds(updatedRequestIds);
                                                ArrayList<String> updatedPendingIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
                                                if (updatedPendingIds == null) {
                                                    updatedPendingIds = new ArrayList<String>();
                                                }
                                                updatedPendingIds.add(session.getSessionID());

                                                tutorRef.update("sessionsPendingIds", updatedPendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            ActivityUtils.CURRENT_USER_OBJECT.putNewPendingSessionId(session.getSessionID());
                                                            // Now update the tutee's pendingSessionIds
                                                            ArrayList<String> tuteePendingIds = (ArrayList<String>) tuteeUser.getSessionsPendingIds();
                                                            if (tuteePendingIds == null) {
                                                                tuteePendingIds = new ArrayList<String>();
                                                            }
                                                            tuteePendingIds.add(session.getSessionID());

                                                            tuteeRef.update("sessionsPendingIds", tuteePendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {

                                                                        // Set alarm for the tutor
                                                                        AlarmService.setSessionAlarms(getApplicationContext(), session);

                                                                        // Send notification back to tutee with session data
                                                                        final String title = session.getTutorUID() + " has accepted your tutor request!";
                                                                        final String msg = "Your request in " + session.getCourseID() + " was accepted!";
                                                                        final String topic = "/topics/" + MessageService.DM_TOPIC + session.getTuteeUID();
                                                                        NotificationService notificationService = new NotificationService(getApplicationContext());
                                                                        notificationService.setTopic(topic);
                                                                        notificationService.setHighPriority(true);
                                                                        notificationService.setTitle(title);
                                                                        notificationService.setMessage(msg);

                                                                        Gson gson = new Gson();
                                                                        session.initNullValues();
                                                                        String sessionString = gson.toJson(session);
                                                                        Pair<String, String> data = new Pair<String, String>("sessionResponse", sessionString);
                                                                        List<Pair<String, String>> dataList = new ArrayList<Pair<String, String>>();
                                                                        dataList.add(data);

                                                                        notificationService.sendNotification(dataList);

                                                                        finish();
                                                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                                        ActivityUtils.SESSION_REQUESTED = null;
                                                                    }
                                                                    else {
                                                                        String msg = "Failed to accept session!";
                                                                        Log.d(TAG, "Could not update sessionsPendingIds for user : " + session.getTuteeUID());
                                                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            String msg = "Failed to accept session!";
                                                            Log.d(TAG, "Could not update sessionsPendingIds for user : " + session.getTutorUID());
                                                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });
                                            }
                                            else {
                                                String msg = "Failed to accept session!";
                                                Log.d(TAG, "Could not update requestIds for user : " + session.getTutorUID());
                                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }
                                else {
                                    String msg = "Failed to accept session!";
                                    Log.d(TAG, "Could not update wasAccepted for session : " + session.getSessionID());
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void setDenyListener() {
        this.denyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                denyBtn.setClickable(false);
                Session session = ActivityUtils.SESSION_REQUESTED;

                database.collection("sessions").document(session.getSessionID())
                        .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    ArrayList<String> updatedRequestIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getRequestIds();
                                    updatedRequestIds.remove(session.getSessionID());
                                    database.collection("users").document(session.getTutorUID())
                                            .update("requestIds", updatedRequestIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        final String title = session.getTutorUID() + " denied your tutor request";
                                                        final String msg = "Your request in " + session.getCourseID() + " was denied";
                                                        final String topic = "/topics/" + MessageService.DM_TOPIC + session.getTuteeUID();
                                                        NotificationService notificationService = new NotificationService(getApplicationContext());
                                                        notificationService.setTopic(topic);
                                                        notificationService.setHighPriority(true);
                                                        notificationService.setTitle(title);
                                                        notificationService.setMessage(msg);

                                                        notificationService.sendNotification(null);

                                                        finish();
                                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                        ActivityUtils.SESSION_REQUESTED = null;
                                                    }
                                                    else {
                                                        denyBtn.setClickable(true);
                                                        String msg = "Failed to deny request! Integrity has been lost...";
                                                        Log.d(TAG, "Deleted document : " + session.getSessionID() +
                                                                "\n" + "Failed to update requestIds for user : " + session.getTutorUID());
                                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                }
                                else {
                                    denyBtn.setClickable(true);
                                    String msg = "Failed to deny request!";
                                    Log.d(TAG, "Could not delete sessions document : " + session.getSessionID() +
                                            "\n" + "Does this document exist?");
                                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //---------------------------------------------------------------------------------------------
}
