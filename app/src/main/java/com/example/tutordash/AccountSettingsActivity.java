package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.dialog.TuteeNotificationsDialog;
import com.example.tutordash.dialog.TutorNotificationsDialog;
import com.example.tutordash.model.CourseEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class AccountSettingsActivity extends AppCompatActivity {

    public static final String TAG = "AccountSettingsActivity";

    public Toolbar toolbar;
    public Switch tutorNotificationSwitch;
    public Switch tuteeNotificationSwitch;
    public TextView addCoursesSeekingView;
    public TextView addCoursesOfferingView;
    public TextView uploadTranscriptView;
    public TextView memberProfileView;
    public TextView termsView;
    public TextView manualView;
    public TextView changePassView;
    public TextView sendAlertView;
    public LinearLayout tutorPreferencesLayout;
    public Switch offerOnlineSwitch;
    public Switch offerInPersonSwitch;
    public TextView sessionRequestsView;
    public TextView pendingSessionsView;
    public TextView previousSessionsView;
    public TextView cancelledSessionsView;

    public FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            Log.d(TAG, "Current user object was not valid and may have been null!");
            String msg = "An error occurred. Please restart and try again.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            database = FirebaseFirestore.getInstance();
            bindView();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
        initView();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Binds all types to layout.
     */
    public void bindView() {
        this.toolbar = findViewById(R.id.accountSettingsToolbar);
        this.tutorNotificationSwitch = findViewById(R.id.enableNotificationsTutorsSwitch);
        this.tuteeNotificationSwitch = findViewById(R.id.enableNotificationsTuteesSwitch);
        this.addCoursesSeekingView = findViewById(R.id.addSeekingCoursesView);
        this.addCoursesOfferingView = findViewById(R.id.addOfferingCoursesView);
        this.uploadTranscriptView = findViewById(R.id.uploadTranscriptView);
        this.memberProfileView = findViewById(R.id.memberProfileOptionView);
        this.termsView = findViewById(R.id.termsOfUseView);
        this.manualView = findViewById(R.id.userManualView);
        this.changePassView = findViewById(R.id.changePasswordOptionView);
        this.sendAlertView = findViewById(R.id.sendAlertOptionView);
        this.tutorPreferencesLayout = findViewById(R.id.tutorPreferencesLayout);
        this.offerInPersonSwitch = findViewById(R.id.offerInPersonSwitch);
        this.offerOnlineSwitch = findViewById(R.id.offerOnlineSwitch);
        this.sessionRequestsView = findViewById(R.id.sessionRequestsOptionView);
        this.pendingSessionsView = findViewById(R.id.pendingSessionsView);
        this.previousSessionsView = findViewById(R.id.previousSessionsView);
        this.cancelledSessionsView = findViewById(R.id.cancelledSessionsView);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Logic needed to determine what renders in the settings.
     */
    public void initView() {
        toolbar.setTitle("Account Settings");

        // Bind tutor notification preferences.
        if (ActivityUtils.CURRENT_USER_OBJECT.getHasTutorNotifications()) {
            tutorNotificationSwitch.setChecked(true);
        }
        else {
            tutorNotificationSwitch.setChecked(false);
        }
        tutorNotificationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tutorNotificationSwitch.setChecked(!tutorNotificationSwitch.isChecked());
                TutorNotificationsDialog dialog = new TutorNotificationsDialog();
                dialog.show(getSupportFragmentManager(), "TutorNotificationsDialog");
            }
        });

        // Bind tutee notification preferences.
        if (ActivityUtils.CURRENT_USER_OBJECT.getHasTuteeNotifications()) {
            tuteeNotificationSwitch.setChecked(true);
        }
        else {
            tuteeNotificationSwitch.setChecked(false);
        }
        tuteeNotificationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tuteeNotificationSwitch.setChecked(!tuteeNotificationSwitch.isChecked());
                TuteeNotificationsDialog dialog = new TuteeNotificationsDialog();
                dialog.show(getSupportFragmentManager(), "TuteeNotificationsDialog");
            }
        });

        // Bind online tutoring options for user
        if (ActivityUtils.CURRENT_USER_OBJECT.isOffersOnline()) {
            offerOnlineSwitch.setChecked(true);
        }
        else {
            offerOnlineSwitch.setChecked(false);
        }
        offerOnlineSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOfferingSwitchLogic(offerOnlineSwitch, "offersOnline");
            }
        });

        // Bind in-person tutoring options for user
        if (ActivityUtils.CURRENT_USER_OBJECT.isOffersInPerson()) {
            offerInPersonSwitch.setChecked(true);
        }
        else {
            offerInPersonSwitch.setChecked(false);
        }
        offerInPersonSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOfferingSwitchLogic(offerInPersonSwitch, "offersInPerson");
            }
        });

        // Restrict view if user is not a tutor
        if (!ActivityUtils.CURRENT_USER_OBJECT.isTutor()) {
            addCoursesOfferingView.setVisibility(View.GONE);
            tuteeNotificationSwitch.setVisibility(View.GONE);
            tutorPreferencesLayout.setVisibility(View.GONE);
        }

        setListeners();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Updates offering option based on switch and alias store on field for user in Firestore.
     *
     * @param s
     * @param alias
     */
    public void setOfferingSwitchLogic(Switch s, String alias) {
        Switch otherSwitch;
        if (alias.equals("offersOnline")) {
            otherSwitch = this.offerInPersonSwitch;
        }
        else {
            otherSwitch = this.offerOnlineSwitch;
        }

        if (!s.isChecked() && !otherSwitch.isChecked()) {
            s.setChecked(true);
            String msg = "You must offer tutoring in-person or online (or both)!";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
        else {
            boolean option;
            if (alias.equals("offersOnline")) {
                option = ActivityUtils.CURRENT_USER_OBJECT.isOffersOnline();
            }
            else {
                option = ActivityUtils.CURRENT_USER_OBJECT.isOffersInPerson();
            }
            database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName())
                    .update(alias, !option)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                String msg = "Updated offering option";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                if (alias.equals("offersOnline")) {
                                    ActivityUtils.CURRENT_USER_OBJECT.setOffersOnline(!option);
                                }
                                else {
                                    ActivityUtils.CURRENT_USER_OBJECT.setOffersInPerson(!option);
                                }

                            }
                            else {
                                s.setChecked(!s.isChecked());
                                String msg = "Failed to update offering option. Please try again later";
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Failed to update " + alias);
                            }
                        }
                    });
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets onClick listeners for each option.
     */
    public void setListeners() {
        memberProfileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile = new Intent(getApplicationContext(), MemberProfileActivity.class);
                profile.putExtra("USERNAME_CONFIRMATION", ActivityUtils.CURRENT_USER_OBJECT.getuName());
                startActivity(profile);
            }
        });

        termsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TermsOfUseActivity.class));
            }
        });

        manualView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), UserManualActivity.class));
            }
        });

        uploadTranscriptView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), UploadTranscriptActivity.class));
            }
        });

        addCoursesOfferingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityUtils.CURRENT_USER_OBJECT.getCourses() == null ||
                        ActivityUtils.CURRENT_USER_OBJECT.getCourses().size() < 1)
                {
                    String msg = "You are not currently eligible to tutor any courses. " +
                            "Please upload your most recen transcript.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
                else {
                    Set<String> courseKeys = ActivityUtils.CURRENT_USER_OBJECT.getCourses().keySet();
                    ArrayList<String> courseEntries = new ArrayList<String>();
                    for (String c : courseKeys) {
                        try {
                            if (!c.equals("BLANK")) {
                                CourseEntry entry = new CourseEntry();
                                entry.setGrade("A");
                                entry.setCourseName("Some Course Name");
                                entry.setCourseId(c);
                                entry.setUniversityName(ActivityUtils.CURRENT_USER_SCHOOL);

                                courseEntries.add(entry.toString());
                            }
                        }
                        catch (Exception ex) {
                            Log.d(TAG, "This shouldn't have happened : " + ex + "");
                            finish();
                            break;
                        }
                    }
                    Collections.sort(courseEntries);
                    ActivityUtils.ELIGIBLE_COURSES = courseEntries;
                    startActivity(new Intent(getApplicationContext(), ChooseCoursesActivity.class));
                }
            }
        });

        addCoursesSeekingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ChooseCoursesSeekingActivity.class));
            }
        });

        changePassView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ChangePasswordActivity.class));
            }
        });

        sendAlertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), IAmLookingForActivity.class));
            }
        });

        sessionRequestsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SessionRequestsActivity.class));
            }
        });

        pendingSessionsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.PENDING_SESSIONS_ID;
                startActivity(new Intent(getApplicationContext(), ShowSessionsActivity.class));
            }
        });

        previousSessionsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.PREVIOUS_SESSIONS_ID;
                startActivity(new Intent(getApplicationContext(), ShowSessionsActivity.class));
            }
        });

        cancelledSessionsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.CANCELLED_SESSIONS_ID;
                startActivity(new Intent(getApplicationContext(), ShowSessionsActivity.class));
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
