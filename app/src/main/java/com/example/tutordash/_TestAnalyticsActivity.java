package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.tutordash.model.CourseAnalytics;
import com.example.tutordash.model.SchoolAnalytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * TODO : ?
 * This class is the test for the analytics that are relevant to tutor pay rates.
 */
public class _TestAnalyticsActivity extends AppCompatActivity {

    public static final String TAG = "_TestAnalyticsActivity";

    public FirebaseFirestore database;
    // Views go here...
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity___test_analytics);

        this.database = FirebaseFirestore.getInstance();
        runTest();
    }
    //---------------------------------------------------------------------------------------------

    public void runTest() {
        database.collection("schoolAnalytics")
                .document("Old Dominion University").collection("courseAnalytics")
                .document("CS330")
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot item = task.getResult();
                if (task.isSuccessful() && task.getResult() != null) {
                    CourseAnalytics courseAnalytics = task.getResult().toObject(CourseAnalytics.class);
                    if (courseAnalytics != null) {
                        Log.d(TAG, "courseAnalytics NOT null.");
                    }
                    else {
                        Log.d(TAG, "courseAnalytics was null!");
                    }
                }
                else {
                    String ex = "";
                    if (task.getException() != null) {
                        ex = task.getException().getLocalizedMessage();
                    }
                    Log.d(TAG, "Failed to complete task :\n" + ex);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
