package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.Course;

import java.util.ArrayList;

/**
 * When users click on "Request Session" on another user's profile, if that user is indeed a
 * tutor, then they will be redirected here. In this activity, they will choose a course to
 * request from this tutor (form this tutor's pool of offered courses).
 */
public class SelectCourseForSessionActivity extends AppCompatActivity {

    public static final String TAG = "SelectCourseForSession";

    public Toolbar toolbar;
    public TextView prompt;
    public Spinner spinner;
    public Button continueBtn;

    private Context context;
    private String defaultSpinnerText = " ";
    private String spinnerText = defaultSpinnerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_course_for_session);

        this.context = this.getApplicationContext();

        if (hasIntegrity()) {
            this.toolbar = findViewById(R.id.request_toolbar);
            this.prompt = findViewById(R.id.request_prompt);
            this.spinner = findViewById(R.id.request_spinner);
            this.continueBtn = findViewById(R.id.request_continue);

            this.toolbar.setTitle("Request Session");
            this.prompt.setText(this.prompt.getText().toString().replace("%USER%", ActivityUtils.TMP_USR.getuName()));
            setupSpinner();

            this.continueBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityUtils.TMP_USR.coursesOffered().containsKey(spinnerText)) {
                        final String courseId = spinnerText;
                        final Course course = ActivityUtils.TMP_USR.coursesOffered().get(courseId);
                        ActivityUtils.COURSE_OF_INTEREST = new Pair<String, Course>(courseId, course);

                        Intent intent = new Intent(context, ScheduleSessionActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else {
            String msg = "Something went wrong! Please restart the application.";
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    private boolean hasIntegrity() {
        boolean hasIntegrity = true;
        if (ActivityUtils.CURRENT_USER_OBJECT == null) {
            hasIntegrity = false;
            Log.d(TAG, "The current user object was null!");
        }
        else if (!ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "The current user object was not valid!");
        }
        if (ActivityUtils.TMP_USR == null) {
            hasIntegrity = false;
            Log.d(TAG, "The tutor's object was null!");
        }
        else if (!ActivityUtils.TMP_USR.isValidUser()) {
            hasIntegrity = false;
            Log.d(TAG, "The tutor's object was not valid!");
        }
        if (!(ActivityUtils.TMP_USR.isTutor() &&
                ActivityUtils.TMP_USR.getCourses() != null &&
                ActivityUtils.TMP_USR.coursesOffered().size() > 0))
        {
            hasIntegrity = false;
            Log.d(TAG, "This tutor has no classes to offer! The user shouldn't be here!");
        }

        return hasIntegrity;
    }
    //---------------------------------------------------------------------------------------------

    private void setupSpinner() {
        ArrayList<String> courses = new ArrayList<String>();
        courses.add(this.defaultSpinnerText);
        for (String courseId : ActivityUtils.TMP_USR.coursesOffered().keySet()) {
            courses.add(courseId);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                courses
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner.setAdapter(adapter);
        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerText = parent.getItemAtPosition(position).toString();
                if (!spinnerText.equals(defaultSpinnerText)) {
                    continueBtn.setVisibility(View.VISIBLE);
                }
                else {
                    continueBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ?
            }
        });
    }
    //---------------------------------------------------------------------------------------------
}
