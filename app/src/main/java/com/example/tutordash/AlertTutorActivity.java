package com.example.tutordash;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.dialog.SendAlertDialog;
import com.example.tutordash.model.Course;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 * The user uses this activity to send alerts to tutors (they themselves are taking on
 * the role of a tutee).
 */
public class AlertTutorActivity extends AppCompatActivity {

    public static final String TAG = "AlertTutorActivity";

    Button submitAlertBtn;
    Spinner coursesSpinner;

    final String defaultSpinnerText = "PLEASE SELECT A COURSE";
    String spinnerText = defaultSpinnerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_tutor);

        this.submitAlertBtn = findViewById(R.id.submitAlert2);
        this.coursesSpinner = findViewById(R.id.spinnerSelect2);

        if (ActivityUtils.CURRENT_USER_OBJECT.getCoursesSeekingAssistance() == null ||
                ActivityUtils.CURRENT_USER_OBJECT.getCoursesSeekingAssistance().size() == 0) {
            String msg = "You are not currently seeking assistance in any course. " +
                    "If you wish to alert tutors, please add at least one course that you need assistance for.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
        else {
            initSpinner();

            this.submitAlertBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spinnerText.equals(defaultSpinnerText)) {
                        String msg = "Please select a course from your courses seeking assistance";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        SendAlertDialog dialog = new SendAlertDialog();
                        dialog.show(getSupportFragmentManager(), "SendAlertDialog");
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initSpinner() {
        ArrayList<String> courses = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getCoursesSeekingAssistance();
        if (courses != null) {
            Collections.sort(courses);
            ArrayList<String> spinnerOptions = new ArrayList<String>(courses);
            spinnerOptions.add(0, defaultSpinnerText);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    getApplicationContext(),
                    android.R.layout.simple_spinner_item,
                    spinnerOptions
            );
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            coursesSpinner.setAdapter(adapter);
            coursesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    spinnerText = parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // ?
                }
            });
        }
        else {
            String msg = "An error occurred. Please try again later.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, "coursesSeekingAssistance was null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Alerts all tutors that the tutee (current user) needs help in course spinnerText.
     * TODO make it so users are limited to X amount of requests for a course (per day).
     */
    public void alertTutors() {
        NotificationService notificationService = new NotificationService(getApplicationContext());

        notificationService.setTitle(ActivityUtils.CURRENT_USERNAME + " is looking for a tutor for " + spinnerText);
        notificationService.setMessage("Are you looking to tutor " + spinnerText + "? " + ActivityUtils.CURRENT_USERNAME + " is available!");
        notificationService.setTopic("/topics/" + MessageService.getTopic(
                spinnerText,
                ActivityUtils.CURRENT_USER_SCHOOL,
                false)
        );
        notificationService.setHighPriority(false);

        notificationService.sendNotification(null);
    }
    //---------------------------------------------------------------------------------------------
}
