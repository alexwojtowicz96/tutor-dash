package com.example.tutordash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.FileUtils;
import com.example.tutordash.core.PermissionUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AvatarUploadActivity extends AppCompatActivity {

    public static final String TAG = "AvatarUploadActivity";

    public LinearLayout uploadImgLayout;
    public Button selectImg;
    public Button uploadImg;
    public TextView uploadImgPrompt;
    public ImageView avatar;
    public ProgressBar imageProgressBar;

    public StorageReference mStorageRef;
    public DatabaseReference mDatabaseRef;
    public FirebaseAuth mAuth;

    public Uri imguri;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar_upload);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference("avatar");
        mStorageRef = FirebaseStorage.getInstance().getReference("avatar");
        mAuth = FirebaseAuth.getInstance();

        uploadImgLayout = findViewById(R.id.uploadAvatarLayout);
        selectImg = findViewById(R.id.selectAvatarBtn);
        uploadImg = findViewById(R.id.uploadAvatarBtn);
        uploadImgPrompt = findViewById(R.id.avatarPrompt);
        avatar = findViewById(R.id.avatarPlaceholder);
        imageProgressBar = findViewById(R.id.imageProgress);

        selectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionUtils.hasReadPermission(AvatarUploadActivity.this)) {
                    FileUtils.selectImage(AvatarUploadActivity.this);
                }
                else {
                    ActivityCompat.requestPermissions(
                            AvatarUploadActivity.this,
                            new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                            PermissionUtils.REQUEST_CODE_FOR_AVATAR_UPLOAD
                    );
                }
            }
        });

        uploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imguri != null) {
                    if (ActivityUtils.CURRENT_USER_OBJECT.getPicURL() != null &&
                            !ActivityUtils.CURRENT_USER_OBJECT.getPicURL().equals(""))
                    {
                        deletePrevious();
                        uploadImage();
                    }

                }
                else {
                    String msg = "Please select an image to upload";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets new path of new, uploaded avatar and returns it as a string.
     *
     * @return New picURL of new avatar
     */
    private String getAvatarUrl() {
        return "avatar/" + ActivityUtils.CURRENT_USER_OBJECT.getuName() +
                "." + FileUtils.getExtension(imguri, AvatarUploadActivity.this);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Deletes previously saved profile image if it exists
     */
    private void deletePrevious() {
        final String currentPic = ActivityUtils.CURRENT_USER_OBJECT.getPicURL().substring(7);
        StorageReference fileRef = mStorageRef.child(currentPic);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        fileRef.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.d(TAG, "Could not delete file in storage : " + fileRef.getPath());
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Finishes the activity (Should return the user to their profile).
     */
    private void returnToMain() {
        //Intent intent = new Intent(getApplicationContext(), MainDiscoveryActivity.class);
        //startActivity(intent);
        finish();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Allows user to upload selected image.
     */
    private void uploadImage() {
        if (imguri != null) {
            final String avatarName = FileUtils.generateToken(9) + "." +
                    FileUtils.getExtension(imguri, AvatarUploadActivity.this);
            StorageReference fileRef = mStorageRef.child(avatarName);
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference cUser = db.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());

            fileRef.putFile(imguri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageProgressBar.setProgress(0);
                                }
                            }, 1000);

                            Toast.makeText(AvatarUploadActivity.this, "Upload successful", Toast.LENGTH_LONG).show();
                            cUser.update("picURL", "avatar/" + avatarName).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful() && task.getResult() != null) {
                                        // File and picURL uploaded and updated successfully
                                    }
                                    else {
                                        Log.d(TAG, "An error has occurred.");
                                    }
                                }
                            });

                            returnToMain();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AvatarUploadActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            imageProgressBar.setProgress((int) progress);
                        }
                    });
            ActivityUtils.CURRENT_USER_OBJECT.setPicURL("avatar/" + avatarName);
        }
        else {
            String msg = "Please select an image to upload first";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults)
    {
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_AVATAR_UPLOAD &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            FileUtils.selectImage(this);
        }
        else {
            String msg = "Please allow permission";
            Toast.makeText(AvatarUploadActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionUtils.REQUEST_CODE_FOR_AVATAR_UPLOAD
                && resultCode == RESULT_OK
                && data != null && data.getData() != null
        ) {
            imguri = data.getData();
            avatar.setImageURI(imguri);
        }
    }
    //---------------------------------------------------------------------------------------------
}
