package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Switch;

import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CachingService;
import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.PermissionUtils;
import com.example.tutordash.dialog.SignOutDialog;
import com.example.tutordash.dialog.ToggleAvailabilityDialog;
import com.example.tutordash.dialog.ToggleViewModeDialog;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.SearchModel;
import com.example.tutordash.base.MainDiscoveryFragment;
import com.example.tutordash.model.User;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;
import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 * This is the main activity that the holds the major fragments of the app.
 * Upon login, the user will be navigated here.
 */
public class MainDiscoveryActivity extends AppCompatActivity {

    final public String TAG = "MainDiscoveryActivity";
    FirebaseAuth firebaseAuth;
    FirebaseFirestore database;
    StorageReference storageRef;

    public Toolbar toolbar;
    public Switch viewModeToggleSwitch;
    public BottomNavigationView bottomNav;

    private boolean hasAttachedListeners;
    public boolean isFirstTextDisplay;
    public boolean isTuteeMode;
    public int currentFragmentId;
    public boolean isOnMapFragment = true;

    // IDs of items on bottom navigation view
    public int mapSelID = 0;
    public int textSelID = 0;
    public int searchSelID = 0;

    public SearchModel searchModel;
    public int initializerValue = 0;
    public int userListSizeFromGet = 0;

    public MainDiscoveryFragment mapFragment;
    public MainDiscoveryFragment textFragment;
    public Fragment searchFragment;

    public ArrayList<String> courseIds;
    public ArrayList<User> queriedUsers;
    public ArrayList<User> allUsers;

    public LatLng latLng;
    public LocationManager locManager;
    public String provider;

    // Availability window
    public ConstraintLayout currUserContainer;
    public TextView currUserNameView;
    public TextView currUserAvailText;
    public FrameLayout currUserAvailBeacon;
    public CircleImageView currUserAvatar;

    private Drawable activeBeacon;
    private Drawable inactiveBeacon;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_discovery);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (ActivityUtils.CURRENT_USER_OBJECT != null) {
                if (ActivityUtils.CURRENT_USER_OBJECT.isBlacklisted()) {
                    // TODO show screen indicating that user cannot use app
                    Toast.makeText(getApplicationContext(), "YOU ARE BLACKLISTED!", Toast.LENGTH_LONG).show();
                    finish();
                }
                else if (!ActivityUtils.CURRENT_USER_OBJECT.isVerified()) {
                    startActivity(new Intent(getApplicationContext(), VerifyEmailWithTokenActivity.class));
                    finish();
                }
                else if (ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID() != null) {
                    String ratingHold = ActivityUtils.CURRENT_USER_OBJECT.getPendingSessionRatingID();
                    if (ratingHold != null && !ratingHold.isEmpty()) {
                        Intent intent = new Intent(getApplicationContext(), RatingActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        initOnCreate();
                    }
                }
                else {
                    initOnCreate();

                    // To test video chats, comment out initOnCreate(), and uncomment the below line.
                    //startActivity(new Intent(getApplicationContext(), _TestVideoChatActivity.class));

                    // To test Braintree components, comment out initOnCreate(), and uncomment the below line.
                    //startActivity(new Intent(getApplicationContext(), _TestBraintreeActivity.class));

                    // To test Analytic components, comment out initOnCreate(), and uncomment the below line.
                    //startActivity(new Intent(getApplicationContext(), _TestAnalyticsActivity.class));
                }
            }
            else {
                Log.d(TAG, "Critical Error! Current user is null!");
                String msg = "Please restart the app. Something went horribly wrong.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        else {
            Log.d(TAG, "The currently logged-in user was null in FirebaseAuth");
            String msg = "Please restart the app. Something went horribly wrong.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initOnCreate() {
        this.locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        this.provider = locManager.GPS_PROVIDER;

        this.mapSelID = R.id.navigation_map;
        this.textSelID = R.id.navigation_text;
        this.searchSelID = R.id.navigation_search;

        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        hasAttachedListeners = false;
        isFirstTextDisplay = true;

        viewModeToggleSwitch = findViewById(R.id.viewModeToggleSwitch);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(MapFragment.NAME);
        setSupportActionBar(toolbar);

        getAllUsers();
        getCourseIds();

        this.mapFragment = new MapFragment();
        this.textFragment = new TextFragment();
        this.searchFragment = new SearchFragment();
        this.searchModel = new SearchModel();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, (MapFragment) mapFragment).commit();

        this.currentFragmentId = R.id.navigation_map;
        bottomNav = findViewById(R.id.navigation);
        setBottomNavListener();

        initBeacons();
        initUsernameWindow();
        setAvailabilityListener();

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                attachSnapshotListeners();
            }
        }, 3000);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sets the listeners that listen for availability changes on other users.
     */
    private void setAvailabilityListener() {
        DocumentReference userDoc = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        userDoc.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(
                    @javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                    @javax.annotation.Nullable FirebaseFirestoreException e)
            {
                if (documentSnapshot != null && e == null) {
                    if (documentSnapshot.get("avail") != null) {
                        boolean isAvail = (boolean) documentSnapshot.get("avail");
                        if (isAvail) {
                            currUserAvailText.setText("Available");
                            currUserAvailBeacon.setBackground(activeBeacon);
                        }
                        else {
                            currUserAvailText.setText("Unavailable");
                            currUserAvailBeacon.setBackground(inactiveBeacon);
                        }
                    }
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Initializes the layout for the availability toggle window.
     */
    public void initUsernameWindow() {
        this.currUserContainer = findViewById(R.id.currUserContainer);
        this.currUserAvailBeacon = findViewById(R.id.currUserAvailBeacon);
        this.currUserAvailText = findViewById(R.id.currUserAvailText);
        this.currUserAvatar = findViewById(R.id.currUserAvatar);
        this.currUserNameView = findViewById(R.id.currUserNameView);
        setUserDataInWindow();

        this.currUserContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleAvailabilityDialog dialog = new ToggleAvailabilityDialog();
                dialog.show(getSupportFragmentManager(), "ToggleAvailDialog");
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Actually binds the data to the availability toggle window.
     */
    public void setUserDataInWindow() {
        if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            User currUser = ActivityUtils.CURRENT_USER_OBJECT;
            if (currUser.getPicURL() != null && !currUser.getPicURL().equals("")) {
                GlideApp.with(getApplicationContext()).load(storageRef.child(currUser.getPicURL())).into(currUserAvatar);
            }
            currUserNameView.setText(currUser.getuName());

            if (currUser.isAvail()) {
                currUserAvailText.setText("Available");
                currUserAvailBeacon.setBackground(activeBeacon);
            }
            else {
                currUserAvailText.setText("Unavailable");
                currUserAvailBeacon.setBackground(inactiveBeacon);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Creates and initializes the drawables used for the beacons in the availability
     * toggle window.
     */
    public void initBeacons() {
        DrawableBuilder builder = new DrawableBuilder();
        int activeColor = getApplicationContext().getResources().getColor(R.color.beacon_green);
        int inactiveColor = getApplicationContext().getResources().getColor(R.color.beacon_grey);
        activeBeacon = builder.oval().height(30).width(30).solidColor(activeColor).build();
        builder = new DrawableBuilder();
        inactiveBeacon = builder.oval().height(30).width(30).solidColor(inactiveColor).build();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityUtils.CURRENT_USER_OBJECT == null || !ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            Log.d(TAG, "ERROR! The current user object was invalid onStart of MainDiscoveryActivity!");
            String msg = "A critical error occurred. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            isTuteeMode = ActivityUtils.IS_TUTEE_MODE;
            if (ActivityUtils.CURRENT_USER_OBJECT.isTutor()) {
                setViewModeToggleListener();
            }
            else {
                viewModeToggleSwitch.setVisibility(View.GONE);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accountSettings:
                Intent account = new Intent(this, AccountSettingsActivity.class);
                startActivity(account);
                break;

            case R.id.profile:
                Intent profile = new Intent(this, MemberProfileActivity.class);
                profile.putExtra("USERNAME_CONFIRMATION", ActivityUtils.CURRENT_USER_OBJECT.getuName());
                startActivity(profile);
                break;

            case R.id.messages:
                Intent messages = new Intent(this, MessageLandingActivity.class);
                startActivity(messages);
                break;

            case R.id.userManual:
                Intent manual = new Intent(this, UserManualActivity.class);
                startActivity(manual);
                break;

            case R.id.signOutOption:
                SignOutDialog dialog = new SignOutDialog();
                dialog.show(getSupportFragmentManager(), "SignOutDialog");
                break;
        }

        return true;
    }
    //---------------------------------------------------------------------------------------------

    public void setViewModeToggleListener() {
        this.viewModeToggleSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToggleViewModeDialog dialog = new ToggleViewModeDialog();
                dialog.show(getSupportFragmentManager(), "ToggleViewModeDialog");

                // Switch shouldn't be activated onClick (dialog option "YES" activates it)
                if (isTuteeMode) {
                    viewModeToggleSwitch.setChecked(false);
                }
                else {
                    viewModeToggleSwitch.setChecked(true);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Adds snapshot listeners to local user objects. This way, information can be updated
     * visually in real-time.
     */
    public void attachSnapshotListeners() {
        if (this.allUsers != null && !hasAttachedListeners) {
            hasAttachedListeners = true;
            for (User u : this.allUsers) {
                DocumentReference ref = database.collection("users").document(u.getuName());
                ref.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "Failed to attach snapshot listeners to this.allUsers : user => " + u.getuName());
                        }
                        else if (documentSnapshot != null && documentSnapshot.exists()) {
                            updateUserInAllUsers(commitChangesToUser(u, documentSnapshot));
                            try {
                                if (textFragment != null) {
                                    textFragment.notifyAdapter();
                                }
                                if (mapFragment != null) {
                                    /*
                                     * .addNearbyUsers() is not going to get called right after
                                     * we attach the listeners. Since after we attach the listeners,
                                     * we receive all of the data, the number of times the map will
                                     * refresh onCreate of this activity is the number of users
                                     * in the query (could be hundreds!). This logic will prevent that
                                     * by happening. How? It counts all the users in the list
                                     * when we get it, and saves that size globally. Then, when we
                                     * are ready to attach the snapshot listeners, we compare the size
                                     * of this list to the number of times we have attempted to
                                     * all .addNearbyUsers(). If we have N user in the collection, then
                                     * we know that the first N times that we try to call this method
                                     * will be redundant. Therefore, we only care about the Nth
                                     * time this gets called. Every subsequent call will get
                                     * executed after this initial onCreate workaround.
                                     */
                                    if (initializerValue >= userListSizeFromGet) {
                                        mapFragment.addNearbyUsers();
                                    }
                                    else {
                                        initializerValue += 1;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                Log.d(TAG, ex.getMessage());
                            }
                        }
                    }
                });
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Looks for all the changes to the user (other than changes that occurred in the map of courses)
     * Commits these changes to the current user object.
     *
     * @param u
     * @param userSnapshot
     */
    public User commitChangesToUser(User u, DocumentSnapshot userSnapshot) {
        // u.uName and u.email are immutable. u.picURL and u.school should also be
        // immutable, but for now, I will assume they have the ability to change.

        if (userSnapshot.get("fName") != null) {
            String fName = userSnapshot.get("fName").toString();
            if (!u.getfName().equals(fName)) {
                u.setfName(fName);
            }
        }
        if (userSnapshot.get("lName") != null) {
            String lName = userSnapshot.get("lName").toString();
            if (!u.getlName().equals(lName)) {
                u.setlName(lName);
            }
        }
        if (userSnapshot.get("avail") != null) {
            boolean isAvail = (boolean) userSnapshot.get("avail");
            if (!(u.isAvail() && isAvail)) {
                u.setAvail(isAvail);
            }
        }
        if (userSnapshot.get("tutor") != null) {
            boolean isTutor = (boolean) userSnapshot.get("tutor");
            if (!(u.isTutor() && isTutor)) {
                u.setTutor(isTutor);
            }
        }
        if (userSnapshot.get("location") != null) {
            GeoPoint location = (GeoPoint) userSnapshot.get("location");
            if (!u.getLocation().equals(location)) {
                u.setLocation(location);
            }
        }
        if (userSnapshot.get("offersInPerson") != null) {
            boolean offersInPerson = (boolean) userSnapshot.get("offersInPerson");
            if (!(u.isOffersInPerson() && offersInPerson)) {
                u.setOffersInPerson(offersInPerson);
            }
        }
        if (userSnapshot.get("offersOnline") != null) {
            boolean offersOnline = (boolean) userSnapshot.get("offersOnline");
            if (!(u.isOffersOnline() && offersOnline)) {
                u.setOffersOnline(offersOnline);
            }
        }
        if (userSnapshot.get("picURL") != null) {
            String picUrl = userSnapshot.get("picURL").toString();
            if (!u.getPicURL().equals(picUrl)) {
                u.setPicURL(picUrl);
            }
        }
        if (userSnapshot.get("timeOfLastRequest") != null) {
            Timestamp timeOfLastRequest = (Timestamp) userSnapshot.get("timeOfLastRequest");
            if (!u.getTimeOfLastRequest().equals(timeOfLastRequest)) {
                u.setTimeOfLastRequest(timeOfLastRequest);
            }
        }
        if (userSnapshot.get("tuteeRating") != null) {
            Float tuteeRating = Float.parseFloat(userSnapshot.get("tuteeRating").toString());
            if (!u.getTuteeRating().toString().equals(tuteeRating.toString())) {
                u.setTuteeRating(tuteeRating);
            }
        }
        if (userSnapshot.get("tutorRating") != null) {
            Float tutorRating = Float.parseFloat(userSnapshot.get("tutorRating").toString());
            if (!u.getTutorRating().toString().equals(tutorRating.toString())) {
                u.setTutorRating(tutorRating);
            }
        }
        if (userSnapshot.get("coursesSeekingAssistance") != null) {
            ArrayList<String> coursesSeekingAssistance = (ArrayList<String>) userSnapshot.get("coursesSeekingAssistance");
            if (coursesSeekingAssistance == null) {
                coursesSeekingAssistance = new ArrayList<String>();
            }
            u.setCoursesSeekingAssistance(coursesSeekingAssistance);
        }
        if (userSnapshot.get("blacklisted") != null) {
            boolean isBlacklisted = (boolean) userSnapshot.get("blacklisted");
            u.setBlacklisted(isBlacklisted);
        }
        if (userSnapshot.get("offeredCourseStrings") != null) {
            ArrayList<String> courseStrings = (ArrayList<String>) userSnapshot.get("offeredCourseStrings");
            if (courseStrings == null) {
                courseStrings = new ArrayList<String>();
            }
            u.setOfferedCourseStrings(courseStrings);
        }
        if (userSnapshot.get("sessionsPendingIds") != null) {
            ArrayList<String> ids = (ArrayList<String>) userSnapshot.get("sessionsPendingIds");
            if (ids == null) {
                ids = new ArrayList<String>();
            }
            u.setSessionsPendingIds(ids);
        }
        if (userSnapshot.get("sessionsCancelledIds") != null) {
            ArrayList<String> ids = (ArrayList<String>) userSnapshot.get("sessionsCancelledIds");
            if (ids == null) {
                ids = new ArrayList<String>();
            }
            u.setSessionsCancelledIds(ids);
        }
        if (userSnapshot.get("sessionsFinishedIds") != null) {
            ArrayList<String> ids = (ArrayList<String>) userSnapshot.get("sessionsFinishedIds");
            if (ids == null) {
                ids = new ArrayList<String>();
            }
            u.setSessionsFinishedIds(ids);
        }

        return u;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets all the coures at the current user's university. As long as the task is
     * successful, availableCourses will be initialized (even if size is 0).
     */
    public void getCourseIds() {
        if (ActivityUtils.CURRENT_USER_SCHOOL != null) {
            CollectionReference schools = database.collection("schoolList");
            schools.whereEqualTo("name", ActivityUtils.CURRENT_USER_SCHOOL)
                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (result != null) {
                            if (result.size() == 1) {
                                for (DocumentSnapshot s : result) {
                                    if (s.get("courses") != null) {
                                        List<String> availableCourses = (List<String>) s.get("courses");
                                        if (availableCourses != null) {
                                            courseIds = new ArrayList<String>();
                                            courseIds.addAll(availableCourses);
                                            if (courseIds.size() == 0) {
                                                Log.d(TAG, "No courses for school : " + ActivityUtils.CURRENT_USER_SCHOOL);
                                            }
                                        }
                                    }
                                    else {
                                        Log.d(TAG, "Could not find courses field on document in schoolList " +
                                                "where 'name' equalTo '" + ActivityUtils.CURRENT_USER_SCHOOL + "'");
                                    }
                                }
                            }
                            else {
                                String msg;
                                if (result.size() == 0) {
                                    msg = "Result returned non-null value but zero results for task : " +
                                            "Find document in schoolList where 'name' equalTo '" +
                                            ActivityUtils.CURRENT_USER_SCHOOL + "'";
                                }
                                else {
                                    msg = "Result returned more than one result for task : " +
                                            "Find document in schoolList where 'name' equalTo '" +
                                            ActivityUtils.CURRENT_USER_SCHOOL + "'";
                                }
                                Log.d(TAG, msg);
                            }
                        }
                        else {
                            Log.d(TAG, "Returned null result for task : Find document " +
                                    "in schoolList where 'name' equalTo '" +
                                    ActivityUtils.CURRENT_USER_SCHOOL + "'");
                        }
                    }
                    else {
                        Log.d(TAG, "Failed to complete task : Could not find document " +
                                "in schoolList where 'name' equalTo '" +
                                ActivityUtils.CURRENT_USER_SCHOOL + "'");
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This will get all the users in the users collection where the school field is
     * equal to the school field of the currently logged-in user.
     */
    public void getAllUsers() {
        /*
        CachingService cachingService = new CachingService(getApplicationContext());

        this.allUsers = cachingService.loadUserListFromCache(
                cachingService.getFileForCachedUsersBySchool(ActivityUtils.CURRENT_USER_SCHOOL));
        if (this.allUsers != null) {
            syncAllUsers();
            attachSnapshotListeners();
        }
        else {*/
            String error = "An error occurred. Please sign out and restart the application.";
            CollectionReference userCollection = database.collection("users");
            userCollection.whereEqualTo("school", ActivityUtils.CURRENT_USER_SCHOOL)
                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (result != null) {
                            allUsers = new ArrayList<User>();
                            for (DocumentSnapshot user : result) {
                                if (!user.getId().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                                    try {
                                        User userObj = user.toObject(User.class);
                                        if (userObj != null && userObj.isValidUser()) {
                                            allUsers.add(userObj.initNullValues());
                                            userListSizeFromGet += 1;
                                            userCollection.document(userObj.getuName()).collection("courses")
                                                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful() && task.getResult()!= null) {
                                                        Map<String, Course> userCourses = ConversionUtils.getCoursesFromQuerySnapshot(task.getResult());
                                                        if (userCourses != null) {
                                                            boolean foundUser = false;
                                                            for (int i = 0; i < allUsers.size(); i++) {
                                                                User u = allUsers.get(i);
                                                                if (u.getuName().equals(userObj.getuName())) {
                                                                    allUsers.get(i).setCourses(userCourses);
                                                                    foundUser = true;
                                                                    break;
                                                                }
                                                            }
                                                            if (!foundUser) {
                                                                Log.d(TAG, "Could not find user '" + userObj.getuName() +
                                                                        "' in allUsers obj when trying to update courses");
                                                            }
                                                        }
                                                        else {
                                                            Log.d(TAG, "Could not covert course collection" +
                                                                    " to Map<String, Course> for user '" + userObj.getuName() + "'");
                                                        }
                                                    }
                                                    else {
                                                        Log.d(TAG, "Could not find course collection for user '" + userObj.getuName() + "'");
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            Log.d(TAG, "Could not instantiate userObj for '" + user.getId() + "'");
                                        }
                                    }
                                    catch (Exception ex) {
                                        Log.d(TAG, ex.getLocalizedMessage());
                                    }
                                }
                            }
                        }
                        else {
                            Log.d(TAG, "Found users collection but returned null result");
                            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Log.d(TAG, "Could not get users collection");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }
                }
            });
        //}
    }
    //---------------------------------------------------------------------------------------------

    /** TODO needs testing
     * NOTE: This WILL NOT sync everything. Doing that will be too expensive. The point of
     * this method is to provide a way of getting the essential data out of the documents.
     * Every user attribute in the current user's query will be updated right before the
     * results are place into the adapter with the exception of courses. These are handled
     * separately. For this reason, this method has to exist to sync up the courses in the
     * database with our current model in the list of all users.
     *
     * Given a list of current users, this will look for any new additions to the collection
     * of users in the database. This will also update the courses for all current users.
     *
     * Apparently we can't do a whereNotEqualTo operation...So the logic behind this method
     * is that it should not be called every time we enter the main activity (only on onCreate).
     * At the very least, we won't have the overhead of importing the entire collection of
     * courses for each user as long as they are already in our local collection.
     */
    public void syncAllUsers() {
        if (this.allUsers != null) {
            CollectionReference users = database.collection("users");
            final Query query = users.whereEqualTo("school", ActivityUtils.CURRENT_USER_SCHOOL);
            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        if (task.getResult().size() != allUsers.size() + 1) {
                            if (task.getResult().size() > allUsers.size()) {
                                // We have new users to add to our list
                                final Set<String> curruNames = new HashSet<String>();
                                for (User u : allUsers) {
                                    curruNames.add(u.getuName());
                                }
                                QuerySnapshot result = task.getResult();
                                for (DocumentSnapshot d : result) {
                                    if (!d.getId().equals(ActivityUtils.CURRENT_USERNAME)) {
                                        if (!curruNames.contains(d.getId())) { // new user
                                            User user = d.toObject(User.class);
                                            if (user != null && user.isValidUser()) {
                                                CollectionReference courseCol = users.document(d.getId()).collection("courses");
                                                courseCol.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if (task.isSuccessful() && task.getResult() != null) {
                                                            QuerySnapshot result = task.getResult();
                                                            if (result.size() == 0) {
                                                                user.setCourses(new HashMap<String, Course>());
                                                            }
                                                            else {
                                                                Map<String, Course> courseMap = ConversionUtils.getCoursesFromQuerySnapshot(result);
                                                                if (courseMap != null) {
                                                                    user.setCourses(courseMap);
                                                                }
                                                                else {
                                                                    Log.d(TAG, "Encountered problem converting course map for " + d.getId());
                                                                }
                                                            }
                                                            allUsers.add(user.initNullValues());
                                                        }
                                                    }
                                                });
                                            }
                                            else {
                                                Log.d(TAG, "Could not cast user as object : " + d.getId());
                                            }
                                        }
                                        else { // existing user
                                            CollectionReference courseCol = users.document(d.getId()).collection("courses");
                                            courseCol.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful() && task.getResult() != null) {
                                                        QuerySnapshot result = task.getResult();
                                                        User user = new User();
                                                        for (User u : allUsers) {
                                                            if (u.getuName().equals(d.getId())) {
                                                                user = u;
                                                                break;
                                                            }
                                                        }
                                                        if (user.isValidUser()) {
                                                            if (result.size() == 0) {
                                                                user.setCourses(new HashMap<String, Course>());
                                                            }
                                                            else {
                                                                Map<String, Course> courseMap = ConversionUtils.getCoursesFromQuerySnapshot(result);
                                                                if (courseMap != null) {
                                                                    user.setCourses(courseMap);
                                                                }
                                                                else {
                                                                    Log.d(TAG, "Encountered problem converting course map for " + d.getId());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            else { // The assumption is that user cannot delete accounts...
                                Log.d(TAG, "This is a major problem! Why are there less users than before?\n" +
                                        "\ttask.getResult().size() => " + task.getResult().size() + "\n" +
                                        "\tcurrUsers.size()        => " + allUsers.size() + "\n");
                            }
                        }
                    }
                }
            });
        }
        else {
            Log.d(TAG, "this.allUsers was null!");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Logs the user out
     */
    public void userLogout() {
        firebaseAuth.signOut();
        /*
        CachingService cachingService = new CachingService(getApplicationContext());
        cachingService.invalidateCurrentUserCache();
        cachingService.invalidateCacheByFilename(
                cachingService.getFileForCachedUsersBySchool(ActivityUtils.CURRENT_USER_SCHOOL));
                */

        Toast.makeText(getApplicationContext(), "User has successfully logged out.", Toast.LENGTH_SHORT).show();
        Intent backToLogin = new Intent(this, LoginActivity.class);
        startActivity(backToLogin);
        clearUser();
        finish();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * @return this.allUsers
     */
    public ArrayList<User> getAllUsersList() {
        return allUsers;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * User this to update the list of all users when you get a document result that has changed.
     * @param u
     */
    public void updateUserInAllUsers(User u) {
        int indexOf = -1;
        for (int i = 0; i < this.allUsers.size(); i++) {
            if (this.allUsers.get(i).getuName().equals(u.getuName())) {
                indexOf = i;
                break;
            }
        }
        if (indexOf != -1) {
            this.allUsers.remove(indexOf);
            this.allUsers.add(indexOf, u);
        }
        else {
            Log.d(TAG, "Couldn't find user '" + u.getuName() + "' in this.allUsers");
        }
    }
    //---------------------------------------------------------------------------------------------

    public void initiateSearch(SearchModel model) {
        this.searchModel = model;
        this.queriedUsers = SearchModel.getQuery(this.allUsers, model);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.fragment_container,
                (TextFragment) textFragment
        ).commit();
        currentFragmentId = R.id.navigation_text;
        toolbar.setTitle(TextFragment.NAME);
    }
    //---------------------------------------------------------------------------------------------

    public void nullifyQuery() {
        this.queriedUsers = null;
        this.searchModel = new SearchModel(this.isTuteeMode);
    }
    //---------------------------------------------------------------------------------------------

    public void setBottomNavListener() {
        this.bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment selectedFragment = null;
                boolean ignoreSelect = false;
                switch (menuItem.getItemId()) {
                    case R.id.navigation_map:
                        if (currentFragmentId != R.id.navigation_map) {
                            selectedFragment = (MapFragment) mapFragment;
                            currentFragmentId = R.id.navigation_map;
                            toolbar.setTitle(MapFragment.NAME);
                        }
                        else {
                            ignoreSelect = true;
                        }
                        isOnMapFragment = true;
                        break;

                    case R.id.navigation_text:
                        if (currentFragmentId != R.id.navigation_text) {
                            selectedFragment = (TextFragment) textFragment;
                            currentFragmentId = R.id.navigation_text;
                            toolbar.setTitle(TextFragment.NAME);
                        }
                        else {
                            ignoreSelect = true;
                        }
                        isOnMapFragment = false;
                        break;

                    case R.id.navigation_search:
                        if (currentFragmentId != R.id.navigation_search) {
                            selectedFragment = searchFragment;
                            currentFragmentId = R.id.navigation_search;
                            toolbar.setTitle(SearchFragment.NAME);
                        }
                        else {
                            ignoreSelect = true;
                        }
                        isOnMapFragment = false;
                        break;
                }
                if (!ignoreSelect) {
                    if (selectedFragment != null) {
                        getSupportFragmentManager().beginTransaction().replace(
                                R.id.fragment_container,
                                selectedFragment
                        ).commit();
                        if (isOnMapFragment) {
                            currUserContainer.setVisibility(View.VISIBLE);
                        }
                        else {
                            currUserContainer.setVisibility(View.GONE);
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Already Selected", Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    private void saveData() {
        try {
            CachingService cachingService = new CachingService(getApplicationContext());
            cachingService.saveCurrentUser();
            cachingService.saveUserList(
                    this.allUsers,
                    cachingService.getFileForCachedUsersBySchool(ActivityUtils.CURRENT_USER_SCHOOL)
            );
            ActivityUtils.IS_TUTEE_MODE = this.isTuteeMode;
        }
        catch (Exception ex) {
            Log.d(TAG, ex.toString());
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * When uses sign out, we will unsubscribe them from all of their topics. This way,
     * they won't get bugged while they aren't signed in. Also, if they were to sign in
     * to another account on the same device, then it is possible that this new user
     * will be receiving notifications that only the other user should get.
     *
     * When they log back in, they will be resubscribed to all of their topics since we
     * are storing them in Firestore.
     */
    private void clearUser() {
        ActivityUtils.CURRENT_USER_OBJECT.unsubscribe();

        ActivityUtils.CURRENT_USER_OBJECT = null;
        ActivityUtils.CURRENT_USERNAME = null;
        ActivityUtils.CURRENT_USER_SCHOOL = null;
        ActivityUtils.CURRENT_USER_EMAIL = null;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Called when the user selects an option to allow or disallow permissions. Right now,
     * this only has the location permission included.
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults)
    {
        try {
            if (requestCode == PermissionUtils.REQUEST_CODE_FOR_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locManager.requestSingleUpdate(provider, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                }, null);

                Location location = this.locManager.getLastKnownLocation(this.provider);
                this.latLng = new LatLng(location.getLatitude(), location.getLongitude());

                // Refreshes map after user enables location permission
                this.mapFragment = new MapFragment();
                getSupportFragmentManager().beginTransaction().replace(
                        R.id.fragment_container,
                        (MapFragment) this.mapFragment
                ).commit();
            }
            else {
                String msg = "You must allow location permissions to use the map.";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        }
        catch (SecurityException ex) {
            String msg = "You must allow location permissions to use the map.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, ex + "");
        }
        catch (Exception ex) {
            String msg = "Something went wrong. Please restart the application.";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            Log.d(TAG, ex + "");
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Not sure if we will use this or if it is even necessary.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //String x = "";
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveData();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onPause() {
        super.onPause();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        SignOutDialog dialog = new SignOutDialog();
        dialog.show(getSupportFragmentManager(), "SignOutDialog");
    }
    //---------------------------------------------------------------------------------------------

    public MapFragment getMapFragment() {
        return (MapFragment) this.mapFragment;
    }
    //---------------------------------------------------------------------------------------------

    public TextFragment getTextFragment() {
        return (TextFragment) this.textFragment;
    }
    //---------------------------------------------------------------------------------------------
}
