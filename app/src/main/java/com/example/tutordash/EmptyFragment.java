package com.example.tutordash;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Literally just an empty fragment. Use it as a placeholder if you are trying to swap
 * a meaningful fragment in and out of an activity (like a popup window/dialog).
 */
public class EmptyFragment extends Fragment {
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_empty, container, false);
        return view;
    }
}
