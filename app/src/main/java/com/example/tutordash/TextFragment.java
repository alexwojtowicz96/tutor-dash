package com.example.tutordash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.adapter.UserSearchResultsAdapter;
import com.example.tutordash.base.MainDiscoveryFragment;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.SortingUtils;
import com.example.tutordash.model.SearchModel;
import com.example.tutordash.model.User;

import java.util.ArrayList;

public class TextFragment extends Fragment implements MainDiscoveryFragment {

    final public static String NAME = "Text Discovery";
    final public String TAG = "TextFragment";

    Button sortButton;
    Button refreshButton;
    TextView noResultsView;
    ConstraintLayout noResultsLayout;
    RecyclerView searchResultsRecyclerView;
    UserSearchResultsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text, container, false);

        sortButton = view.findViewById(R.id.sortPopupButton);
        refreshButton = view.findViewById(R.id.refreshButton);
        noResultsView = view.findViewById(R.id.noResultsView);
        noResultsLayout = view.findViewById(R.id.noResultsWidget);
        searchResultsRecyclerView = view.findViewById(R.id.searchResultsRecyclerView);

        if (((MainDiscoveryActivity) getActivity()).allUsers != null) {
            if (((MainDiscoveryActivity) getActivity()).queriedUsers != null) {
                setAdapter(((MainDiscoveryActivity) getActivity()).queriedUsers, 0);
            }
            else {
                setAdapter(((MainDiscoveryActivity) getActivity()).allUsers, 0);
            }
        }
        if (((MainDiscoveryActivity) getActivity()).isFirstTextDisplay) {
            searchResultsRecyclerView.setVisibility(View.GONE);
            noResultsLayout.setVisibility(View.VISIBLE);
            noResultsView.setText("Please refresh to view results");
            ((MainDiscoveryActivity) getActivity()).isFirstTextDisplay = false;
        }

        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(), sortButton);
                Menu menu = getSortMenu(popupMenu);
                popupMenu.getMenuInflater().inflate(R.menu.popup_sort, menu);
                popupMenu.getMenuInflater();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        ArrayList<User> usersList = getUsersList();
                        String itemText = item.getTitle().toString().trim();
                        String msg = itemText;
                        if (itemText.equals(SortingUtils.SORT1_TEXT)) {
                            setAdapter(usersList, SortingUtils.SORT_PRICE_LOW_TO_HIGH);
                        }
                        else if (itemText.equals(SortingUtils.SORT2_TEXT)) {
                            setAdapter(usersList, SortingUtils.SORT_PRICE_HIGH_TO_LOW);
                        }
                        else if (itemText.equals(SortingUtils.SORT3_TEXT)) {
                           setAdapter(usersList, SortingUtils.SORT_RATING_LOW_TO_HIGH);
                        }
                        else if (itemText.equals(SortingUtils.SORT4_TEXT)) {
                            setAdapter(usersList, SortingUtils.SORT_RATING_HIGH_TO_LOW);
                        }
                        else if (itemText.equals(SortingUtils.SORT5_TEXT)) {
                            setAdapter(usersList, SortingUtils.SORT_DISTANCE_CLOSE_TO_FAR);
                        }
                        else if (itemText.equals(SortingUtils.SORT6_TEXT)) {
                            setAdapter(usersList, SortingUtils.SORT_DISTANCE_FAR_TO_CLOSE);
                        }
                        else {
                            msg = "This wasn't an option?";
                        }
                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();

                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        return view;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
    }
    //---------------------------------------------------------------------------------------------

    public void clear() {
        searchResultsRecyclerView.setAdapter(new UserSearchResultsAdapter());
        searchResultsRecyclerView.setLayoutManager(new RecyclerView.LayoutManager() {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return null;
            }
        });
        ((MainDiscoveryActivity) getActivity()).nullifyQuery();
        setAdapter(((MainDiscoveryActivity) getActivity()).allUsers, 0);

        String u = "tutees";
        if (((MainDiscoveryActivity) getActivity()).isTuteeMode) {
            u = "tutors";
        }
        String msg =  "Showing all " + u + " at " + ActivityUtils.CURRENT_USER_SCHOOL;
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    public void setAdapter(ArrayList<User> users, int sortOption) {
        searchResultsRecyclerView.setVisibility(View.VISIBLE);
        if (users == null) {
            String msg = "An error occurred. Please try again later.";
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        }
        else { // We have users to render
            if (users.size() != 0) {
                noResultsLayout.setVisibility(View.GONE);
                noResultsView.setText("No Results Found");
                final SearchModel searchModel = ((MainDiscoveryActivity) getActivity()).searchModel;
                if (searchModel.getTuteeMode()) {
                    users = SearchModel.getOnlyTutors(users);
                }
                else {
                    users = SearchModel.getOnlyTutees(users);
                }
                LinearLayoutManager manager = new LinearLayoutManager(getContext());
                this.adapter = new UserSearchResultsAdapter(
                        getContext(),
                        users,
                        ActivityUtils.CURRENT_USER_OBJECT,
                        searchModel,
                        sortOption
                );
                searchResultsRecyclerView.setAdapter(adapter);
                searchResultsRecyclerView.setLayoutManager(manager);
            }
            else {
                noResultsLayout.setVisibility(View.VISIBLE);
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void notifyAdapter() {
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void addNearbyUsers() {
        //Do Nothing
    }
    //---------------------------------------------------------------------------------------------

    public ArrayList<User> getUsersList() {
        if (((MainDiscoveryActivity) getActivity()).allUsers != null) {
            if (((MainDiscoveryActivity) getActivity()).queriedUsers != null) {
                return (((MainDiscoveryActivity) getActivity()).queriedUsers);
            }
            else {
                return (((MainDiscoveryActivity) getActivity()).allUsers);
            }
        }
        else {
            return new ArrayList<User>();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This will return the options in the sort menu based on what viewing mode we are in
     * and the type of results we are seeing.
     *
     * @param popupMenu
     * @return sortMenu
     */
    public Menu getSortMenu(PopupMenu popupMenu) {
        Menu menu = popupMenu.getMenu();
        if ((((MainDiscoveryActivity) getActivity()).searchModel != null &&
                ((MainDiscoveryActivity) getActivity()).searchModel.getCourseId().equals("")) ||
                !((MainDiscoveryActivity) getActivity()).isTuteeMode) {
            menu.add(SortingUtils.SORT3_TEXT);
            menu.add(SortingUtils.SORT4_TEXT);
            menu.add(SortingUtils.SORT5_TEXT);
            menu.add(SortingUtils.SORT6_TEXT);
        }
        else {
            menu.add(SortingUtils.SORT1_TEXT);
            menu.add(SortingUtils.SORT2_TEXT);
            menu.add(SortingUtils.SORT3_TEXT);
            menu.add(SortingUtils.SORT4_TEXT);
            menu.add(SortingUtils.SORT5_TEXT);
            menu.add(SortingUtils.SORT6_TEXT);
        }

        return menu;
    }
    //---------------------------------------------------------------------------------------------
}
