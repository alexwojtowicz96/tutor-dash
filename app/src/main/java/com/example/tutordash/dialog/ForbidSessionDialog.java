package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.ScheduleSessionActivity;

/**
 * Appears when the user tries to request a session from a tutor, but either they or the tutor
 * do not have payments set up. It also appears if they cancel the dialog and then subsequently
 * try to click the submit button after input has been validated.
 */
public class ForbidSessionDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ScheduleSessionActivity activity = ((ScheduleSessionActivity) getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title = activity.forbidSessionTitle;
        String msg = activity.forbidSessionReason;

        builder.setTitle(title).setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}
