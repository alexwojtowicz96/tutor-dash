package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.AccountSettingsActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Appears when the user clicks on the switch to disable their tutor notifications in the
 * Account Settings Activity.
 */
public class TutorNotificationsDialog extends AppCompatDialogFragment {

    public static final String TAG = "TutorNotifDialog";

    FirebaseFirestore database;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        AccountSettingsActivity activity = ((AccountSettingsActivity) getActivity());
        String title;
        String msg;

        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        if (cUser.getHasTutorNotifications()) {
            title = "Disable Tutor Notifications?";
            msg = "By selecting YES, you will be disabling notifications "
                    + "that may help you find tutors. Continue?";
        }
        else {
            title = "Enable Notifications?";
            msg = "By selecting YES, you will be enabling tutors to send "
                    + "you notifications regarding courses you need assistance for. Continue?";
        }

        builder.setTitle(title).setMessage(msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                database = FirebaseFirestore.getInstance();

                DocumentReference ref = database.collection("users").document(cUser.getuName());
                ref.update("hasTutorNotifications", !cUser.getHasTutorNotifications())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            if (ActivityUtils.CURRENT_USER_OBJECT.getHasTutorNotifications()) {
                                // They clicked YES and previously had tutor notifications.
                                // Now they want to disable them, so unsubscribe.
                                ActivityUtils.CURRENT_USER_OBJECT.unsubscribeToDisableTutorNotifications();
                            }
                            else {
                                // They clicked YES and previously did NOT have tutor notifications.
                                // Now they want to enable them, so subscribe.
                                ActivityUtils.CURRENT_USER_OBJECT.subscribeToReceiveTutorNotifications();
                            }
                            ActivityUtils.CURRENT_USER_OBJECT.setHasTutorNotifications(!cUser.getHasTutorNotifications());
                            activity.tutorNotificationSwitch.setChecked(!activity.tutorNotificationSwitch.isChecked());
                        }
                        else {
                            Log.d(TAG, "Failed to update hasTutorNotifications for user : " + cUser.getuName());
                            String msg = "An error occurred while trying to update your settings";
                            Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}