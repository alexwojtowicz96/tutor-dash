package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.HandshakeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Appears on the handshake agreement screen when a user clicks the button to indicate that
 * they are ready to enter the session.
 */
public class SessionStartDialog extends AppCompatDialogFragment {

    public static final String TAG = "SessionStartDialog";

    FirebaseFirestore db;
    CollectionReference cr;
    DocumentReference sessionRef;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "You are about to enter your session. By doing this, " +
                "you are acknowledging that you are currently present. Continue?";

        HandshakeActivity activity = ((HandshakeActivity) getActivity());
        Session currentSession = ActivityUtils.SESSION_REQUESTED;
        User currentUser = ActivityUtils.CURRENT_USER_OBJECT;
        db = FirebaseFirestore.getInstance();
        cr = db.collection("sessions");
        sessionRef = cr.document(currentSession.getSessionID());

        builder.setTitle("Start Session?");
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (currentUser.getuName().equals(currentSession.getTutorUID())) {
                    sessionRef.update("tutorPresent", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ActivityUtils.SESSION_REQUESTED = currentSession;
                                activity.checkHandshakeStatus();
                            }
                            else {
                                activity.setButtonsClickable(true);
                                activity.toastFalure("Something went wrong. Please restart and try again.");
                                Log.d(TAG, "Failed to update tutorPresent for session : "
                                        + currentSession.getSessionID());
                            }
                        }
                    });
                }
                else if (currentUser.getuName().equals(currentSession.getTuteeUID())) {
                    sessionRef.update("tuteePresent", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ActivityUtils.SESSION_REQUESTED = currentSession;
                                activity.waitingLayout.setVisibility(View.VISIBLE);
                            }
                            else {
                                activity.checkHandshakeStatus();
                                activity.toastFalure("Something went wrong. Please restart and try again.");
                                Log.d(TAG, "Failed to update tuteePresent for session : "
                                        + currentSession.getSessionID());
                            }
                        }
                    });
                }
                else {
                    String msg = "Error: Username does not match tutor or tutee";
                    activity.toastFalure(msg);
                    Log.d(TAG, msg);
                }
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}
