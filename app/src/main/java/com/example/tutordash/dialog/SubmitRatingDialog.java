package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.RatingActivity;

/**
 * Appears in the rating activity when a user submits a rating for their partner.
 */
public class SubmitRatingDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        RatingActivity activity = ((RatingActivity) getActivity());

        final String title = "Submit Rating?";
        final String msg = "Are you sure you want to give " + activity.getOtherUsername()
                + " a " + activity.getRating() + " star rating?";

        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.submitRating();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}