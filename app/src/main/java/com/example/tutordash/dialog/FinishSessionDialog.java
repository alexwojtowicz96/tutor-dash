package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.ActiveSessionInPersonActivity;
import com.example.tutordash.ActiveSessionOnlineActivity;

/**
 * Appears when the user clicks on "END SESSION" during an active session (online OR in-person)
 */
public class FinishSessionDialog extends AppCompatDialogFragment {

    public static final String TAG = "FinishSessionDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "Do you really want to end this session?";

        builder.setTitle("Conclude Session?");
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    ((ActiveSessionInPersonActivity) getActivity()).endSession(true);
                }
                catch (Exception ex1) {
                    try {
                        ((ActiveSessionOnlineActivity) getActivity()).endSession(true);
                    }
                    catch (Exception ex2) {
                        Log.d(TAG, "Could not end session! : " + ex2);
                    }
                }
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    ((ActiveSessionInPersonActivity) getActivity()).clickableEndButton(true);
                }
                catch (Exception ex1) {
                    try {
                        ((ActiveSessionOnlineActivity) getActivity()).clickableEndButton(true);
                    }
                    catch (Exception ex2) {
                        Log.d(TAG, "Failed to cancel dialog properly : " + ex2);
                    }
                }
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}