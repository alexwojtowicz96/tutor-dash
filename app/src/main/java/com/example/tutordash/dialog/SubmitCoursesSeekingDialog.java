package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.ChooseCoursesSeekingActivity;

/**
 * Appears when the user submits courses for academic assistance from the list of all
 * courses offered at their respective university.
 */
public class SubmitCoursesSeekingDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ChooseCoursesSeekingActivity activity = ((ChooseCoursesSeekingActivity) getActivity());
        String title = "Continue?";
        String msg = "Are you sure you want to submit these changes to the " +
                "courses for which you are seeking academic assistance?";

        builder.setTitle(title).setMessage(msg)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.submitCourses();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}