package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.ScheduleSessionActivity;

/**
 * Appears when a user clicks the FINISH button in the Confirm Session Request Fragment.
 */
public class SubmitRequestDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "By clicking YES, you are acknowledging to pay the full amount after " +
                "the session concludes. This amount will be adjusted after your session ends " +
                "based on the actual length of the meeting. Continue?";

        builder.setTitle("Ready to submit?");
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((ScheduleSessionActivity) getActivity()).submitRequest();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}