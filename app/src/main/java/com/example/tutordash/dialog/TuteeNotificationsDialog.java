package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.AccountSettingsActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Appears when the user clicks on the switch to disable their tutee notifications in the
 * Account Settings Activity.
 */
public class TuteeNotificationsDialog extends AppCompatDialogFragment {

    public static final String TAG = "TuteeNotifDialog";

    FirebaseFirestore database;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        AccountSettingsActivity activity = ((AccountSettingsActivity) getActivity());
        String title;
        String msg;

        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        if (cUser.getHasTuteeNotifications()) {
            title = "Disable Tutee Notifications?";
            msg = "By selecting YES, you will be disabling notifications "
                    + "that may help you find tutees. Continue?";
        }
        else {
            title = "Enable Notifications?";
            msg = "By selecting YES, you will be enabling tutees to send "
                    + "you notifications regarding courses you tutor. Continue?";
        }

        builder.setTitle(title).setMessage(msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                database = FirebaseFirestore.getInstance();

                DocumentReference ref = database.collection("users").document(cUser.getuName());
                ref.update("hasTuteeNotifications", !cUser.getHasTuteeNotifications())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            if (ActivityUtils.CURRENT_USER_OBJECT.getHasTuteeNotifications()) {
                                // They clicked YES and previously had tutee notifications.
                                // Now they want to disable them, so unsubscribe.
                                ActivityUtils.CURRENT_USER_OBJECT.unsubscribeToDisableTuteeNotifications();
                            }
                            else {
                                // They clicked YES and previously did NOT have tutee notifications.
                                // Now they want to enable them, so subscribe.
                                ActivityUtils.CURRENT_USER_OBJECT.subscribeToReceiveTuteeNotifications();
                            }
                            ActivityUtils.CURRENT_USER_OBJECT.setHasTuteeNotifications(!cUser.getHasTuteeNotifications());
                            activity.tuteeNotificationSwitch.setChecked(!activity.tuteeNotificationSwitch.isChecked());
                        }
                        else {
                            Log.d(TAG, "Failed to update hasTuteeNotifications for user : " + cUser.getuName());
                            String msg = "An error occurred while trying to update your settings";
                            Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}