package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.SessionItemActivity;

/**
 * Appears when the user clicks on "CANCEL SESSION" on the session item screen.
 */
public class CancelSessionDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "Cancelling a session will permanently delete it from your pending sessions. Continue?";

        builder.setTitle("Are You Sure?");
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((SessionItemActivity) getActivity()).cancelSession();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}