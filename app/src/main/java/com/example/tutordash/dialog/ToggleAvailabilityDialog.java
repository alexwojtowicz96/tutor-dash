package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.core.ActivityUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Appears when the user clicks the window to toggle their availability in the Main Discovery
 * Activity.
 */
public class ToggleAvailabilityDialog extends AppCompatDialogFragment {

    public static final String TAG = "ToggleAvailDialog";
    FirebaseFirestore database;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title = "Toggle Availability?";
        String msg = "";
        boolean avail;

        if (ActivityUtils.CURRENT_USER_OBJECT != null && ActivityUtils.CURRENT_USER_OBJECT.isValidUser()) {
            if (ActivityUtils.CURRENT_USER_OBJECT.isAvail()) {
                msg = "Clicking YES will make you unavailable";
                avail = true;
            }
            else {
                msg = "Clicking YES will make you available";
                avail = false;
            }

            builder.setTitle(title)
                    .setMessage(msg)
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            database = FirebaseFirestore.getInstance();
                            DocumentReference doc = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
                            doc.update("avail", !avail).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        ActivityUtils.CURRENT_USER_OBJECT.setAvail(!avail);
                                    }
                                    else {
                                        String x = "An error occurred. Could not toggle availability";
                                        String e = "";
                                        if (task.getException() != null) {
                                            e = task.getException().getMessage();
                                        }
                                        Log.d(TAG, x + " : \n" + e);
                                    }
                                }
                            });
                        }
                    }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
        }
        else {
            builder.setTitle("An error occurred")
                    .setMessage("Please restart the application.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d(TAG, "An error occurred. Current user object was not valid!");
                            dialog.cancel();
                        }
                    });
        }

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}
