package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.HandshakeActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.core.MessageService;
import com.example.tutordash.core.NotificationService;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO : IDK what this does
 */
public class SessionCancelDialog extends AppCompatDialogFragment {

    public static final String TAG = "SessionCancelDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "Cancelling at this time is very inconvenient for the other user. " +
                "If the other user is present, then you may be rated poorly. Continue?";

        User currUser = ActivityUtils.CURRENT_USER_OBJECT;
        Session currSession = ActivityUtils.SESSION_REQUESTED;

        builder.setTitle("Are you sure you want to cancel?");
        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateCancelledSession();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------

    public void updateCancelledSession() {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        final Session sessionBeforeCancel = ActivityUtils.SESSION_REQUESTED;
        ActivityUtils.SESSION_REQUESTED.setIsPending(false);
        ActivityUtils.SESSION_REQUESTED.setWasCancelled(true);
        ActivityUtils.SESSION_REQUESTED.setCancellerUID(ActivityUtils.CURRENT_USER_OBJECT.getuName());
        Session s = ActivityUtils.SESSION_REQUESTED;

        // Update the session's cancellerUID and status in Firestore by overwriting
        database.collection("sessions").document(s.getSessionID()).set(s).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // Update the current user's pending session id's array
                    ArrayList<String> updatedPendingIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getSessionsPendingIds();
                    updatedPendingIds.remove(s.getSessionID());
                    DocumentReference uRef = database.collection("users").document(ActivityUtils.CURRENT_USER_OBJECT.getuName());
                    uRef.update("sessionsPendingIds", updatedPendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ActivityUtils.CURRENT_USER_OBJECT.setSessionsPendingIds(updatedPendingIds);

                                // Update the current user's cancelled session id's array
                                ArrayList<String> updatedCancelledIds = (ArrayList<String>) ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds();
                                updatedCancelledIds.add(s.getSessionID());
                                uRef.update("sessionsCancelledIds", updatedCancelledIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            ActivityUtils.CURRENT_USER_OBJECT.setSessionsCancelledIds(updatedCancelledIds);

                                            // Get the other user as an object (we will use this in a second)
                                            String otherUID = s.getTuteeUID();
                                            if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(s.getTuteeUID())) {
                                                otherUID = s.getTutorUID();
                                            }
                                            database.collection("users").document(otherUID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if (task.isSuccessful() && task.getResult() != null) {
                                                        User otherUser = task.getResult().toObject(User.class);
                                                        if (otherUser != null && otherUser.isValidUser()) {
                                                            final User oUser = otherUser.initNullValues();

                                                            // Update the other user's pending session ids array
                                                            List<String> otherUserUpdatedPendingIds = oUser.getSessionsPendingIds();
                                                            otherUserUpdatedPendingIds.remove(s.getSessionID());
                                                            DocumentReference oRef = database.collection("users").document(oUser.getuName());
                                                            oRef.update("sessionsPendingIds", otherUserUpdatedPendingIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {

                                                                        // Update the other user's cancelled session ids array
                                                                        List<String> otherUserUpdatedCancelledIds = oUser.getSessionsCancelledIds();
                                                                        otherUserUpdatedCancelledIds.add(s.getSessionID());
                                                                        oRef.update("sessionsCancelledIds", otherUserUpdatedCancelledIds).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if (task.isSuccessful()) {
                                                                                    sendCancelAlert();
                                                                                }
                                                                                else {
                                                                                    // Don't undo because at this point our current user has been updated successfully.
                                                                                    String m1 = "Failed to update cancel session. Please try again later.";
                                                                                    String m2 = "Could not update sessionsCancelledIds for user : " + oUser.getuName();
                                                                                    Log.d(TAG, m2);
                                                                                    Toast.makeText(getContext(), m1, Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                    else {
                                                                        // Don't undo because at this point our current user has been updated successfully.
                                                                        String m1 = "Failed to update cancel session. Please try again later.";
                                                                        String m2 = "Could not update sessionsPendingIds for user : " + oUser.getuName();
                                                                        Log.d(TAG, m2);
                                                                        Toast.makeText(getContext(), m1, Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            // Don't undo because at this point our current user has been updated successfully.
                                                            String m1 = "Failed to update cancel session. Please try again later.";
                                                            String m2 = "Could not convert user document to User object";
                                                            Log.d(TAG, m2);
                                                            Toast.makeText(getContext(), m1, Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                    else {
                                                        // Don't undo because at this point our current user has been updated successfully.
                                                        String m1 = "Failed to update cancel session. Please try again later.";
                                                        String m2 = "Could not retrieve the other user document";
                                                        String ex = "";
                                                        if (task.getException() != null) {
                                                            ex = task.getException().getMessage();
                                                        }
                                                        Log.d(TAG, m2 + "\n" + ex);
                                                        Toast.makeText(getContext(), m1, Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            String m1 = "Something went wrong. Could not cancel session";
                                            String m2 = "Failed to update sessionsCancelledIds for user : " + ActivityUtils.CURRENT_USER_OBJECT.getuName();
                                            alertAndUndo(sessionBeforeCancel, m1, m2);
                                        }
                                    }
                                });
                            }
                            else {
                                String m1 = "Something went wrong. Could not cancel session";
                                String m2 = "Failed to update sessionsPendingIds for user : " + ActivityUtils.CURRENT_USER_OBJECT.getuName();
                                alertAndUndo(sessionBeforeCancel, m1, m2);
                            }
                        }
                    });
                }
                else {
                    String m1 = "Something went wrong. Could not cancel session";
                    String m2 = "Failed to overwrite session : " + s.getSessionID();
                    alertAndUndo(sessionBeforeCancel, m1, m2);
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void alertAndUndo(Session sessionBeforeCancel, String userErrorMsg, String logErrorMsg) {
        User cUser = ActivityUtils.CURRENT_USER_OBJECT;
        ActivityUtils.SESSION_REQUESTED = sessionBeforeCancel;

        if (!cUser.getSessionsPendingIds().contains(ActivityUtils.SESSION_REQUESTED.getSessionID())) {
            ActivityUtils.CURRENT_USER_OBJECT.putNewPendingSessionId(ActivityUtils.SESSION_REQUESTED.getSessionID());
        }
        if (cUser.getSessionsCancelledIds().contains(ActivityUtils.SESSION_REQUESTED.getSessionID())) {
            ActivityUtils.CURRENT_USER_OBJECT.getSessionsCancelledIds().remove(ActivityUtils.SESSION_REQUESTED.getSessionID());
        }
        Log.d(TAG, logErrorMsg);
        Toast.makeText(getContext(), userErrorMsg, Toast.LENGTH_LONG).show();
    }
    //---------------------------------------------------------------------------------------------

    public void sendCancelAlert() {
        Session session = ActivityUtils.SESSION_REQUESTED;
        String otherUID;
        if (ActivityUtils.CURRENT_USER_OBJECT.getuName().equals(session.getTuteeUID())) {
            otherUID = session.getTutorUID();
        }
        else {
            otherUID = session.getTuteeUID();
        }

        final String title = ActivityUtils.CURRENT_USER_OBJECT.getuName() + " has cancelled an upcoming session";
        final String msg = "Session for " + session.getCourseID() + " on "
                + CalendarService.getFormattedDateFromDate(session.getStart().toDate()) + " at "
                + CalendarService.getFormattedTimeFromDate(session.getStart().toDate())
                + " has been cancelled";
        final String topic = "/topics/" + MessageService.DM_TOPIC + otherUID;
        NotificationService notificationService = new NotificationService(getContext());
        notificationService.setTopic(topic);
        notificationService.setHighPriority(true);
        notificationService.setTitle(title);
        notificationService.setMessage(msg);

        Gson gson = new Gson();
        session.initNullValues();
        String sessionString = gson.toJson(session);
        Pair<String, String> data = new Pair<String, String>("sessionCancelled", sessionString);
        List<Pair<String, String>> dataList = new ArrayList<Pair<String, String>>();
        dataList.add(data);

        notificationService.sendNotification(dataList);

        ((HandshakeActivity) getActivity()).finish();
        ActivityUtils.SESSION_REQUESTED = null;
    }
    //---------------------------------------------------------------------------------------------
}
