package com.example.tutordash.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.AlertTuteeActivity;
import com.example.tutordash.AlertTutorActivity;

/**
 * Asks the user if they really want to send this alert when they click the "SEND ALERT" button.
 */
public class SendAlertDialog extends AppCompatDialogFragment {

    public static final String TAG = "SendAlertDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String msg = "Are you sure you want to send this alert?";

        builder.setMessage(msg);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    // If AlertTuteeActivity called the dialog, this will execute with no problem.
                    if (getActivity().getTaskId() == ((AlertTuteeActivity) getActivity()).getTaskId()) {
                        ((AlertTuteeActivity) getActivity()).alertTutees();
                    }
                }
                catch (Exception ex) {
                    // So, if it's the AlertTutorActivity, it will throw casting exception. Try this.
                    try {
                        if (getActivity().getTaskId() == ((AlertTutorActivity) getActivity()).getTaskId()) {
                            ((AlertTutorActivity) getActivity()).alertTutors();
                        }
                    }
                    catch (Exception e) {
                        // Something else went wrong, so just cancel.
                        Log.d(TAG, e + "");
                        dialogInterface.cancel();
                    }
                }
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}
