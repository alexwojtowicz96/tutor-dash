package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.MainDiscoveryActivity;
import com.example.tutordash.MapFragment;
import com.example.tutordash.R;
import com.example.tutordash.SearchFragment;
import com.example.tutordash.TextFragment;

/**
 * Appears when the user clicks on the switch to toggle between tutor/tutee mode in the Main
 * Discovery Activity.
 */
public class ToggleViewModeDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        MainDiscoveryActivity activity = ((MainDiscoveryActivity) getActivity());

        String title = "Continue?";
        String msg = "";

        boolean isTuteeMode = activity.isTuteeMode;
        if (isTuteeMode) {
            msg = "Are you sure you want to enter the Tutor Viewing Mode?";
        }
        else {
            msg = "Are you sure you want to enter the Tutee Viewing Mode?";
        }

        builder.setTitle(title).setMessage(msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.isTuteeMode = !activity.isTuteeMode;
                if (isTuteeMode) {
                    activity.viewModeToggleSwitch.setText("Tutor Mode   ");
                    activity.viewModeToggleSwitch.setChecked(true);
                    activity.searchModel.setTuteeMode(false);
                }
                else {
                    activity.viewModeToggleSwitch.setText("Tutee Mode   ");
                    activity.viewModeToggleSwitch.setChecked(false);
                    activity.searchModel.setTuteeMode(true);
                }

                // Refreshes the recycler view onViewModeChanged
                if (activity.currentFragmentId == activity.textSelID) {
                    ((TextFragment) activity.getSupportFragmentManager()
                            .findFragmentById(activity.getTextFragment().getId())).clear();
                }
                // Refresh map if we are already on map
                else if (activity.currentFragmentId == activity.mapSelID) {
                    activity.mapFragment = new MapFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(
                            R.id.fragment_container,
                            activity.getMapFragment()
                    ).commit();
                }
                // Refresh search if we are already on search
                else if (activity.currentFragmentId == activity.searchSelID) {
                    activity.searchFragment = new SearchFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(
                            R.id.fragment_container,
                            activity.searchFragment
                    ).commit();
                }
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}

