package com.example.tutordash.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.tutordash.ChooseCoursesActivity;

/**
 * Appears when the user clicks on the submit button for their course submission from their
 * academic transcript.
 */
public class SubmitCoursesDialog extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ChooseCoursesActivity activity = ((ChooseCoursesActivity) getActivity());
        String title = "Ready to tutor?";
        String msg = "Once you submit your offered courses, tutees will be able to " +
                "request session from you. Continue?";

        builder.setTitle(title).setMessage(msg)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.submitCourses();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder.create();
    }
    //---------------------------------------------------------------------------------------------
}