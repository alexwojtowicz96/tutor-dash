package com.example.tutordash;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.core.FormatValidationUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import com.example.tutordash.core.ActivityUtils;
import java.util.HashMap;
import java.util.Map;

import com.example.tutordash.model.*;

/**
 * This is the login screen
 */
public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";

    private EditText usernameForm;
    private EditText passwordForm;
    private TextView forgotPasswordView;
    private TextView signUpView;
    private TextView failedLogin;
    private Button loginBtn;
    private Button tempBtn;

    public FirebaseAuth firebaseAuth;
    public FirebaseFirestore database;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        usernameForm =  findViewById(R.id.usernameForm);
        passwordForm = findViewById(R.id.passwordForm);
        forgotPasswordView = findViewById(R.id.forgotPasswordView);
        signUpView = findViewById(R.id.signUpView);
        loginBtn = findViewById(R.id.loginBtn);
        tempBtn = findViewById(R.id.tempBtn);
        failedLogin = findViewById(R.id.failedLogin);

        tempBtn.setVisibility(View.GONE);
        tempBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                failedLogin.setVisibility(View.INVISIBLE);
                startActivity(new Intent(ActivityUtils.makeIntent(
                        getApplicationContext(), UploadTranscriptActivity.class)));
            }
        });

        signUpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                failedLogin.setVisibility(View.INVISIBLE);
                startActivity(new Intent(ActivityUtils.makeIntent(
                        getApplicationContext(), CreateAccountActivity.class)));
            }
        });

        forgotPasswordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                failedLogin.setVisibility(View.INVISIBLE);
                startActivity(new Intent(ActivityUtils.makeIntent(
                        getApplicationContext(), ForgotPasswordActivity.class)));
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                failedLogin.setVisibility(View.INVISIBLE);
                handleLogin(v);
                loginBtn.setClickable(true);
            }
        });

        usernameForm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                failedLogin.setVisibility(View.INVISIBLE);
            }
        });

        passwordForm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                failedLogin.setVisibility(View.INVISIBLE);
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        setClickableView(true);

        // I refactored the login... This may have side effects, but I think it will be rare.
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) { // Make sure they are valid!
            setClickableView(false);
            Toast.makeText(getApplicationContext(), "Please wait. Signing you in...", Toast.LENGTH_SHORT).show();
            cacheUserAndProceed(currentUser);
        }
        else {
            Toast.makeText(getApplicationContext(), "Please Sign In", Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------

    public void handleLogin(View view) {
        String uName = this.usernameForm.getText().toString().trim();
        String password = this.passwordForm.getText().toString().trim();
        if (!inputIsValid(uName, password)) {
            return;
        }

        setClickableView(false);
        DocumentReference docRef = database.collection("users").document(uName);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    User user = task.getResult().toObject(User.class);
                    if (user != null) {
                        Map<String, Course> courseMap = new HashMap<String, Course>();
                        CollectionReference userCourses = database.collection("users").document(user.getuName()).collection("courses");
                        userCourses.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    user.setCourses(ConversionUtils.getCoursesFromQuerySnapshot(task.getResult()));
                                    CollectionReference userSessions = database.collection("sessions");
                                    userSessions.whereArrayContains("involvedUsers", user.getuName()).whereEqualTo("wasAccepted", true)
                                            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        user.setSessions(ConversionUtils.getSessionsFromQuerySnapshot(task.getResult()));
                                                        if (user != null && user.getuName().equals(uName)) {
                                                            if (user.isValidUser()) {
                                                                loginUser(user, password);
                                                            }
                                                            else {
                                                                loginFailed();
                                                                String msg = "An error occurred. Could not log in.";
                                                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                                                                Log.d(TAG, "User is invalid?");
                                                            }
                                                        }
                                                        else {
                                                            loginFailed();
                                                        }
                                                    }
                                                    else {
                                                        loginFailed();
                                                        Log.d(TAG, "Could not get sessions for " + user.getuName());
                                                    }
                                                }
                                            });
                                }
                                else {
                                    loginFailed();
                                }
                            }
                        });
                    }
                    else {
                        loginFailed();
                        String msg = "Something went wrong. Please restart the application";
                        Log.d(TAG, "User was null!");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    loginFailed();
                    Log.d(TAG, "Cached get failed: ", task.getException());
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Logs user into Firebase Auth with their authEmail field (NOT the email field).
     *
     * @param user
     * @param password
     */
    private void loginUser(User user, String password) {
        firebaseAuth.signInWithEmailAndPassword(user.getAuthEmail(), password).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                setClickableView(false);
                failedLogin.setVisibility(View.INVISIBLE);
                cacheUserAndProceed(firebaseAuth.getCurrentUser());
            }
            else {
                loginFailed();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    private void launchMainActivity() {
        Intent goToMain = new Intent(getApplicationContext(), MainDiscoveryActivity.class);
        startActivity(goToMain);
        finish();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets user from DB and stores it. Updates global variables for current user, school, and email.
     * Launches the main activity. All of this happens on success of getting the current user.
     */
    private void cacheUserAndProceed(FirebaseUser authUser) {
        /*
        CachingService cachingService = new CachingService(getApplicationContext());

        if (cachingService.loadCurrentUserFromCache()) {
            String msg = "Loaded " + ActivityUtils.CURRENT_USERNAME + " from cache";
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            launchMainActivity();
        }
        else {*/
            final String error = "An error occurred while trying to sign in. Please try again later";
            final String email = authUser.getEmail();

            Query query = database.collection("users").whereEqualTo("authEmail", email);
            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        QuerySnapshot result = task.getResult();
                        if (result != null) {
                            if (result.size() == 1) {
                                for (DocumentSnapshot userDoc : result) {
                                    User currUser = userDoc.toObject(User.class);
                                    CollectionReference userCourses = database.collection("users")
                                            .document(currUser.getuName()).collection("courses");
                                    userCourses.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                QuerySnapshot coursesCollection = task.getResult();
                                                if (coursesCollection != null) {
                                                    try {
                                                        Map<String, Course> userCourses = ConversionUtils.getCoursesFromQuerySnapshot(coursesCollection);
                                                        currUser.setCourses(userCourses);

                                                        ActivityUtils.CURRENT_USER_EMAIL = currUser.getEmail();
                                                        ActivityUtils.CURRENT_USER_SCHOOL = currUser.getSchool();
                                                        ActivityUtils.CURRENT_USERNAME = currUser.getuName();
                                                        ActivityUtils.CURRENT_USER_OBJECT = currUser;

                                                        // Re-subscribe to all topics based on settings upon sign-in.
                                                        ActivityUtils.CURRENT_USER_OBJECT.initializeSubscriptions();

                                                        if (currUser.isVerified()) {
                                                            Bundle intentExtras = getIntent().getExtras();
                                                            if (intentExtras != null) {
                                                                String goToMessages = intentExtras.getString("GO_TO_MESSAGES");
                                                                if (goToMessages != null && goToMessages.equals("true")) {
                                                                    goToMessages();
                                                                }
                                                                else {
                                                                    displayWelcomeMsg(currUser.getFullName());
                                                                    launchMainActivity();
                                                                }
                                                            }
                                                            else {
                                                                displayWelcomeMsg(currUser.getFullName());
                                                                launchMainActivity();
                                                            }
                                                            finish();
                                                        }
                                                        else {
                                                            startActivity(ActivityUtils.makeIntent(
                                                                    getApplicationContext(),
                                                                    VerifyEmailWithTokenActivity.class));
                                                            finish();
                                                        }
                                                    }
                                                    catch (Exception ex) {
                                                        Log.d(TAG, ex.toString());
                                                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                                                        setClickableView(true);
                                                    }
                                                }
                                                else {
                                                    Log.d(TAG, "Null result returned when querying for courses collection on user '" + currUser.getuName() + "'");
                                                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                                                    setClickableView(true);
                                                }
                                            }
                                            else {
                                                Log.d(TAG, "Could not find courses collection for user '" + currUser.getuName() + "'");
                                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                                                setClickableView(true);
                                            }
                                        }
                                    });
                                }
                            }
                            else {
                                Log.d(TAG, "Result size = '" + result.size() + "'" + " where email in users was equal to '" + email + "'");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                                setClickableView(true);
                            }
                        }
                        else {
                            Log.d(TAG, "Null result returned where email in users was equal to '" + email + "'");
                            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                            setClickableView(true);
                        }
                    }
                    else {
                        Log.d(TAG, "Failed to execute query where email in users was equal to '" + email + "'");
                        String msg = error + ". Did you reset your password recently?";
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        setClickableView(true);
                    }
                }
            });
        //}
    }
    //---------------------------------------------------------------------------------------------

    /** TODO this doesn't work 100%
     * Sets items in the view to either clickable or non-clickable. This way, if we are
     * logging a user in, they can't click on items while they wait.
     *
     * @param isClickable
     */
    private void setClickableView(boolean isClickable) {
        if (isClickable) {
            this.usernameForm.setClickable(true);
            this.passwordForm.setClickable(true);
            this.forgotPasswordView.setClickable(true);
            this.signUpView.setClickable(true);
            this.loginBtn.setClickable(true);
            this.tempBtn.setClickable(true);
        }
        else {
            this.usernameForm.setClickable(false);
            this.passwordForm.setClickable(false);
            this.forgotPasswordView.setClickable(false);
            this.signUpView.setClickable(false);
            this.loginBtn.setClickable(false);
            this.tempBtn.setClickable(false);
        }
    }
    //---------------------------------------------------------------------------------------------

    private boolean inputIsValid(String uName, String password) {
        boolean isValid = true;
        if (TextUtils.isEmpty(uName) || !FormatValidationUtils.usernameIsValidFormat(uName)) {
            if (TextUtils.isEmpty(uName)) {
                usernameForm.setError("Username Required");
            }
            else {
                usernameForm.setError("Invalid Username");
            }
            isValid = false;
        }
        if (TextUtils.isEmpty(password) || !FormatValidationUtils.passwordIsValidFormat(password, uName, "bogus@soup.go")) {
            if (TextUtils.isEmpty(password)) {
                passwordForm.setError("Password Required");
            }
            else {
                passwordForm.setError("Invalid Password");
            }
            isValid = false;
        }

        return isValid;
    }
    //---------------------------------------------------------------------------------------------

    private void goToMessages() {
        Intent intent = new Intent(getApplicationContext(), MessageLandingActivity.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------

    private void loginFailed() {
        setClickableView(true);
        failedLogin.setVisibility(View.VISIBLE);
        usernameForm.setText("");
        passwordForm.setText("");
    }
    //---------------------------------------------------------------------------------------------

    private void displayWelcomeMsg(String name) {
        Toast.makeText(getApplicationContext(), "Welcome, " + name + "!", Toast.LENGTH_SHORT).show();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * If they are on the login screen and they want to go back, kill the app to avoid potential
     * problems with the back stack.
     */
    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }
    //---------------------------------------------------------------------------------------------
}
