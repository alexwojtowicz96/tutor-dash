package com.example.tutordash.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.braintreegateway.CreditCard;
import com.braintreegateway.PayPalAccount;
import com.example.tutordash.R;

import java.util.List;

/**
 * This adapter will handle the initialization of all the data for every payment method that
 * a user has stored in their Braintree customer account.
 */
public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.ViewHolder> {

    public static final String TAG = "PaymentMethodsAdapter";

    private List<CreditCard> creditCards;
    private List<PayPalAccount> payPalAccounts;
    private boolean isPaypalMode;
    private Context context;

    private Drawable drawable_generic;
    private Drawable drawable_visa;
    private Drawable drawable_discover;
    private Drawable drawable_jcb;
    private Drawable drawable_americanExpress;
    private Drawable drawable_mastercard;
    private Drawable drawable_paypal;
    //---------------------------------------------------------------------------------------------

    public PaymentMethodsAdapter() {}

    public PaymentMethodsAdapter(
            List<CreditCard> creditCards,
            List<PayPalAccount> payPalAccounts,
            boolean isPaypalMode,
            Context context
    ) {
        this.context = context;
        this.isPaypalMode = isPaypalMode;
        this.creditCards = creditCards;
        this.payPalAccounts = payPalAccounts;

        this.drawable_generic = this.context.getResources().getDrawable(R.drawable.generic_card_logo);
        this.drawable_americanExpress = this.context.getResources().getDrawable(R.drawable.american_express_logo);
        this.drawable_discover = this.context.getResources().getDrawable(R.drawable.discover_logo);
        this.drawable_jcb = this.context.getResources().getDrawable(R.drawable.jcb_logo);
        this.drawable_visa = this.context.getResources().getDrawable(R.drawable.visa_logo);
        this.drawable_mastercard = this.context.getResources().getDrawable(R.drawable.mastercard_logo);
        this.drawable_paypal = this.context.getResources().getDrawable(R.drawable.paypal_logo);
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_payment_method, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        if (this.isPaypalMode) {
            return this.payPalAccounts.size();
        }
        else {
            return this.creditCards.size();
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This inner class holds all the views for each payment method. Credit cards AND Paypal
     * accounts use the same model.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView logo;
        public TextView cardNumberOrEmail;
        public TextView accountHolderName;
        public TextView expiration;
        public TextView providerCompany;

        public ViewHolder(View itemView) {
            super(itemView);

            this.logo = itemView.findViewById(R.id.payment_method_logo);
            this.cardNumberOrEmail = itemView.findViewById(R.id.payment_method_number_or_email);
            this.accountHolderName = itemView.findViewById(R.id.payment_method_user);
            this.expiration = itemView.findViewById(R.id.payment_method_expiration);
            this.providerCompany = itemView.findViewById(R.id.payment_method_provider);
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            if (isPaypalMode) {
                final PayPalAccount paypal = payPalAccounts.get(position);
                String email = paypal.getEmail();
                String accountName = paypal.getCustomerId();
                String provider = "PAYPAL";
                String expirationDate = "NO EXPIRATION";
                Drawable logoImg = drawable_paypal;

                this.logo.setImageDrawable(logoImg);
                this.cardNumberOrEmail.setText(email);
                this.accountHolderName.setText(accountName);
                this.expiration.setText(expirationDate);
                this.providerCompany.setText(provider);
            }
            else {
                final CreditCard creditCard = creditCards.get(position);
                String name = getNameFromCard(creditCard);
                String number = "**** **** **** " + creditCard.getLast4();
                String provider = creditCard.getCardType().trim().toUpperCase();
                String expirationDate = creditCard.getExpirationDate();
                Drawable logoImg = getDrawableFromProvider(provider);

                this.logo.setImageDrawable(logoImg);
                this.cardNumberOrEmail.setText(number);
                this.accountHolderName.setText(name);
                this.expiration.setText(expirationDate);
                this.providerCompany.setText(provider);
            }
        }
        //-----------------------------------------------------------------------------------------

        private Drawable getDrawableFromProvider(String provider) {
            Drawable drawable;
            if (provider.equals("AMERICAN EXPRESS")) {
                drawable = drawable_americanExpress;
            }
            else if (provider.equals("DISCOVER")) {
                drawable = drawable_discover;
            }
            else if (provider.equals("VISA")) {
                drawable = drawable_visa;
            }
            else if (provider.equals("MASTERCARD")) {
                drawable = drawable_mastercard;
            }
            else if (provider.equals("JCB")) {
                drawable = drawable_jcb;
            }
            else if (provider.equals("PAYPAL")) {
                drawable = drawable_paypal;
            }
            else {
                drawable = drawable_generic;
            }

            return drawable;
        }
        //-----------------------------------------------------------------------------------------

        private String getNameFromCard(CreditCard card) {
            if (card.getCardholderName() != null && !card.getCardholderName().isEmpty()) {
                return card.getCardholderName().toUpperCase();
            }
            else {
                return "No Specified Cardholder".toUpperCase();
            }
        }
    }
    //---------------------------------------------------------------------------------------------
}
