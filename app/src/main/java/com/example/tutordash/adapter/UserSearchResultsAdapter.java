package com.example.tutordash.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.MainDiscoveryActivity;
import com.example.tutordash.MemberProfileActivity;
import com.example.tutordash.R;
import com.example.tutordash.ScheduleSessionActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.ConversionUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.core.LocationService;
import com.example.tutordash.core.SortingUtils;
import com.example.tutordash.model.Course;
import com.example.tutordash.model.SearchModel;
import com.example.tutordash.model.Session;
import com.example.tutordash.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;
import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 *
 */
public class UserSearchResultsAdapter extends RecyclerView.Adapter<UserSearchResultsAdapter.ViewHolder> {

    public static final String TAG = "UserSearchResultsAdptr";
    public int colorBlack;
    public int starColor;
    public int activeColor;
    public int inactiveColor;

    public StorageReference storageRef;
    public FirebaseFirestore database;

    private Context context;
    private ArrayList<User> users;
    public SearchModel searchModel;
    public User currentUser;

    public UserSearchResultsAdapter() {}

    public UserSearchResultsAdapter(
            final Context context,
            final ArrayList<User> users,
            final User currentUser,
            final SearchModel searchModel,
            final int sortOption
    ) {
        this.users = users;
        this.storageRef = FirebaseStorage.getInstance().getReference();
        this.database = FirebaseFirestore.getInstance();
        this.context = context;
        this.searchModel = searchModel;
        this.currentUser = currentUser.initNullValues();
        this.users = getSortedUsers(sortOption);

        this.starColor = this.context.getResources().getColor(R.color.pencil);
        this.colorBlack = this.context.getResources().getColor(R.color.black);
        this.activeColor = this.context.getResources().getColor(R.color.beacon_green);
        this.inactiveColor = this.context.getResources().getColor(R.color.beacon_grey);
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.model_user_search_result, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        holder.resultContainerWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.TMP_USR = users.get(holder.getAdapterPosition());
                Intent profile = new Intent(context, MemberProfileActivity.class);
                profile.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                profile.putExtra("USERNAME_CONFIRMATION", "");
                context.startActivity(profile);
            }
        });

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return users.size();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Sorts the users based on the option.
     *
     * @param sortOption
     * @return sorted list of users
     */
    public ArrayList<User> getSortedUsers(int sortOption) {
        if (sortOption == SortingUtils.SORT_DISTANCE_CLOSE_TO_FAR) {
            return SortingUtils.distanceCloseToFar(this.users);
        }
        else if (sortOption == SortingUtils.SORT_DISTANCE_FAR_TO_CLOSE) {
            return SortingUtils.distanceFarToClose(this.users);
        }
        else if (sortOption == SortingUtils.SORT_PRICE_HIGH_TO_LOW) {
            return SortingUtils.priceHighToLow(
                    this.users,
                    this.searchModel.getCourseId());
        }
        else if (sortOption == SortingUtils.SORT_PRICE_LOW_TO_HIGH) {
            return SortingUtils.priceLowToHigh(
                    this.users,
                    this.searchModel.getCourseId());
        }
        else if (sortOption == SortingUtils.SORT_RATING_HIGH_TO_LOW) {
            return SortingUtils.ratingHighToLow(
                    this.users,
                    this.searchModel.getCourseId(),
                    this.searchModel.getTuteeMode());
        }
        else if (sortOption == SortingUtils.SORT_RATING_LOW_TO_HIGH) {
            return SortingUtils.ratingLowToHigh(
                    this.users,
                    this.searchModel.getCourseId(),
                    this.searchModel.getTuteeMode());
        }
        else {
            return this.users;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds the views for each user result.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout resultContainerWrapper;
        public ConstraintLayout resultContainer;
        public FrameLayout avatarContainer;
        public ConstraintLayout userSpecs;
        public ConstraintLayout userExtras;
        public CircleImageView avatar;
        public TextView username;
        public TextView location;
        public RatingBar ratingBar;
        public ConstraintLayout availabilityContainer;
        public TextView availabilityText;
        public TextView priceView;
        public TextView ratingLabel;
        public TextView ratingTextView;
        public FrameLayout beaconHolder; // The background is set to a shape and wraps that content
        public Drawable inactiveBeacon;  // The dot that shows user is unavailable (the actual shape)
        public Drawable activeBeacon;    // The dot that shows user is available (the actual shape)

        public ViewHolder(View itemView) {
            super(itemView);
            this.initView(itemView);
        }
        //-----------------------------------------------------------------------------------------

        private void initView(View view) {
            resultContainerWrapper = view.findViewById(R.id.resultContainerWrapper);
            resultContainer = view.findViewById(R.id.resultContainer);
            avatarContainer = view.findViewById(R.id.avatarContainer);
            userSpecs = view.findViewById(R.id.userSpecs);
            userExtras = view.findViewById(R.id.userExtras);
            avatar = view.findViewById(R.id.avatar);
            username = view.findViewById(R.id.usernameDisplay);
            location = view.findViewById(R.id.locationDisplay);
            ratingBar = view.findViewById(R.id.ratingBarDisplay);
            availabilityText = view.findViewById(R.id.availabilityText);
            priceView = view.findViewById(R.id.priceView);
            beaconHolder = view.findViewById(R.id.beaconFrame);
            ratingLabel = view.findViewById(R.id.ratingLabel);
            availabilityContainer = view.findViewById(R.id.availabilityContainer);
            ratingTextView = view.findViewById(R.id.ratingTextView);

            ratingBar.setNumStars(5);
            ratingBar.setMax(5);
            ratingBar.setIsIndicator(true);
            ratingBar.setStepSize(new Float(0.1));
            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(starColor, PorterDuff.Mode.SRC_ATOP);

            DrawableBuilder builder = new DrawableBuilder();
            activeBeacon = builder.oval().height(30).width(30).solidColor(activeColor).build();
            builder = new DrawableBuilder();
            inactiveBeacon = builder.oval().height(30).width(30).solidColor(inactiveColor).build();
        }
        //-----------------------------------------------------------------------------------------

        public void setClickAbilities() {
            resultContainerWrapper.setClickable(true);
            resultContainer.setClickable(false);
            avatarContainer.setClickable(false);
            userSpecs.setClickable(false);
            userExtras.setClickable(false);
            avatar.setClickable(false);
            username.setClickable(false);
            location.setClickable(false);
            ratingBar.setClickable(false);
            availabilityText.setClickable(false);
            priceView.setClickable(false);
            beaconHolder.setClickable(false);
            ratingLabel.setClickable(false);
            availabilityContainer.setClickable(false);
            ratingTextView.setClickable(false);
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            this.setClickAbilities();
            DecimalFormat decimalFormat1 = new DecimalFormat("#.##");
            DecimalFormat decimalFormat2 = new DecimalFormat("##.##");

            username.setText(users.get(position).getuName());

            avatar.setImageDrawable(context.getDrawable(R.drawable.user_placeholder));
            if (users.get(position).getPicURL() != null && !users.get(position).getPicURL().isEmpty()) {
                GlideApp.with(context).load(storageRef.child(users.get(position).getPicURL())).into(avatar);
            }

            Float milesFromCurrentUser = Float.valueOf(-1);
            if (!currentUser.getLocation().equals(new GeoPoint(0, 0))) {
                milesFromCurrentUser = LocationService.getMilesAway(currentUser.getLocation(), users.get(position).getLocation());
            }
            if (!milesFromCurrentUser.equals(Float.valueOf(-1))) {
                if (milesFromCurrentUser.doubleValue() >= 10.00) {
                    location.setText(decimalFormat2.format(milesFromCurrentUser.doubleValue()) + " miles away");
                }
                else {
                    location.setText(decimalFormat1.format(milesFromCurrentUser.doubleValue()) + " miles away");
                }
            }
            else {
                location.setText("");
            }

            if (users.get(position).isAvail()) {
                beaconHolder.setBackground(activeBeacon);
                availabilityText.setText("Available");
            }
            else {
                beaconHolder.setBackground(inactiveBeacon);
                availabilityText.setText("Unavailable");
            }

            if (searchModel.getTuteeMode()) { // Users are tutors
                Number rating = 0.00;
                Number price = 0.00;
                if (searchModel.getCourseId().equals("")) { // no price and rating is overall
                    priceView.setVisibility(View.GONE);
                    if (users.get(position).getTutorRating() != null) {
                        rating = users.get(position).getTutorRating();
                    }
                    ratingLabel.setText("Overall Tutor Rating");
                }
                else { // price exists and rating is course-specific
                    if (users.get(position).coursesOffered().containsKey(searchModel.getCourseId())) {
                        if (users.get(position).coursesOffered().get(searchModel.getCourseId()).getRating() != null) {
                            rating = users.get(position).coursesOffered().get(searchModel.getCourseId()).getRating();
                            ratingLabel.setText("Rating For " + searchModel.getCourseId());
                        }
                        else {
                            ratingLabel.setText("ERROR"); // This shouldn't happen
                        }
                        if (users.get(position).coursesOffered().get(searchModel.getCourseId()).getPayRate() != null) {
                            price = users.get(position).coursesOffered().get(searchModel.getCourseId()).getPayRate();
                        }
                    }
                }
                ratingBar.setRating(rating.floatValue());
                String ratingView = decimalFormat1.format(ratingBar.getRating());
                if (ratingView.length() == 1) {
                    ratingView += ".0";
                }
                ratingTextView.setText("(" + ratingView + ")");

                if (price.doubleValue() >= 10.00) {
                    priceView.setText("$" + decimalFormat2.format(price.doubleValue()) + "/hr");
                }
                else {
                    priceView.setText("$" + decimalFormat1.format(price.doubleValue()) + "/hr");
                }
            }
            else { // Users are tutees
                priceView.setVisibility(View.GONE);
                if (users.get(position).getTuteeRating() != null) {
                    ratingBar.setRating(users.get(position).getTuteeRating().floatValue());
                }
                else {
                    ratingBar.setRating(0);
                }
                ratingLabel.setText("Overall Tutee Rating");

                String ratingView = decimalFormat1.format(ratingBar.getRating());
                if (ratingView.length() == 1) {
                    ratingView += ".0";
                }
                ratingTextView.setText("(" + ratingView + ")");
            }
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}
