package com.example.tutordash.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.AcceptRequestActivity;
import com.example.tutordash.R;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.Session;

import java.util.ArrayList;

/**
 * Binds the data to the recycler view in the SessionRequestsActivity.
 */
public class SessionRequestsAdapter extends RecyclerView.Adapter<SessionRequestsAdapter.ViewHolder> {

    private ArrayList<Session> sessions;
    private Context context;

    public SessionRequestsAdapter() {}

    public SessionRequestsAdapter(Context context, ArrayList<Session> sessions) {
        this.context = context;
        this.sessions = sessions;
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_render_session_request, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Session session = sessions.get(holder.getAdapterPosition());
                ActivityUtils.SESSION_REQUESTED = session;
                Intent intent = new Intent(context, AcceptRequestActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return sessions.size();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds the view for the session request.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ConstraintLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.requestDisplay);
            layout = itemView.findViewById(R.id.requestResultContainer);
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            final Session s = sessions.get(position);
            final String baseText = "Request from %TUTEE% for %COURSE%";
            String text = baseText.replace("%TUTEE%", s.getTuteeUID());
            text = text.replace("%COURSE%", s.getCourseID());

            textView.setText(text);
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}