package com.example.tutordash.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.CalendarActivity;
import com.example.tutordash.R;
import com.example.tutordash.SessionItemActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.model.CalendarEventModel;
import com.example.tutordash.model.Session;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

/**
 * Used to render the sessions on a given day (on the calendar view).
 */
public class ListingAdapter extends BaseAdapter {

    public static final String TAG = "ListingAdapter";

    private Activity activity;
    private Activity context;
    private ArrayList<CalendarEventModel> alCustom;
    private String sturl;
    private String mode;
    private Boolean isMe;

    private FirebaseFirestore database;

    public ListingAdapter(Activity context, ArrayList<CalendarEventModel> alCustom, Boolean isMe) {
        this.context = context;
        this.alCustom = alCustom;
        this.isMe = isMe;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getCount() {
        return alCustom.size();
    }

    @Override
    public Object getItem(int i) {
        return alCustom.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.model_calendar_event_row, null);
            holder = new ViewHolder();
            holder.sesTitle = convertView.findViewById(R.id.sesTitle);
            holder.sesCourse = convertView.findViewById(R.id.sesCourse);
            holder.sesTimeframe = convertView.findViewById(R.id.sesTimeframe);
            holder.sesRole = convertView.findViewById(R.id.sesRole);
            holder.sesStatus = convertView.findViewById(R.id.sesStatus);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String item1 = "Tutoring session with " + alCustom.get(pos).getSessionTitle();
        String item2 = "For " + alCustom.get(pos).getSessionCourseID();
        String item3 = "From  " + alCustom.get(pos).getSessionTimeframe();
        String item4 = "Role: " + alCustom.get(pos).getSessionRole();
        String item5 = "Session Status: " + alCustom.get(pos).getSessionStatus();

        holder.sesTitle.setText(item1);
        holder.sesCourse.setText(item2);
        holder.sesTimeframe.setText(item3);
        holder.sesRole.setText(item4);
        holder.sesStatus.setText(item5);

        mode = alCustom.get(pos).getSessionStatus();

        switch (mode) {
            case "Pending" :
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.PENDING_SESSIONS_ID;
                break;
            case "Cancelled" :
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.CANCELLED_SESSIONS_ID;
                break;
            case "Completed" :
                ActivityUtils.SESSIONS_VIEW_MODE = ActivityUtils.PREVIOUS_SESSIONS_ID;
                break;
        }

        if (isMe) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String sessionID = alCustom.get(pos).getSessionID();
                    database = FirebaseFirestore.getInstance();
                    database.collection("sessions").document(sessionID).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Session session = documentSnapshot.toObject(Session.class);
                            ActivityUtils.SESSION_REQUESTED = session;
                            goToSession();
                        }
                    });
                }
            });
        }

        return convertView;
    }

    public void goToSession() {
        Intent intent = new Intent(context, SessionItemActivity.class);
        context.startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds the view for each session item on the calendar.
     */
    private class ViewHolder {
        private TextView sesTitle;
        private TextView sesCourse;
        private TextView sesTimeframe;
        private TextView sesRole;
        private TextView sesStatus;
    }
    //---------------------------------------------------------------------------------------------
}
