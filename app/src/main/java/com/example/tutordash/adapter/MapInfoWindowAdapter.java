package com.example.tutordash.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tutordash.R;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.model.User;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 * Adapter for the user information window attached to map markers on the Map Discovery screen
 */
public class MapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    public static final String TAG = "MapInfoWindowAdapter";
    private final Map<Marker, Drawable> images = new HashMap<>();

    private Context context;
    private StorageReference storageRef;
    private int activeColor;
    private int inactiveColor;
    //---------------------------------------------------------------------------------------------

    public MapInfoWindowAdapter(Context context) {
        this.context = context;
        this.storageRef = FirebaseStorage.getInstance().getReference();
        this.activeColor = this.context.getResources().getColor(R.color.beacon_green);
        this.inactiveColor = this.context.getResources().getColor(R.color.beacon_grey);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * TODO this has a bug where the avatar doesn't appear until the second click.
     * Gets the view with marker data.
     *
     * @param marker
     * @return view
     */
    @Override
    public View getInfoWindow(Marker marker) {
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.model_info_window, null);

        DecimalFormat decimalFormat1 = new DecimalFormat("#.##");

        CircleImageView avatar;
        TextView username;
        RatingBar ratingBar;
        TextView availabilityText;
        TextView ratingTextView;
        FrameLayout beaconHolder; // The background is set to a shape and wraps that content
        Drawable inactiveBeacon;  // The dot that shows user is unavailable (the actual shape)
        Drawable activeBeacon;    // The dot that shows user is available (the actual shape)

        DrawableBuilder builder = new DrawableBuilder();
        activeBeacon = builder.oval().height(30).width(30).solidColor(activeColor).build();
        builder = new DrawableBuilder();
        inactiveBeacon = builder.oval().height(30).width(30).solidColor(inactiveColor).build();

        avatar = view.findViewById(R.id.pic);
        username = view.findViewById(R.id.nameDisplay);
        ratingBar = view.findViewById(R.id.ratingDisplay);
        availabilityText = view.findViewById(R.id.availability);
        beaconHolder = view.findViewById(R.id.beacon);
        ratingTextView = view.findViewById(R.id.ratingText);

        User user = (User) marker.getTag();

        if (user != null && user.isValidUser()) {
            Drawable image = images.get(marker);
            if (image != null) {
                avatar.setImageDrawable(image);
            }
            else if (user.getPicURL() != null && !user.getPicURL().isEmpty()) {
                GlideApp.with(context).asDrawable().load(storageRef.child(user.getPicURL())).override(50, 50).into(avatar);
                images.put(marker, avatar.getDrawable());
            }
            username.setText(user.getuName());

            if (user.isTutor() && user.isAvail()) {
                if (user.getTutorRating() != null) {
                    ratingBar.setRating(user.getTutorRating());
                }
                availabilityText.setText("Available");
                beaconHolder.setBackground(activeBeacon);
            }
            else {
                if (user.getTuteeRating() != null) {
                    ratingBar.setRating(user.getTuteeRating());
                }
                availabilityText.setText("Unavailable");
                beaconHolder.setBackground(inactiveBeacon);
            }

            String ratingView = decimalFormat1.format(ratingBar.getRating());
            if (ratingView.length() == 1) {
                ratingView += ".0";
            }
            ratingTextView.setText("(" + ratingView + ")");
        }

        return view;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
    //---------------------------------------------------------------------------------------------
}
