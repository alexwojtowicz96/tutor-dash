package com.example.tutordash.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.R;
import com.example.tutordash.model.RenderCourseModel;

import java.util.ArrayList;

/**
 * Binds the data to the recycler view in the ChooseCoursesSeekingActivity.
 */
public class SeekingCoursesAdapter extends RecyclerView.Adapter<SeekingCoursesAdapter.ViewHolder> {

    private ArrayList<RenderCourseModel> courses;
    private Context context;
    public int selectedBgColor;
    public int unselectedBgColor;
    public int disabledBgColor;

    public SeekingCoursesAdapter() {}

    public SeekingCoursesAdapter(Context context, ArrayList<String> courseIds, ArrayList<String> alreadySeeking) {
        this.context = context;
        this.selectedBgColor = this.context.getResources().getColor(R.color.jewel_green);
        this.unselectedBgColor = this.context.getResources().getColor(R.color.cello);
        this.disabledBgColor = this.context.getResources().getColor(R.color.transparent);
        this.courses = new ArrayList<RenderCourseModel>();

        for (String c : courseIds) {
            RenderCourseModel model = new RenderCourseModel();
            model.setCourseId(c);
            model.setIsSelected(alreadySeeking.contains(c));
            model.setIsDisabled(false);
            this.courses.add(model);
        }
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_seeking_course, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        holder.courseSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.toggleSelection(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return courses.size();
    }
    //---------------------------------------------------------------------------------------------

    public ArrayList<String> getSelectedCourseIds() {
        ArrayList<String> courseIds = new ArrayList<String>();
        for (RenderCourseModel c : this.courses) {
            if (c.getIsSelected()) {
                courseIds.add(c.getCourseId());
            }
        }

        return courseIds;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds the view for the seeking course item (literally just a switch with text).
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public Switch courseSwitch;

        public ViewHolder(View itemView) {
            super(itemView);
            courseSwitch = itemView.findViewById(R.id.seekingCourseSwitch);
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            courseSwitch.setText(courses.get(position).getCourseId());
            if (courses.get(position).getIsDisabled()) {
                courseSwitch.setTextColor(context.getResources().getColor(R.color.dark_gray));
                courseSwitch.setBackgroundColor(disabledBgColor);
                courseSwitch.setChecked(false);
            }
            else {
                if (courses.get(position).getIsSelected()) {
                    courseSwitch.setBackgroundColor(selectedBgColor);
                    courseSwitch.setChecked(true);
                }
                else {
                    courseSwitch.setBackgroundColor(unselectedBgColor);
                    courseSwitch.setChecked(false);
                }
            }
        }
        //-----------------------------------------------------------------------------------------

        public void toggleSelection(int position) {
            if (!courses.get(position).getIsDisabled()) {
                courses.get(position).setIsSelected(!courses.get(position).getIsSelected());
            }
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}
