package com.example.tutordash.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.tutordash.R;
import com.example.tutordash.model.CalendarEvent;
import com.example.tutordash.model.CalendarEventModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used in calendar view for the sessions.
 */
public class CalendarAdapter extends BaseAdapter {

    public static final String TAG = "CalendarAdapter";

    private Activity context;
    private java.util.Calendar month;
    public GregorianCalendar previousMonth;

    public GregorianCalendar pmonthmaxset;
    private GregorianCalendar selectedDate;
    private int firstDay;
    private int maxWeeknumber;
    private int maxP;
    private int calMaxP;
    private int totalDays;
    private String itemvalue;
    private String currentDateString;
    private DateFormat df;
    private Boolean isMe;

    private ArrayList<String> items;
    public static List<String> day_string;
    public ArrayList<CalendarEvent> calendarListings;
    private String gridVal;
    private ListView listOfEvents;
    private ArrayList<CalendarEventModel> alCustom = new ArrayList<>();
    //---------------------------------------------------------------------------------------------

    public CalendarAdapter(
            Activity context,
            GregorianCalendar monthCalendar,
            ArrayList<CalendarEvent> calendarListings,
            Boolean isMe
    ) {
        CalendarAdapter.day_string = new ArrayList<String>();
        Locale.setDefault(Locale.US);

        this.calendarListings = calendarListings;
        this.isMe = isMe;
        this.month = monthCalendar;
        this.selectedDate = (GregorianCalendar) monthCalendar.clone();
        this.context = context;
        this.month.set(GregorianCalendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<String>();
        this.df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        this.currentDateString = df.format(selectedDate.getTime());
    }
    //---------------------------------------------------------------------------------------------

    public int getCount() {
        return day_string.size();
    }

    public Object getItem(int p) {
        return day_string.get(p);
    }

    public long getItemId(int position) {
        return 0;
    }

    // Create a new view for each item referenced by the adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView dayView;
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.model_calendar_item, parent, false);
        }

        dayView = v.findViewById(R.id.date);
        String[] separatedTime = day_string.get(position).split("-");
        gridVal = separatedTime[2].replaceFirst("^0*", "");

        // Color-coding days
        if ((Integer.parseInt(gridVal) > 1) && (position < firstDay)) {
            // Set color for days outside of current month (spilling into previous month)
            dayView.setTextColor(ContextCompat.getColor(context, R.color.date_oor));
            dayView.setClickable(false);
            dayView.setFocusable(false);
        }
        else if ((Integer.parseInt(gridVal) < 7) && (position > 28)) {
            // Set color for days outside of current month (spilling into next month)
            dayView.setTextColor(ContextCompat.getColor(context, R.color.date_oor));
            dayView.setClickable(false);
            dayView.setFocusable(false);
        }
        else {
            // Set color for days inside of current month
            dayView.setTextColor(ContextCompat.getColor(context, R.color.date_ir));
        }

        // Set bg color for each grid item
        v.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        dayView.setText(gridVal);

        // Create date string for comparison
        String date = day_string.get(position);
        if (date.length() == 1) {
            date = "0" + date;
        }
        String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }
        setEventView(v, position, dayView);

        return v;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Refreshes calendar upon start up and switching months
     */
    public void refreshDays() {
        // Clear all items
        items.clear();
        day_string.clear();
        Locale.setDefault(Locale.US);
        previousMonth = (GregorianCalendar) month.clone();

        // Get first day of the month (i.e. Friday, Nov 1)
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);

        // Get number of weeks in a given month
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);

        // Adjust number of rows for gridview based on month
        totalDays = maxWeeknumber * 7;  // Get num of days that will appear on Gridview
        maxP = getMaxP();               // get last day of previous month (i.e. Oct 31, if current month is Nov)
        calMaxP = maxP - (firstDay - 1);
        pmonthmaxset = (GregorianCalendar) previousMonth.clone();
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

        for (int i = 0; i < totalDays; i++) {
            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);
            day_string.add(itemvalue);
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Get very last day of the month
     * @return int value of the last (or maximum) day of a given month
     */
    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {
            previousMonth.set(
                    month.get(GregorianCalendar.YEAR) - 1,
                    month.getActualMaximum(GregorianCalendar.MONTH),
                    1
            );
        }
        else {
            previousMonth.set(
                    GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1
            );
        }
        maxP = previousMonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Apply an indicator on days with events
     *
     * @param v
     * @param pos
     * @param tv
     */
    public void setEventView(View v, int pos, TextView tv) {
        int len = CalendarEvent.calendarListings.size();

        for (int i = 0; i < len; i++) {
            CalendarEvent cal_obj = CalendarEvent.calendarListings.get(i);
            String date = cal_obj.date;
            int len1 = day_string.size();

            if (len1 > pos) {
                if (day_string.get(pos).equals(date)) {
                    if ((Integer.parseInt(gridVal) > 1) && (pos < firstDay)) {
                        // Do nothing if day with event is out of bounds
                    }
                    else if ((Integer.parseInt(gridVal) < 7) && (pos > 28)) {
                        // Do nothing if day with event is out of bounds
                    }
                    else {
                        v.setBackgroundResource(R.drawable.event_indicator);
                        tv.setTextColor(ContextCompat.getColor(context, R.color.black));
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------

    public void getPositionList(String date, final Activity act) {
        int length = CalendarEvent.calendarListings.size();
        JSONArray jbarrays = new JSONArray();

        for (int j = 0; j < length; j++) {
            if (CalendarEvent.calendarListings.get(j).date.equals(date)){
                HashMap<String, String> maplist = new HashMap<String, String>();
                maplist.put("sTitle", CalendarEvent.calendarListings.get(j).sessionTitle);
                maplist.put("sCourse", CalendarEvent.calendarListings.get(j).sessionCourse);
                maplist.put("sTime", CalendarEvent.calendarListings.get(j).sessionTime);
                maplist.put("sRole", CalendarEvent.calendarListings.get(j).sessionRole);
                maplist.put("sStatus", CalendarEvent.calendarListings.get(j).sessionStatus);
                maplist.put("sID", CalendarEvent.calendarListings.get(j).sessionID);
                JSONObject json1 = new JSONObject(maplist);
                jbarrays.put(json1);
            }
        }
        if (jbarrays.length() != 0) {
            final Dialog dialogs = new Dialog(context);
            dialogs.setContentView(R.layout.model_calendar_information);
            listOfEvents = dialogs.findViewById(R.id.events);
            listOfEvents.setAdapter(new ListingAdapter(context, getMatchList(jbarrays + ""), isMe));
            dialogs.show();
        }
    }
    //---------------------------------------------------------------------------------------------

    private ArrayList<CalendarEventModel> getMatchList(String detail) {
        try {
            JSONArray jsonArray = new JSONArray(detail);
            alCustom = new ArrayList<CalendarEventModel>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                CalendarEventModel ced = new CalendarEventModel();

                ced.setSessionTitle(jsonObject.optString("sTitle"));
                ced.setSessionCourseID(jsonObject.optString("sCourse"));
                ced.setSessionTimeframe(jsonObject.optString("sTime"));
                ced.setSessionRole(jsonObject.optString("sRole"));
                ced.setSessionStatus(jsonObject.optString("sStatus"));
                ced.setSessionID(jsonObject.optString("sID"));

                alCustom.add(ced);
            }
        }
        catch (JSONException e) {
            Log.d(TAG, e.getLocalizedMessage());
        }
        return alCustom;
    }
    //---------------------------------------------------------------------------------------------
}
