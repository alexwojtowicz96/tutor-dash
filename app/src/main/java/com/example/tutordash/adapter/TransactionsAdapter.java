package com.example.tutordash.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.R;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.model.BraintreeTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * In the finance transactions activity, there is a recycler view for all of the transactions.
 * This adapter handles the initialization of all that data.
 */
public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    public static final String TAG = "TransactionsAdapter";

    private List<BraintreeTransaction> transactions;
    private Context context;

    public Drawable iconIncoming;
    public Drawable iconOutgoing;
    //---------------------------------------------------------------------------------------------

    public TransactionsAdapter() {}

    public TransactionsAdapter(List<BraintreeTransaction> transactions, Context context) {
        this.context = context;
        this.transactions = transactions;

        this.iconIncoming = this.context.getResources().getDrawable(R.drawable.smooth_transaction);
        this.iconOutgoing = this.context.getResources().getDrawable(R.drawable.smooth_transaction_alt);
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_transaction, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder.isUninitialized()) {
            holder.initialize();
        }
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return this.transactions.size();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * This inner class holds all views for the transaction layout.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView iconView;
        public LinearLayout voidView;
        public TextView submittedByView;
        public TextView receivedByView;
        public TextView amountView;
        public TextView statusView;
        public TextView accountTypeView;
        public TextView dateView;
        public TextView timeView;

        public ViewHolder(View itemView) {
            super(itemView);
            initialize();
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            BraintreeTransaction transaction = transactions.get(position);
            String tag = "UNAVAILABLE";
            if (transaction.getTags() != null && transaction.getTags().size() > 0) {
                tag = transaction.getTags().get(0);
            }

            if (transaction.getIsVoid()) {
                this.voidView.setVisibility(View.VISIBLE);
            }
            else {
                this.voidView.setVisibility(View.GONE);
            }
            if (transaction.getInitiatorUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                this.iconView.setImageDrawable(iconOutgoing);
            }
            else {
                this.iconView.setImageDrawable(iconIncoming);
            }

            final String submittedBy = transaction.getInitiatorUID();
            final String receivedBy = transaction.getReceiverUID();
            final String amount = "$" + ActivityUtils.getFormattedFloat(transaction.getAmount());
            final String status = tag;
            final String accountType = transaction.getPaymentMethod().replace("_", " ");
            final String date = CalendarService.getFormattedDateFromDate(transaction.getDateSubmitted().toDate());
            final String time = CalendarService.getFormattedTimeFromDate(transaction.getDateSubmitted().toDate());

            this.submittedByView.setText(submittedBy);
            this.receivedByView.setText(receivedBy);
            this.amountView.setText(amount);
            this.statusView.setText(status);
            this.accountTypeView.setText(accountType);
            this.dateView.setText(date);
            this.timeView.setText(time);
        }
        //-----------------------------------------------------------------------------------------

        public void initialize() {
            this.iconView = itemView.findViewById(R.id.transactionModel_image);
            this.voidView = itemView.findViewById(R.id.transactionModel_voidLayout);
            this.submittedByView = itemView.findViewById(R.id.transactionModel_submittedBy2);
            this.receivedByView = itemView.findViewById(R.id.transactionModel_receivedBy2);
            this.amountView = itemView.findViewById(R.id.transactionModel_amount2);
            this.statusView = itemView.findViewById(R.id.transactionModel_status2);
            this.accountTypeView = itemView.findViewById(R.id.transactionModel_accountType);
            this.dateView = itemView.findViewById(R.id.transactionModel_date);
            this.timeView = itemView.findViewById(R.id.transactionModel_time);
        }
        //-----------------------------------------------------------------------------------------

        public boolean isUninitialized() {
            boolean isUninitialized = false;

            if (this.iconView == null) {
                isUninitialized = true;
            }
            else if (this.voidView == null) {
                isUninitialized = true;
            }
            else if (this.submittedByView == null) {
                isUninitialized = true;
            }
            else if (this.receivedByView == null) {
                isUninitialized = true;
            }
            else if (this.amountView == null) {
                isUninitialized = true;
            }
            else if (this.statusView == null) {
                isUninitialized = true;
            }
            else if (this.accountTypeView == null) {
                isUninitialized = true;
            }
            else if (this.dateView == null) {
                isUninitialized = true;
            }
            else if (this.timeView == null) {
                isUninitialized = true;
            }

            return isUninitialized;
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}
