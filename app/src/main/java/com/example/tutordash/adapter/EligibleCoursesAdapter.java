package com.example.tutordash.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.R;
import com.example.tutordash.model.RenderCourseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the link between the data and the course views in the recycler view.
 */
public class EligibleCoursesAdapter extends RecyclerView.Adapter<EligibleCoursesAdapter.ViewHolder> {

    private ArrayList<RenderCourseModel> courses;
    private Context context;
    public int selectedBgColor;
    public int unselectedBgColor;
    public int disabledBgColor;

    public EligibleCoursesAdapter() {}

    public EligibleCoursesAdapter(
            Context context,
            ArrayList<String> courseIds,
            ArrayList<String> alreadyOffered,
            final boolean isTranscriptMode)
    {
        this.context = context;
        this.selectedBgColor = this.context.getResources().getColor(R.color.jewel_green);
        this.unselectedBgColor = this.context.getResources().getColor(R.color.cello);
        this.disabledBgColor = this.context.getResources().getColor(R.color.dark_gray);
        this.courses = new ArrayList<RenderCourseModel>();

        for (String courseId : courseIds) {
            RenderCourseModel course;
            if (!isTranscriptMode) {
                boolean isSelected = false;
                if (alreadyOffered.contains(courseId)) {
                    isSelected = true;
                }
                course = new RenderCourseModel(courseId, isSelected, false);
            }
            else {
                boolean isDisabled = false;
                if (alreadyOffered.contains(courseId) ) {
                    isDisabled = true;
                }
                course = new RenderCourseModel(courseId, false, isDisabled);
            }
            this.courses.add(course);
        }
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_eligible_course, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        holder.singleCourseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.toggleSelection(holder.getAdapterPosition());
                holder.bind(holder.getAdapterPosition());
                if (courses.get(holder.getAdapterPosition()).getIsDisabled()) {
                    Toast.makeText(context, "You already offer this course", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.singleCourseSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.toggleSelection(holder.getAdapterPosition());
                holder.bind(holder.getAdapterPosition());
                if (courses.get(holder.getAdapterPosition()).getIsDisabled()) {
                    Toast.makeText(context, "You already offer this course", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return courses.size();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets all the selected courses in the recycler view.
     *
     * @return selectedCourses
     */
    public List<RenderCourseModel> getSelectedCourses() {
        List<RenderCourseModel> selectedCourses = new ArrayList<RenderCourseModel>();
        for (RenderCourseModel course : this.courses) {
            if (course.getIsSelected() && !course.getIsDisabled()) {
                selectedCourses.add(course);
            }
        }

        return selectedCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets all of the disabled courses (courses that were already offered when the user
     * uploaded their transcript.
     */
    public List<RenderCourseModel> getDisabledCourses() {
        List<RenderCourseModel> disabledCourses = new ArrayList<RenderCourseModel>();
        for (RenderCourseModel course : this.courses) {
            if (course.getIsDisabled()) {
                disabledCourses.add(course);
            }
        }

        return disabledCourses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Gets all courses
     * @return this.courses
     */
    public List<RenderCourseModel> getAllCourses() {
        return this.courses;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes all courses in the adapter selected.
     */
    public void selectAll() {
        for (int i = 0; i < this.courses.size(); i++) {
            if (!this.courses.get(i).getIsDisabled()) {
                this.courses.get(i).setIsSelected(true);
            }
        }
        notifyDataSetChanged();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Makes all courses in the adapter deselected.
     */
    public void deselectAll() {
        for (int i = 0; i < this.courses.size(); i++) {
            if (!this.courses.get(i).getIsDisabled()) {
                this.courses.get(i).setIsSelected(false);
            }
        }
        notifyDataSetChanged();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds each view for an eligible course.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        Switch singleCourseSwitch;
        TextView singleCourseIdView;
        LinearLayout singleCourseLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            singleCourseIdView = itemView.findViewById(R.id.singleCourseIdView);
            singleCourseSwitch = itemView.findViewById(R.id.singleCourseSwitch);
            singleCourseLayout = itemView.findViewById(R.id.singleCourseLayout);
        }
        //-----------------------------------------------------------------------------------------

        /**
         * Binds the data in the view at some position in the data list
         *
         * @param position
         */
        public void bind(int position) {
            this.singleCourseIdView.setText(courses.get(position).getCourseId());

            if (!courses.get(position).getIsDisabled()) {
                if (courses.get(position).getIsSelected()) {
                    this.makeSelected();
                }
                else {
                    this.makeDeselected();
                }
            }
            else {
                this.makeDisabled();
            }
        }
        //-----------------------------------------------------------------------------------------

        public void toggleSelection(int position) {
            if (!courses.get(position).getIsDisabled()) {
                if (courses.get(position).getIsSelected()) {
                    courses.get(position).setIsSelected(false);
                }
                else {
                    courses.get(position).setIsSelected(true);
                }
            }
        }
        //-----------------------------------------------------------------------------------------


        public void makeSelected() {
            this.singleCourseLayout.setBackgroundColor(selectedBgColor);
            this.singleCourseSwitch.setChecked(true);
        }
        //-----------------------------------------------------------------------------------------


        public void makeDeselected() {
            this.singleCourseLayout.setBackgroundColor(unselectedBgColor);
            this.singleCourseSwitch.setChecked(false);
        }
        //-----------------------------------------------------------------------------------------

        public void makeDisabled() {
            this.singleCourseSwitch.setChecked(true);
            this.singleCourseLayout.setBackgroundColor(disabledBgColor);
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}
