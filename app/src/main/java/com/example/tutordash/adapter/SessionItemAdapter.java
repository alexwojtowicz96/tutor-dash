package com.example.tutordash.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutordash.R;
import com.example.tutordash.SessionItemActivity;
import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.CalendarService;
import com.example.tutordash.model.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This adapter handles the rendering of sessions in the user accounts when the users go to
 * view their sessions (pending, cancelled, or completed).
 */
public class SessionItemAdapter extends RecyclerView.Adapter<SessionItemAdapter.ViewHolder> {

    public static final String TAG = "SessionItemAdapter";

    private ArrayList<Session> sessions;
    private int mode;
    private Context context;
    //---------------------------------------------------------------------------------------------

    public SessionItemAdapter() {}

    public SessionItemAdapter(Context context, ArrayList<Session> sessions, int mode) {
        this.context = context;
        this.mode = mode;
        this.sessions = (ArrayList<Session>) orderSessionsByClosestDescending(sessions);
    }
    //---------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_render_session_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.SESSIONS_VIEW_MODE = mode;  // possibly redundant
                ActivityUtils.SESSION_REQUESTED = sessions.get(holder.getAdapterPosition());

                Intent intent = new Intent(context, SessionItemActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        return holder;
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public int getItemCount() {
        return sessions.size();
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Orders a list of sessions closest date descending. So, it is assumed that sessions
     * in this adapter are either in the future or in the past, so this should work as
     * expected.
     *
     * @param sessions
     * @return ordered list of sessions
     */
    public List<Session> orderSessionsByClosestDescending(final List<Session> sessions) {
        List<Session> sortedSessions = new ArrayList<Session>();
        Map<String, Long> map = new HashMap<String, Long>(); // "sessionID" : minutesAwayFromNow
        final Date now = new Date();

        for (Session s : sessions) {
            long minutesAwayFromNow = CalendarService.getSecondsBetweenDates(now, s.getStart().toDate());
            map.put(s.getSessionID(), minutesAwayFromNow);
        }

        List<Long> longList = new ArrayList<Long>();
        for (String k : map.keySet()) {
            longList.add(map.get(k));
        }
        Collections.sort(longList);

        for (long v : longList) {
            String used = "";
            for (String k : map.keySet()) {
                if (map.get(k) == v) {
                    sortedSessions.add(findElemWithId(sessions, k));
                    used = k;
                    break;
                }
            }
            map.remove(used);
        }

        return sortedSessions;
    }
    //---------------------------------------------------------------------------------------------

    private Session findElemWithId(List<Session> sessions, String id) {
        Session elem = new Session();
        for (Session s : sessions) {
            if (s.getSessionID().equals(id)) {
                elem = s;
                break;
            }
        }

        return elem;
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Holds all of the views for this layout
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout layout;
        public ImageView icon;
        public TextView courseView;
        public TextView myRoleView;
        public TextView withUserView;
        public TextView deliveryMethodView;
        public TextView dateView;
        public TextView timeView;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.tblnd9);
            icon = itemView.findViewById(R.id.j4yyup);
            courseView = itemView.findViewById(R.id.abcdefg1);
            myRoleView = itemView.findViewById(R.id.abcdefg2);
            withUserView = itemView.findViewById(R.id.abcdefg3);
            deliveryMethodView = itemView.findViewById(R.id.abcdefg4);
            dateView = itemView.findViewById(R.id.abcdefg5);
            timeView = itemView.findViewById(R.id.abcdefg6);
        }
        //-----------------------------------------------------------------------------------------

        public void bind(int position) {
            final Drawable pendingIcon = context.getResources().getDrawable(R.drawable.pending_icon);
            final Drawable previousIcon = context.getResources().getDrawable(R.drawable.green_plus);
            final Drawable cancelledIcon = context.getResources().getDrawable(R.drawable.red_minus);
            final Drawable activeSession = context.getResources().getDrawable(R.drawable.active_session_icon);
            final int colorDefault = context.getResources().getColor(R.color.white);
            final int colorActive = context.getResources().getColor(R.color.leaf_green);

            final Session session = sessions.get(position);

            layout.setBackgroundColor(colorDefault);
            if (mode == ActivityUtils.PENDING_SESSIONS_ID) {
                if (session.findIsWithinWaitingWindow()) {
                    icon.setImageDrawable(activeSession);
                    layout.setBackgroundColor(colorActive);
                }
                else {
                    icon.setImageDrawable(pendingIcon);
                }
            }
            else if (mode == ActivityUtils.PREVIOUS_SESSIONS_ID) {
                icon.setImageDrawable(previousIcon);
            }
            else {
                icon.setImageDrawable(cancelledIcon);
            }

            courseView.setText("Course: " + session.getCourseID());
            if (session.getTutorUID().equals(ActivityUtils.CURRENT_USER_OBJECT.getuName())) {
                myRoleView.setText("My Role: TUTOR");
                withUserView.setText("With: " + session.getTuteeUID());
            }
            else {
                myRoleView.setText("My Role: TUTEE");
                withUserView.setText("With: " + session.getTutorUID());
            }
            if (session.isOnline()) {
                deliveryMethodView.setText("Online");
            }
            else {
                deliveryMethodView.setText("In-Person");
            }
            dateView.setText(CalendarService.getFormattedDateFromDate(session.getStart().toDate()));
            timeView.setText(CalendarService.getFormattedTimeFromDate(session.getStart().toDate())
                    + " - " + CalendarService.getFormattedTimeFromDate(session.getEnd().toDate()));
        }
        //-----------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------
}