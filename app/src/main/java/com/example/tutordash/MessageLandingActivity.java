package com.example.tutordash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutordash.core.ActivityUtils;
import com.example.tutordash.core.GlideApp;
import com.example.tutordash.model.ChatUser;
import com.example.tutordash.model.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageLandingActivity extends AppCompatActivity {

    public static final String TAG = "MessageLandingActivity";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private Toolbar toolbar;
    private ImageButton refreshBtn;

    private FirebaseFirestore database;
    private StorageReference storageRef;
    private FirestoreRecyclerAdapter adapter;

    private User user;
    private Context context;
    private String otherUser;
    //---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_landing);

        this.recyclerView = findViewById(R.id.messages_view);
        this.context = getApplicationContext();
        this.toolbar = findViewById(R.id.message_landing_toolbar);
        this.refreshBtn = findViewById(R.id.refreshMessages);

        init();
        getMessageList();
    }
    //---------------------------------------------------------------------------------------------

    private void init() {
        this.toolbar.setTitle("Messages");
        this.linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.database = FirebaseFirestore.getInstance();
        this.storageRef = FirebaseStorage.getInstance().getReference();
        this.user = ActivityUtils.CURRENT_USER_OBJECT;

        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
                Toast.makeText(context, "Refreshed", Toast.LENGTH_SHORT).show();
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    public void setMessageAsReadForThisUser() {
        DocumentReference refForThisUser = database.collection("chat")
                .document(user.getuName())
                .collection("userList")
                .document(otherUser);

        refForThisUser.update("hasBeenRead", true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.d(TAG, "Could no update 'hasBeenRead' for document : " + refForThisUser.getPath());
                }
            }
        });
    }
    //---------------------------------------------------------------------------------------------

    private void getMessageList() {
        Query query = database.collection("chat")
                .document(user.getuName())
                .collection("userList")
                .whereEqualTo("isValid", true);

        FirestoreRecyclerOptions<ChatUser> userList = new FirestoreRecyclerOptions
                .Builder<ChatUser>()
                .setQuery(query, ChatUser.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<ChatUser, ChatHolder>(userList) {
            @NonNull
            @Override
            public ChatHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.model_message_list, group, false);

                return new ChatHolder(view);
            }
            //-------------------------------------------------------------------------------------

            @Override
            public void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull ChatUser model) {
                model.initNullValues();

                if (!model.getHasBeenRead() && !model.getLastMessageSender().equals(user.getuName())) {
                    holder.newMessageIcon.setVisibility(View.VISIBLE);
                    holder.messageLayout.setBackgroundColor(getResources().getColor(R.color.tutor_blue));
                }
                else {
                    holder.newMessageIcon.setVisibility(View.INVISIBLE);
                    holder.messageLayout.setBackgroundColor(getResources().getColor(R.color.black_overlay));
                }
                if (model.getLastMessageSender().equals(user.getuName())) {
                    holder.lastMessage.setText("You:  " + model.getMsgForMsgLanding());
                }
                else {
                    holder.lastMessage.setText(model.getMsgForMsgLanding());
                }
                holder.timestamp.setText(model.getTimestamp());
                holder.name.setText(model.getUserName());
                holder.getUserAvatar(model.getName());

                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(context, MessageActivity.class);
                    Bundle extras = new Bundle();
                    otherUser = model.getUserName();
                    extras.putString("MESSAGING", String.valueOf(otherUser));
                    intent.putExtras(extras);
                    startActivity(intent);

                    if (!model.getHasBeenRead()) {
                        setMessageAsReadForThisUser();
                    }
                });
            }
            //-------------------------------------------------------------------------------------

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
            //-------------------------------------------------------------------------------------
        };

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //---------------------------------------------------------------------------------------------

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
    //---------------------------------------------------------------------------------------------

    public class ChatHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView timestamp;
        TextView lastMessage;
        CircleImageView avatar;
        ConstraintLayout messageLayout;
        ImageView newMessageIcon;

        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.usersName);
            timestamp = itemView.findViewById(R.id.postTime);
            lastMessage = itemView.findViewById(R.id.messagePreview);
            avatar = itemView.findViewById(R.id.postAuthorPhoto);
            messageLayout = itemView.findViewById(R.id.messageListItemLayout);
            newMessageIcon = itemView.findViewById(R.id.uiy);
        }

        public void getUserAvatar(String uName) {
            FirebaseFirestore.getInstance().collection("users").document(uName).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        if (task.getResult().get("picURL") != null) {
                            String url = (String) task.getResult().get("picURL");
                            if (url != null && !url.isEmpty()) {
                                GlideApp.with(context).load(storageRef.child(url)).into(avatar);
                            }
                        }
                    }
                    else {
                        String ex = "";
                        if (task.getException() != null) {
                            ex = task.getException().getMessage();
                        }
                        Log.d(TAG, "Could not complete retrieval of user : " + uName + "\n" + ex);
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------
}
